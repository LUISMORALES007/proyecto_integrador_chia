PGDMP                         x            infochia    11.1    11.5 �    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                        0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false                       1262    95851    infochia    DATABASE     �   CREATE DATABASE infochia WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Spain.1252' LC_CTYPE = 'Spanish_Spain.1252';
    DROP DATABASE infochia;
             postgres    false                        2615    95855    evento    SCHEMA        CREATE SCHEMA evento;
    DROP SCHEMA evento;
             postgres    false                        2615    104719    reporte    SCHEMA        CREATE SCHEMA reporte;
    DROP SCHEMA reporte;
             postgres    false            
            2615    95852    security    SCHEMA        CREATE SCHEMA security;
    DROP SCHEMA security;
             postgres    false                        2615    95854    usuario    SCHEMA        CREATE SCHEMA usuario;
    DROP SCHEMA usuario;
             postgres    false            &           1255    104714    f_actualizar_estado_por_fecha()    FUNCTION     �  CREATE FUNCTION evento.f_actualizar_estado_por_fecha() RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    DECLARE 
	
	datos_tabla CURSOR FOR select * from evento.evento ;
	fila_datos_tabla evento.evento%ROWTYPE;
    BEGIN
	 
	  open datos_tabla;
	    LOOP
		
		    FETCH datos_tabla into fila_datos_tabla;
			IF NOT FOUND THEN
			EXIT;
		    END IF;
		      RAISE NOTICE ' PROCESANDO  %', fila_datos_tabla;  
			  
			 
		          
				   UPDATE evento.evento SET


					id_estado= 3 ,
					ultima_modificacion=current_timestamp
                    
					where 
					 
					 id_estado!=5 
					 AND
					 fecha_fin  < current_date ;

		    
			  
        END LOOP;
		close datos_tabla;
    END
$$;
 6   DROP FUNCTION evento.f_actualizar_estado_por_fecha();
       evento       postgres    false    6            �            1259    96315    evento_id_seq    SEQUENCE     v   CREATE SEQUENCE evento.evento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE evento.evento_id_seq;
       evento       postgres    false    6            �            1259    96320    evento    TABLE     �  CREATE TABLE evento.evento (
    id bigint DEFAULT nextval('evento.evento_id_seq'::regclass) NOT NULL,
    nombre text,
    descripcion text,
    fecha_inicio date,
    fecha_fin date,
    hora time without time zone,
    imagen text,
    id_usuario bigint,
    id_categoria integer,
    id_estado integer,
    cantidad_interesados integer,
    cantidad_denuncias integer,
    session text,
    ultima_modificacion timestamp without time zone,
    lugar text
);
    DROP TABLE evento.evento;
       evento         postgres    false    219    6            �            1255    96397 '   f_buscar_existencia_imagen_evento(text)    FUNCTION     �   CREATE FUNCTION evento.f_buscar_existencia_imagen_evento(_url_imagen text) RETURNS SETOF evento.evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.evento
		WHERE
			imagen = _url_imagen;
	end
$$;
 J   DROP FUNCTION evento.f_buscar_existencia_imagen_evento(_url_imagen text);
       evento       postgres    false    220    6                       1255    104682 (   f_cancelar_eventos_usuario(bigint, text)    FUNCTION     +  CREATE FUNCTION evento.f_cancelar_eventos_usuario(_id_usuario bigint, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    UPDATE evento.evento SET

	 id_estado= 5,
	 session=_session,
	 ultima_modificacion=current_timestamp

	where 
	
	  id_usuario =_id_usuario;
 END
$$;
 T   DROP FUNCTION evento.f_cancelar_eventos_usuario(_id_usuario bigint, _session text);
       evento       postgres    false    6            �            1259    96863    mostrar_cantidad_denuncias_view    VIEW     h   CREATE VIEW evento.mostrar_cantidad_denuncias_view AS
 SELECT (0)::bigint AS cantidad_denuncias_evento;
 2   DROP VIEW evento.mostrar_cantidad_denuncias_view;
       evento       postgres    false    6                       1255    96867    f_contar_denuncias(bigint)    FUNCTION       CREATE FUNCTION evento.f_contar_denuncias(_id_evento bigint) RETURNS SETOF evento.mostrar_cantidad_denuncias_view
    LANGUAGE plpgsql
    AS $$
    BEGIN
     return query
	 select count(*)
	 
	 from
	 evento.denuncia_evento
	 
	 where
	 
		id_evento=_id_evento;
	 
 END
$$;
 <   DROP FUNCTION evento.f_contar_denuncias(_id_evento bigint);
       evento       postgres    false    6    227            �            1259    96839 !   mostrar_cantidad_interesados_view    VIEW     l   CREATE VIEW evento.mostrar_cantidad_interesados_view AS
 SELECT (0)::bigint AS cantidad_interesados_evento;
 4   DROP VIEW evento.mostrar_cantidad_interesados_view;
       evento       postgres    false    6                       1255    96851    f_contar_interesa(bigint)    FUNCTION     *  CREATE FUNCTION evento.f_contar_interesa(_id_evento bigint) RETURNS SETOF evento.mostrar_cantidad_interesados_view
    LANGUAGE plpgsql
    AS $$
    BEGIN
     return query
	 select count(*)
	 
	 from
	 evento.interes
	 
	 where
	 
	    habilitado = true
		AND
		id_evento=_id_evento;
	 
 END
$$;
 ;   DROP FUNCTION evento.f_contar_interesa(_id_evento bigint);
       evento       postgres    false    6    226                       1255    96868 0   f_editar_denuncias_evento(bigint, integer, text)    FUNCTION     k  CREATE FUNCTION evento.f_editar_denuncias_evento(_id_evento bigint, _cantidad_denuncias integer, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
	
	 UPDATE evento.evento SET

	    cantidad_denuncias=_cantidad_denuncias,
	    session=_session,
	    ultima_modificacion=current_timestamp
       
	where 
	
	  id =_id_evento;
   
 END
$$;
 o   DROP FUNCTION evento.f_editar_denuncias_evento(_id_evento bigint, _cantidad_denuncias integer, _session text);
       evento       postgres    false    6            �            1255    96526 $   f_editar_estado_evento(bigint, text)    FUNCTION       CREATE FUNCTION evento.f_editar_estado_evento(_id_evento bigint, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    UPDATE evento.evento SET

	 id_estado= 5,
	 session=_session,
	 ultima_modificacion=current_timestamp

	where 
	
	  id =_id_evento;
 END
$$;
 O   DROP FUNCTION evento.f_editar_estado_evento(_id_evento bigint, _session text);
       evento       postgres    false    6                       1255    96849 A   f_editar_estado_interesados_evento(bigint, bigint, boolean, text)    FUNCTION     �  CREATE FUNCTION evento.f_editar_estado_interesados_evento(_id_usuario bigint, _id_evento bigint, _habilitado boolean, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
	
	 UPDATE evento.interes SET

	    habilitado=_habilitado,
	    session=_session,
	    ultima_modificacion=current_timestamp
       
	where 
	
	  id_usuario =_id_usuario
	  AND
	  id_evento=_id_evento;
   
 END
$$;
 �   DROP FUNCTION evento.f_editar_estado_interesados_evento(_id_usuario bigint, _id_evento bigint, _habilitado boolean, _session text);
       evento       postgres    false    6                       1255    96807 k   f_editar_evento(bigint, text, text, date, date, time without time zone, text, integer, integer, text, text)    FUNCTION     �  CREATE FUNCTION evento.f_editar_evento(_id bigint, _nombre text, _descripcion text, _fecha_inicio date, _fecha_fin date, _hora time without time zone, _imagen text, _id_categoria integer, _id_estado integer, _session text, _lugar text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
	
	 UPDATE evento.evento SET

		nombre = _nombre,
		descripcion= _descripcion ,
		fecha_inicio =_fecha_inicio,
		fecha_fin = _fecha_fin ,
		hora = _hora ,
		imagen= _imagen ,
		id_categoria = _id_categoria,
		id_estado= _id_estado ,
	    session=_session,
	    ultima_modificacion=current_timestamp,
        lugar=_lugar
	where 
	
	  id =_id;
   
 END
$$;
 �   DROP FUNCTION evento.f_editar_evento(_id bigint, _nombre text, _descripcion text, _fecha_inicio date, _fecha_fin date, _hora time without time zone, _imagen text, _id_categoria integer, _id_estado integer, _session text, _lugar text);
       evento       postgres    false    6                       1255    96844 2   f_editar_interesados_evento(bigint, integer, text)    FUNCTION     s  CREATE FUNCTION evento.f_editar_interesados_evento(_id_evento bigint, _cantidad_interesados integer, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
	
	 UPDATE evento.evento SET

	    cantidad_interesados=_cantidad_interesados,
	    session=_session,
	    ultima_modificacion=current_timestamp
       
	where 
	
	  id =_id_evento;
   
 END
$$;
 s   DROP FUNCTION evento.f_editar_interesados_evento(_id_evento bigint, _cantidad_interesados integer, _session text);
       evento       postgres    false    6                       1255    96555 1   f_insertar_cancelacion_evento(text, bigint, text)    FUNCTION     i  CREATE FUNCTION evento.f_insertar_cancelacion_evento(_razon text, _id_evento bigint, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    INSERT INTO evento.cancelar_evento

	(
		razon,
		id_evento,
	    session,
	    ultima_modificacion
	) 
	
	VALUES (
		_razon,
        _id_evento,
	    _session,
	    current_timestamp
	);
 END
$$;
 c   DROP FUNCTION evento.f_insertar_cancelacion_evento(_razon text, _id_evento bigint, _session text);
       evento       postgres    false    6            $           1255    96932     f_insertar_categoria(text, text)    FUNCTION     D  CREATE FUNCTION evento.f_insertar_categoria(_descripcion text, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
BEGIN
    INSERT INTO evento.categoria_evento

	(
	    descripcion,
	    session,
	    ultima_modificacion
		
	) 
	
	VALUES (
		_descripcion,
	    _session,
	    current_timestamp
		
	);
END

$$;
 M   DROP FUNCTION evento.f_insertar_categoria(_descripcion text, _session text);
       evento       postgres    false    6                       1255    96861 6   f_insertar_denuncia_evento(text, bigint, bigint, text)    FUNCTION     �  CREATE FUNCTION evento.f_insertar_denuncia_evento(_descripcion text, _id_evento bigint, _id_usuario bigint, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    INSERT INTO evento.denuncia_evento

	(
		descripcion,
		id_evento,
	    session,
		ultima_modificacion,
		id_usuario  
	) 
	
	VALUES (
		_descripcion,
        _id_evento,
		_session,
		 current_timestamp,
	    _id_usuario
	   
	);
 END
$$;
 z   DROP FUNCTION evento.f_insertar_denuncia_evento(_descripcion text, _id_evento bigint, _id_usuario bigint, _session text);
       evento       postgres    false    6                       1255    96598 d   f_insertar_evento(text, text, date, date, time without time zone, text, bigint, integer, text, text)    FUNCTION     �  CREATE FUNCTION evento.f_insertar_evento(_nombre text, _descripcion text, _fecha_inicio date, _fecha_fin date, _hora time without time zone, _imagen text, _id_usuario bigint, _id_categoria integer, _session text, _lugar text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    INSERT INTO evento.evento

	(
		nombre,
		descripcion ,
		fecha_inicio,
		fecha_fin ,
		hora ,
		imagen ,
		id_usuario ,
		id_categoria ,
		id_estado ,
		cantidad_interesados ,
		cantidad_denuncias,
	    session,
	    ultima_modificacion,
		lugar
	) 
	
	VALUES (
		_nombre,
        _descripcion ,
		_fecha_inicio,
		_fecha_fin ,
		_hora ,
		_imagen ,
		_id_usuario ,
		_id_categoria ,
		 1 ,
		0,
		0,
	    _session,
	    current_timestamp,
		_lugar
	);
 END
$$;
 �   DROP FUNCTION evento.f_insertar_evento(_nombre text, _descripcion text, _fecha_inicio date, _fecha_fin date, _hora time without time zone, _imagen text, _id_usuario bigint, _id_categoria integer, _session text, _lugar text);
       evento       postgres    false    6            
           1255    96835 /   f_insertar_interes_evento(bigint, bigint, text)    FUNCTION     �  CREATE FUNCTION evento.f_insertar_interes_evento(_id_usuario bigint, _id_evento bigint, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    INSERT INTO evento.interes

	(
		id_usuario,
		id_evento,
		habilitado,
	    session,
	    ultima_modificacion
	) 
	
	VALUES (
		_id_usuario,
        _id_evento,
		 true,
	    _session,
	    current_timestamp
	);
 END
$$;
 f   DROP FUNCTION evento.f_insertar_interes_evento(_id_usuario bigint, _id_evento bigint, _session text);
       evento       postgres    false    6            #           1255    96926 *   f_modificar_categoria(integer, text, text)    FUNCTION     5  CREATE FUNCTION evento.f_modificar_categoria(_id integer, _descripcion text, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
BEGIN
    UPDATE evento.categoria_evento SET

	id=_id,
	descripcion=_descripcion,
	session=_session,
	ultima_modificacion=current_timestamp
	
	WHERE id =_id;
END

$$;
 [   DROP FUNCTION evento.f_modificar_categoria(_id integer, _descripcion text, _session text);
       evento       postgres    false    6            �            1259    96101    categoria_evento    TABLE     �   CREATE TABLE evento.categoria_evento (
    id integer NOT NULL,
    descripcion character varying(25),
    session text,
    ultima_modificacion timestamp without time zone
);
 $   DROP TABLE evento.categoria_evento;
       evento         postgres    false    6            �            1255    96881    f_mostrar_categorias()    FUNCTION     �   CREATE FUNCTION evento.f_mostrar_categorias() RETURNS SETOF evento.categoria_evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.categoria_evento
			
			order by id asc;
		
	end
$$;
 -   DROP FUNCTION evento.f_mostrar_categorias();
       evento       postgres    false    6    217            "           1255    96923 $   f_mostrar_categorias_administrador()    FUNCTION     �   CREATE FUNCTION evento.f_mostrar_categorias_administrador() RETURNS SETOF evento.categoria_evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.categoria_evento
		where
		   id!=1
			order by id asc;
		
	end
$$;
 ;   DROP FUNCTION evento.f_mostrar_categorias_administrador();
       evento       postgres    false    217    6            �            1259    96001    usuario    TABLE     <  CREATE TABLE usuario.usuario (
    id bigint NOT NULL,
    nombre character varying(20),
    apellido character varying(20),
    correo text,
    clave text,
    url_imagen text,
    estado integer,
    habilitado boolean,
    id_rol integer,
    session text,
    ultima_modificacion timestamp without time zone
);
    DROP TABLE usuario.usuario;
       usuario         postgres    false    8            �            1255    96900 !   f_mostrar_contacto_evento(bigint)    FUNCTION     �   CREATE FUNCTION evento.f_mostrar_contacto_evento(_id_usuario bigint) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			usuario.usuario
		WHERE
			id = _id_usuario;
	end
$$;
 D   DROP FUNCTION evento.f_mostrar_contacto_evento(_id_usuario bigint);
       evento       postgres    false    6    201            �            1259    96933    denuncias_eventos_view    VIEW     �  CREATE VIEW evento.denuncias_eventos_view AS
 SELECT (0)::bigint AS id,
    ''::text AS nombre,
    ''::character varying(25) AS descripcion_categoria,
    ''::character varying(20) AS descripcion_estado,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    0 AS cantidad_denuncias,
    ''::text AS imagen;
 )   DROP VIEW evento.denuncias_eventos_view;
       evento       postgres    false    6            %           1255    96961 )   f_mostrar_denuncias_filtro(text, integer)    FUNCTION     �  CREATE FUNCTION evento.f_mostrar_denuncias_filtro(_nombre_filtro text, _categoria_filtro integer) RETURNS SETOF evento.denuncias_eventos_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		
		select distinct 
		IE.id_evento,
		DE.nombre, 
		CA.descripcion,
		EE.descripcion,
		DE.fecha_inicio,
		DE.fecha_fin,
		DE.hora,
		DE.lugar,
		DE.cantidad_denuncias,
		DE.imagen

		from evento.denuncia_evento as IE
		inner join evento.evento as DE
		ON IE.id_evento = DE.id  
		inner join evento.categoria_evento as CA
		ON DE.id_categoria=CA.id
	    inner join evento.estado_evento as EE
		ON DE.id_estado = EE.id
		
		where
		
		(CASE _nombre_filtro 
				WHEN '' THEN 
					DE.nombre ilike '%' || _nombre_filtro  || '%' 
				ELSE 
					DE.nombre ilike '%' || _nombre_filtro || '%' 
				END)
		AND
			(CASE _categoria_filtro 
				WHEN -1 THEN 
					DE.id_categoria = CA.id
				ELSE 
					DE.id_categoria = _categoria_filtro
				END)	;
				
	
		 
		
	end

$$;
 a   DROP FUNCTION evento.f_mostrar_denuncias_filtro(_nombre_filtro text, _categoria_filtro integer);
       evento       postgres    false    230    6            �            1259    96942    detalle_denuncia_evento_view    VIEW     �   CREATE VIEW evento.detalle_denuncia_evento_view AS
 SELECT ''::text AS descripcion,
    ''::text AS nombre_completo,
    ''::text AS correo,
    ''::text AS imagen;
 /   DROP VIEW evento.detalle_denuncia_evento_view;
       evento       postgres    false    6                       1255    96946 )   f_mostrar_detalle_denuncia_evento(bigint)    FUNCTION       CREATE FUNCTION evento.f_mostrar_detalle_denuncia_evento(_id_evento bigint) RETURNS SETOF evento.detalle_denuncia_evento_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		
		select 
		
		ED.descripcion,
	    (UU.nombre || '  ' || UU.apellido),
        UU.correo,
		uu.url_imagen
		
		from evento.denuncia_evento as ED
		inner join usuario.usuario as UU
		ON ED.id_usuario = UU.id  
		inner join evento.evento as EE
		ON ED.id_evento = EE.id 
		
		where
		ED.id_evento=_id_evento;
	   
		
		 
		
	end

$$;
 K   DROP FUNCTION evento.f_mostrar_detalle_denuncia_evento(_id_evento bigint);
       evento       postgres    false    231    6            �            1259    96079    estado_evento    TABLE     �   CREATE TABLE evento.estado_evento (
    id integer NOT NULL,
    descripcion character varying(20),
    session text,
    ultima_modificacion timestamp without time zone
);
 !   DROP TABLE evento.estado_evento;
       evento         postgres    false    6            �            1255    96494    f_mostrar_estados()    FUNCTION     �   CREATE FUNCTION evento.f_mostrar_estados() RETURNS SETOF evento.estado_evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.estado_evento
		WHERE
		    id!=5;
		
	end
$$;
 *   DROP FUNCTION evento.f_mostrar_estados();
       evento       postgres    false    213    6            �            1259    96537    cancelar_evento    TABLE     �   CREATE TABLE evento.cancelar_evento (
    id bigint NOT NULL,
    razon text,
    id_evento bigint,
    session text,
    ultima_modificacion timestamp without time zone
);
 #   DROP TABLE evento.cancelar_evento;
       evento         postgres    false    6                       1255    96824 "   f_mostrar_evento_cancelado(bigint)    FUNCTION     	  CREATE FUNCTION evento.f_mostrar_evento_cancelado(_id_evento bigint) RETURNS SETOF evento.cancelar_evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.cancelar_evento
			
	     where	
		    id_evento = _id_evento;
		
	end
$$;
 D   DROP FUNCTION evento.f_mostrar_evento_cancelado(_id_evento bigint);
       evento       postgres    false    6    223                       1255    96392    f_mostrar_evento_id(bigint)    FUNCTION     �   CREATE FUNCTION evento.f_mostrar_evento_id(_id_evento bigint) RETURNS SETOF evento.evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.evento
		WHERE
		    id=_id_evento;
		
	end
$$;
 =   DROP FUNCTION evento.f_mostrar_evento_id(_id_evento bigint);
       evento       postgres    false    220    6            �            1259    96808    mostrar_eventos_filtro_view    VIEW       CREATE VIEW evento.mostrar_eventos_filtro_view AS
 SELECT (0)::bigint AS id,
    ''::text AS nombre,
    ''::text AS descripcion,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    ''::text AS imagen,
    (0)::bigint AS id_usuario,
    0 AS id_categoria,
    ''::character varying(25) AS descripcion_categoria,
    0 AS id_estado,
    ''::character varying(20) AS descripcion_estado,
    0 AS cantidad_interesados;
 .   DROP VIEW evento.mostrar_eventos_filtro_view;
       evento       postgres    false    6                       1255    96812 -   f_mostrar_eventos_filtro(text, integer, date)    FUNCTION     �  CREATE FUNCTION evento.f_mostrar_eventos_filtro(_nombre_filtro text, _categoria_filtro integer, _fecha_inicio_filtro date) RETURNS SETOF evento.mostrar_eventos_filtro_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		SELECT
			evento.evento.id,
			evento.evento.nombre,
			evento.evento.descripcion,
			evento.evento.fecha_inicio,
			evento.evento.fecha_fin,
			evento.evento.hora,
			evento.evento.lugar,
			evento.evento.imagen,
			evento.evento.id_usuario,
			evento.evento.id_categoria,
			evento.categoria_evento.descripcion,
			evento.evento.id_estado,
			evento.estado_evento.descripcion,
			evento.evento.cantidad_interesados
			
			
		FROM
			evento.evento,
			evento.categoria_evento,
			evento.estado_evento
		WHERE
			
			evento.evento.id_categoria = evento.categoria_evento.id
			AND
			evento.evento.id_estado = evento.estado_evento.id
            AND
			(CASE _nombre_filtro 
				WHEN '' THEN 
					evento.evento.nombre ilike '%' || _nombre_filtro || '%' 
				ELSE 
					evento.evento.nombre ilike '%' || _nombre_filtro || '%' 
				END)
			AND
			(CASE _categoria_filtro 
				WHEN -1 THEN 
					evento.evento.id_categoria = evento.categoria_evento.id
				ELSE 
					evento.evento.id_categoria = _categoria_filtro
				END)
			AND
			(CASE _fecha_inicio_filtro 
				WHEN '2019-03-31' THEN 
			 	 evento.evento.id_categoria = evento.categoria_evento.id
				ELSE 
					evento.evento.fecha_inicio  = _fecha_inicio_filtro 
				END);
			
			
	end
$$;
 z   DROP FUNCTION evento.f_mostrar_eventos_filtro(_nombre_filtro text, _categoria_filtro integer, _fecha_inicio_filtro date);
       evento       postgres    false    225    6            �            1259    96377    mostrar_mis_eventos_view    VIEW     �  CREATE VIEW evento.mostrar_mis_eventos_view AS
 SELECT (0)::bigint AS id,
    ''::text AS nombre,
    ''::text AS descripcion,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS imagen,
    (0)::bigint AS id_usuario,
    0 AS id_categoria,
    ''::character varying(25) AS descripcion_categoria,
    0 AS id_estado,
    ''::character varying(20) AS descripcion_estado,
    0 AS cantidad_interesados;
 +   DROP VIEW evento.mostrar_mis_eventos_view;
       evento       postgres    false    6            	           1255    96561    f_mostrar_eventos_finalizados()    FUNCTION     #  CREATE FUNCTION evento.f_mostrar_eventos_finalizados() RETURNS SETOF evento.mostrar_mis_eventos_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		SELECT
			evento.evento.id,
			evento.evento.nombre,
			evento.evento.descripcion,
			evento.evento.fecha_inicio,
			evento.evento.fecha_fin,
			evento.evento.hora,
			evento.evento.imagen,
			evento.evento.id_usuario,
			evento.evento.id_categoria,
			evento.categoria_evento.descripcion,
			evento.evento.id_estado,
			evento.estado_evento.descripcion,
			evento.evento.cantidad_interesados
			
			
		FROM
			evento.evento,
			evento.categoria_evento,
			evento.estado_evento
		WHERE
			
			evento.evento.id_categoria = evento.categoria_evento.id
			AND
			evento.evento.id_estado = evento.estado_evento.id
		    AND
			id_estado=3;
	end

$$;
 6   DROP FUNCTION evento.f_mostrar_eventos_finalizados();
       evento       postgres    false    6    221            �            1259    96891    me_interesa_eventos_view    VIEW     �  CREATE VIEW evento.me_interesa_eventos_view AS
 SELECT (0)::bigint AS id,
    ''::text AS nombre,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    ''::text AS imagen,
    ''::character varying(25) AS descripcion_categoria,
    ''::character varying(20) AS descripcion_estado,
    0 AS cantidad_interesados;
 +   DROP VIEW evento.me_interesa_eventos_view;
       evento       postgres    false    6                       1255    96895 %   f_mostrar_me_interesa_eventos(bigint)    FUNCTION     �  CREATE FUNCTION evento.f_mostrar_me_interesa_eventos(_id_usuario bigint) RETURNS SETOF evento.me_interesa_eventos_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		select 
		
		evento.evento.id,
		evento.evento.nombre,
		evento.evento.fecha_inicio,
		evento.evento.fecha_fin,
		evento.evento.hora,
		evento.evento.lugar,
		evento.evento.imagen,
		evento.categoria_evento.descripcion,
		evento.estado_evento.descripcion,
		evento.evento.cantidad_interesados
		
		from evento.evento
		   
		
		   
		inner join evento.interes on evento.interes.id_evento =evento.evento.id and evento.interes.habilitado = true and evento.interes.id_usuario =_id_usuario
		inner join evento.categoria_evento on evento.evento.id_categoria = evento.categoria_evento.id 
		inner join evento.estado_evento on evento.evento.id_estado = evento.estado_evento.id
		
		where
		     evento.evento.id_categoria=evento.categoria_evento.id;
	end

$$;
 H   DROP FUNCTION evento.f_mostrar_me_interesa_eventos(_id_usuario bigint);
       evento       postgres    false    228    6            �            1259    96599    mis_eventos_view    VIEW     �  CREATE VIEW evento.mis_eventos_view AS
 SELECT (0)::bigint AS id,
    ''::text AS nombre,
    ''::text AS descripcion,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    ''::text AS imagen,
    (0)::bigint AS id_usuario,
    0 AS id_categoria,
    ''::character varying(25) AS descripcion_categoria,
    0 AS id_estado,
    ''::character varying(20) AS descripcion_estado,
    0 AS cantidad_interesados;
 #   DROP VIEW evento.mis_eventos_view;
       evento       postgres    false    6                       1255    96876    f_mostrar_mis_eventos(bigint)    FUNCTION     �  CREATE FUNCTION evento.f_mostrar_mis_eventos(_id_usuario bigint) RETURNS SETOF evento.mis_eventos_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		SELECT
			evento.evento.id,
			evento.evento.nombre,
			evento.evento.descripcion,
			evento.evento.fecha_inicio,
			evento.evento.fecha_fin,
			evento.evento.hora,
			evento.evento.lugar,
			evento.evento.imagen,
			evento.evento.id_usuario,
			evento.evento.id_categoria,
			evento.categoria_evento.descripcion,
			evento.evento.id_estado,
			evento.estado_evento.descripcion,
			evento.evento.cantidad_interesados
			
			
		FROM
			evento.evento,
			evento.categoria_evento,
			evento.estado_evento
		WHERE
			
			evento.evento.id_categoria = evento.categoria_evento.id
			AND
			evento.evento.id_estado = evento.estado_evento.id
			AND
			evento.evento.id_usuario = _id_usuario
                                                                AND
			evento.evento.id_estado != 5;
	end
$$;
 @   DROP FUNCTION evento.f_mostrar_mis_eventos(_id_usuario bigint);
       evento       postgres    false    6    224            �            1259    96947    usuario_denuncia_evento_view    VIEW     �   CREATE VIEW evento.usuario_denuncia_evento_view AS
 SELECT ''::text AS nombre_completo,
    ''::text AS correo,
    ''::text AS imagen;
 /   DROP VIEW evento.usuario_denuncia_evento_view;
       evento       postgres    false    6                       1255    96951 )   f_mostrar_usuario_denuncia_evento(bigint)    FUNCTION     �  CREATE FUNCTION evento.f_mostrar_usuario_denuncia_evento(_id_evento bigint) RETURNS SETOF evento.usuario_denuncia_evento_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		
		select 
		
		(UU.nombre || '  ' || UU.apellido),
		 UU.correo,
		 UU.url_imagen
		
		from evento.evento as EE
		inner join usuario.usuario as UU
		ON EE.id_usuario =UU.id  
		
		where
		EE.id = _id_evento;
	   
		
		 
		
	end

$$;
 K   DROP FUNCTION evento.f_mostrar_usuario_denuncia_evento(_id_evento bigint);
       evento       postgres    false    232    6            �            1259    96090    denuncia_evento    TABLE     �   CREATE TABLE evento.denuncia_evento (
    id integer NOT NULL,
    descripcion text,
    id_evento bigint,
    session text,
    ultima_modificacion timestamp without time zone,
    id_usuario bigint
);
 #   DROP TABLE evento.denuncia_evento;
       evento         postgres    false    6                       1255    96862 *   f_validar_denuncia_usuario(bigint, bigint)    FUNCTION     C  CREATE FUNCTION evento.f_validar_denuncia_usuario(_id_usuario bigint, _id_evento bigint) RETURNS SETOF evento.denuncia_evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.denuncia_evento
			
	     where	
		    id_usuario = _id_usuario
			AND
		    id_evento = _id_evento;
		
	end
$$;
 X   DROP FUNCTION evento.f_validar_denuncia_usuario(_id_usuario bigint, _id_evento bigint);
       evento       postgres    false    6    215            �            1259    104687    valida_razon_eventos_view    VIEW     �   CREATE VIEW evento.valida_razon_eventos_view AS
 SELECT ''::text AS razon,
    (0)::bigint AS id_evento,
    (0)::bigint AS id_usuario;
 ,   DROP VIEW evento.valida_razon_eventos_view;
       evento       postgres    false    6                       1255    104691 "   f_validar_existencia_razon(bigint)    FUNCTION     q  CREATE FUNCTION evento.f_validar_existencia_razon(_id_evento bigint) RETURNS SETOF evento.valida_razon_eventos_view
    LANGUAGE plpgsql
    AS $$
    BEGIN
	return query
	   
    Select 
	EC.razon,
	EC.id_evento,
	EE.id_usuario
	from
	evento.cancelar_evento  as EC
	inner join evento.evento as EE
	ON EC.id_evento = EE.id

	where 
	
	  id_evento =_id_evento;
 END
$$;
 D   DROP FUNCTION evento.f_validar_existencia_razon(_id_evento bigint);
       evento       postgres    false    233    6            �            1259    96056    interes    TABLE     �   CREATE TABLE evento.interes (
    id bigint NOT NULL,
    id_usuario bigint,
    id_evento bigint,
    habilitado boolean,
    session text,
    ultima_modificacion timestamp without time zone
);
    DROP TABLE evento.interes;
       evento         postgres    false    6                       1255    96829 )   f_validar_interes_usuario(bigint, bigint)    FUNCTION     2  CREATE FUNCTION evento.f_validar_interes_usuario(_id_usuario bigint, _id_evento bigint) RETURNS SETOF evento.interes
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.interes
			
	     where	
		    id_usuario = _id_usuario
			AND
		    id_evento = _id_evento;
		
	end
$$;
 W   DROP FUNCTION evento.f_validar_interes_usuario(_id_usuario bigint, _id_evento bigint);
       evento       postgres    false    211    6            �            1259    104737    mostrar_eventos_activos_view    VIEW     E  CREATE VIEW reporte.mostrar_eventos_activos_view AS
 SELECT ''::text AS nombre,
    '2019-03-31'::date AS fecha_inicio,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    ''::text AS imagen,
    0 AS id_categoria,
    ''::character varying(25) AS descripcion_categoria,
    0 AS cantidad_interesados;
 0   DROP VIEW reporte.mostrar_eventos_activos_view;
       reporte       postgres    false    11            )           1255    104741    f_mostrar_eventos_activos(date)    FUNCTION     <  CREATE FUNCTION reporte.f_mostrar_eventos_activos(_fecha_inicio_filtro date) RETURNS SETOF reporte.mostrar_eventos_activos_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		SELECT
			evento.evento.nombre,
			evento.evento.fecha_inicio,
			evento.evento.hora,
			evento.evento.lugar,
			evento.evento.imagen,
			evento.evento.id_categoria,
			evento.categoria_evento.descripcion,
			evento.evento.cantidad_interesados
			
			
		FROM
			evento.evento,
			evento.categoria_evento
	        
		WHERE
			
			evento.evento.id_categoria = evento.categoria_evento.id
			AND
			evento.evento.id_estado=1
			AND
			(CASE _fecha_inicio_filtro 
				WHEN '2019-03-31' THEN 
			 	 evento.evento.id_categoria = evento.categoria_evento.id
				ELSE 
					evento.evento.fecha_inicio  = _fecha_inicio_filtro 
				END);
			
			
	end
$$;
 L   DROP FUNCTION reporte.f_mostrar_eventos_activos(_fecha_inicio_filtro date);
       reporte       postgres    false    236    11            �            1259    104726    mostrar_eventos_cancelados_view    VIEW     \  CREATE VIEW reporte.mostrar_eventos_cancelados_view AS
 SELECT ''::text AS nombre,
    ''::text AS descripcion,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    ''::text AS imagen,
    (0)::bigint AS id_usuario,
    ''::text AS nombre_completo;
 3   DROP VIEW reporte.mostrar_eventos_cancelados_view;
       reporte       postgres    false    11            '           1255    104730    f_mostrar_eventos_cancelados()    FUNCTION       CREATE FUNCTION reporte.f_mostrar_eventos_cancelados() RETURNS SETOF reporte.mostrar_eventos_cancelados_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		SELECT
		
			evento.evento.nombre,
			evento.evento.descripcion,
			evento.evento.fecha_inicio,
			evento.evento.fecha_fin,
			evento.evento.hora,
			evento.evento.lugar,
			evento.evento.imagen,
			evento.evento.id_usuario,
			usuario.usuario.nombre ||usuario.usuario.apellido
			
			
			
		FROM
			evento.evento,
			evento.categoria_evento,
			evento.estado_evento,
			usuario.usuario
		WHERE
			evento.evento.id_usuario=usuario.usuario.id
			AND
			evento.evento.id_categoria = evento.categoria_evento.id
			AND
			evento.evento.id_estado = evento.estado_evento.id
			AND
			evento.evento.id_estado=5;
            
			
	end
$$;
 6   DROP FUNCTION reporte.f_mostrar_eventos_cancelados();
       reporte       postgres    false    11    234            �            1259    104731     mostrar_eventos_denunciados_view    VIEW     r  CREATE VIEW reporte.mostrar_eventos_denunciados_view AS
 SELECT ''::text AS nombre,
    ''::character varying(25) AS categoria,
    ''::character varying(20) AS estado,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    0 AS cantidad_denuncias,
    ''::text AS imagen;
 4   DROP VIEW reporte.mostrar_eventos_denunciados_view;
       reporte       postgres    false    11            (           1255    104736    f_mostrar_eventos_denunciados()    FUNCTION     E  CREATE FUNCTION reporte.f_mostrar_eventos_denunciados() RETURNS SETOF reporte.mostrar_eventos_denunciados_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		select distinct 
		DE.nombre, 
		CA.descripcion,
		EE.descripcion,
		DE.fecha_inicio,
		DE.fecha_fin,
		DE.hora,
		DE.lugar,
		DE.cantidad_denuncias,
		DE.imagen

		from evento.denuncia_evento as IE
		inner join evento.evento as DE
		ON IE.id_evento = DE.id  
		inner join evento.categoria_evento as CA
		ON DE.id_categoria=CA.id
	    inner join evento.estado_evento as EE
		ON DE.id_estado = EE.id;
			
	end
$$;
 7   DROP FUNCTION reporte.f_mostrar_eventos_denunciados();
       reporte       postgres    false    11    235                       1255    96190 #   f_cerrar_sesion_autentication(text)    FUNCTION     �   CREATE FUNCTION security.f_cerrar_sesion_autentication(_session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
	
	BEGIN
		UPDATE
			security.autentication
		SET
			fecha_fin = current_timestamp
		WHERE
			session = _session;
			
	END

$$;
 E   DROP FUNCTION security.f_cerrar_sesion_autentication(_session text);
       security       postgres    false    10                       1255    96178 R   f_guardar_sesion_autentication(bigint, character varying, character varying, text)    FUNCTION     �  CREATE FUNCTION security.f_guardar_sesion_autentication(_id bigint, _ip character varying, _mac character varying, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
	
	BEGIN
		INSERT INTO security.autentication
		(
			codigo_usuario,
			ip,
			mac,
			fecha_inicio,
			session
		)
	VALUES 
		(
			_id,
			_ip,
			_mac,
			current_timestamp,
			_session
		);

		
		
	END

$$;
 �   DROP FUNCTION security.f_guardar_sesion_autentication(_id bigint, _ip character varying, _mac character varying, _session text);
       security       postgres    false    10            �            1259    96165    autenticate_view    VIEW     �   CREATE VIEW security.autenticate_view AS
 SELECT (0)::text AS correo_usuario,
    ''::character varying(20) AS nombre,
    ''::character varying(20) AS apellido,
    0 AS codigo_rol,
    ''::character varying(20) AS nombre_rol;
 %   DROP VIEW security.autenticate_view;
       security       postgres    false    10                       1255    96873 &   f_validacion_inicio_sesion(text, text)    FUNCTION     C  CREATE FUNCTION security.f_validacion_inicio_sesion(_correo text, _clave text) RETURNS SETOF security.autenticate_view
    LANGUAGE plpgsql
    AS $$

	BEGIN
		return query
		SELECT
			usuario.usuario.correo as correo_usuario,
			usuario.usuario.nombre as nombre,
			usuario.usuario.apellido as apellido,
			usuario.rol.id as codigo_rol,
			usuario.rol.descripcion as nombre_rol
			
		FROM
			usuario.usuario,
			usuario.rol
		WHERE
			
			usuario.usuario.correo = _correo
			AND
			usuario.usuario.clave = _clave
			AND
			usuario.usuario.id_rol = usuario.rol.id;
			
	end

$$;
 N   DROP FUNCTION security.f_validacion_inicio_sesion(_correo text, _clave text);
       security       postgres    false    10    218                       1255    96875     f_actualizar_clave(bigint, text)    FUNCTION     F  CREATE FUNCTION usuario.f_actualizar_clave(_user_id bigint, _clave text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
	
	BEGIN
		DELETE FROM
			usuario.token_recuperacion_usuario
		WHERE
			user_id = _user_id;
	
		UPDATE
			usuario.usuario
		SET
			estado = 1,
			clave = _clave
			
		WHERE
			id = _user_id;		
	END

$$;
 H   DROP FUNCTION usuario.f_actualizar_clave(_user_id bigint, _clave text);
       usuario       postgres    false    8                       1255    96198 4   f_almacenar_token_recuperacion_usuario(text, bigint)    FUNCTION     �  CREATE FUNCTION usuario.f_almacenar_token_recuperacion_usuario(_token text, _user_id bigint) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$	
	BEGIN
		INSERT INTO usuario.token_recuperacion_usuario
		(
			user_id,
			token, 
			fecha_creado,
			fecha_vigencia
			
		)
		VALUES 
		(
			_user_id,
			_token,
			current_timestamp,
			current_timestamp + interval '2 hours'
		);

			
	END

$$;
 \   DROP FUNCTION usuario.f_almacenar_token_recuperacion_usuario(_token text, _user_id bigint);
       usuario       postgres    false    8            �            1255    96150     f_buscar_existencia_correo(text)    FUNCTION     �   CREATE FUNCTION usuario.f_buscar_existencia_correo(_correo text) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			usuario.usuario
		WHERE
			correo = _correo;
	end
$$;
 @   DROP FUNCTION usuario.f_buscar_existencia_correo(_correo text);
       usuario       postgres    false    201    8            �            1255    96218     f_buscar_existencia_imagen(text)    FUNCTION     �   CREATE FUNCTION usuario.f_buscar_existencia_imagen(_url_imagen text) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			usuario.usuario
		WHERE
			url_imagen = _url_imagen;
	end
$$;
 D   DROP FUNCTION usuario.f_buscar_existencia_imagen(_url_imagen text);
       usuario       postgres    false    8    201            !           1255    96918 -   f_deshabilitar_usuario(bigint, boolean, text)    FUNCTION     X  CREATE FUNCTION usuario.f_deshabilitar_usuario(_id_usuario bigint, _habilitado boolean, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
	
	 UPDATE usuario.usuario SET

	    habilitado=_habilitado,
	    session=_session,
	    ultima_modificacion=current_timestamp
       
	where 
	
	  id =_id_usuario;
	 
   
 END
$$;
 f   DROP FUNCTION usuario.f_deshabilitar_usuario(_id_usuario bigint, _habilitado boolean, _session text);
       usuario       postgres    false    8            �            1255    96219 4   f_editar_usuario(text, text, text, text, text, text)    FUNCTION     �  CREATE FUNCTION usuario.f_editar_usuario(_nombre text, _apellido text, _correo text, _clave text, _url_imagen text, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    UPDATE usuario.usuario SET

	 nombre = _nombre ,
	 apellido=_apellido,
	 correo= correo,
	 clave= _clave,
	 url_imagen=_url_imagen ,
	 session=_session,
	 ultima_modificacion=current_timestamp

	where 
	
	  correo =_correo;
 END
$$;
 �   DROP FUNCTION usuario.f_editar_usuario(_nombre text, _apellido text, _correo text, _clave text, _url_imagen text, _session text);
       usuario       postgres    false    8            �            1255    96145 6   f_insertar_usuario(text, text, text, text, text, text)    FUNCTION     �  CREATE FUNCTION usuario.f_insertar_usuario(_nombre text, _apellido text, _correo text, _clave text, _url_imagen text, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    INSERT INTO usuario.usuario 

	(
	 nombre ,
	 apellido,
	 correo ,
	 clave ,
	 url_imagen ,
	 estado ,
	 habilitado,
	 id_rol ,
	 session,
	 ultima_modificacion
	) 
	
	VALUES (
    _nombre ,
	_apellido ,
	_correo,
	_clave ,
	_url_imagen ,
	 1,
	 true ,
	 2,
	_session,
	current_timestamp
	);
 END
$$;
 �   DROP FUNCTION usuario.f_insertar_usuario(_nombre text, _apellido text, _correo text, _clave text, _url_imagen text, _session text);
       usuario       postgres    false    8                       1255    96170    f_mostrar_datos_usuario(text)    FUNCTION       CREATE FUNCTION usuario.f_mostrar_datos_usuario(_correo text) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return query
        SELECT
            *
        FROM
            usuario.usuario
        WHERE
            correo = _correo;
    end
$$;
 =   DROP FUNCTION usuario.f_mostrar_datos_usuario(_correo text);
       usuario       postgres    false    201    8            �            1259    96901    mostrar_usuarios_view    VIEW     �   CREATE VIEW usuario.mostrar_usuarios_view AS
 SELECT (0)::bigint AS id_usuario,
    ''::text AS nombre_completo,
    ''::text AS correo,
    true AS habilitado,
    ''::text AS imagen;
 )   DROP VIEW usuario.mostrar_usuarios_view;
       usuario       postgres    false    8                        1255    96917 1   f_mostrar_datos_usuarios_filtro(text, text, text)    FUNCTION     �  CREATE FUNCTION usuario.f_mostrar_datos_usuarios_filtro(_nombre_filtro text, _apellido_filtro text, _correo_filtro text) RETURNS SETOF usuario.mostrar_usuarios_view
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return query
        SELECT
		
		   usuario.usuario.id,
		   (usuario.usuario.nombre || '  ' || usuario.usuario.apellido),
		   usuario.usuario.correo,
		   usuario.usuario.habilitado,
		   usuario.usuario.url_imagen
		    
   
        FROM
            usuario.usuario
			
        WHERE
		    id_rol!=1
			 AND
			(CASE _nombre_filtro 
				WHEN '' THEN 
					usuario.usuario.nombre ilike '%' || _nombre_filtro || '%'
				ELSE 
					usuario.usuario.nombre ilike '%' || _nombre_filtro || '%'
			    
				END)
				AND
				(CASE _apellido_filtro 
				WHEN '' THEN 
					usuario.usuario.apellido ilike '%' || _apellido_filtro || '%'
				ELSE 
					usuario.usuario.apellido ilike '%' || _apellido_filtro || '%'
			    
				END)
			AND
			(CASE _correo_filtro 
				WHEN '' THEN 
					usuario.usuario.correo ilike '%' || _correo_filtro || '%' 
				ELSE 
					usuario.usuario.correo ilike '%' || _correo_filtro || '%' 
				END);
            
    end
$$;
 x   DROP FUNCTION usuario.f_mostrar_datos_usuarios_filtro(_nombre_filtro text, _apellido_filtro text, _correo_filtro text);
       usuario       postgres    false    229    8                       1255    96201 *   f_obtener_usuario_token_recuperacion(text)    FUNCTION     �  CREATE FUNCTION usuario.f_obtener_usuario_token_recuperacion(_token text) RETURNS SETOF bigint
    LANGUAGE plpgsql
    AS $$
	
	BEGIN
		IF (SELECT COUNT(*) FROM usuario.token_recuperacion_usuario WHERE token = _token) = 0
			THEN RETURN QUERY
				SELECT
					-1::INTEGER;
		elseIF (SELECT COUNT(*)FROM usuario.token_recuperacion_usuario WHERE token = _token AND current_timestamp between fecha_creado AND fecha_vigencia) = 0
			THEN
				DELETE FROM usuario.token_recuperacion_usuario WHERE token = _token;
			
				RETURN QUERY
				SELECT
					-2::INTEGER;
		ELSE	
			RETURN QUERY 
				SELECT
					user_id
				FROM
					usuario.token_recuperacion_usuario
				WHERE
					token = _token;
		
		END IF;

			
	END

$$;
 I   DROP FUNCTION usuario.f_obtener_usuario_token_recuperacion(_token text);
       usuario       postgres    false    8                       1255    96874    f_validar_usuario_usuario(text)    FUNCTION     �  CREATE FUNCTION usuario.f_validar_usuario_usuario(_correo text) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	
	BEGIN
		IF (SELECT COUNT(*) FROM usuario.usuario WHERE correo = _correo) = 0
			then RETURN QUERY
				SELECT
					-1::BIGINT,
					''::CHARACTER VARYING(20),
					''::CHARACTER VARYING(20),
					''::TEXT,
					''::TEXT,
					''::TEXT,
					1::INTEGER,
					true::BOOLEAN,
					1::INTEGER,
					''::TEXT,
					'2018-01-01 00:00'::TIMESTAMP;
					
		elseIF (SELECT COUNT(*) FROM usuario.usuario uu join usuario.token_recuperacion_usuario ut on ut.user_id = uu.id WHERE uu.correo = _correo and current_timestamp between ut.fecha_creado AND ut.fecha_vigencia) > 0

			then RETURN QUERY
				SELECT
					-2::BIGINT,
					''::CHARACTER VARYING(20),
					''::CHARACTER VARYING(20),
					''::TEXT,
					''::TEXT,
					''::TEXT,
					1::INTEGER,
					true::BOOLEAN,
					1::INTEGER,
					''::TEXT,
					'2018-01-01 00:00'::TIMESTAMP;
		ELSEIF (SELECT COUNT(*) FROM usuario.usuario WHERE correo = _correo) > 0
			THEN
				update 
					usuario.usuario
				set
					estado = 2
				where
					correo = _correo;
				
				RETURN QUERY 
				SELECT
					*
				FROM
					usuario.usuario 
				WHERE
					correo = _correo;
		ELSE
			RETURN QUERY
				SELECT
					-1::BIGINT,
					''::CHARACTER VARYING(20),
					''::CHARACTER VARYING(20),
					''::TEXT,
					''::TEXT,
					''::TEXT,
					1::INTEGER,
					true::BOOLEAN,
					1::INTEGER,
					''::TEXT,
					'2018-01-01 00:00'::TIMESTAMP;
		END IF;
			
	END

$$;
 ?   DROP FUNCTION usuario.f_validar_usuario_usuario(_correo text);
       usuario       postgres    false    201    8            �            1259    96535    cancelar_evento_id_seq    SEQUENCE        CREATE SEQUENCE evento.cancelar_evento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE evento.cancelar_evento_id_seq;
       evento       postgres    false    223    6                       0    0    cancelar_evento_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE evento.cancelar_evento_id_seq OWNED BY evento.cancelar_evento.id;
            evento       postgres    false    222            �            1259    96099    categoria_evento_id_seq    SEQUENCE     �   CREATE SEQUENCE evento.categoria_evento_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE evento.categoria_evento_id_seq;
       evento       postgres    false    6    217                       0    0    categoria_evento_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE evento.categoria_evento_id_seq OWNED BY evento.categoria_evento.id;
            evento       postgres    false    216            �            1259    96088    denuncia_evento_id_seq    SEQUENCE     �   CREATE SEQUENCE evento.denuncia_evento_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE evento.denuncia_evento_id_seq;
       evento       postgres    false    6    215                       0    0    denuncia_evento_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE evento.denuncia_evento_id_seq OWNED BY evento.denuncia_evento.id;
            evento       postgres    false    214            �            1259    96077    estado_evento_id_seq    SEQUENCE     �   CREATE SEQUENCE evento.estado_evento_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE evento.estado_evento_id_seq;
       evento       postgres    false    213    6                       0    0    estado_evento_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE evento.estado_evento_id_seq OWNED BY evento.estado_evento.id;
            evento       postgres    false    212            �            1259    96054    interes_id_seq    SEQUENCE     w   CREATE SEQUENCE evento.interes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE evento.interes_id_seq;
       evento       postgres    false    211    6                       0    0    interes_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE evento.interes_id_seq OWNED BY evento.interes.id;
            evento       postgres    false    210            �            1259    96045 	   auditoria    TABLE       CREATE TABLE security.auditoria (
    id bigint NOT NULL,
    fecha timestamp without time zone,
    accion character varying(100),
    schema character varying(200),
    tabla character varying(200),
    session text,
    user_bd character varying(100),
    data jsonb,
    pk text
);
    DROP TABLE security.auditoria;
       security         postgres    false    10            �            1259    96043    auditoria_id_seq    SEQUENCE     {   CREATE SEQUENCE security.auditoria_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE security.auditoria_id_seq;
       security       postgres    false    10    209                       0    0    auditoria_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE security.auditoria_id_seq OWNED BY security.auditoria.id;
            security       postgres    false    208            �            1259    96034    autentication    TABLE       CREATE TABLE security.autentication (
    id bigint NOT NULL,
    codigo_usuario integer,
    ip character varying(100),
    mac character varying(100),
    fecha_inicio timestamp without time zone,
    session text,
    fecha_fin timestamp without time zone
);
 #   DROP TABLE security.autentication;
       security         postgres    false    10            �            1259    96032    autentication_id_seq    SEQUENCE        CREATE SEQUENCE security.autentication_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE security.autentication_id_seq;
       security       postgres    false    207    10            	           0    0    autentication_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE security.autentication_id_seq OWNED BY security.autentication.id;
            security       postgres    false    206            �            1259    96012    rol    TABLE     �   CREATE TABLE usuario.rol (
    id integer NOT NULL,
    descripcion character varying(20),
    session text,
    ultima_modificacion timestamp without time zone
);
    DROP TABLE usuario.rol;
       usuario         postgres    false    8            �            1259    96010 
   rol_id_seq    SEQUENCE     �   CREATE SEQUENCE usuario.rol_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE usuario.rol_id_seq;
       usuario       postgres    false    8    203            
           0    0 
   rol_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE usuario.rol_id_seq OWNED BY usuario.rol.id;
            usuario       postgres    false    202            �            1259    96023    token_recuperacion_usuario    TABLE     �   CREATE TABLE usuario.token_recuperacion_usuario (
    id integer NOT NULL,
    user_id bigint,
    token text,
    fecha_creado timestamp without time zone,
    fecha_vigencia timestamp without time zone
);
 /   DROP TABLE usuario.token_recuperacion_usuario;
       usuario         postgres    false    8            �            1259    96021 !   token_recuperacion_usuario_id_seq    SEQUENCE     �   CREATE SEQUENCE usuario.token_recuperacion_usuario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE usuario.token_recuperacion_usuario_id_seq;
       usuario       postgres    false    205    8                       0    0 !   token_recuperacion_usuario_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE usuario.token_recuperacion_usuario_id_seq OWNED BY usuario.token_recuperacion_usuario.id;
            usuario       postgres    false    204            �            1259    95999    usuario_id_seq    SEQUENCE     x   CREATE SEQUENCE usuario.usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE usuario.usuario_id_seq;
       usuario       postgres    false    8    201                       0    0    usuario_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE usuario.usuario_id_seq OWNED BY usuario.usuario.id;
            usuario       postgres    false    200            ?           2604    96540    cancelar_evento id    DEFAULT     x   ALTER TABLE ONLY evento.cancelar_evento ALTER COLUMN id SET DEFAULT nextval('evento.cancelar_evento_id_seq'::regclass);
 A   ALTER TABLE evento.cancelar_evento ALTER COLUMN id DROP DEFAULT;
       evento       postgres    false    222    223    223            =           2604    96104    categoria_evento id    DEFAULT     z   ALTER TABLE ONLY evento.categoria_evento ALTER COLUMN id SET DEFAULT nextval('evento.categoria_evento_id_seq'::regclass);
 B   ALTER TABLE evento.categoria_evento ALTER COLUMN id DROP DEFAULT;
       evento       postgres    false    216    217    217            <           2604    96093    denuncia_evento id    DEFAULT     x   ALTER TABLE ONLY evento.denuncia_evento ALTER COLUMN id SET DEFAULT nextval('evento.denuncia_evento_id_seq'::regclass);
 A   ALTER TABLE evento.denuncia_evento ALTER COLUMN id DROP DEFAULT;
       evento       postgres    false    215    214    215            ;           2604    96082    estado_evento id    DEFAULT     t   ALTER TABLE ONLY evento.estado_evento ALTER COLUMN id SET DEFAULT nextval('evento.estado_evento_id_seq'::regclass);
 ?   ALTER TABLE evento.estado_evento ALTER COLUMN id DROP DEFAULT;
       evento       postgres    false    213    212    213            :           2604    96059 
   interes id    DEFAULT     h   ALTER TABLE ONLY evento.interes ALTER COLUMN id SET DEFAULT nextval('evento.interes_id_seq'::regclass);
 9   ALTER TABLE evento.interes ALTER COLUMN id DROP DEFAULT;
       evento       postgres    false    210    211    211            9           2604    96048    auditoria id    DEFAULT     p   ALTER TABLE ONLY security.auditoria ALTER COLUMN id SET DEFAULT nextval('security.auditoria_id_seq'::regclass);
 =   ALTER TABLE security.auditoria ALTER COLUMN id DROP DEFAULT;
       security       postgres    false    208    209    209            8           2604    96037    autentication id    DEFAULT     x   ALTER TABLE ONLY security.autentication ALTER COLUMN id SET DEFAULT nextval('security.autentication_id_seq'::regclass);
 A   ALTER TABLE security.autentication ALTER COLUMN id DROP DEFAULT;
       security       postgres    false    206    207    207            6           2604    96015    rol id    DEFAULT     b   ALTER TABLE ONLY usuario.rol ALTER COLUMN id SET DEFAULT nextval('usuario.rol_id_seq'::regclass);
 6   ALTER TABLE usuario.rol ALTER COLUMN id DROP DEFAULT;
       usuario       postgres    false    202    203    203            7           2604    96026    token_recuperacion_usuario id    DEFAULT     �   ALTER TABLE ONLY usuario.token_recuperacion_usuario ALTER COLUMN id SET DEFAULT nextval('usuario.token_recuperacion_usuario_id_seq'::regclass);
 M   ALTER TABLE usuario.token_recuperacion_usuario ALTER COLUMN id DROP DEFAULT;
       usuario       postgres    false    205    204    205            5           2604    96004 
   usuario id    DEFAULT     j   ALTER TABLE ONLY usuario.usuario ALTER COLUMN id SET DEFAULT nextval('usuario.usuario_id_seq'::regclass);
 :   ALTER TABLE usuario.usuario ALTER COLUMN id DROP DEFAULT;
       usuario       postgres    false    200    201    201            �          0    96537    cancelar_evento 
   TABLE DATA               ]   COPY evento.cancelar_evento (id, razon, id_evento, session, ultima_modificacion) FROM stdin;
    evento       postgres    false    223   �      �          0    96101    categoria_evento 
   TABLE DATA               Y   COPY evento.categoria_evento (id, descripcion, session, ultima_modificacion) FROM stdin;
    evento       postgres    false    217   ]      �          0    96090    denuncia_evento 
   TABLE DATA               o   COPY evento.denuncia_evento (id, descripcion, id_evento, session, ultima_modificacion, id_usuario) FROM stdin;
    evento       postgres    false    215         �          0    96079    estado_evento 
   TABLE DATA               V   COPY evento.estado_evento (id, descripcion, session, ultima_modificacion) FROM stdin;
    evento       postgres    false    213         �          0    96320    evento 
   TABLE DATA               �   COPY evento.evento (id, nombre, descripcion, fecha_inicio, fecha_fin, hora, imagen, id_usuario, id_categoria, id_estado, cantidad_interesados, cantidad_denuncias, session, ultima_modificacion, lugar) FROM stdin;
    evento       postgres    false    220   l      �          0    96056    interes 
   TABLE DATA               f   COPY evento.interes (id, id_usuario, id_evento, habilitado, session, ultima_modificacion) FROM stdin;
    evento       postgres    false    211   2      �          0    96045 	   auditoria 
   TABLE DATA               c   COPY security.auditoria (id, fecha, accion, schema, tabla, session, user_bd, data, pk) FROM stdin;
    security       postgres    false    209   �      �          0    96034    autentication 
   TABLE DATA               h   COPY security.autentication (id, codigo_usuario, ip, mac, fecha_inicio, session, fecha_fin) FROM stdin;
    security       postgres    false    207         �          0    96012    rol 
   TABLE DATA               M   COPY usuario.rol (id, descripcion, session, ultima_modificacion) FROM stdin;
    usuario       postgres    false    203   !&      �          0    96023    token_recuperacion_usuario 
   TABLE DATA               g   COPY usuario.token_recuperacion_usuario (id, user_id, token, fecha_creado, fecha_vigencia) FROM stdin;
    usuario       postgres    false    205   l&      �          0    96001    usuario 
   TABLE DATA               �   COPY usuario.usuario (id, nombre, apellido, correo, clave, url_imagen, estado, habilitado, id_rol, session, ultima_modificacion) FROM stdin;
    usuario       postgres    false    201   �&                 0    0    cancelar_evento_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('evento.cancelar_evento_id_seq', 2, true);
            evento       postgres    false    222                       0    0    categoria_evento_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('evento.categoria_evento_id_seq', 6, true);
            evento       postgres    false    216                       0    0    denuncia_evento_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('evento.denuncia_evento_id_seq', 4, true);
            evento       postgres    false    214                       0    0    estado_evento_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('evento.estado_evento_id_seq', 1, false);
            evento       postgres    false    212                       0    0    evento_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('evento.evento_id_seq', 8, true);
            evento       postgres    false    219                       0    0    interes_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('evento.interes_id_seq', 7, true);
            evento       postgres    false    210                       0    0    auditoria_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('security.auditoria_id_seq', 1, false);
            security       postgres    false    208                       0    0    autentication_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('security.autentication_id_seq', 73, true);
            security       postgres    false    206                       0    0 
   rol_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('usuario.rol_id_seq', 1, false);
            usuario       postgres    false    202                       0    0 !   token_recuperacion_usuario_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('usuario.token_recuperacion_usuario_id_seq', 4, true);
            usuario       postgres    false    204                       0    0    usuario_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('usuario.usuario_id_seq', 6, true);
            usuario       postgres    false    200            U           2606    96545 $   cancelar_evento cancelar_evento_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY evento.cancelar_evento
    ADD CONSTRAINT cancelar_evento_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY evento.cancelar_evento DROP CONSTRAINT cancelar_evento_pkey;
       evento         postgres    false    223            Q           2606    96109 &   categoria_evento categoria_evento_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY evento.categoria_evento
    ADD CONSTRAINT categoria_evento_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY evento.categoria_evento DROP CONSTRAINT categoria_evento_pkey;
       evento         postgres    false    217            O           2606    96098 $   denuncia_evento denuncia_evento_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY evento.denuncia_evento
    ADD CONSTRAINT denuncia_evento_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY evento.denuncia_evento DROP CONSTRAINT denuncia_evento_pkey;
       evento         postgres    false    215            M           2606    96087     estado_evento estado_evento_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY evento.estado_evento
    ADD CONSTRAINT estado_evento_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY evento.estado_evento DROP CONSTRAINT estado_evento_pkey;
       evento         postgres    false    213            S           2606    96328    evento evento_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY evento.evento
    ADD CONSTRAINT evento_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY evento.evento DROP CONSTRAINT evento_pkey;
       evento         postgres    false    220            K           2606    96064    interes interes_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY evento.interes
    ADD CONSTRAINT interes_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY evento.interes DROP CONSTRAINT interes_pkey;
       evento         postgres    false    211            I           2606    96053    auditoria auditoria_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY security.auditoria
    ADD CONSTRAINT auditoria_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY security.auditoria DROP CONSTRAINT auditoria_pkey;
       security         postgres    false    209            G           2606    96042     autentication autentication_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY security.autentication
    ADD CONSTRAINT autentication_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY security.autentication DROP CONSTRAINT autentication_pkey;
       security         postgres    false    207            C           2606    96020    rol rol_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY usuario.rol
    ADD CONSTRAINT rol_pkey PRIMARY KEY (id);
 7   ALTER TABLE ONLY usuario.rol DROP CONSTRAINT rol_pkey;
       usuario         postgres    false    203            E           2606    96031 :   token_recuperacion_usuario token_recuperacion_usuario_pkey 
   CONSTRAINT     y   ALTER TABLE ONLY usuario.token_recuperacion_usuario
    ADD CONSTRAINT token_recuperacion_usuario_pkey PRIMARY KEY (id);
 e   ALTER TABLE ONLY usuario.token_recuperacion_usuario DROP CONSTRAINT token_recuperacion_usuario_pkey;
       usuario         postgres    false    205            A           2606    96009    usuario usuario_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY usuario.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);
 ?   ALTER TABLE ONLY usuario.usuario DROP CONSTRAINT usuario_pkey;
       usuario         postgres    false    201            [           2606    96329    evento id_categoria    FK CONSTRAINT     �   ALTER TABLE ONLY evento.evento
    ADD CONSTRAINT id_categoria FOREIGN KEY (id_categoria) REFERENCES evento.categoria_evento(id);
 =   ALTER TABLE ONLY evento.evento DROP CONSTRAINT id_categoria;
       evento       postgres    false    2897    220    217            \           2606    96334    evento id_estado    FK CONSTRAINT     y   ALTER TABLE ONLY evento.evento
    ADD CONSTRAINT id_estado FOREIGN KEY (id_estado) REFERENCES evento.estado_evento(id);
 :   ALTER TABLE ONLY evento.evento DROP CONSTRAINT id_estado;
       evento       postgres    false    220    213    2893            Y           2606    96344    denuncia_evento id_evento    FK CONSTRAINT     {   ALTER TABLE ONLY evento.denuncia_evento
    ADD CONSTRAINT id_evento FOREIGN KEY (id_evento) REFERENCES evento.evento(id);
 C   ALTER TABLE ONLY evento.denuncia_evento DROP CONSTRAINT id_evento;
       evento       postgres    false    215    220    2899            X           2606    96349    interes id_evento    FK CONSTRAINT     s   ALTER TABLE ONLY evento.interes
    ADD CONSTRAINT id_evento FOREIGN KEY (id_evento) REFERENCES evento.evento(id);
 ;   ALTER TABLE ONLY evento.interes DROP CONSTRAINT id_evento;
       evento       postgres    false    220    211    2899            ^           2606    96546    cancelar_evento id_evento    FK CONSTRAINT     {   ALTER TABLE ONLY evento.cancelar_evento
    ADD CONSTRAINT id_evento FOREIGN KEY (id_evento) REFERENCES evento.evento(id);
 C   ALTER TABLE ONLY evento.cancelar_evento DROP CONSTRAINT id_evento;
       evento       postgres    false    223    2899    220            W           2606    96135    interes id_usuario    FK CONSTRAINT     w   ALTER TABLE ONLY evento.interes
    ADD CONSTRAINT id_usuario FOREIGN KEY (id_usuario) REFERENCES usuario.usuario(id);
 <   ALTER TABLE ONLY evento.interes DROP CONSTRAINT id_usuario;
       evento       postgres    false    211    201    2881            ]           2606    96339    evento id_usuario    FK CONSTRAINT     v   ALTER TABLE ONLY evento.evento
    ADD CONSTRAINT id_usuario FOREIGN KEY (id_usuario) REFERENCES usuario.usuario(id);
 ;   ALTER TABLE ONLY evento.evento DROP CONSTRAINT id_usuario;
       evento       postgres    false    220    201    2881            Z           2606    96856    denuncia_evento id_usuario    FK CONSTRAINT        ALTER TABLE ONLY evento.denuncia_evento
    ADD CONSTRAINT id_usuario FOREIGN KEY (id_usuario) REFERENCES usuario.usuario(id);
 D   ALTER TABLE ONLY evento.denuncia_evento DROP CONSTRAINT id_usuario;
       evento       postgres    false    2881    201    215            V           2606    96110    usuario rol_id    FK CONSTRAINT     l   ALTER TABLE ONLY usuario.usuario
    ADD CONSTRAINT rol_id FOREIGN KEY (id_rol) REFERENCES usuario.rol(id);
 9   ALTER TABLE ONLY usuario.usuario DROP CONSTRAINT rol_id;
       usuario       postgres    false    203    201    2883            �   �   x�-�KR�  �5��/�H0;kjv~.�MHm>��;Vy��$˩���Vٕ�"|�rq@�\s�#�O����f��^2�n�lȔP�C�,H;k5k�+;�+v&�5��k+��I8=���O����Ŗ�o�|P�\�o���Y�� ����T����*����ԝ4 լ�Y�~2�'��s���D/      �   �   x�m��� Eg�
@��%�ƱN�K(Q����k:�&mr�{���.�����`'�	���X5�0\1�Q*I����Ӕ|ү��#�P�����]��4���5҆T}�~��jmq>��T��v�ʶ~Z��6潌#������`ą5RT��(�o-�F      �   �   x����m� @�5T���cbSK6�{�6��IW�!��(�HY��{���r�9�4�|���TCz��)~<C��ާ�Xw;��1�1Bhɴ�wý�*�+���o�Ã���>�N����|�M�F�f�KڨV\.��?�~	Bj&4bǱ�"�J�����e+���f��ݔ�*9 �&��@ʡl�R��WRnc�iթpi'[��\������*��C7�0�H$}�(�_�h      �   D   x�3�tL.�,���".#N�<�Ңb��1�[f^bNfUb
LȄӱ 'I���91/95!���� ��      �   �  x���͎�0���)�A���]E�,*u�]��l�Il3���M���d&�HSU�|t�织5ڿ*�<��f�y�u��l\La���#���X�(�!�!�>�,�ʩx8\����Q9�qD��C��t���N����c�ip/�Tb*Rg���)ڎ�I>��;�B4tx7��8�Z(�7�[���Skd���}�o��+V=J�i��4�
��� bab3"��������,�� ���4��ט��Y���J���	�1�%�mQ�A��T�`�����sp��ܐ�D<@ �4�^�K�������-Du��KK��p�It���^�����P�H�0YR�����dF�è� �z�v%��n�2�w9��P�9F6���ٸ;]�����x:�Zq1m��K�i��!�R�5��G;ǆ9�
9^��i[ʢ(� ǲ�,      �   �   x�U�AR�0@�u�s����i�9�H2IȠ@9szэz�W�0X��w����%�K=�B%i�Wm�/h�1�M�V�X	�Ӗ��>梳�K~�(��ϊ��k�ڗVY �N���y��v;��}�0y��|ѝ��'�ٲQ��,0B��C��{�~J��:U��k����A��(�� �2>�      �      x������ � �      �   �  x���I��&��z��� i�tg��q��		5�^�[}%\��~/�	�<	$4������RHXFh�����	%��N�� ~���h��O�����w�iے)�����6��?4J� �%a	��7P0uY�m0Ը��P�PF<�ȳ����������۝3㼮�C��@�"��
^$嵝[u�-Tt_މ���.�\CIJ8�~D�f�?kA?���	g�L�~gn�fKv������X(%B>���Q.��ۆrj߹�h_�s�r��U�}M��YZ��?�W��>D���(��W�{��L��s![�M�ۺ���m9{�r��?�P�r�h.�_،s3�SE0��>��ݪ��Ԯ�u��M�^[��,�YN����;& ������^F��5�Cx�s�s��+��A^�(K`�
6~�U���P;M�p�mjJ�4��+�igi.8��n�H�7Xz�̥���n���ݭ@C}g���n�hS�ˁhʋ����K/��o�>�}(K�Br*��ZV�9��dlWm��}�S#���b���

x2+��Z���k�sܮv��@�Tz�
�Έ��2�%���К5텞#ܻeg9j������"-�I�	j_��i��^ݾp5>ƅ��h�����RH"�lGhV:^��XIWoK�j�r���_��2�h_g�Gۅ��ޖ�@9��a5R���g@�YBL��AW��������pu��RD���"x�D�J;���Z����T�os�k��4Rq*�k9k�i����U�i����d�k������R�S�JI�E�@��/�IH5hQ��u<W��6���i��|�D�"^X�{���v�v�n˗���W�� �G�z-��6Ѝ���E��zݔ�|'����ʒ�?����8�ɪ��aۛג�4|>��=P�%�c1l4��L&�3���\o	�='V�+B���R�hIJr<�"��5��i~�~w�HWިk��K�x(X����v�Ǹ�vkmw�IT��h��(���)��G.ba*ݹ��}�g,���S��)���|�#>\��Hޫ&d���^nZ�i�n'�j����R����X�j<��8o��u���j��Y���
�]��;���5�v�"�+�I���Cm��,Ek�|�% ����V�}/�>�S�n��6T+�S֗*�7WL�L@��%Ls3�c�gW�aR[��S���2�4.X���Q���.A{O�`Թ�4@����+R����)�ϣu4�Qw��+_��	�K�܏+E��T����Ҟ4��s���p�<NW*�ԁ�gh-2�]�(_�2�]��G��O��_T�g��-y[������<�Gm�{T��$P����V������ꎌ�=��٧�,�I$���录xo�9���(������t�U��
=����]pZ�Eb�<�Y��Pƍ��Yf{���,�W~�JA�qs�x��a�W�2�1�8D���,�Z�JBr�r�Ռ��N� Q�9��ӹ�j���*0fb��;Z`l�����j���y��v���Ғ>���%�=�iTb엶��ܭ7�naH�K��] A&y2���-_��G�6m[�[���}���/g��c�&�j#Թ����_&U��.ⵕ���_:P[4y߾zx_M�{H;��:��x��hma+���"%î$J���fz�+�~e�CT�Fk�SѶY�)є�ޕy��-o�640ex��ڢ~ B/L	B��0���U�Ua���UɃi�hmy�T*2lۃ���j�WiVw�E.��߄�-��婅�-E�� Kt阺���G�,8�|�n5R[�/���k�@]��WRs+(�5i�R�W����?i�^H� / Y�^6-��:�\ nN��'�"���J�����ᮖa��UY���^T���ΚY��տx�b�Α::���V8SuS�
��ᶲHm*ͼ�
�S4�Q��ЮجǞ���_hBee���\*�a(
��3���b_^N{�o0#@�0����.��JR�R���F��ݘ�{�5Yy�|����R���'c���j���mΙ|9���F����R���K�v���e�=mΚ�ӵ�
��Ej+PEx���R���z���ʑv��\{�C�:�hmy��)0��+a3��1[�s)F\�aO�Y��(����g��[7��y����k����=�--���M)-��'myzA��ξr�Mv�7�=j��<�?
bae6����L���>�ު���r�!KFK�S�oܙ�y;O�t�֛��������J@FK�~֒�!�!��m<4��>�ʹ;�
��S�L~~�	��      �   ;   x�3�-.M,��W��L�M*���".CNǔ�̼�⒢Ĕ�"�����L�d� $�S      �   _   x�Uɱ�0�Z�"�|�Y��B���u��7��)3`e�sm�H��߾�v��|��]�����JFx:��e|I�ǫ�U�{��ь�      �   �  x��RM��0<;��������9U�X�+�S��_I�&q����o'������b���o�a����>vK��}�L���8�4���C��ҥ�HM�k�K!�VK����L�`+�VZUV��Q�AT9'?���`�0��4��b��V�/q%��Ӽ���ѥq{}�u�^H���<v�L�,�y�tm��nz<S��cKetK���d���������h�8�9�d�=s̢�*H����AJЖ܃4��h�Q(40��\�m�\z�w0����u�t>�c�`���2�y��d�DMU�D����Ō�C�_�+�!;3u�<�Y���ٱ��	��ald��y�������FVHb�ǘ���4���~��O���f��y(y�^�O�m/׊w��+����l�������c�h+c5�%0]i,��(���<     