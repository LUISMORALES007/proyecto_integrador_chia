PGDMP                         x            infochia    11.1    11.5 �               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false                       1262    95851    infochia    DATABASE     �   CREATE DATABASE infochia WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Spain.1252' LC_CTYPE = 'Spanish_Spain.1252';
    DROP DATABASE infochia;
             postgres    false                        2615    95855    evento    SCHEMA        CREATE SCHEMA evento;
    DROP SCHEMA evento;
             postgres    false                        2615    104719    reporte    SCHEMA        CREATE SCHEMA reporte;
    DROP SCHEMA reporte;
             postgres    false            
            2615    95852    security    SCHEMA        CREATE SCHEMA security;
    DROP SCHEMA security;
             postgres    false                        2615    95854    usuario    SCHEMA        CREATE SCHEMA usuario;
    DROP SCHEMA usuario;
             postgres    false            +           1255    104714    f_actualizar_estado_por_fecha()    FUNCTION     �  CREATE FUNCTION evento.f_actualizar_estado_por_fecha() RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    DECLARE 
	
	datos_tabla CURSOR FOR select * from evento.evento ;
	fila_datos_tabla evento.evento%ROWTYPE;
    BEGIN
	 
	  open datos_tabla;
	    LOOP
		
		    FETCH datos_tabla into fila_datos_tabla;
			IF NOT FOUND THEN
			EXIT;
		    END IF;
		      RAISE NOTICE ' PROCESANDO  %', fila_datos_tabla;  
			  
			 
		          
				   UPDATE evento.evento SET


					id_estado= 3 ,
					ultima_modificacion=current_timestamp
                    
					where 
					 
					 id_estado!=5 
					 AND
					 fecha_fin  < current_date ;

		    
			  
        END LOOP;
		close datos_tabla;
    END
$$;
 6   DROP FUNCTION evento.f_actualizar_estado_por_fecha();
       evento       postgres    false    6            �            1259    96315    evento_id_seq    SEQUENCE     v   CREATE SEQUENCE evento.evento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE evento.evento_id_seq;
       evento       postgres    false    6            �            1259    96320    evento    TABLE     �  CREATE TABLE evento.evento (
    id bigint DEFAULT nextval('evento.evento_id_seq'::regclass) NOT NULL,
    nombre text,
    descripcion text,
    fecha_inicio date,
    fecha_fin date,
    hora time without time zone,
    imagen text,
    id_usuario bigint,
    id_categoria integer,
    id_estado integer,
    cantidad_interesados integer,
    cantidad_denuncias integer,
    session text,
    ultima_modificacion timestamp without time zone,
    lugar text
);
    DROP TABLE evento.evento;
       evento         postgres    false    219    6            �            1255    96397 '   f_buscar_existencia_imagen_evento(text)    FUNCTION     �   CREATE FUNCTION evento.f_buscar_existencia_imagen_evento(_url_imagen text) RETURNS SETOF evento.evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.evento
		WHERE
			imagen = _url_imagen;
	end
$$;
 J   DROP FUNCTION evento.f_buscar_existencia_imagen_evento(_url_imagen text);
       evento       postgres    false    220    6                       1255    104682 (   f_cancelar_eventos_usuario(bigint, text)    FUNCTION     +  CREATE FUNCTION evento.f_cancelar_eventos_usuario(_id_usuario bigint, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    UPDATE evento.evento SET

	 id_estado= 5,
	 session=_session,
	 ultima_modificacion=current_timestamp

	where 
	
	  id_usuario =_id_usuario;
 END
$$;
 T   DROP FUNCTION evento.f_cancelar_eventos_usuario(_id_usuario bigint, _session text);
       evento       postgres    false    6            �            1259    96863    mostrar_cantidad_denuncias_view    VIEW     h   CREATE VIEW evento.mostrar_cantidad_denuncias_view AS
 SELECT (0)::bigint AS cantidad_denuncias_evento;
 2   DROP VIEW evento.mostrar_cantidad_denuncias_view;
       evento       postgres    false    6                       1255    96867    f_contar_denuncias(bigint)    FUNCTION       CREATE FUNCTION evento.f_contar_denuncias(_id_evento bigint) RETURNS SETOF evento.mostrar_cantidad_denuncias_view
    LANGUAGE plpgsql
    AS $$
    BEGIN
     return query
	 select count(*)
	 
	 from
	 evento.denuncia_evento
	 
	 where
	 
		id_evento=_id_evento;
	 
 END
$$;
 <   DROP FUNCTION evento.f_contar_denuncias(_id_evento bigint);
       evento       postgres    false    6    227            �            1259    96839 !   mostrar_cantidad_interesados_view    VIEW     l   CREATE VIEW evento.mostrar_cantidad_interesados_view AS
 SELECT (0)::bigint AS cantidad_interesados_evento;
 4   DROP VIEW evento.mostrar_cantidad_interesados_view;
       evento       postgres    false    6                       1255    96851    f_contar_interesa(bigint)    FUNCTION     *  CREATE FUNCTION evento.f_contar_interesa(_id_evento bigint) RETURNS SETOF evento.mostrar_cantidad_interesados_view
    LANGUAGE plpgsql
    AS $$
    BEGIN
     return query
	 select count(*)
	 
	 from
	 evento.interes
	 
	 where
	 
	    habilitado = true
		AND
		id_evento=_id_evento;
	 
 END
$$;
 ;   DROP FUNCTION evento.f_contar_interesa(_id_evento bigint);
       evento       postgres    false    226    6                       1255    96868 0   f_editar_denuncias_evento(bigint, integer, text)    FUNCTION     k  CREATE FUNCTION evento.f_editar_denuncias_evento(_id_evento bigint, _cantidad_denuncias integer, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
	
	 UPDATE evento.evento SET

	    cantidad_denuncias=_cantidad_denuncias,
	    session=_session,
	    ultima_modificacion=current_timestamp
       
	where 
	
	  id =_id_evento;
   
 END
$$;
 o   DROP FUNCTION evento.f_editar_denuncias_evento(_id_evento bigint, _cantidad_denuncias integer, _session text);
       evento       postgres    false    6            �            1255    96526 $   f_editar_estado_evento(bigint, text)    FUNCTION       CREATE FUNCTION evento.f_editar_estado_evento(_id_evento bigint, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    UPDATE evento.evento SET

	 id_estado= 5,
	 session=_session,
	 ultima_modificacion=current_timestamp

	where 
	
	  id =_id_evento;
 END
$$;
 O   DROP FUNCTION evento.f_editar_estado_evento(_id_evento bigint, _session text);
       evento       postgres    false    6                       1255    96849 A   f_editar_estado_interesados_evento(bigint, bigint, boolean, text)    FUNCTION     �  CREATE FUNCTION evento.f_editar_estado_interesados_evento(_id_usuario bigint, _id_evento bigint, _habilitado boolean, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
	
	 UPDATE evento.interes SET

	    habilitado=_habilitado,
	    session=_session,
	    ultima_modificacion=current_timestamp
       
	where 
	
	  id_usuario =_id_usuario
	  AND
	  id_evento=_id_evento;
   
 END
$$;
 �   DROP FUNCTION evento.f_editar_estado_interesados_evento(_id_usuario bigint, _id_evento bigint, _habilitado boolean, _session text);
       evento       postgres    false    6                       1255    96807 k   f_editar_evento(bigint, text, text, date, date, time without time zone, text, integer, integer, text, text)    FUNCTION     �  CREATE FUNCTION evento.f_editar_evento(_id bigint, _nombre text, _descripcion text, _fecha_inicio date, _fecha_fin date, _hora time without time zone, _imagen text, _id_categoria integer, _id_estado integer, _session text, _lugar text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
	
	 UPDATE evento.evento SET

		nombre = _nombre,
		descripcion= _descripcion ,
		fecha_inicio =_fecha_inicio,
		fecha_fin = _fecha_fin ,
		hora = _hora ,
		imagen= _imagen ,
		id_categoria = _id_categoria,
		id_estado= _id_estado ,
	    session=_session,
	    ultima_modificacion=current_timestamp,
        lugar=_lugar
	where 
	
	  id =_id;
   
 END
$$;
 �   DROP FUNCTION evento.f_editar_evento(_id bigint, _nombre text, _descripcion text, _fecha_inicio date, _fecha_fin date, _hora time without time zone, _imagen text, _id_categoria integer, _id_estado integer, _session text, _lugar text);
       evento       postgres    false    6                       1255    96844 2   f_editar_interesados_evento(bigint, integer, text)    FUNCTION     s  CREATE FUNCTION evento.f_editar_interesados_evento(_id_evento bigint, _cantidad_interesados integer, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
	
	 UPDATE evento.evento SET

	    cantidad_interesados=_cantidad_interesados,
	    session=_session,
	    ultima_modificacion=current_timestamp
       
	where 
	
	  id =_id_evento;
   
 END
$$;
 s   DROP FUNCTION evento.f_editar_interesados_evento(_id_evento bigint, _cantidad_interesados integer, _session text);
       evento       postgres    false    6            
           1255    96555 1   f_insertar_cancelacion_evento(text, bigint, text)    FUNCTION     i  CREATE FUNCTION evento.f_insertar_cancelacion_evento(_razon text, _id_evento bigint, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    INSERT INTO evento.cancelar_evento

	(
		razon,
		id_evento,
	    session,
	    ultima_modificacion
	) 
	
	VALUES (
		_razon,
        _id_evento,
	    _session,
	    current_timestamp
	);
 END
$$;
 c   DROP FUNCTION evento.f_insertar_cancelacion_evento(_razon text, _id_evento bigint, _session text);
       evento       postgres    false    6            '           1255    96932     f_insertar_categoria(text, text)    FUNCTION     D  CREATE FUNCTION evento.f_insertar_categoria(_descripcion text, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
BEGIN
    INSERT INTO evento.categoria_evento

	(
	    descripcion,
	    session,
	    ultima_modificacion
		
	) 
	
	VALUES (
		_descripcion,
	    _session,
	    current_timestamp
		
	);
END

$$;
 M   DROP FUNCTION evento.f_insertar_categoria(_descripcion text, _session text);
       evento       postgres    false    6                       1255    96861 6   f_insertar_denuncia_evento(text, bigint, bigint, text)    FUNCTION     �  CREATE FUNCTION evento.f_insertar_denuncia_evento(_descripcion text, _id_evento bigint, _id_usuario bigint, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    INSERT INTO evento.denuncia_evento

	(
		descripcion,
		id_evento,
	    session,
		ultima_modificacion,
		id_usuario  
	) 
	
	VALUES (
		_descripcion,
        _id_evento,
		_session,
		 current_timestamp,
	    _id_usuario
	   
	);
 END
$$;
 z   DROP FUNCTION evento.f_insertar_denuncia_evento(_descripcion text, _id_evento bigint, _id_usuario bigint, _session text);
       evento       postgres    false    6                       1255    96598 d   f_insertar_evento(text, text, date, date, time without time zone, text, bigint, integer, text, text)    FUNCTION     �  CREATE FUNCTION evento.f_insertar_evento(_nombre text, _descripcion text, _fecha_inicio date, _fecha_fin date, _hora time without time zone, _imagen text, _id_usuario bigint, _id_categoria integer, _session text, _lugar text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    INSERT INTO evento.evento

	(
		nombre,
		descripcion ,
		fecha_inicio,
		fecha_fin ,
		hora ,
		imagen ,
		id_usuario ,
		id_categoria ,
		id_estado ,
		cantidad_interesados ,
		cantidad_denuncias,
	    session,
	    ultima_modificacion,
		lugar
	) 
	
	VALUES (
		_nombre,
        _descripcion ,
		_fecha_inicio,
		_fecha_fin ,
		_hora ,
		_imagen ,
		_id_usuario ,
		_id_categoria ,
		 1 ,
		0,
		0,
	    _session,
	    current_timestamp,
		_lugar
	);
 END
$$;
 �   DROP FUNCTION evento.f_insertar_evento(_nombre text, _descripcion text, _fecha_inicio date, _fecha_fin date, _hora time without time zone, _imagen text, _id_usuario bigint, _id_categoria integer, _session text, _lugar text);
       evento       postgres    false    6                       1255    96835 /   f_insertar_interes_evento(bigint, bigint, text)    FUNCTION     �  CREATE FUNCTION evento.f_insertar_interes_evento(_id_usuario bigint, _id_evento bigint, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    INSERT INTO evento.interes

	(
		id_usuario,
		id_evento,
		habilitado,
	    session,
	    ultima_modificacion
	) 
	
	VALUES (
		_id_usuario,
        _id_evento,
		 true,
	    _session,
	    current_timestamp
	);
 END
$$;
 f   DROP FUNCTION evento.f_insertar_interes_evento(_id_usuario bigint, _id_evento bigint, _session text);
       evento       postgres    false    6            &           1255    96926 *   f_modificar_categoria(integer, text, text)    FUNCTION     5  CREATE FUNCTION evento.f_modificar_categoria(_id integer, _descripcion text, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
BEGIN
    UPDATE evento.categoria_evento SET

	id=_id,
	descripcion=_descripcion,
	session=_session,
	ultima_modificacion=current_timestamp
	
	WHERE id =_id;
END

$$;
 [   DROP FUNCTION evento.f_modificar_categoria(_id integer, _descripcion text, _session text);
       evento       postgres    false    6            �            1259    96101    categoria_evento    TABLE     �   CREATE TABLE evento.categoria_evento (
    id integer NOT NULL,
    descripcion character varying(25),
    session text,
    ultima_modificacion timestamp without time zone
);
 $   DROP TABLE evento.categoria_evento;
       evento         postgres    false    6            �            1255    96881    f_mostrar_categorias()    FUNCTION     �   CREATE FUNCTION evento.f_mostrar_categorias() RETURNS SETOF evento.categoria_evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.categoria_evento
			
			order by id asc;
		
	end
$$;
 -   DROP FUNCTION evento.f_mostrar_categorias();
       evento       postgres    false    6    217            %           1255    96923 $   f_mostrar_categorias_administrador()    FUNCTION     �   CREATE FUNCTION evento.f_mostrar_categorias_administrador() RETURNS SETOF evento.categoria_evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.categoria_evento
		where
		   id!=1
			order by id asc;
		
	end
$$;
 ;   DROP FUNCTION evento.f_mostrar_categorias_administrador();
       evento       postgres    false    6    217            �            1259    96001    usuario    TABLE     <  CREATE TABLE usuario.usuario (
    id bigint NOT NULL,
    nombre character varying(20),
    apellido character varying(20),
    correo text,
    clave text,
    url_imagen text,
    estado integer,
    habilitado boolean,
    id_rol integer,
    session text,
    ultima_modificacion timestamp without time zone
);
    DROP TABLE usuario.usuario;
       usuario         postgres    false    8            �            1255    96900 !   f_mostrar_contacto_evento(bigint)    FUNCTION     �   CREATE FUNCTION evento.f_mostrar_contacto_evento(_id_usuario bigint) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			usuario.usuario
		WHERE
			id = _id_usuario;
	end
$$;
 D   DROP FUNCTION evento.f_mostrar_contacto_evento(_id_usuario bigint);
       evento       postgres    false    201    6            �            1259    96933    denuncias_eventos_view    VIEW     �  CREATE VIEW evento.denuncias_eventos_view AS
 SELECT (0)::bigint AS id,
    ''::text AS nombre,
    ''::character varying(25) AS descripcion_categoria,
    ''::character varying(20) AS descripcion_estado,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    0 AS cantidad_denuncias,
    ''::text AS imagen;
 )   DROP VIEW evento.denuncias_eventos_view;
       evento       postgres    false    6            )           1255    96961 )   f_mostrar_denuncias_filtro(text, integer)    FUNCTION     �  CREATE FUNCTION evento.f_mostrar_denuncias_filtro(_nombre_filtro text, _categoria_filtro integer) RETURNS SETOF evento.denuncias_eventos_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		
		select distinct 
		IE.id_evento,
		DE.nombre, 
		CA.descripcion,
		EE.descripcion,
		DE.fecha_inicio,
		DE.fecha_fin,
		DE.hora,
		DE.lugar,
		DE.cantidad_denuncias,
		DE.imagen

		from evento.denuncia_evento as IE
		inner join evento.evento as DE
		ON IE.id_evento = DE.id  
		inner join evento.categoria_evento as CA
		ON DE.id_categoria=CA.id
	    inner join evento.estado_evento as EE
		ON DE.id_estado = EE.id
		
		where
		
		(CASE _nombre_filtro 
				WHEN '' THEN 
					DE.nombre ilike '%' || _nombre_filtro  || '%' 
				ELSE 
					DE.nombre ilike '%' || _nombre_filtro || '%' 
				END)
		AND
			(CASE _categoria_filtro 
				WHEN -1 THEN 
					DE.id_categoria = CA.id
				ELSE 
					DE.id_categoria = _categoria_filtro
				END)	;
				
	
		 
		
	end

$$;
 a   DROP FUNCTION evento.f_mostrar_denuncias_filtro(_nombre_filtro text, _categoria_filtro integer);
       evento       postgres    false    230    6            �            1259    96942    detalle_denuncia_evento_view    VIEW     �   CREATE VIEW evento.detalle_denuncia_evento_view AS
 SELECT ''::text AS descripcion,
    ''::text AS nombre_completo,
    ''::text AS correo,
    ''::text AS imagen;
 /   DROP VIEW evento.detalle_denuncia_evento_view;
       evento       postgres    false    6                       1255    96946 )   f_mostrar_detalle_denuncia_evento(bigint)    FUNCTION       CREATE FUNCTION evento.f_mostrar_detalle_denuncia_evento(_id_evento bigint) RETURNS SETOF evento.detalle_denuncia_evento_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		
		select 
		
		ED.descripcion,
	    (UU.nombre || '  ' || UU.apellido),
        UU.correo,
		uu.url_imagen
		
		from evento.denuncia_evento as ED
		inner join usuario.usuario as UU
		ON ED.id_usuario = UU.id  
		inner join evento.evento as EE
		ON ED.id_evento = EE.id 
		
		where
		ED.id_evento=_id_evento;
	   
		
		 
		
	end

$$;
 K   DROP FUNCTION evento.f_mostrar_detalle_denuncia_evento(_id_evento bigint);
       evento       postgres    false    6    231            �            1259    96079    estado_evento    TABLE     �   CREATE TABLE evento.estado_evento (
    id integer NOT NULL,
    descripcion character varying(20),
    session text,
    ultima_modificacion timestamp without time zone
);
 !   DROP TABLE evento.estado_evento;
       evento         postgres    false    6            �            1255    96494    f_mostrar_estados()    FUNCTION     �   CREATE FUNCTION evento.f_mostrar_estados() RETURNS SETOF evento.estado_evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.estado_evento
		WHERE
		    id!=5;
		
	end
$$;
 *   DROP FUNCTION evento.f_mostrar_estados();
       evento       postgres    false    6    213            �            1259    96537    cancelar_evento    TABLE     �   CREATE TABLE evento.cancelar_evento (
    id bigint NOT NULL,
    razon text,
    id_evento bigint,
    session text,
    ultima_modificacion timestamp without time zone
);
 #   DROP TABLE evento.cancelar_evento;
       evento         postgres    false    6                       1255    96824 "   f_mostrar_evento_cancelado(bigint)    FUNCTION     	  CREATE FUNCTION evento.f_mostrar_evento_cancelado(_id_evento bigint) RETURNS SETOF evento.cancelar_evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.cancelar_evento
			
	     where	
		    id_evento = _id_evento;
		
	end
$$;
 D   DROP FUNCTION evento.f_mostrar_evento_cancelado(_id_evento bigint);
       evento       postgres    false    223    6                       1255    96392    f_mostrar_evento_id(bigint)    FUNCTION     �   CREATE FUNCTION evento.f_mostrar_evento_id(_id_evento bigint) RETURNS SETOF evento.evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.evento
		WHERE
		    id=_id_evento;
		
	end
$$;
 =   DROP FUNCTION evento.f_mostrar_evento_id(_id_evento bigint);
       evento       postgres    false    220    6            �            1259    96808    mostrar_eventos_filtro_view    VIEW       CREATE VIEW evento.mostrar_eventos_filtro_view AS
 SELECT (0)::bigint AS id,
    ''::text AS nombre,
    ''::text AS descripcion,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    ''::text AS imagen,
    (0)::bigint AS id_usuario,
    0 AS id_categoria,
    ''::character varying(25) AS descripcion_categoria,
    0 AS id_estado,
    ''::character varying(20) AS descripcion_estado,
    0 AS cantidad_interesados;
 .   DROP VIEW evento.mostrar_eventos_filtro_view;
       evento       postgres    false    6                       1255    96812 -   f_mostrar_eventos_filtro(text, integer, date)    FUNCTION     �  CREATE FUNCTION evento.f_mostrar_eventos_filtro(_nombre_filtro text, _categoria_filtro integer, _fecha_inicio_filtro date) RETURNS SETOF evento.mostrar_eventos_filtro_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		SELECT
			evento.evento.id,
			evento.evento.nombre,
			evento.evento.descripcion,
			evento.evento.fecha_inicio,
			evento.evento.fecha_fin,
			evento.evento.hora,
			evento.evento.lugar,
			evento.evento.imagen,
			evento.evento.id_usuario,
			evento.evento.id_categoria,
			evento.categoria_evento.descripcion,
			evento.evento.id_estado,
			evento.estado_evento.descripcion,
			evento.evento.cantidad_interesados
			
			
		FROM
			evento.evento,
			evento.categoria_evento,
			evento.estado_evento
		WHERE
			
			evento.evento.id_categoria = evento.categoria_evento.id
			AND
			evento.evento.id_estado = evento.estado_evento.id
            AND
			(CASE _nombre_filtro 
				WHEN '' THEN 
					evento.evento.nombre ilike '%' || _nombre_filtro || '%' 
				ELSE 
					evento.evento.nombre ilike '%' || _nombre_filtro || '%' 
				END)
			AND
			(CASE _categoria_filtro 
				WHEN -1 THEN 
					evento.evento.id_categoria = evento.categoria_evento.id
				ELSE 
					evento.evento.id_categoria = _categoria_filtro
				END)
			AND
			(CASE _fecha_inicio_filtro 
				WHEN '2019-03-31' THEN 
			 	 evento.evento.id_categoria = evento.categoria_evento.id
				ELSE 
					evento.evento.fecha_inicio  = _fecha_inicio_filtro 
				END);
			
			
	end
$$;
 z   DROP FUNCTION evento.f_mostrar_eventos_filtro(_nombre_filtro text, _categoria_filtro integer, _fecha_inicio_filtro date);
       evento       postgres    false    225    6            �            1259    96377    mostrar_mis_eventos_view    VIEW     �  CREATE VIEW evento.mostrar_mis_eventos_view AS
 SELECT (0)::bigint AS id,
    ''::text AS nombre,
    ''::text AS descripcion,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS imagen,
    (0)::bigint AS id_usuario,
    0 AS id_categoria,
    ''::character varying(25) AS descripcion_categoria,
    0 AS id_estado,
    ''::character varying(20) AS descripcion_estado,
    0 AS cantidad_interesados;
 +   DROP VIEW evento.mostrar_mis_eventos_view;
       evento       postgres    false    6                       1255    96561    f_mostrar_eventos_finalizados()    FUNCTION     #  CREATE FUNCTION evento.f_mostrar_eventos_finalizados() RETURNS SETOF evento.mostrar_mis_eventos_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		SELECT
			evento.evento.id,
			evento.evento.nombre,
			evento.evento.descripcion,
			evento.evento.fecha_inicio,
			evento.evento.fecha_fin,
			evento.evento.hora,
			evento.evento.imagen,
			evento.evento.id_usuario,
			evento.evento.id_categoria,
			evento.categoria_evento.descripcion,
			evento.evento.id_estado,
			evento.estado_evento.descripcion,
			evento.evento.cantidad_interesados
			
			
		FROM
			evento.evento,
			evento.categoria_evento,
			evento.estado_evento
		WHERE
			
			evento.evento.id_categoria = evento.categoria_evento.id
			AND
			evento.evento.id_estado = evento.estado_evento.id
		    AND
			id_estado=3;
	end

$$;
 6   DROP FUNCTION evento.f_mostrar_eventos_finalizados();
       evento       postgres    false    221    6            �            1259    96891    me_interesa_eventos_view    VIEW     �  CREATE VIEW evento.me_interesa_eventos_view AS
 SELECT (0)::bigint AS id,
    ''::text AS nombre,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    ''::text AS imagen,
    ''::character varying(25) AS descripcion_categoria,
    ''::character varying(20) AS descripcion_estado,
    0 AS cantidad_interesados;
 +   DROP VIEW evento.me_interesa_eventos_view;
       evento       postgres    false    6            "           1255    96895 %   f_mostrar_me_interesa_eventos(bigint)    FUNCTION     �  CREATE FUNCTION evento.f_mostrar_me_interesa_eventos(_id_usuario bigint) RETURNS SETOF evento.me_interesa_eventos_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		select 
		
		evento.evento.id,
		evento.evento.nombre,
		evento.evento.fecha_inicio,
		evento.evento.fecha_fin,
		evento.evento.hora,
		evento.evento.lugar,
		evento.evento.imagen,
		evento.categoria_evento.descripcion,
		evento.estado_evento.descripcion,
		evento.evento.cantidad_interesados
		
		from evento.evento
		   
		
		   
		inner join evento.interes on evento.interes.id_evento =evento.evento.id and evento.interes.habilitado = true and evento.interes.id_usuario =_id_usuario
		inner join evento.categoria_evento on evento.evento.id_categoria = evento.categoria_evento.id 
		inner join evento.estado_evento on evento.evento.id_estado = evento.estado_evento.id
		
		where
		     evento.evento.id_categoria=evento.categoria_evento.id;
	end

$$;
 H   DROP FUNCTION evento.f_mostrar_me_interesa_eventos(_id_usuario bigint);
       evento       postgres    false    228    6            �            1259    96599    mis_eventos_view    VIEW     �  CREATE VIEW evento.mis_eventos_view AS
 SELECT (0)::bigint AS id,
    ''::text AS nombre,
    ''::text AS descripcion,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    ''::text AS imagen,
    (0)::bigint AS id_usuario,
    0 AS id_categoria,
    ''::character varying(25) AS descripcion_categoria,
    0 AS id_estado,
    ''::character varying(20) AS descripcion_estado,
    0 AS cantidad_interesados;
 #   DROP VIEW evento.mis_eventos_view;
       evento       postgres    false    6                        1255    96876    f_mostrar_mis_eventos(bigint)    FUNCTION     �  CREATE FUNCTION evento.f_mostrar_mis_eventos(_id_usuario bigint) RETURNS SETOF evento.mis_eventos_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		SELECT
			evento.evento.id,
			evento.evento.nombre,
			evento.evento.descripcion,
			evento.evento.fecha_inicio,
			evento.evento.fecha_fin,
			evento.evento.hora,
			evento.evento.lugar,
			evento.evento.imagen,
			evento.evento.id_usuario,
			evento.evento.id_categoria,
			evento.categoria_evento.descripcion,
			evento.evento.id_estado,
			evento.estado_evento.descripcion,
			evento.evento.cantidad_interesados
			
			
		FROM
			evento.evento,
			evento.categoria_evento,
			evento.estado_evento
		WHERE
			
			evento.evento.id_categoria = evento.categoria_evento.id
			AND
			evento.evento.id_estado = evento.estado_evento.id
			AND
			evento.evento.id_usuario = _id_usuario
                                                                AND
			evento.evento.id_estado != 5;
	end
$$;
 @   DROP FUNCTION evento.f_mostrar_mis_eventos(_id_usuario bigint);
       evento       postgres    false    224    6            �            1259    96947    usuario_denuncia_evento_view    VIEW     �   CREATE VIEW evento.usuario_denuncia_evento_view AS
 SELECT ''::text AS nombre_completo,
    ''::text AS correo,
    ''::text AS imagen;
 /   DROP VIEW evento.usuario_denuncia_evento_view;
       evento       postgres    false    6                       1255    96951 )   f_mostrar_usuario_denuncia_evento(bigint)    FUNCTION     �  CREATE FUNCTION evento.f_mostrar_usuario_denuncia_evento(_id_evento bigint) RETURNS SETOF evento.usuario_denuncia_evento_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		
		select 
		
		(UU.nombre || '  ' || UU.apellido),
		 UU.correo,
		 UU.url_imagen
		
		from evento.evento as EE
		inner join usuario.usuario as UU
		ON EE.id_usuario =UU.id  
		
		where
		EE.id = _id_evento;
	   
		
		 
		
	end

$$;
 K   DROP FUNCTION evento.f_mostrar_usuario_denuncia_evento(_id_evento bigint);
       evento       postgres    false    232    6            �            1259    96090    denuncia_evento    TABLE     �   CREATE TABLE evento.denuncia_evento (
    id integer NOT NULL,
    descripcion text,
    id_evento bigint,
    session text,
    ultima_modificacion timestamp without time zone,
    id_usuario bigint
);
 #   DROP TABLE evento.denuncia_evento;
       evento         postgres    false    6                       1255    96862 *   f_validar_denuncia_usuario(bigint, bigint)    FUNCTION     C  CREATE FUNCTION evento.f_validar_denuncia_usuario(_id_usuario bigint, _id_evento bigint) RETURNS SETOF evento.denuncia_evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.denuncia_evento
			
	     where	
		    id_usuario = _id_usuario
			AND
		    id_evento = _id_evento;
		
	end
$$;
 X   DROP FUNCTION evento.f_validar_denuncia_usuario(_id_usuario bigint, _id_evento bigint);
       evento       postgres    false    6    215            �            1259    104687    valida_razon_eventos_view    VIEW     �   CREATE VIEW evento.valida_razon_eventos_view AS
 SELECT ''::text AS razon,
    (0)::bigint AS id_evento,
    (0)::bigint AS id_usuario;
 ,   DROP VIEW evento.valida_razon_eventos_view;
       evento       postgres    false    6                       1255    104691 "   f_validar_existencia_razon(bigint)    FUNCTION     q  CREATE FUNCTION evento.f_validar_existencia_razon(_id_evento bigint) RETURNS SETOF evento.valida_razon_eventos_view
    LANGUAGE plpgsql
    AS $$
    BEGIN
	return query
	   
    Select 
	EC.razon,
	EC.id_evento,
	EE.id_usuario
	from
	evento.cancelar_evento  as EC
	inner join evento.evento as EE
	ON EC.id_evento = EE.id

	where 
	
	  id_evento =_id_evento;
 END
$$;
 D   DROP FUNCTION evento.f_validar_existencia_razon(_id_evento bigint);
       evento       postgres    false    6    233            �            1259    96056    interes    TABLE     �   CREATE TABLE evento.interes (
    id bigint NOT NULL,
    id_usuario bigint,
    id_evento bigint,
    habilitado boolean,
    session text,
    ultima_modificacion timestamp without time zone
);
    DROP TABLE evento.interes;
       evento         postgres    false    6                       1255    96829 )   f_validar_interes_usuario(bigint, bigint)    FUNCTION     2  CREATE FUNCTION evento.f_validar_interes_usuario(_id_usuario bigint, _id_evento bigint) RETURNS SETOF evento.interes
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.interes
			
	     where	
		    id_usuario = _id_usuario
			AND
		    id_evento = _id_evento;
		
	end
$$;
 W   DROP FUNCTION evento.f_validar_interes_usuario(_id_usuario bigint, _id_evento bigint);
       evento       postgres    false    211    6            �            1259    104737    mostrar_eventos_activos_view    VIEW     E  CREATE VIEW reporte.mostrar_eventos_activos_view AS
 SELECT ''::text AS nombre,
    '2019-03-31'::date AS fecha_inicio,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    ''::text AS imagen,
    0 AS id_categoria,
    ''::character varying(25) AS descripcion_categoria,
    0 AS cantidad_interesados;
 0   DROP VIEW reporte.mostrar_eventos_activos_view;
       reporte       postgres    false    11            .           1255    104741    f_mostrar_eventos_activos(date)    FUNCTION     <  CREATE FUNCTION reporte.f_mostrar_eventos_activos(_fecha_inicio_filtro date) RETURNS SETOF reporte.mostrar_eventos_activos_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		SELECT
			evento.evento.nombre,
			evento.evento.fecha_inicio,
			evento.evento.hora,
			evento.evento.lugar,
			evento.evento.imagen,
			evento.evento.id_categoria,
			evento.categoria_evento.descripcion,
			evento.evento.cantidad_interesados
			
			
		FROM
			evento.evento,
			evento.categoria_evento
	        
		WHERE
			
			evento.evento.id_categoria = evento.categoria_evento.id
			AND
			evento.evento.id_estado=1
			AND
			(CASE _fecha_inicio_filtro 
				WHEN '2019-03-31' THEN 
			 	 evento.evento.id_categoria = evento.categoria_evento.id
				ELSE 
					evento.evento.fecha_inicio  = _fecha_inicio_filtro 
				END);
			
			
	end
$$;
 L   DROP FUNCTION reporte.f_mostrar_eventos_activos(_fecha_inicio_filtro date);
       reporte       postgres    false    11    236            �            1259    104726    mostrar_eventos_cancelados_view    VIEW     \  CREATE VIEW reporte.mostrar_eventos_cancelados_view AS
 SELECT ''::text AS nombre,
    ''::text AS descripcion,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    ''::text AS imagen,
    (0)::bigint AS id_usuario,
    ''::text AS nombre_completo;
 3   DROP VIEW reporte.mostrar_eventos_cancelados_view;
       reporte       postgres    false    11            ,           1255    104730    f_mostrar_eventos_cancelados()    FUNCTION       CREATE FUNCTION reporte.f_mostrar_eventos_cancelados() RETURNS SETOF reporte.mostrar_eventos_cancelados_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		SELECT
		
			evento.evento.nombre,
			evento.evento.descripcion,
			evento.evento.fecha_inicio,
			evento.evento.fecha_fin,
			evento.evento.hora,
			evento.evento.lugar,
			evento.evento.imagen,
			evento.evento.id_usuario,
			usuario.usuario.nombre ||usuario.usuario.apellido
			
			
			
		FROM
			evento.evento,
			evento.categoria_evento,
			evento.estado_evento,
			usuario.usuario
		WHERE
			evento.evento.id_usuario=usuario.usuario.id
			AND
			evento.evento.id_categoria = evento.categoria_evento.id
			AND
			evento.evento.id_estado = evento.estado_evento.id
			AND
			evento.evento.id_estado=5;
            
			
	end
$$;
 6   DROP FUNCTION reporte.f_mostrar_eventos_cancelados();
       reporte       postgres    false    234    11            �            1259    104731     mostrar_eventos_denunciados_view    VIEW     r  CREATE VIEW reporte.mostrar_eventos_denunciados_view AS
 SELECT ''::text AS nombre,
    ''::character varying(25) AS categoria,
    ''::character varying(20) AS estado,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    0 AS cantidad_denuncias,
    ''::text AS imagen;
 4   DROP VIEW reporte.mostrar_eventos_denunciados_view;
       reporte       postgres    false    11            -           1255    104736    f_mostrar_eventos_denunciados()    FUNCTION     E  CREATE FUNCTION reporte.f_mostrar_eventos_denunciados() RETURNS SETOF reporte.mostrar_eventos_denunciados_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		select distinct 
		DE.nombre, 
		CA.descripcion,
		EE.descripcion,
		DE.fecha_inicio,
		DE.fecha_fin,
		DE.hora,
		DE.lugar,
		DE.cantidad_denuncias,
		DE.imagen

		from evento.denuncia_evento as IE
		inner join evento.evento as DE
		ON IE.id_evento = DE.id  
		inner join evento.categoria_evento as CA
		ON DE.id_categoria=CA.id
	    inner join evento.estado_evento as EE
		ON DE.id_estado = EE.id;
			
	end
$$;
 7   DROP FUNCTION reporte.f_mostrar_eventos_denunciados();
       reporte       postgres    false    235    11                       1255    96190 #   f_cerrar_sesion_autentication(text)    FUNCTION     �   CREATE FUNCTION security.f_cerrar_sesion_autentication(_session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
	
	BEGIN
		UPDATE
			security.autentication
		SET
			fecha_fin = current_timestamp
		WHERE
			session = _session;
			
	END

$$;
 E   DROP FUNCTION security.f_cerrar_sesion_autentication(_session text);
       security       postgres    false    10                       1255    96178 R   f_guardar_sesion_autentication(bigint, character varying, character varying, text)    FUNCTION     �  CREATE FUNCTION security.f_guardar_sesion_autentication(_id bigint, _ip character varying, _mac character varying, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
	
	BEGIN
		INSERT INTO security.autentication
		(
			codigo_usuario,
			ip,
			mac,
			fecha_inicio,
			session
		)
	VALUES 
		(
			_id,
			_ip,
			_mac,
			current_timestamp,
			_session
		);

		
		
	END

$$;
 �   DROP FUNCTION security.f_guardar_sesion_autentication(_id bigint, _ip character varying, _mac character varying, _session text);
       security       postgres    false    10            !           1255    104798    f_log_auditoria()    FUNCTION     �  CREATE FUNCTION security.f_log_auditoria() RETURNS trigger
    LANGUAGE plpgsql
    AS $$	 DECLARE
		_pk TEXT :='';		-- Representa la llave primaria de la tabla que esta siedno modificada.
		_sql TEXT;		-- Variable para la creacion del procedured.
		_column_guia RECORD; 	-- Variable para el FOR guarda los nombre de las columnas.
		_column_key RECORD; 	-- Variable para el FOR guarda los PK de las columnas.
		_session TEXT;	-- Almacena el usuario que genera el cambio.
		_user_db TEXT;		-- Almacena el usuario de bd que genera la transaccion.
		_control INT;		-- Variabel de control par alas llaves primarias.
		_count_key INT = 0;	-- Cantidad de columnas pertenecientes al PK.
		_sql_insert TEXT;	-- Variable para la construcción del insert del json de forma dinamica.
		_sql_delete TEXT;	-- Variable para la construcción del delete del json de forma dinamica.
		_sql_update TEXT;	-- Variable para la construcción del update del json de forma dinamica.
		_new_data RECORD; 	-- Fila que representa los campos nuevos del registro.
		_old_data RECORD;	-- Fila que representa los campos viejos del registro.

	BEGIN

			-- Se genera la evaluacion para determianr el tipo de accion sobre la tabla
		 IF (TG_OP = 'INSERT') THEN
			_new_data := NEW;
			_old_data := NEW;
		ELSEIF (TG_OP = 'UPDATE') THEN
			_new_data := NEW;
			_old_data := OLD;
		ELSE
			_new_data := OLD;
			_old_data := OLD;
		END IF;

		-- Se genera la evaluacion para determianr el tipo de accion sobre la tabla
		IF ((SELECT COUNT(*) FROM information_schema.columns WHERE table_schema = TG_TABLE_SCHEMA AND table_name = TG_TABLE_NAME AND column_name = 'codigo' ) > 0) THEN
			_pk := _new_data.codigo;
		ELSE
			_pk := '-1';
		END IF;

		-- Se valida que exista el campo modified_by
		IF ((SELECT COUNT(*) FROM information_schema.columns WHERE table_schema = TG_TABLE_SCHEMA AND table_name = TG_TABLE_NAME AND column_name = 'session') > 0) THEN
			_session := _new_data.session;
		ELSE
			_session := '';
		END IF;

		-- Se guarda el susuario de bd que genera la transaccion
		_user_db := (SELECT CURRENT_USER);

		-- Se evalua que exista el procedimeinto adecuado
		IF (SELECT COUNT(*) FROM security.function_db_view acfdv WHERE acfdv.b_function = 'field_audit' AND acfdv.b_type_parameters = TG_TABLE_SCHEMA || '.'|| TG_TABLE_NAME || ', '|| TG_TABLE_SCHEMA || '.'|| TG_TABLE_NAME || ', character varying, character varying, character varying, text, character varying, text, text') > 0
			THEN
				-- Se realiza la invocación del procedured generado dinamivamente
				PERFORM security.field_audit(_new_data, _old_data, TG_OP, _session, _user_db , _pk, ''::text);
		ELSE
			-- Se empieza la construcción del Procedured generico
			_sql := 'CREATE OR REPLACE FUNCTION security.field_audit( _data_new '|| TG_TABLE_SCHEMA || '.'|| TG_TABLE_NAME || ', _data_old '|| TG_TABLE_SCHEMA || '.'|| TG_TABLE_NAME || ', _accion character varying, _session text, _user_db character varying, _table_pk text, _init text)'
			|| ' RETURNS TEXT AS ''
'
			|| '
'
	|| '	DECLARE
'
	|| '		_column_data TEXT;
	 	_datos jsonb;
	 	
'
	|| '	BEGIN
			_datos = ''''{}'''';
';
			-- Se evalua si hay que actualizar la pk del registro de auditoria.
			IF _pk = '-1'
				THEN
					_sql := _sql
					|| '
		_column_data := ';

					-- Se genera el update con la clave pk de la tabla
					SELECT
						COUNT(isk.column_name)
					INTO
						_control
					FROM
						information_schema.table_constraints istc JOIN information_schema.key_column_usage isk ON isk.constraint_name = istc.constraint_name
					WHERE
						istc.table_schema = TG_TABLE_SCHEMA
					 AND	istc.table_name = TG_TABLE_NAME
					 AND	istc.constraint_type ilike '%primary%';

					-- Se agregan las columnas que componen la pk de la tabla.
					FOR _column_key IN SELECT
							isk.column_name
						FROM
							information_schema.table_constraints istc JOIN information_schema.key_column_usage isk ON isk.constraint_name = istc.constraint_name
						WHERE
							istc.table_schema = TG_TABLE_SCHEMA
						 AND	istc.table_name = TG_TABLE_NAME
						 AND	istc.constraint_type ilike '%primary%'
						ORDER BY 
							isk.ordinal_position  LOOP

						_sql := _sql || ' _data_new.' || _column_key.column_name;
						
						_count_key := _count_key + 1 ;
						
						IF _count_key < _control THEN
							_sql :=	_sql || ' || ' || ''''',''''' || ' ||';
						END IF;
					END LOOP;
				_sql := _sql || ';';
			END IF;

			_sql_insert:='
		IF _accion = ''''INSERT''''
			THEN
				';
			_sql_delete:='
		ELSEIF _accion = ''''DELETE''''
			THEN
				';
			_sql_update:='
		ELSE
			';

			-- Se genera el ciclo de agregado de columnas para el nuevo procedured
			FOR _column_guia IN SELECT column_name, data_type FROM information_schema.columns WHERE table_schema = TG_TABLE_SCHEMA AND table_name = TG_TABLE_NAME
				LOOP
						
					_sql_insert:= _sql_insert || '_datos := _datos || json_build_object('''''
					|| _column_guia.column_name
					|| '_nuevo'
					|| ''''', '
					|| '_data_new.'
					|| _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea', 'USER-DEFINED') THEN 
						_sql_insert:= _sql_insert
						||'::text';
					END IF;

					_sql_insert:= _sql_insert || ')::jsonb;
				';

					_sql_delete := _sql_delete || '_datos := _datos || json_build_object('''''
					|| _column_guia.column_name
					|| '_anterior'
					|| ''''', '
					|| '_data_old.'
					|| _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea', 'USER-DEFINED') THEN 
						_sql_delete:= _sql_delete
						||'::text';
					END IF;

					_sql_delete:= _sql_delete || ')::jsonb;
				';

					_sql_update := _sql_update || 'IF _data_old.' || _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea','USER-DEFINED') THEN 
						_sql_update:= _sql_update
						||'::text';
					END IF;

					_sql_update:= _sql_update || ' <> _data_new.' || _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea','USER-DEFINED') THEN 
						_sql_update:= _sql_update
						||'::text';
					END IF;

					_sql_update:= _sql_update || '
				THEN _datos := _datos || json_build_object('''''
					|| _column_guia.column_name
					|| '_anterior'
					|| ''''', '
					|| '_data_old.'
					|| _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea','USER-DEFINED') THEN 
						_sql_update:= _sql_update
						||'::text';
					END IF;

					_sql_update:= _sql_update
					|| ', '''''
					|| _column_guia.column_name
					|| '_nuevo'
					|| ''''', _data_new.'
					|| _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea', 'USER-DEFINED') THEN 
						_sql_update:= _sql_update
						||'::text';
					END IF;

					_sql_update:= _sql_update
					|| ')::jsonb;
			END IF;
			';
			END LOOP;

			-- Se le agrega la parte final del procedured generico
			
			_sql:= _sql || _sql_insert || _sql_delete || _sql_update
			|| ' 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			''''' || TG_TABLE_SCHEMA || ''''',
			''''' || TG_TABLE_NAME || ''''',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;'''
|| '
LANGUAGE plpgsql;';

			-- Se genera la ejecución de _sql, es decir se crea el nuevo procedured de forma generica.
			EXECUTE _sql;

		-- Se realiza la invocación del procedured generado dinamivamente
			PERFORM security.field_audit(_new_data, _old_data, TG_OP::character varying, _session, _user_db, _pk, ''::text);

		END IF;

		RETURN NULL;

END;
$$;
 *   DROP FUNCTION security.f_log_auditoria();
       security       postgres    false    10            �            1259    96165    autenticate_view    VIEW     �   CREATE VIEW security.autenticate_view AS
 SELECT (0)::text AS correo_usuario,
    ''::character varying(20) AS nombre,
    ''::character varying(20) AS apellido,
    0 AS codigo_rol,
    ''::character varying(20) AS nombre_rol;
 %   DROP VIEW security.autenticate_view;
       security       postgres    false    10                       1255    96873 &   f_validacion_inicio_sesion(text, text)    FUNCTION     C  CREATE FUNCTION security.f_validacion_inicio_sesion(_correo text, _clave text) RETURNS SETOF security.autenticate_view
    LANGUAGE plpgsql
    AS $$

	BEGIN
		return query
		SELECT
			usuario.usuario.correo as correo_usuario,
			usuario.usuario.nombre as nombre,
			usuario.usuario.apellido as apellido,
			usuario.rol.id as codigo_rol,
			usuario.rol.descripcion as nombre_rol
			
		FROM
			usuario.usuario,
			usuario.rol
		WHERE
			
			usuario.usuario.correo = _correo
			AND
			usuario.usuario.clave = _clave
			AND
			usuario.usuario.id_rol = usuario.rol.id;
			
	end

$$;
 N   DROP FUNCTION security.f_validacion_inicio_sesion(_correo text, _clave text);
       security       postgres    false    10    218            (           1255    105118 s   field_audit(evento.cancelar_evento, evento.cancelar_evento, character varying, text, character varying, text, text)    FUNCTION     C
  CREATE FUNCTION security.field_audit(_data_new evento.cancelar_evento, _data_old evento.cancelar_evento, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_nuevo', _data_new.id)::jsonb;
				_datos := _datos || json_build_object('razon_nuevo', _data_new.razon)::jsonb;
				_datos := _datos || json_build_object('id_evento_nuevo', _data_new.id_evento)::jsonb;
				_datos := _datos || json_build_object('session_nuevo', _data_new.session)::jsonb;
				_datos := _datos || json_build_object('ultima_modificacion_nuevo', _data_new.ultima_modificacion)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_anterior', _data_old.id)::jsonb;
				_datos := _datos || json_build_object('razon_anterior', _data_old.razon)::jsonb;
				_datos := _datos || json_build_object('id_evento_anterior', _data_old.id_evento)::jsonb;
				_datos := _datos || json_build_object('session_anterior', _data_old.session)::jsonb;
				_datos := _datos || json_build_object('ultima_modificacion_anterior', _data_old.ultima_modificacion)::jsonb;
				
		ELSE
			IF _data_old.id <> _data_new.id
				THEN _datos := _datos || json_build_object('id_anterior', _data_old.id, 'id_nuevo', _data_new.id)::jsonb;
			END IF;
			IF _data_old.razon <> _data_new.razon
				THEN _datos := _datos || json_build_object('razon_anterior', _data_old.razon, 'razon_nuevo', _data_new.razon)::jsonb;
			END IF;
			IF _data_old.id_evento <> _data_new.id_evento
				THEN _datos := _datos || json_build_object('id_evento_anterior', _data_old.id_evento, 'id_evento_nuevo', _data_new.id_evento)::jsonb;
			END IF;
			IF _data_old.session <> _data_new.session
				THEN _datos := _datos || json_build_object('session_anterior', _data_old.session, 'session_nuevo', _data_new.session)::jsonb;
			END IF;
			IF _data_old.ultima_modificacion <> _data_new.ultima_modificacion
				THEN _datos := _datos || json_build_object('ultima_modificacion_anterior', _data_old.ultima_modificacion, 'ultima_modificacion_nuevo', _data_new.ultima_modificacion)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'evento',
			'cancelar_evento',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new evento.cancelar_evento, _data_old evento.cancelar_evento, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    223    10    223            /           1255    105119 u   field_audit(evento.categoria_evento, evento.categoria_evento, character varying, text, character varying, text, text)    FUNCTION     	  CREATE FUNCTION security.field_audit(_data_new evento.categoria_evento, _data_old evento.categoria_evento, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_nuevo', _data_new.id)::jsonb;
				_datos := _datos || json_build_object('descripcion_nuevo', _data_new.descripcion)::jsonb;
				_datos := _datos || json_build_object('session_nuevo', _data_new.session)::jsonb;
				_datos := _datos || json_build_object('ultima_modificacion_nuevo', _data_new.ultima_modificacion)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_anterior', _data_old.id)::jsonb;
				_datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion)::jsonb;
				_datos := _datos || json_build_object('session_anterior', _data_old.session)::jsonb;
				_datos := _datos || json_build_object('ultima_modificacion_anterior', _data_old.ultima_modificacion)::jsonb;
				
		ELSE
			IF _data_old.id <> _data_new.id
				THEN _datos := _datos || json_build_object('id_anterior', _data_old.id, 'id_nuevo', _data_new.id)::jsonb;
			END IF;
			IF _data_old.descripcion <> _data_new.descripcion
				THEN _datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion, 'descripcion_nuevo', _data_new.descripcion)::jsonb;
			END IF;
			IF _data_old.session <> _data_new.session
				THEN _datos := _datos || json_build_object('session_anterior', _data_old.session, 'session_nuevo', _data_new.session)::jsonb;
			END IF;
			IF _data_old.ultima_modificacion <> _data_new.ultima_modificacion
				THEN _datos := _datos || json_build_object('ultima_modificacion_anterior', _data_old.ultima_modificacion, 'ultima_modificacion_nuevo', _data_new.ultima_modificacion)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'evento',
			'categoria_evento',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new evento.categoria_evento, _data_old evento.categoria_evento, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    10    217    217            1           1255    104839 a   field_audit(evento.evento, evento.evento, character varying, text, character varying, text, text)    FUNCTION     �  CREATE FUNCTION security.field_audit(_data_new evento.evento, _data_old evento.evento, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_nuevo', _data_new.id)::jsonb;
				_datos := _datos || json_build_object('nombre_nuevo', _data_new.nombre)::jsonb;
				_datos := _datos || json_build_object('descripcion_nuevo', _data_new.descripcion)::jsonb;
				_datos := _datos || json_build_object('fecha_inicio_nuevo', _data_new.fecha_inicio)::jsonb;
				_datos := _datos || json_build_object('fecha_fin_nuevo', _data_new.fecha_fin)::jsonb;
				_datos := _datos || json_build_object('hora_nuevo', _data_new.hora)::jsonb;
				_datos := _datos || json_build_object('imagen_nuevo', _data_new.imagen)::jsonb;
				_datos := _datos || json_build_object('id_usuario_nuevo', _data_new.id_usuario)::jsonb;
				_datos := _datos || json_build_object('id_categoria_nuevo', _data_new.id_categoria)::jsonb;
				_datos := _datos || json_build_object('id_estado_nuevo', _data_new.id_estado)::jsonb;
				_datos := _datos || json_build_object('cantidad_interesados_nuevo', _data_new.cantidad_interesados)::jsonb;
				_datos := _datos || json_build_object('cantidad_denuncias_nuevo', _data_new.cantidad_denuncias)::jsonb;
				_datos := _datos || json_build_object('session_nuevo', _data_new.session)::jsonb;
				_datos := _datos || json_build_object('ultima_modificacion_nuevo', _data_new.ultima_modificacion)::jsonb;
				_datos := _datos || json_build_object('lugar_nuevo', _data_new.lugar)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_anterior', _data_old.id)::jsonb;
				_datos := _datos || json_build_object('nombre_anterior', _data_old.nombre)::jsonb;
				_datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion)::jsonb;
				_datos := _datos || json_build_object('fecha_inicio_anterior', _data_old.fecha_inicio)::jsonb;
				_datos := _datos || json_build_object('fecha_fin_anterior', _data_old.fecha_fin)::jsonb;
				_datos := _datos || json_build_object('hora_anterior', _data_old.hora)::jsonb;
				_datos := _datos || json_build_object('imagen_anterior', _data_old.imagen)::jsonb;
				_datos := _datos || json_build_object('id_usuario_anterior', _data_old.id_usuario)::jsonb;
				_datos := _datos || json_build_object('id_categoria_anterior', _data_old.id_categoria)::jsonb;
				_datos := _datos || json_build_object('id_estado_anterior', _data_old.id_estado)::jsonb;
				_datos := _datos || json_build_object('cantidad_interesados_anterior', _data_old.cantidad_interesados)::jsonb;
				_datos := _datos || json_build_object('cantidad_denuncias_anterior', _data_old.cantidad_denuncias)::jsonb;
				_datos := _datos || json_build_object('session_anterior', _data_old.session)::jsonb;
				_datos := _datos || json_build_object('ultima_modificacion_anterior', _data_old.ultima_modificacion)::jsonb;
				_datos := _datos || json_build_object('lugar_anterior', _data_old.lugar)::jsonb;
				
		ELSE
			IF _data_old.id <> _data_new.id
				THEN _datos := _datos || json_build_object('id_anterior', _data_old.id, 'id_nuevo', _data_new.id)::jsonb;
			END IF;
			IF _data_old.nombre <> _data_new.nombre
				THEN _datos := _datos || json_build_object('nombre_anterior', _data_old.nombre, 'nombre_nuevo', _data_new.nombre)::jsonb;
			END IF;
			IF _data_old.descripcion <> _data_new.descripcion
				THEN _datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion, 'descripcion_nuevo', _data_new.descripcion)::jsonb;
			END IF;
			IF _data_old.fecha_inicio <> _data_new.fecha_inicio
				THEN _datos := _datos || json_build_object('fecha_inicio_anterior', _data_old.fecha_inicio, 'fecha_inicio_nuevo', _data_new.fecha_inicio)::jsonb;
			END IF;
			IF _data_old.fecha_fin <> _data_new.fecha_fin
				THEN _datos := _datos || json_build_object('fecha_fin_anterior', _data_old.fecha_fin, 'fecha_fin_nuevo', _data_new.fecha_fin)::jsonb;
			END IF;
			IF _data_old.hora <> _data_new.hora
				THEN _datos := _datos || json_build_object('hora_anterior', _data_old.hora, 'hora_nuevo', _data_new.hora)::jsonb;
			END IF;
			IF _data_old.imagen <> _data_new.imagen
				THEN _datos := _datos || json_build_object('imagen_anterior', _data_old.imagen, 'imagen_nuevo', _data_new.imagen)::jsonb;
			END IF;
			IF _data_old.id_usuario <> _data_new.id_usuario
				THEN _datos := _datos || json_build_object('id_usuario_anterior', _data_old.id_usuario, 'id_usuario_nuevo', _data_new.id_usuario)::jsonb;
			END IF;
			IF _data_old.id_categoria <> _data_new.id_categoria
				THEN _datos := _datos || json_build_object('id_categoria_anterior', _data_old.id_categoria, 'id_categoria_nuevo', _data_new.id_categoria)::jsonb;
			END IF;
			IF _data_old.id_estado <> _data_new.id_estado
				THEN _datos := _datos || json_build_object('id_estado_anterior', _data_old.id_estado, 'id_estado_nuevo', _data_new.id_estado)::jsonb;
			END IF;
			IF _data_old.cantidad_interesados <> _data_new.cantidad_interesados
				THEN _datos := _datos || json_build_object('cantidad_interesados_anterior', _data_old.cantidad_interesados, 'cantidad_interesados_nuevo', _data_new.cantidad_interesados)::jsonb;
			END IF;
			IF _data_old.cantidad_denuncias <> _data_new.cantidad_denuncias
				THEN _datos := _datos || json_build_object('cantidad_denuncias_anterior', _data_old.cantidad_denuncias, 'cantidad_denuncias_nuevo', _data_new.cantidad_denuncias)::jsonb;
			END IF;
			IF _data_old.session <> _data_new.session
				THEN _datos := _datos || json_build_object('session_anterior', _data_old.session, 'session_nuevo', _data_new.session)::jsonb;
			END IF;
			IF _data_old.ultima_modificacion <> _data_new.ultima_modificacion
				THEN _datos := _datos || json_build_object('ultima_modificacion_anterior', _data_old.ultima_modificacion, 'ultima_modificacion_nuevo', _data_new.ultima_modificacion)::jsonb;
			END IF;
			IF _data_old.lugar <> _data_new.lugar
				THEN _datos := _datos || json_build_object('lugar_anterior', _data_old.lugar, 'lugar_nuevo', _data_new.lugar)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'evento',
			'evento',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new evento.evento, _data_old evento.evento, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    220    220    10            	           1255    105113 c   field_audit(evento.interes, evento.interes, character varying, text, character varying, text, text)    FUNCTION     �  CREATE FUNCTION security.field_audit(_data_new evento.interes, _data_old evento.interes, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_nuevo', _data_new.id)::jsonb;
				_datos := _datos || json_build_object('id_usuario_nuevo', _data_new.id_usuario)::jsonb;
				_datos := _datos || json_build_object('id_evento_nuevo', _data_new.id_evento)::jsonb;
				_datos := _datos || json_build_object('habilitado_nuevo', _data_new.habilitado)::jsonb;
				_datos := _datos || json_build_object('session_nuevo', _data_new.session)::jsonb;
				_datos := _datos || json_build_object('ultima_modificacion_nuevo', _data_new.ultima_modificacion)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_anterior', _data_old.id)::jsonb;
				_datos := _datos || json_build_object('id_usuario_anterior', _data_old.id_usuario)::jsonb;
				_datos := _datos || json_build_object('id_evento_anterior', _data_old.id_evento)::jsonb;
				_datos := _datos || json_build_object('habilitado_anterior', _data_old.habilitado)::jsonb;
				_datos := _datos || json_build_object('session_anterior', _data_old.session)::jsonb;
				_datos := _datos || json_build_object('ultima_modificacion_anterior', _data_old.ultima_modificacion)::jsonb;
				
		ELSE
			IF _data_old.id <> _data_new.id
				THEN _datos := _datos || json_build_object('id_anterior', _data_old.id, 'id_nuevo', _data_new.id)::jsonb;
			END IF;
			IF _data_old.id_usuario <> _data_new.id_usuario
				THEN _datos := _datos || json_build_object('id_usuario_anterior', _data_old.id_usuario, 'id_usuario_nuevo', _data_new.id_usuario)::jsonb;
			END IF;
			IF _data_old.id_evento <> _data_new.id_evento
				THEN _datos := _datos || json_build_object('id_evento_anterior', _data_old.id_evento, 'id_evento_nuevo', _data_new.id_evento)::jsonb;
			END IF;
			IF _data_old.habilitado <> _data_new.habilitado
				THEN _datos := _datos || json_build_object('habilitado_anterior', _data_old.habilitado, 'habilitado_nuevo', _data_new.habilitado)::jsonb;
			END IF;
			IF _data_old.session <> _data_new.session
				THEN _datos := _datos || json_build_object('session_anterior', _data_old.session, 'session_nuevo', _data_new.session)::jsonb;
			END IF;
			IF _data_old.ultima_modificacion <> _data_new.ultima_modificacion
				THEN _datos := _datos || json_build_object('ultima_modificacion_anterior', _data_old.ultima_modificacion, 'ultima_modificacion_nuevo', _data_new.ultima_modificacion)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'evento',
			'interes',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new evento.interes, _data_old evento.interes, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    211    10    211            �            1259    96023    token_recuperacion_usuario    TABLE     �   CREATE TABLE usuario.token_recuperacion_usuario (
    id integer NOT NULL,
    user_id bigint,
    token text,
    fecha_creado timestamp without time zone,
    fecha_vigencia timestamp without time zone
);
 /   DROP TABLE usuario.token_recuperacion_usuario;
       usuario         postgres    false    8            0           1255    105120 �   field_audit(usuario.token_recuperacion_usuario, usuario.token_recuperacion_usuario, character varying, text, character varying, text, text)    FUNCTION     S
  CREATE FUNCTION security.field_audit(_data_new usuario.token_recuperacion_usuario, _data_old usuario.token_recuperacion_usuario, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_nuevo', _data_new.id)::jsonb;
				_datos := _datos || json_build_object('user_id_nuevo', _data_new.user_id)::jsonb;
				_datos := _datos || json_build_object('token_nuevo', _data_new.token)::jsonb;
				_datos := _datos || json_build_object('fecha_creado_nuevo', _data_new.fecha_creado)::jsonb;
				_datos := _datos || json_build_object('fecha_vigencia_nuevo', _data_new.fecha_vigencia)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_anterior', _data_old.id)::jsonb;
				_datos := _datos || json_build_object('user_id_anterior', _data_old.user_id)::jsonb;
				_datos := _datos || json_build_object('token_anterior', _data_old.token)::jsonb;
				_datos := _datos || json_build_object('fecha_creado_anterior', _data_old.fecha_creado)::jsonb;
				_datos := _datos || json_build_object('fecha_vigencia_anterior', _data_old.fecha_vigencia)::jsonb;
				
		ELSE
			IF _data_old.id <> _data_new.id
				THEN _datos := _datos || json_build_object('id_anterior', _data_old.id, 'id_nuevo', _data_new.id)::jsonb;
			END IF;
			IF _data_old.user_id <> _data_new.user_id
				THEN _datos := _datos || json_build_object('user_id_anterior', _data_old.user_id, 'user_id_nuevo', _data_new.user_id)::jsonb;
			END IF;
			IF _data_old.token <> _data_new.token
				THEN _datos := _datos || json_build_object('token_anterior', _data_old.token, 'token_nuevo', _data_new.token)::jsonb;
			END IF;
			IF _data_old.fecha_creado <> _data_new.fecha_creado
				THEN _datos := _datos || json_build_object('fecha_creado_anterior', _data_old.fecha_creado, 'fecha_creado_nuevo', _data_new.fecha_creado)::jsonb;
			END IF;
			IF _data_old.fecha_vigencia <> _data_new.fecha_vigencia
				THEN _datos := _datos || json_build_object('fecha_vigencia_anterior', _data_old.fecha_vigencia, 'fecha_vigencia_nuevo', _data_new.fecha_vigencia)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'token_recuperacion_usuario',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new usuario.token_recuperacion_usuario, _data_old usuario.token_recuperacion_usuario, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    205    205    10            *           1255    104802 e   field_audit(usuario.usuario, usuario.usuario, character varying, text, character varying, text, text)    FUNCTION     �  CREATE FUNCTION security.field_audit(_data_new usuario.usuario, _data_old usuario.usuario, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_nuevo', _data_new.id)::jsonb;
				_datos := _datos || json_build_object('nombre_nuevo', _data_new.nombre)::jsonb;
				_datos := _datos || json_build_object('apellido_nuevo', _data_new.apellido)::jsonb;
				_datos := _datos || json_build_object('correo_nuevo', _data_new.correo)::jsonb;
				_datos := _datos || json_build_object('clave_nuevo', _data_new.clave)::jsonb;
				_datos := _datos || json_build_object('url_imagen_nuevo', _data_new.url_imagen)::jsonb;
				_datos := _datos || json_build_object('estado_nuevo', _data_new.estado)::jsonb;
				_datos := _datos || json_build_object('habilitado_nuevo', _data_new.habilitado)::jsonb;
				_datos := _datos || json_build_object('id_rol_nuevo', _data_new.id_rol)::jsonb;
				_datos := _datos || json_build_object('session_nuevo', _data_new.session)::jsonb;
				_datos := _datos || json_build_object('ultima_modificacion_nuevo', _data_new.ultima_modificacion)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_anterior', _data_old.id)::jsonb;
				_datos := _datos || json_build_object('nombre_anterior', _data_old.nombre)::jsonb;
				_datos := _datos || json_build_object('apellido_anterior', _data_old.apellido)::jsonb;
				_datos := _datos || json_build_object('correo_anterior', _data_old.correo)::jsonb;
				_datos := _datos || json_build_object('clave_anterior', _data_old.clave)::jsonb;
				_datos := _datos || json_build_object('url_imagen_anterior', _data_old.url_imagen)::jsonb;
				_datos := _datos || json_build_object('estado_anterior', _data_old.estado)::jsonb;
				_datos := _datos || json_build_object('habilitado_anterior', _data_old.habilitado)::jsonb;
				_datos := _datos || json_build_object('id_rol_anterior', _data_old.id_rol)::jsonb;
				_datos := _datos || json_build_object('session_anterior', _data_old.session)::jsonb;
				_datos := _datos || json_build_object('ultima_modificacion_anterior', _data_old.ultima_modificacion)::jsonb;
				
		ELSE
			IF _data_old.id <> _data_new.id
				THEN _datos := _datos || json_build_object('id_anterior', _data_old.id, 'id_nuevo', _data_new.id)::jsonb;
			END IF;
			IF _data_old.nombre <> _data_new.nombre
				THEN _datos := _datos || json_build_object('nombre_anterior', _data_old.nombre, 'nombre_nuevo', _data_new.nombre)::jsonb;
			END IF;
			IF _data_old.apellido <> _data_new.apellido
				THEN _datos := _datos || json_build_object('apellido_anterior', _data_old.apellido, 'apellido_nuevo', _data_new.apellido)::jsonb;
			END IF;
			IF _data_old.correo <> _data_new.correo
				THEN _datos := _datos || json_build_object('correo_anterior', _data_old.correo, 'correo_nuevo', _data_new.correo)::jsonb;
			END IF;
			IF _data_old.clave <> _data_new.clave
				THEN _datos := _datos || json_build_object('clave_anterior', _data_old.clave, 'clave_nuevo', _data_new.clave)::jsonb;
			END IF;
			IF _data_old.url_imagen <> _data_new.url_imagen
				THEN _datos := _datos || json_build_object('url_imagen_anterior', _data_old.url_imagen, 'url_imagen_nuevo', _data_new.url_imagen)::jsonb;
			END IF;
			IF _data_old.estado <> _data_new.estado
				THEN _datos := _datos || json_build_object('estado_anterior', _data_old.estado, 'estado_nuevo', _data_new.estado)::jsonb;
			END IF;
			IF _data_old.habilitado <> _data_new.habilitado
				THEN _datos := _datos || json_build_object('habilitado_anterior', _data_old.habilitado, 'habilitado_nuevo', _data_new.habilitado)::jsonb;
			END IF;
			IF _data_old.id_rol <> _data_new.id_rol
				THEN _datos := _datos || json_build_object('id_rol_anterior', _data_old.id_rol, 'id_rol_nuevo', _data_new.id_rol)::jsonb;
			END IF;
			IF _data_old.session <> _data_new.session
				THEN _datos := _datos || json_build_object('session_anterior', _data_old.session, 'session_nuevo', _data_new.session)::jsonb;
			END IF;
			IF _data_old.ultima_modificacion <> _data_new.ultima_modificacion
				THEN _datos := _datos || json_build_object('ultima_modificacion_anterior', _data_old.ultima_modificacion, 'ultima_modificacion_nuevo', _data_new.ultima_modificacion)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'usuario',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new usuario.usuario, _data_old usuario.usuario, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    10    201    201                       1255    96875     f_actualizar_clave(bigint, text)    FUNCTION     F  CREATE FUNCTION usuario.f_actualizar_clave(_user_id bigint, _clave text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
	
	BEGIN
		DELETE FROM
			usuario.token_recuperacion_usuario
		WHERE
			user_id = _user_id;
	
		UPDATE
			usuario.usuario
		SET
			estado = 1,
			clave = _clave
			
		WHERE
			id = _user_id;		
	END

$$;
 H   DROP FUNCTION usuario.f_actualizar_clave(_user_id bigint, _clave text);
       usuario       postgres    false    8                       1255    96198 4   f_almacenar_token_recuperacion_usuario(text, bigint)    FUNCTION     �  CREATE FUNCTION usuario.f_almacenar_token_recuperacion_usuario(_token text, _user_id bigint) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$	
	BEGIN
		INSERT INTO usuario.token_recuperacion_usuario
		(
			user_id,
			token, 
			fecha_creado,
			fecha_vigencia
			
		)
		VALUES 
		(
			_user_id,
			_token,
			current_timestamp,
			current_timestamp + interval '2 hours'
		);

			
	END

$$;
 \   DROP FUNCTION usuario.f_almacenar_token_recuperacion_usuario(_token text, _user_id bigint);
       usuario       postgres    false    8            �            1255    96150     f_buscar_existencia_correo(text)    FUNCTION     �   CREATE FUNCTION usuario.f_buscar_existencia_correo(_correo text) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			usuario.usuario
		WHERE
			correo = _correo;
	end
$$;
 @   DROP FUNCTION usuario.f_buscar_existencia_correo(_correo text);
       usuario       postgres    false    201    8            �            1255    96218     f_buscar_existencia_imagen(text)    FUNCTION     �   CREATE FUNCTION usuario.f_buscar_existencia_imagen(_url_imagen text) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			usuario.usuario
		WHERE
			url_imagen = _url_imagen;
	end
$$;
 D   DROP FUNCTION usuario.f_buscar_existencia_imagen(_url_imagen text);
       usuario       postgres    false    8    201            $           1255    96918 -   f_deshabilitar_usuario(bigint, boolean, text)    FUNCTION     X  CREATE FUNCTION usuario.f_deshabilitar_usuario(_id_usuario bigint, _habilitado boolean, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
	
	 UPDATE usuario.usuario SET

	    habilitado=_habilitado,
	    session=_session,
	    ultima_modificacion=current_timestamp
       
	where 
	
	  id =_id_usuario;
	 
   
 END
$$;
 f   DROP FUNCTION usuario.f_deshabilitar_usuario(_id_usuario bigint, _habilitado boolean, _session text);
       usuario       postgres    false    8            �            1255    96219 4   f_editar_usuario(text, text, text, text, text, text)    FUNCTION     �  CREATE FUNCTION usuario.f_editar_usuario(_nombre text, _apellido text, _correo text, _clave text, _url_imagen text, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    UPDATE usuario.usuario SET

	 nombre = _nombre ,
	 apellido=_apellido,
	 correo= correo,
	 clave= _clave,
	 url_imagen=_url_imagen ,
	 session=_session,
	 ultima_modificacion=current_timestamp

	where 
	
	  correo =_correo;
 END
$$;
 �   DROP FUNCTION usuario.f_editar_usuario(_nombre text, _apellido text, _correo text, _clave text, _url_imagen text, _session text);
       usuario       postgres    false    8            �            1255    96145 6   f_insertar_usuario(text, text, text, text, text, text)    FUNCTION     �  CREATE FUNCTION usuario.f_insertar_usuario(_nombre text, _apellido text, _correo text, _clave text, _url_imagen text, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    INSERT INTO usuario.usuario 

	(
	 nombre ,
	 apellido,
	 correo ,
	 clave ,
	 url_imagen ,
	 estado ,
	 habilitado,
	 id_rol ,
	 session,
	 ultima_modificacion
	) 
	
	VALUES (
    _nombre ,
	_apellido ,
	_correo,
	_clave ,
	_url_imagen ,
	 1,
	 true ,
	 2,
	_session,
	current_timestamp
	);
 END
$$;
 �   DROP FUNCTION usuario.f_insertar_usuario(_nombre text, _apellido text, _correo text, _clave text, _url_imagen text, _session text);
       usuario       postgres    false    8                       1255    96170    f_mostrar_datos_usuario(text)    FUNCTION       CREATE FUNCTION usuario.f_mostrar_datos_usuario(_correo text) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return query
        SELECT
            *
        FROM
            usuario.usuario
        WHERE
            correo = _correo;
    end
$$;
 =   DROP FUNCTION usuario.f_mostrar_datos_usuario(_correo text);
       usuario       postgres    false    8    201            �            1259    96901    mostrar_usuarios_view    VIEW     �   CREATE VIEW usuario.mostrar_usuarios_view AS
 SELECT (0)::bigint AS id_usuario,
    ''::text AS nombre_completo,
    ''::text AS correo,
    true AS habilitado,
    ''::text AS imagen;
 )   DROP VIEW usuario.mostrar_usuarios_view;
       usuario       postgres    false    8            #           1255    96917 1   f_mostrar_datos_usuarios_filtro(text, text, text)    FUNCTION     �  CREATE FUNCTION usuario.f_mostrar_datos_usuarios_filtro(_nombre_filtro text, _apellido_filtro text, _correo_filtro text) RETURNS SETOF usuario.mostrar_usuarios_view
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return query
        SELECT
		
		   usuario.usuario.id,
		   (usuario.usuario.nombre || '  ' || usuario.usuario.apellido),
		   usuario.usuario.correo,
		   usuario.usuario.habilitado,
		   usuario.usuario.url_imagen
		    
   
        FROM
            usuario.usuario
			
        WHERE
		    id_rol!=1
			 AND
			(CASE _nombre_filtro 
				WHEN '' THEN 
					usuario.usuario.nombre ilike '%' || _nombre_filtro || '%'
				ELSE 
					usuario.usuario.nombre ilike '%' || _nombre_filtro || '%'
			    
				END)
				AND
				(CASE _apellido_filtro 
				WHEN '' THEN 
					usuario.usuario.apellido ilike '%' || _apellido_filtro || '%'
				ELSE 
					usuario.usuario.apellido ilike '%' || _apellido_filtro || '%'
			    
				END)
			AND
			(CASE _correo_filtro 
				WHEN '' THEN 
					usuario.usuario.correo ilike '%' || _correo_filtro || '%' 
				ELSE 
					usuario.usuario.correo ilike '%' || _correo_filtro || '%' 
				END);
            
    end
$$;
 x   DROP FUNCTION usuario.f_mostrar_datos_usuarios_filtro(_nombre_filtro text, _apellido_filtro text, _correo_filtro text);
       usuario       postgres    false    229    8                       1255    96201 *   f_obtener_usuario_token_recuperacion(text)    FUNCTION     �  CREATE FUNCTION usuario.f_obtener_usuario_token_recuperacion(_token text) RETURNS SETOF bigint
    LANGUAGE plpgsql
    AS $$
	
	BEGIN
		IF (SELECT COUNT(*) FROM usuario.token_recuperacion_usuario WHERE token = _token) = 0
			THEN RETURN QUERY
				SELECT
					-1::INTEGER;
		elseIF (SELECT COUNT(*)FROM usuario.token_recuperacion_usuario WHERE token = _token AND current_timestamp between fecha_creado AND fecha_vigencia) = 0
			THEN
				DELETE FROM usuario.token_recuperacion_usuario WHERE token = _token;
			
				RETURN QUERY
				SELECT
					-2::INTEGER;
		ELSE	
			RETURN QUERY 
				SELECT
					user_id
				FROM
					usuario.token_recuperacion_usuario
				WHERE
					token = _token;
		
		END IF;

			
	END

$$;
 I   DROP FUNCTION usuario.f_obtener_usuario_token_recuperacion(_token text);
       usuario       postgres    false    8                       1255    96874    f_validar_usuario_usuario(text)    FUNCTION     �  CREATE FUNCTION usuario.f_validar_usuario_usuario(_correo text) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	
	BEGIN
		IF (SELECT COUNT(*) FROM usuario.usuario WHERE correo = _correo) = 0
			then RETURN QUERY
				SELECT
					-1::BIGINT,
					''::CHARACTER VARYING(20),
					''::CHARACTER VARYING(20),
					''::TEXT,
					''::TEXT,
					''::TEXT,
					1::INTEGER,
					true::BOOLEAN,
					1::INTEGER,
					''::TEXT,
					'2018-01-01 00:00'::TIMESTAMP;
					
		elseIF (SELECT COUNT(*) FROM usuario.usuario uu join usuario.token_recuperacion_usuario ut on ut.user_id = uu.id WHERE uu.correo = _correo and current_timestamp between ut.fecha_creado AND ut.fecha_vigencia) > 0

			then RETURN QUERY
				SELECT
					-2::BIGINT,
					''::CHARACTER VARYING(20),
					''::CHARACTER VARYING(20),
					''::TEXT,
					''::TEXT,
					''::TEXT,
					1::INTEGER,
					true::BOOLEAN,
					1::INTEGER,
					''::TEXT,
					'2018-01-01 00:00'::TIMESTAMP;
		ELSEIF (SELECT COUNT(*) FROM usuario.usuario WHERE correo = _correo) > 0
			THEN
				update 
					usuario.usuario
				set
					estado = 2
				where
					correo = _correo;
				
				RETURN QUERY 
				SELECT
					*
				FROM
					usuario.usuario 
				WHERE
					correo = _correo;
		ELSE
			RETURN QUERY
				SELECT
					-1::BIGINT,
					''::CHARACTER VARYING(20),
					''::CHARACTER VARYING(20),
					''::TEXT,
					''::TEXT,
					''::TEXT,
					1::INTEGER,
					true::BOOLEAN,
					1::INTEGER,
					''::TEXT,
					'2018-01-01 00:00'::TIMESTAMP;
		END IF;
			
	END

$$;
 ?   DROP FUNCTION usuario.f_validar_usuario_usuario(_correo text);
       usuario       postgres    false    201    8            �            1259    96535    cancelar_evento_id_seq    SEQUENCE        CREATE SEQUENCE evento.cancelar_evento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE evento.cancelar_evento_id_seq;
       evento       postgres    false    6    223                       0    0    cancelar_evento_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE evento.cancelar_evento_id_seq OWNED BY evento.cancelar_evento.id;
            evento       postgres    false    222            �            1259    96099    categoria_evento_id_seq    SEQUENCE     �   CREATE SEQUENCE evento.categoria_evento_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE evento.categoria_evento_id_seq;
       evento       postgres    false    217    6                       0    0    categoria_evento_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE evento.categoria_evento_id_seq OWNED BY evento.categoria_evento.id;
            evento       postgres    false    216            �            1259    96088    denuncia_evento_id_seq    SEQUENCE     �   CREATE SEQUENCE evento.denuncia_evento_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE evento.denuncia_evento_id_seq;
       evento       postgres    false    215    6                       0    0    denuncia_evento_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE evento.denuncia_evento_id_seq OWNED BY evento.denuncia_evento.id;
            evento       postgres    false    214            �            1259    96077    estado_evento_id_seq    SEQUENCE     �   CREATE SEQUENCE evento.estado_evento_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE evento.estado_evento_id_seq;
       evento       postgres    false    213    6                       0    0    estado_evento_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE evento.estado_evento_id_seq OWNED BY evento.estado_evento.id;
            evento       postgres    false    212            �            1259    96054    interes_id_seq    SEQUENCE     w   CREATE SEQUENCE evento.interes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE evento.interes_id_seq;
       evento       postgres    false    211    6                       0    0    interes_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE evento.interes_id_seq OWNED BY evento.interes.id;
            evento       postgres    false    210            �            1259    96045 	   auditoria    TABLE       CREATE TABLE security.auditoria (
    id bigint NOT NULL,
    fecha timestamp without time zone,
    accion character varying(100),
    schema character varying(200),
    tabla character varying(200),
    session text,
    user_bd character varying(100),
    data jsonb,
    pk text
);
    DROP TABLE security.auditoria;
       security         postgres    false    10            �            1259    96043    auditoria_id_seq    SEQUENCE     {   CREATE SEQUENCE security.auditoria_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE security.auditoria_id_seq;
       security       postgres    false    10    209                       0    0    auditoria_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE security.auditoria_id_seq OWNED BY security.auditoria.id;
            security       postgres    false    208            �            1259    96034    autentication    TABLE       CREATE TABLE security.autentication (
    id bigint NOT NULL,
    codigo_usuario integer,
    ip character varying(100),
    mac character varying(100),
    fecha_inicio timestamp without time zone,
    session text,
    fecha_fin timestamp without time zone
);
 #   DROP TABLE security.autentication;
       security         postgres    false    10            �            1259    96032    autentication_id_seq    SEQUENCE        CREATE SEQUENCE security.autentication_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE security.autentication_id_seq;
       security       postgres    false    10    207                       0    0    autentication_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE security.autentication_id_seq OWNED BY security.autentication.id;
            security       postgres    false    206            �            1259    104793    function_db_view    VIEW     �  CREATE VIEW security.function_db_view AS
 SELECT pp.proname AS b_function,
    oidvectortypes(pp.proargtypes) AS b_type_parameters
   FROM (pg_proc pp
     JOIN pg_namespace pn ON ((pn.oid = pp.pronamespace)))
  WHERE ((pn.nspname)::text <> ALL (ARRAY[('pg_catalog'::character varying)::text, ('information_schema'::character varying)::text, ('admin_control'::character varying)::text, ('vial'::character varying)::text]));
 %   DROP VIEW security.function_db_view;
       security       postgres    false    10            �            1259    96012    rol    TABLE     �   CREATE TABLE usuario.rol (
    id integer NOT NULL,
    descripcion character varying(20),
    session text,
    ultima_modificacion timestamp without time zone
);
    DROP TABLE usuario.rol;
       usuario         postgres    false    8            �            1259    96010 
   rol_id_seq    SEQUENCE     �   CREATE SEQUENCE usuario.rol_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE usuario.rol_id_seq;
       usuario       postgres    false    8    203                       0    0 
   rol_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE usuario.rol_id_seq OWNED BY usuario.rol.id;
            usuario       postgres    false    202            �            1259    96021 !   token_recuperacion_usuario_id_seq    SEQUENCE     �   CREATE SEQUENCE usuario.token_recuperacion_usuario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE usuario.token_recuperacion_usuario_id_seq;
       usuario       postgres    false    8    205                        0    0 !   token_recuperacion_usuario_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE usuario.token_recuperacion_usuario_id_seq OWNED BY usuario.token_recuperacion_usuario.id;
            usuario       postgres    false    204            �            1259    95999    usuario_id_seq    SEQUENCE     x   CREATE SEQUENCE usuario.usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE usuario.usuario_id_seq;
       usuario       postgres    false    8    201            !           0    0    usuario_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE usuario.usuario_id_seq OWNED BY usuario.usuario.id;
            usuario       postgres    false    200            J           2604    96540    cancelar_evento id    DEFAULT     x   ALTER TABLE ONLY evento.cancelar_evento ALTER COLUMN id SET DEFAULT nextval('evento.cancelar_evento_id_seq'::regclass);
 A   ALTER TABLE evento.cancelar_evento ALTER COLUMN id DROP DEFAULT;
       evento       postgres    false    222    223    223            H           2604    96104    categoria_evento id    DEFAULT     z   ALTER TABLE ONLY evento.categoria_evento ALTER COLUMN id SET DEFAULT nextval('evento.categoria_evento_id_seq'::regclass);
 B   ALTER TABLE evento.categoria_evento ALTER COLUMN id DROP DEFAULT;
       evento       postgres    false    217    216    217            G           2604    96093    denuncia_evento id    DEFAULT     x   ALTER TABLE ONLY evento.denuncia_evento ALTER COLUMN id SET DEFAULT nextval('evento.denuncia_evento_id_seq'::regclass);
 A   ALTER TABLE evento.denuncia_evento ALTER COLUMN id DROP DEFAULT;
       evento       postgres    false    215    214    215            F           2604    96082    estado_evento id    DEFAULT     t   ALTER TABLE ONLY evento.estado_evento ALTER COLUMN id SET DEFAULT nextval('evento.estado_evento_id_seq'::regclass);
 ?   ALTER TABLE evento.estado_evento ALTER COLUMN id DROP DEFAULT;
       evento       postgres    false    213    212    213            E           2604    96059 
   interes id    DEFAULT     h   ALTER TABLE ONLY evento.interes ALTER COLUMN id SET DEFAULT nextval('evento.interes_id_seq'::regclass);
 9   ALTER TABLE evento.interes ALTER COLUMN id DROP DEFAULT;
       evento       postgres    false    211    210    211            D           2604    96048    auditoria id    DEFAULT     p   ALTER TABLE ONLY security.auditoria ALTER COLUMN id SET DEFAULT nextval('security.auditoria_id_seq'::regclass);
 =   ALTER TABLE security.auditoria ALTER COLUMN id DROP DEFAULT;
       security       postgres    false    209    208    209            C           2604    96037    autentication id    DEFAULT     x   ALTER TABLE ONLY security.autentication ALTER COLUMN id SET DEFAULT nextval('security.autentication_id_seq'::regclass);
 A   ALTER TABLE security.autentication ALTER COLUMN id DROP DEFAULT;
       security       postgres    false    207    206    207            A           2604    96015    rol id    DEFAULT     b   ALTER TABLE ONLY usuario.rol ALTER COLUMN id SET DEFAULT nextval('usuario.rol_id_seq'::regclass);
 6   ALTER TABLE usuario.rol ALTER COLUMN id DROP DEFAULT;
       usuario       postgres    false    202    203    203            B           2604    96026    token_recuperacion_usuario id    DEFAULT     �   ALTER TABLE ONLY usuario.token_recuperacion_usuario ALTER COLUMN id SET DEFAULT nextval('usuario.token_recuperacion_usuario_id_seq'::regclass);
 M   ALTER TABLE usuario.token_recuperacion_usuario ALTER COLUMN id DROP DEFAULT;
       usuario       postgres    false    204    205    205            @           2604    96004 
   usuario id    DEFAULT     j   ALTER TABLE ONLY usuario.usuario ALTER COLUMN id SET DEFAULT nextval('usuario.usuario_id_seq'::regclass);
 :   ALTER TABLE usuario.usuario ALTER COLUMN id DROP DEFAULT;
       usuario       postgres    false    201    200    201                      0    96537    cancelar_evento 
   TABLE DATA               ]   COPY evento.cancelar_evento (id, razon, id_evento, session, ultima_modificacion) FROM stdin;
    evento       postgres    false    223   �                0    96101    categoria_evento 
   TABLE DATA               Y   COPY evento.categoria_evento (id, descripcion, session, ultima_modificacion) FROM stdin;
    evento       postgres    false    217   ��                0    96090    denuncia_evento 
   TABLE DATA               o   COPY evento.denuncia_evento (id, descripcion, id_evento, session, ultima_modificacion, id_usuario) FROM stdin;
    evento       postgres    false    215   �      	          0    96079    estado_evento 
   TABLE DATA               V   COPY evento.estado_evento (id, descripcion, session, ultima_modificacion) FROM stdin;
    evento       postgres    false    213   �                0    96320    evento 
   TABLE DATA               �   COPY evento.evento (id, nombre, descripcion, fecha_inicio, fecha_fin, hora, imagen, id_usuario, id_categoria, id_estado, cantidad_interesados, cantidad_denuncias, session, ultima_modificacion, lugar) FROM stdin;
    evento       postgres    false    220   =�                0    96056    interes 
   TABLE DATA               f   COPY evento.interes (id, id_usuario, id_evento, habilitado, session, ultima_modificacion) FROM stdin;
    evento       postgres    false    211   5�                0    96045 	   auditoria 
   TABLE DATA               c   COPY security.auditoria (id, fecha, accion, schema, tabla, session, user_bd, data, pk) FROM stdin;
    security       postgres    false    209   ��                0    96034    autentication 
   TABLE DATA               h   COPY security.autentication (id, codigo_usuario, ip, mac, fecha_inicio, session, fecha_fin) FROM stdin;
    security       postgres    false    207   ��      �          0    96012    rol 
   TABLE DATA               M   COPY usuario.rol (id, descripcion, session, ultima_modificacion) FROM stdin;
    usuario       postgres    false    203   �                0    96023    token_recuperacion_usuario 
   TABLE DATA               g   COPY usuario.token_recuperacion_usuario (id, user_id, token, fecha_creado, fecha_vigencia) FROM stdin;
    usuario       postgres    false    205   P�      �          0    96001    usuario 
   TABLE DATA               �   COPY usuario.usuario (id, nombre, apellido, correo, clave, url_imagen, estado, habilitado, id_rol, session, ultima_modificacion) FROM stdin;
    usuario       postgres    false    201   ��      "           0    0    cancelar_evento_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('evento.cancelar_evento_id_seq', 3, true);
            evento       postgres    false    222            #           0    0    categoria_evento_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('evento.categoria_evento_id_seq', 7, true);
            evento       postgres    false    216            $           0    0    denuncia_evento_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('evento.denuncia_evento_id_seq', 4, true);
            evento       postgres    false    214            %           0    0    estado_evento_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('evento.estado_evento_id_seq', 1, false);
            evento       postgres    false    212            &           0    0    evento_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('evento.evento_id_seq', 9, true);
            evento       postgres    false    219            '           0    0    interes_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('evento.interes_id_seq', 7, true);
            evento       postgres    false    210            (           0    0    auditoria_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('security.auditoria_id_seq', 14765, true);
            security       postgres    false    208            )           0    0    autentication_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('security.autentication_id_seq', 73, true);
            security       postgres    false    206            *           0    0 
   rol_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('usuario.rol_id_seq', 1, false);
            usuario       postgres    false    202            +           0    0 !   token_recuperacion_usuario_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('usuario.token_recuperacion_usuario_id_seq', 5, true);
            usuario       postgres    false    204            ,           0    0    usuario_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('usuario.usuario_id_seq', 6, true);
            usuario       postgres    false    200            `           2606    96545 $   cancelar_evento cancelar_evento_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY evento.cancelar_evento
    ADD CONSTRAINT cancelar_evento_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY evento.cancelar_evento DROP CONSTRAINT cancelar_evento_pkey;
       evento         postgres    false    223            \           2606    96109 &   categoria_evento categoria_evento_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY evento.categoria_evento
    ADD CONSTRAINT categoria_evento_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY evento.categoria_evento DROP CONSTRAINT categoria_evento_pkey;
       evento         postgres    false    217            Z           2606    96098 $   denuncia_evento denuncia_evento_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY evento.denuncia_evento
    ADD CONSTRAINT denuncia_evento_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY evento.denuncia_evento DROP CONSTRAINT denuncia_evento_pkey;
       evento         postgres    false    215            X           2606    96087     estado_evento estado_evento_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY evento.estado_evento
    ADD CONSTRAINT estado_evento_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY evento.estado_evento DROP CONSTRAINT estado_evento_pkey;
       evento         postgres    false    213            ^           2606    96328    evento evento_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY evento.evento
    ADD CONSTRAINT evento_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY evento.evento DROP CONSTRAINT evento_pkey;
       evento         postgres    false    220            V           2606    96064    interes interes_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY evento.interes
    ADD CONSTRAINT interes_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY evento.interes DROP CONSTRAINT interes_pkey;
       evento         postgres    false    211            T           2606    96053    auditoria auditoria_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY security.auditoria
    ADD CONSTRAINT auditoria_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY security.auditoria DROP CONSTRAINT auditoria_pkey;
       security         postgres    false    209            R           2606    96042     autentication autentication_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY security.autentication
    ADD CONSTRAINT autentication_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY security.autentication DROP CONSTRAINT autentication_pkey;
       security         postgres    false    207            N           2606    96020    rol rol_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY usuario.rol
    ADD CONSTRAINT rol_pkey PRIMARY KEY (id);
 7   ALTER TABLE ONLY usuario.rol DROP CONSTRAINT rol_pkey;
       usuario         postgres    false    203            P           2606    96031 :   token_recuperacion_usuario token_recuperacion_usuario_pkey 
   CONSTRAINT     y   ALTER TABLE ONLY usuario.token_recuperacion_usuario
    ADD CONSTRAINT token_recuperacion_usuario_pkey PRIMARY KEY (id);
 e   ALTER TABLE ONLY usuario.token_recuperacion_usuario DROP CONSTRAINT token_recuperacion_usuario_pkey;
       usuario         postgres    false    205            L           2606    96009    usuario usuario_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY usuario.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);
 ?   ALTER TABLE ONLY usuario.usuario DROP CONSTRAINT usuario_pkey;
       usuario         postgres    false    201            n           2620    104807    estado_evento tg_estado_evento    TRIGGER     �   CREATE TRIGGER tg_estado_evento AFTER INSERT OR DELETE OR UPDATE ON evento.estado_evento FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 7   DROP TRIGGER tg_estado_evento ON evento.estado_evento;
       evento       postgres    false    213    289            r           2620    104810 )   cancelar_evento tg_evento_cancelar_evento    TRIGGER     �   CREATE TRIGGER tg_evento_cancelar_evento AFTER INSERT OR DELETE OR UPDATE ON evento.cancelar_evento FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 B   DROP TRIGGER tg_evento_cancelar_evento ON evento.cancelar_evento;
       evento       postgres    false    289    223            p           2620    104809 ,   categoria_evento tg_evento_categoria_envento    TRIGGER     �   CREATE TRIGGER tg_evento_categoria_envento AFTER INSERT OR DELETE OR UPDATE ON evento.categoria_evento FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 E   DROP TRIGGER tg_evento_categoria_envento ON evento.categoria_evento;
       evento       postgres    false    217    289            o           2620    104808 )   denuncia_evento tg_evento_denundia_evento    TRIGGER     �   CREATE TRIGGER tg_evento_denundia_evento AFTER INSERT OR DELETE OR UPDATE ON evento.denuncia_evento FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 B   DROP TRIGGER tg_evento_denundia_evento ON evento.denuncia_evento;
       evento       postgres    false    289    215            q           2620    104806    evento tg_evento_evento    TRIGGER     �   CREATE TRIGGER tg_evento_evento AFTER INSERT OR DELETE OR UPDATE ON evento.evento FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 0   DROP TRIGGER tg_evento_evento ON evento.evento;
       evento       postgres    false    220    289            m           2620    104805    interes tg_evento_interes    TRIGGER     �   CREATE TRIGGER tg_evento_interes AFTER INSERT OR DELETE OR UPDATE ON evento.interes FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 2   DROP TRIGGER tg_evento_interes ON evento.interes;
       evento       postgres    false    289    211            l           2620    104803 #   token_recuperacion_usuario tg_token    TRIGGER     �   CREATE TRIGGER tg_token AFTER INSERT OR DELETE OR UPDATE ON usuario.token_recuperacion_usuario FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 =   DROP TRIGGER tg_token ON usuario.token_recuperacion_usuario;
       usuario       postgres    false    205    289            k           2620    104804    rol tg_usuario_rol    TRIGGER     �   CREATE TRIGGER tg_usuario_rol AFTER INSERT OR DELETE OR UPDATE ON usuario.rol FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 ,   DROP TRIGGER tg_usuario_rol ON usuario.rol;
       usuario       postgres    false    203    289            j           2620    104801    usuario tg_usuario_usuario    TRIGGER     �   CREATE TRIGGER tg_usuario_usuario AFTER INSERT OR DELETE OR UPDATE ON usuario.usuario FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 4   DROP TRIGGER tg_usuario_usuario ON usuario.usuario;
       usuario       postgres    false    201    289            f           2606    96329    evento id_categoria    FK CONSTRAINT     �   ALTER TABLE ONLY evento.evento
    ADD CONSTRAINT id_categoria FOREIGN KEY (id_categoria) REFERENCES evento.categoria_evento(id);
 =   ALTER TABLE ONLY evento.evento DROP CONSTRAINT id_categoria;
       evento       postgres    false    220    2908    217            g           2606    96334    evento id_estado    FK CONSTRAINT     y   ALTER TABLE ONLY evento.evento
    ADD CONSTRAINT id_estado FOREIGN KEY (id_estado) REFERENCES evento.estado_evento(id);
 :   ALTER TABLE ONLY evento.evento DROP CONSTRAINT id_estado;
       evento       postgres    false    2904    213    220            d           2606    96344    denuncia_evento id_evento    FK CONSTRAINT     {   ALTER TABLE ONLY evento.denuncia_evento
    ADD CONSTRAINT id_evento FOREIGN KEY (id_evento) REFERENCES evento.evento(id);
 C   ALTER TABLE ONLY evento.denuncia_evento DROP CONSTRAINT id_evento;
       evento       postgres    false    215    2910    220            c           2606    96349    interes id_evento    FK CONSTRAINT     s   ALTER TABLE ONLY evento.interes
    ADD CONSTRAINT id_evento FOREIGN KEY (id_evento) REFERENCES evento.evento(id);
 ;   ALTER TABLE ONLY evento.interes DROP CONSTRAINT id_evento;
       evento       postgres    false    220    211    2910            i           2606    96546    cancelar_evento id_evento    FK CONSTRAINT     {   ALTER TABLE ONLY evento.cancelar_evento
    ADD CONSTRAINT id_evento FOREIGN KEY (id_evento) REFERENCES evento.evento(id);
 C   ALTER TABLE ONLY evento.cancelar_evento DROP CONSTRAINT id_evento;
       evento       postgres    false    220    2910    223            b           2606    96135    interes id_usuario    FK CONSTRAINT     w   ALTER TABLE ONLY evento.interes
    ADD CONSTRAINT id_usuario FOREIGN KEY (id_usuario) REFERENCES usuario.usuario(id);
 <   ALTER TABLE ONLY evento.interes DROP CONSTRAINT id_usuario;
       evento       postgres    false    211    201    2892            h           2606    96339    evento id_usuario    FK CONSTRAINT     v   ALTER TABLE ONLY evento.evento
    ADD CONSTRAINT id_usuario FOREIGN KEY (id_usuario) REFERENCES usuario.usuario(id);
 ;   ALTER TABLE ONLY evento.evento DROP CONSTRAINT id_usuario;
       evento       postgres    false    220    2892    201            e           2606    96856    denuncia_evento id_usuario    FK CONSTRAINT        ALTER TABLE ONLY evento.denuncia_evento
    ADD CONSTRAINT id_usuario FOREIGN KEY (id_usuario) REFERENCES usuario.usuario(id);
 D   ALTER TABLE ONLY evento.denuncia_evento DROP CONSTRAINT id_usuario;
       evento       postgres    false    2892    201    215            a           2606    96110    usuario rol_id    FK CONSTRAINT     l   ALTER TABLE ONLY usuario.usuario
    ADD CONSTRAINT rol_id FOREIGN KEY (id_rol) REFERENCES usuario.rol(id);
 9   ALTER TABLE ONLY usuario.usuario DROP CONSTRAINT rol_id;
       usuario       postgres    false    203    201    2894               �   x�=�Kr� ��p��:��v�Lv}\��hHP������߃���$	��BtVXV��}P`$|8��
Z��|��qGᆹ�������(	XdE�a��8v\��Te�P$gr���19J8=����o��M���Q򲚤�Q񨱫����!r��,���"�S/����u�	`�a�"�Eͫ��ĺ��G�1��"i� ����u)q�{��fӆ�� ����`2̱F�j��SJ�ֵ\f         �   x�m��n� Eg��@��e�����1��Z�p�~}�V�V��=��e�B�Z��}�:��O\�8R1	�k�9*��w�b�S]������ 8 �rY��]�gЀf�+���^�Z��l�ig��uч�����W�m
�Ʋ�i�EB��x.��� 0{dτ�5��kXS��������_���G�s,��M���p��@o@1PȍB�.E]D         �   x����m� @�5T���cbSK6�{�6��IW�!��(�HY��{���r�9�4�|���TCz��)~<C��ާ�Xw;��1�1Bhɴ�wý�*�+���o�Ã���>�N����|�M�F�f�KڨV\.��?�~	Bj&4bǱ�"�J�����e+���f��ݔ�*9 �&��@ʡl�R��WRnc�iթpi'[��\������*��C7�0�H$}�(�_�h      	   D   x�3�tL.�,���".#N�<�Ңb��1�[f^bNfUb
LȄӱ 'I���91/95!���� ��         �  x����n�0���)�A��$�(�t1�lf)�c�l����b�}IJ[��lF��"��s�B��2�ŵ�?4����M|��G�#�,�X����Њ�g��UF���*��~ʝiG�e�=��AQb�s&��E�N�WY�ƴ�ت�$�Q�6�փ���F���o��������c��o;���A��#_;��,�`�ے$�9=(�_"&&�V�HZg��Y͚��=i���Ŝ|��
�U%XU�9'�m�����a��d�;Įw^�:�����d\[3F�k;��� ~�.́��,�̆~y�	A�h��AMh�pშ�mӝ��]Bˊ����!ؿ$��H�#ɦ��)GD�R�����	#�$��Y�����Gm��?�Z���]T�	tݰ[�1�����8h埣$�L�贙�ӕ���xq���I���u'���t��W+�͜9�hF��'{i�6�>ϲ�/��2!         �   x�U�Kn� @��c�@,x�YK'�IblpB�����Upt/��
W�h�.[
u���pF�0���I�I�ΰg�Ľ�BHJ�A��s�瘲N|����g����Y�Z�q=Y;��;�yɴ��q��^r(m�0���G��Λ�'����X�A�*�g[
��=]KK{L��_�cw@3zý�v�S_�R���>�            x���M�5�q�;���uP�Y�=3`��wl�i�ٲ�i����w�������Ɏw�$�^o�~N}de���#�p���os��(?���r�����?��������?�˟������������/���_��ϵ��׿������������������?�������?��������������?��������w������?���?��������w��o�!����������v�g������������_���O���׿�����Z��O?��/������_��M��_���?������ӟ��������w��?�]�����K��.�ǩ�>�?��Sn_�g�w��8\?n�w{��y��֪���c�9?�g�����c�tX���Q��Qr�1�U���q'J�Ht��r<4����?��1ΖϘ��������n4莇��Р��ӑ�Z*s�t�����z<�< <B��:|G#�whd��t\���1RH{uAG�p܁��(�n��m�CD[�t]��r���tX|�Ï��Y�Q�h+;"�z��.V<)_��eK���V���q籺c�ɠ;�C#�v�u]Zr9HѪ���j8�@ ;F����:<4X���-�Ɯ�s��::���N�c$��r��xh��iן�R�1O�tAG���܉@x�D7��C�{xhpO��g��Xt�Xȝ��Ht�Ay<4���n�����VәRе�*�h��;(���F��xhp�z�v��
:Z��N�c$�Ѡ<��C�{�u��^0/-��Xt�Xȝ��Ht������ں�zTT�r�)��-�-�r'�ʣ$��VyD�um�s��n_�e�����1 w"�<J�m�GD[��ЧuO����T�c��-�-�r'�ʣ$�Ѡ<��C�{�u�:��낎�����n4(�������z]�����bU��b= w"P#ѽ���C�{xhpO��_�:{ԯXt�Xȝ��Ht�Ay<4������1���c�肎�����n4(�����ܳV��z��+V-�r'�1�hP���=�D�m�tӤ
:Z��N�c$�Ѡ<��C�{��~�q�,��-�r'�1�%��<"�Cd[��㺄ɯ;��j]��e=#;��%�f[�1��@L�UPY�ef;�#�K|�͏!ّ��(�6ۺ�ɶ"�a�|�ץL?�H1�uAG����HV~�H��#��BD6X�\�3%�^b�邎6�!ّ@~�H��#��BD6Xh-��3�s�:ڬ�dG�1"�_@~D6X����F�u��Dt�YɎ�cD�l��"��BkM��͘ř����zHv$�#�f���l��k��ZHs�>ΠK0U��f=$;ȏi�A~D6X����4#�v�,��m�C�#���6�Gd���l��ZPs�:�ͪ���zHv$�#����Gd���lk����z�~ !m�m�C�#Y�Q"m���m-�d[յ����1�f]��6?�dG��D�l+?&�Z�Ȇ��u-���<��肎6�!ّ��(�6�Gd���l���f��Y��肎6�!ّ@~�H��#��BD6Xh-��}���Bt�YɎ�cD�ٰΞ��`��Ц���ʘ�f]��f=$;ȏi�A~D6X�����n�3�!���h���G��`>DC�b�z��AKn%�^Jt�X�N�c$�l���!��?�ICǈ�*�貞�	�ǈ�٠="Dd��֓�\g	ڗMt�YɎ�cD�ٰ����B��س3hs6]��f=$;��%�f[�1��BL��PK���_��#���m~Ɏd�G���V~L�����[^�n�:S��uAG����HV~�H��#��BD6Xh}`V��$mU��f=$;ȏi�A~D6X���'�A�FT=G����@�>B��ΰ
���� ����j�ϘG_����j@v"�#�f���l���Y��c=诲*�貞�	�ǈ�٠="�c��v�[�/xL�UAG����H�>F����`!",4���ԃ>�.�h�����q6�G#�}hdp�\w$G>�� �����c�ٰ����O��9��X^�j���&?:��GdG*��������Av��NrhqmV�m����D��d�ϱͲ��;µ�9�>"�<:ڬ�dGJ���� ?",�cO�ߝֵL�G���-������^4g
YE�,�N=�;(�i�A�D6���w�5A-�!wς�6�!ّ@ٌH7����lp'��l�����%죞��zDv 6!�F��xh0�kq_g�x��,������H�٠="��c7���&n�TB��xttY�Ȏ�cD�l��"�AA�~��ƌ���Y��f=$;��i��}D6X����#i昍��m�C�#���nv��`!"n ���#dAⳠ��zHv$��dD�l+?&�Z�ɶJ����l�\����1$;��%�f[�1��BD�i-t��W�Y~��[mVm�C�#Y�Q"m6ȏ��`��3T*y�#����zHv$�#�f���l����՜%��gAG����H ?F��=@~D6X������C�B6~t�YɎ�cD�l��"��B��r����Y��f=$;ȏi�A~D6X�Ǟ`!�k����]��d=";����~�k�M>���_t�k���H�� "~�D6�����2�#�<0�]�3�#�@�� "~�D����랽�YC�]}t�YɎd/c(�6ۺ�ɶb����b���\�90�Y��6?�dG��D�l+?&�Z��N�Bׁz�k��9n�Y��f=$;��%�f���l��ZkVJ{n�M�.�h��	�ǈ�� ?",Dd���'fc�Z�~ͪ���zHv$�#��� ?",Dd����J��#�T�gAG����H ?F����`!",t�7ʈ�۬
:ڬ�dG�1"m6ȏ��ذ�>d��v	�
:ڬ�dG�1"m6ȏ��`�u�ޑ�#�t�gAG����H ?F����`!"[Y���Z����J#��n���t�YɎ4?�f���tv� ;[��Ci�uU����w�U��ڌC�#�OD���Av� �[v�?�i��Ǖ܍Rm�C�#����� ?",�c7�кC)�8��_�ߍRm�C�#���6�Gd���l�кC�g=������
:ڬ�dG�1"m6ȏ��`�~]���$mU��f=$;ȏ�fw��"��B���Z>����(U��f=$;ȏi�A~D6X���;�<^�qA�Vm�C�#���6�Gd��x�,4:���Z�����Q����zHv$�#�f���l��m-t}��C�-� ��(]��f=$;��%�f[�1��BL��P��7�y||�����z�7�1";�U#Ѝ�|D�5mt�ѯ]�cn�uAG�Հ�DV{�D��#��?D6����&�:O<�˪���zFv$�#�f���l0�=AAk���s�����Q����zHv$P#�f���l��j��m�<�m�C�#���6�Gd���l��:�+�9KЯYt�YɎ�cDZ�z���l���^�Y_���Yt�YɎ�cD�l��"��Bcݝ����_�*�h��	�ǈ�� ?",�c�s�R�w�����
��x���w�����o�x����ԣ�#���6�Md�;�l����������׮�F邎6�!ّ��)�6�*�ɶ�d�����k�����%r�Y|�͏!ّ��(�nv��c����lk�������UL�UAG����HV~�H��#��BD6Xh��r�����(U��f=$;ȏi�A~D6X��.`������}ĴYt�YɎ�cD�l��"��Bk����YbV�肎6�!ّ@~�H��#��BD6X�_w()3I[t�YɎ�cD���� �"��Bk���F=�n�TAG����H ?F����`!",�v+N����?Խ�
:ڬ�dG�1"m6ȏ�����@��eD�mVm�C�#���6�Gd���lk�r���-�٪���zDv �>F����#����hx�֫Ț��Ͻt��{�d'�g    ��D�����m��d[����y-5~k��O����zFv$�=J����` "Dx��V���9��Gd�������R��cׇ�!�X���_K�S�D F�����K&���.v�G�u�.�貞�	�� "~�D6\Ŭ�~�˶��i�n�*�h��	.c�n6��g��BD6Xhݭ�#��-(tAG����H ?F����`!",t�x�ӵ�FH�UAG����H ?F����`!���u������c�F���6�!ّ@~�H��#��BD��P=.v�e�1�wtAG����HV~�H�m��d[1��B5�ן���ٙw�����ǈ�@V}�@�`�=mDD[�|o8^���ͼ��
:z�d'�ڣ$�l���!��?e->���3��>���.�ّ@{�H��#��@<6,c��I���a�.�h��	�ǈ���>",Dd���:��G�y��:ڬ�dG�1"m6ȏ��`��C����Q�.�h��	�ǈt�a;�"��B�}�G��Z]��f=$;ȏi�A~D6X����t��u�kH�UAG����H ?F����`!������L}Ԡ#����zHv$�#�f���l��m-��9]�^��FtY�s4Y��d�����|D�5m�֓��Ǭ�������j���f�v&���ɶ�ik��2r���V�=��Y�1m48�����`���a�t��gغ���j@v"�#�f���l��K�۽�x>��KC��
:��gdG�1"m6h��٠�{��|�%���z�&�ف@|�@���@<4h=^�����tAG�Հ�D�=F��Kיl����w�i���kU��e=#;h�i�A{D6�����S:���غ���zHv$P#�f���l�����:����;�:ڬ�dG�1"m6ȏ��O�������h3d��gAG����H��6��}~�=,{m1^R�)���,�n�͐�H��n�Yv�e���e�=�ϳՐ��m�C�#�OD�l����{���i��Kg�~?ȶ��<v`�r_���#n��㩇{Gi3"m6H��{����W:�~�Yi�*�h��	�͈�� m"�Id�=��J��#ꏍ*�h��	�͈�� ?",Dd����J��GI�m�C�#���ȏ��`�S!
�x�Y��f=$;ȏi�A~D6X���uMS_��fU��f=$;ȏi�A~D6X��N`�{��s\��Yt�YɎ�cD�l��"��Fp}w�u	�:ڬ�dG��OF�Ͷ�c����lk��vYG9�����m~Ɏd�G�t����m-�d[���\9K
�_�Y��f=$;��%�f���l��ZD�v�,�x�s4Y���#�h0��h}�KoQ�V=V����6�Gd��l���6.�y�|�,�貞�	�ǈ�٠="Dd����P����6���6�!ّ@}�H������`!�{��ڬ
:ڬ�dG�_¯y��4sI![�<:���?��aD�l���f���y}e6�q=�	i�*�h��	$�� "~�D������f����Q��d=";���a�hk>"������gf)]G��X|��z@v"�=J��ݭ��l�&��'���s�y��:��gdG�ڣD�l��"�AA��f�#�Ǭ
:ڬ�dG�1"m6�����'X����%���gAG����H ?F����`!",�>7G�Q�fU��f=$;ȏi�A~D6X����+Kɳ�mVm�C�#���n6,�g��BD6Xh}o6�Yf̃]��f=$;ȏi�A~D6X������h}�Yt�YɎ�cD�l���a)�uR��5K;���F�m�C�#���6�Gd���lk���7{]�䐳P�m�C�#Y�Q"m���m-�d[]�ե��r
�1�Y��6?�dG��DZ���3��BL���u^]�*��.jbڬ
:ڬ�dG��D�l��"��B뻭���ł.�h��	�ǈ�� ?",�c�R�����g�A�GtAG����H ?F����`!",�~:��Q���tH�UAG����H ?F����`!",���:�q-��m�C�#���n6,eg��BD6X�\��ԗc֍肎6�!ّ@~�H��#��BD6Xh�x�k�6���6�!ّ@~�H��#��B<6�g�N��_��y
9��Y��f=$;ȏi�A~D6X�ȶ���+_����<�m�C�#Y�Q"m���m-�d[]�핯�Sm1�f]��6?�dG��D�ٰ��ɶb�������W���5���6�!ّ��(�6�Gd���l��z�0��!۱>:ڬ�dG�1"m6ȏ��ذ���u�r������zHv$�#�f���l��j�F����j�*�h��	�ǈ�� ?",Dd���݋���5晶.�h��	�ǈt�a9>����yz7�������~�Ԍa��1�'|FP׮K��3���c8�h�@�lB��c��`NĹ>�Ky��bU��c5 ;蚑h�A�D6x�ǆ��o����*�貞�	t͈�٠="Dd[���R��E�m�C�#Y�Q"m�u�m-�d[����9[	��@|�͏!ّ��(�n6|@�d[1��Bm�����r2f!�.�h���ʏi�A~D6X����������!mVm�C�#���6�Gd��xl����]�f����m�C�#���6�Gd���l���u��׿\�B]��f=$;ȏi�A~D6X���3��Rȡhς�6�!ّ@~�H�3*�����`��[��߭��+mU��f=$;ȏi�A~D6X���u�r�T�~ͪ���zHv$�#�f���l����_T�G
�_t�YɎ�cD�l��"��������J=b!ĳ���zHv���H��?�>?���x��V�U�w�l�dG����_f�^{/՚R�b�gA�?���ޑ�'"m6H�Ȇ_3���u��[=C>�{t�YɎaD�l���f������R:�(�m�C�#�:F����`!",���������*�h��	�ǈ�� ?",Dd���}�u�F�y/ς�6�!ّ@~�H7����l��Z{/y\����Yt�YɎ�cD�l��"��B뾽��9��Y��f=$;ȏi�A~D6X��n`��>����<Mzt�YɎ�cD�l��"���Һo?�r�ţ���zDv �>F����#����h+���]:S�A�t��{�d'�ڣ$���j�ɶ�a������W�e�|i�,�貞��j�i�A{D6����-��R/��/�h�����q6�G#�}X��S�5L��y�:�=;�h�AxD6�����>�t��:��GdG�1"m6���������x]�,{}t�YɎ�cD���Gd���l�й6��3�}�.�h��	�ǈ�� ?",Dd��ƽ\�����m�C�#���6�Gd��x�	������ i���6�!ّ@~�H��#��BD���u�^�J�_�uG�Yt�YɎd�G���V~L���m-t���_�n�Ԙ+m]��6?�dG��DZ�yX�1��BL��P^���Qf��/]��f=$;��%�f���l��Z;-���5���6�!ّ@~�H��#��B<v����9���T=G����@�>B�����` ����Zy�bU��c5 ;h��h�A{D6�����݊J9Sҵ*�貞�	�ǈt�3h��٠������:�.�ͪ���zHv$P#�f���l��Z;�瑢�.낎6�!ّ@~�H��#��B<6,��k��Q��AmVm�C�#���6�Gd���lk��v��@5Fں���zHv$+?J�Ͷ�c����lxn�w3���m���l�\����r��W�!�
�?���ޑ��)�n6|.�d[{2�֞e�>r�Nni�*�h���J�i�A�D6ؓ�{���9���ȳ���zHv$�6#�f���l������3��^t�YɎ�cD�l��"��B����{�ywς�6�!ّ@~�H��#��BD6X��k<5�]��f=$;ȏ�f�GL6X�����    xmD����6�!ّ@~�H��#��BD6X�>'0��XGt�YɎ�cD�l�����}N�H%�M]��f=$;ȏi�A~D6X�ȶ�������ͺ���zHv$+?J�͆�`D6<#����sϣ�!�?���ǐ�HV~�H7> `����lk������CIA�fU��f=$;��%�f���l��Z+��yA�GtAG����H ?F����`!> ��	�G�!�>:ڬ�dG�1"m6ȏ��`!�ۯ�ڬ
:ڬ�dG�_¯��f��8U��ϥ��w$�#�b�V�S��{f���ί��B��yttZωd�02	L�o�	�+���1{������zP$\�02	,Ȅ���pX����}D=qP�փ"�@��L2��#&���>̮יgL�uAG���H&�AJ&�[R��GT��Q[[�ל�3�"V|�ՏA�LV��Lk��p�#*����ې�q}��:Z�E2YR2	4Ȅ���p��ڊ|������,�h��dd8h�	�Z���K���I�s4Z��$		Hd���lPQ[/VF�Z��:���D"� �d��DL8�hm�Tz�gе�*�贞�dd�pX�N����p��ڔ�uӒ���*�h��dd8X�	1��q���j�{|]��j=(�	4��$p� >"�a|[5��[
�"S�փ"�@��L2��#&��k��c�9Hg���t�Z�dj��$��I��I���aS�-�OL�7[mE2�Od���jx6<}�-|mY>���Գ���zL$Q�@"a��lp�**��r}H�fU��g5$	Ȉ$p &"�O0�ڼ<�v-9�*�贞�dd8�	1� ���y�#π�ަ���zP$H��I�`A&|Ą������8J��S��j=(�	4�ȴ�4Ȅ���p���,��D|�f
:Z�E2��d��GL8�h}������LAG���H&� #��A�L8��������mix�a
:Z�E2��d��GL��Ѻ��:S�)�o�.�h��d5H�$p�A*���
�>Jk��s�qz�)�~��"��)�nx:��p�#*���:گ}�^��uAG���H&�AJ&���p�>Z%�6k��,��փ"�@��L2��#"<��꽽J��MAG���H&� #��A�L8����|���g��K�a��/g������ߢ���!�#.�@��L�3�`R&L�/�L��w캠��zP$��i�3�	�2�`�s��5�P\S��j=(�	��$p� >b��Gk�Q��堫U��j=(�	4��$p� >"��h^��s�8�t�Z�d22	4Ȅ���p�|�::���mD�uAG���H&�AJ&�[R��GT���}�ᨣ���|����~�d��d��j5H�[Q�p��1<s�y���V�A�Lpw��$p� >b��Gk����Ә�2]��j=(�	4��$p� >"��h�=jG�A�uAG���H&� #��A�L8��	�c�6BNe0�փ"�@��L2��#&|���e���Zt�Z�d22m8|�@����p��Z{��6��V���V�A�L�AF&���p�>k/��h)�W�
:Z�E2��d��GD8|�@ym�V�UAG���H&� �=��U��r.5淡:���?�d�2�d��	n�T��U߇��z���z�7�1&�Ȫ��h�a�?�m�L������k����Lt�Y�D��5�H2�`"&L�6?�9�ԭ
::��D2� �d��ED8,��G�l�cVq肎V�A�L AF&���p�>z���%aȖЦ���zP$h��I�A&|Ą������AϐuAG���H&� #�ϰ؟
1�ࣵs��WtЇy����zP$h��I�A&|Ą����|�zڤ:Z�E2��d��GD8,�/k�����7b�!낎V�A�L�AF&���p�n}t�W��"v�6�փ"��)�n5H�[Q��G��|��3b_QS��V?E2YR2m8,��­��p�����S�A�jU��j=(��j��I�A&|Ą��֗n��<%f�.�h��dd8h�	�d�:��i}��jU��j=(�	4��$p� >b��G�����1/�tAG���H&� #��A�L8��	�g���9�.�h��dd�pX2O����p��z&1Zm#�W�
:Z�E2��d��GL8�h�3x��bK�s4Z��$		Hd��xlX-1��k�#楖.���dD8�	1��D�	�G�=��.�贞�dH�$p+@*ܺ�
�2�N�_�נ�%T���Id�H�ٰR�ɶ.b�����{�-����]V#"������#��BD6H��7C�D}��:ڬfD"����c��BD8,���,_y��b�~���s"�@�L�1��"&d����{+]��j=(�	$��$p� >b��G�l�����ً.�h��dd�pX�O����p�ѹ��Z��p��փ"�@��L2��#&|4����J��iU��h=&�$HH$lp �.���y�������m����b�Q��J�D5��{CU�1�j�%��I�n&ʄW3F�y����s���,�贞��>�I�����p��c�1���v/i�*�n�͠H&kAJ��X�Q�>	���y����d��փ"��'2	4Ȅ���p�Q����|���Y��j=(�	4��$p� >"��h��{m!�>:Z�E2��d��GL8�h�~�|Έgς�V�A�L�AF&���p�>���h?Jj�*�h��dd��d��GL8�h�>���{H���9��D�	��$��ED6�h����9�Lt�Y�D2"	Ȅ����&Z�����G�r}ttZωd22	Ȅ���p+�t�i���~�Zt�Z�d��d�� n}D��c�t_���F�Y��V?E2��F&ya5H�[Q��Gi���8���邎V�A�LV��L2��#&|�x_�V�UAG���H&� �c�_���S�G���gA�?���L F&��L�p�U3��n�y?kِV���V�A�L F&��L�p�U3�pq��h;��g�Wς�V�A�Lpq�ȴ�4Ȅ���p���M[�F,�xt�Z�d22	4Ȅ���p������YKЯZt�Z�d22	4Ȅ����|t�6g=�D���V�A�L�AF&���p�n}�חm�]�{EtZ�s4Z��$�d$�u �m]�d[]��:K
9E�Y��>�!�HV��H>� �pk"*ܚ�:�/����V��s"�� )�d��EL8Ȩܧ��rճ���zP$H��I�`A&|D�O��:�/���ܳ���zP$h��I�A&|Ą���ӈԏ��Dt�Z�d22	4Ȅ���p�Q�/���s��:Z�E2��nx���T8��	����׷�1�V�փ"�@��L2��#&|��F�Zb6�~t�Z�d22	4Ȅ���pX,�����또V���V�A�L�AF&���p�n}T�#�6Θ����փ"��)�n5H�[Q��Ge=���r2޳���~�d��d�pX,O�[Q��Ge})6J;[��.�h��d5H�$p� >b��Ge��]kz��:Z�E2��d��GD8,�/�K����#��V���V�A�L�AF&���p�>Z{A�1g���It�Z�d22	4Ȅ���p���V�|�=Jઠ��zP$h��i�a�<>b��G�z�\���V���V�A�L�AF&���p�>�uNn9=-��փ"�@��L2��#"����ؼ�y�W���V�A�L�AF&���p�n}t�*x]�u�,���փ"��)�n5H�[Q��Gu}/�g�Ji�*�~��"��)�6�S��GT��Q]ߋ]kj�͖.�h��d5H�$p� >��<�k�:g�����<�#�+|�p��������<��C�G\2���g���D8|�P�ڣ�uS�����h�I�&$6�    ���� Ѷl1�r�ȳ���jH$��I� @&LĄ���Z~� �A���N�9�L @F����pp2Z_����0Wt�Z�d	22	,Ȅ���p�Ѹ�5�r�ĳ���zP$h��I�A&|D��g
�y�y��b��낎V�A�L�AF&���p�7�k�Q�m�1[�肎V�A�Lp_��$p�A*���
�>��E|]юZ�֮���1(��j��i?߀��p�#*����UG-�������c"����$��ED6�h-8z]ʖ�m�tAG�ՐH$ #��A�L8����S�W��~�<;���s"�@��L2��"&d��Ar�����փ"�@��L2��#&|�NE<�m�bn�tAG���H&� #��p2��#&|�xU�V�UAG���H&� ��"��ת���qiPt���n�2ad8Ȅ	�_5���s����[Q�փ"�@&�L�0��f7y�ӗ�E��m&��փ"��'2	�~~>,|�ڙ��L�K�g�w[mE2�Od�p�̟O��g���������փ"��'2	4Ȅ���p���9����p �ժ���zP$h��I�A&|D�7�Q���)�ǜ5�,�h��dd8h�	1�ࣶ�����s��鳠��zP$h��I�A&|Ą���7nG�]����V�A�L�AF��A&|Ą������y���xt�Z�d22	4Ȅ���p��:��<��}XH�UAG���H&� #��A�L8��?�G��sgȇ�ς�V�A�L�AF&���p�n}t��w}�ԯk��V낎V�A�LV��L��­��p�t��W�vJx|�ՏA�LV��L>��p�#*���>�/���� �ժ���zP$�� %��A�L8��	�g���|�Y��j=(�	4��$p� >"�'�h}.6ϒBv |t�Z�d22	4Ȅ���p�Q��19�u�.�h��dd8h�	1�ࣾw��9V�Y��j=(�	4��t��d��GL8�h}/���-A�պ���zP$h��I�A&|Ą��ƽ��Ŝ��,�h��dd8h�	�	|4�uN>bNrt�Z�d22	4Ȅ���p������n�c�:Z�E2YR2	�j�
�>�­��i����/�u�.�~��"��)�6<[R��GT���u�`�j�z��jU��j=(��j��I�A&|Ą���i��IL!��<:Z�E2��d��GDx���F;G�yZ�:Z�E2��d��GL8��������~H�UAG���H&� #��A�L8��	������%pU��j=(�	4�ȴ��
��'<��:g�#������>V��Il_��Y�Yς�!�#.�@��L�3�`R&L:�:�:f�U�*�h��gd8�	���B���%��wr����zP$��I�A&|Ą[����3��#�,�h��d5H�$p�A*���
�>��I,_�x���\|�ՏA�LV��L+P��GT��QYk��q^�I��Zt�Z�d��d8h�	1���K�VR��f����zP$h��I�A&|D���
�9������l��,�h��dd8h�	1�ࣶ.i�2�V�邎V�A�L�AF&���p�>���4Ϡ�2U��j=(�	4�ȴ��>b��Gk��1�LA�V�փ"�@��L2��#&|�vQ��u�]��h=&�$HH$lp �.��;����N��U��g5$	Ȉ$p &b­���Wm��ko]��i='��
��I�V�T�uneDya�V�U��[��d%H}�h�u������?fQ�.����ܒ�ʄ�I� &~�L8���ڝ:fڇPt�Z�d�02	dï�����^�3F	�/��փ"�`-#��A�L8��	��;=�\c�6邎V�A�L�AF&���p�>Z+`^���ܱ肎V�A�L�AF����T8��	�+`漎K
i�*�h��dd8h�	1�ࣵ&�s�ɳ���zP$h��I�A&|D��b��̾�u��������zL$H��H��@"\Dd[�{�K�)h��.���dH�$p+@*ܚ�
�&jkG�я��JH�U��;���dHɴ�̟
�.�­��Z�Rj*9�G�
:Z�E2Y	R2	,Ȅ���p�Q��F�3h�I]��j=(�	4��$p� >"�a�|�׾̠}��9ڬ�D�<��A�<4x�������"6�|�:��= ���H$pPb��A}mΙ�qļ����c"�@}�L�p�&Z{��1�����փ"�@��L	2��#&|��L��q����փ"�@��L2��#"�_'^�Tb�N낎V�A�L�AF&���p���QY'^w,���%�,�h���>�I������a���v��ς���d��ȴ�f�<�>	���O��ي�Y��j=(��|"��A�L8��	��"����ĴZt�Z�d22	4Ȅ����	>�?����� �Y��j=(�	4��$p� >b��G�3�k�����ς�V�A�L�AF&���p�>Z�$��sF�W?:Z�E2��n�8@�L8��	��$r+9�Lt�Z�d22	4Ȅ���p��z&�ʜ#�Zt�Z�d22	4Ȅ����>Z'�\��>�Y��j=(�	4��$p� >b­���WN��o<�պ���zP$�� %����p�#&���u���7r}���p�_΄'k��l���k�3�6Q|H#.���)�6<[�S�֤T�5i����c�<�U���c"������&���D6H��ǣ��7;�ZRt�Y�Du3"	Ȅ����&���C掘D]��i='�	��$p .b�AF�9�l�����z�F�1�D�@B"a��lp�*Z��c^o�Cڬ
:���D"� �6�� �p0&:/x)%���yttZωd22	Ȅ���p�ѽOzK3䰆gAG���H&� #����L8��o��A�u�|г=]��j=(�	4��$p� >b­�����/d)󳠣�zP$�� %���	OȘp��<���Z;B�||�ՏA�LV��Lޭ�p�#*��(��Fs��sY�:Z�E2YR2	4Ȅ���p������8z�,ς�V�A�L�AF&���p�~����(���f��jU��j=(�	4��$p� >b��G�Weo�Zt�Z�d2�-¯z��y�Z�1KuA�?���L F��e�T8���p�U�U;���AU��j=(�	d��$p�	�j&.n�:�'��W�s4Z��$�KB"a��lp�+��Z�s��Z��:���D"� �d��DL�5QY���9z�g;����zN$� %��� �p�"*�ʨ��^�kb�Ku��[��d%H�t�',�­��p룲��ԣ�����փ"��)�d��GL8�h�~��C����zL$H��H��@"\�c����@��uԖΘ;,]��g5$	Ȉ$p &b��D킟gm1k,U=G���H"�!���~D6x���u���W�ڬ
:���D"���6��S�`"&Lt^��k�5��.�贞�dd8�	1� ��.c�<C��xt�Z�d	22	,Ȅ���pX _֪�t���Dt�Z�d22	4Ȅ���p룺vH?�Q�^�낎V�A�LV��L��­��p룺�*�ˈ1�j]��V?E2YR2m8,��­��p룺vH�G
z���9��DY	2	Hd���lPQY_�|mM�fU��g5$	Ȉ$p &"�a�|]���c�#捥.�贞�dd8�	1� ��V��6��p��փ"�@��L2��#&|�����Sڟ�փ"�@��L�p�>:���y^�u��Zt�Z�d22	4Ȅ���p�Ѹ��]l!�V�փ"�@��L2��#"�_	��s�������V�A�L�AF&���p�n}t�$X�J?R�yE�:Z�E2YR2	�j�
�>�­����W��[H�����ǘH"+AF�͆u�L�u�mUt"���(���!    mV}VC"�� )�d��DL8�h-�9z�!�=::��D2� �d��ED8��N�_�z���V�փ"�@��L2��#<�	_]��^�������/x����&mk?��[�Y�:�T��d�32	΄�I�p0�ZoTҺ�i�*�h��gd�?<�(�
�2�`�s]��2�nTAG���H&8#��A�L8��	��F��K��V���V�A�L�AF&���p�_&�{��Ts�J
]��j=(�	4��$p� >b«��;���ף��w�Q��h=&��} ������ò�s��z��~|���!�H��6�|�@��O��q}�7{+9B�ς�N�9�Lp_��$p .b�AF�9Jϩ���pς�V�A�L AF&���p�^�G뛼�G�A�V�փ"�@��L2��#&|�>˛����'ς�V�A�L�AF&���p�>ZO#z;kȉς�V�A�L�AF���A&|Ą��εu��V;�Lt�Z�d22	4Ȅ���p��Zo��<C6�|t�Z�d22	4Ȅ����>b�0{�ժ���zP$h��������m|���v�Y�����%��	%���L�p������:����8R�x��,�~��"��L(�6�[�P��WM�ۋ��P��u}�S#��?:Z�E2ًJ&���p�>Z��_�x#bA𳠣�zP$h��I�A&|D��ࣵ��H�����]��j=(�	4��$p� >b��G��0�)�I�.�h��dd8h�	1�ࣵO�Y�9��Y��j=(�	4�ȴ�4Ȅ���p���)=��������zL$H��H��@"\Dd���Z�4�y�[t�Y�D2"	Ȅ����	&Z����]K�y��::��D2� �d��EL���u*ߵ�-�!_S?:Z�E2Y	R2	�Z�
�>�­��S�^W8)Ϡ�d����~�d��d����­��p룼��*}�|�,�h��d5H�$p� >b��G�����s��:Z�E2��d��GDx�0g�5Ǽ���փ"�@��L2��#&|���*��A˨uAG���H&� #��A�L8��	���Țg��]��j=(�	4�ȴ�d�
1�����Lg�#Q]��j=(�	4��$p� >b��Gcm4ǈ�U���V�A�L�AF&���p�K��S{�gT�UAG���H&� #��A�L8��	�>*�D�e�l邎V�A�LV��L��­��p룲�Iԙ{Ȏ�ς��1(��j��i�a�<n}D�[��L�k+�V���V�A�LV��L2��#&|�vN�e�$pU��j=(�	4��$p� >"�a�|Y;���)r�y��:Z�E2��d��GL8���9�\���t�����zL$H��H��@"\Dd����j�1/�tAG�ՐH$ #҆�2*LĄ���ӈy)��gAG���H& #��A�L8��	���q����h�I
$$6(�����]�_c����i���?�O�mBY�PF�a�Y�1�j�%��I�n&ʄ[��{�Q:��C����s"���)�n�M�[�R�VGr"b/A���1(��Z��i���*���
�>�El5��&��փ"��)�d��GL8�h=A9f���Pt�Z�d22	4Ȅ���p�@��{A=�ȉgAG���H&� #��A�L8��	��⿏�ԟ�փ"�@��L2��#&|���9r�+W]��j=(�	4�ȴ�|�*|Ą���^Pg�G��jU��j=(�	4��$p� >b��G�^P��t�
:Z�E2��d��GD8|�p��X�z�-h�I]��j=(�	4��$p� >b­���Ū�֎��뺠��zP$�� %����p�#*ާ�������o�cH$�/��<�*�
���"���ڽw՘9hsI]�����DV}�D�1�� &�xS�V�UAG���H&P��",�ok��r�r*����L�sK&	#��A%L8���p�U�g��2s��*�h��2ad8Ȅ	�_54}m�9�k9JH�UAG���H&��ad�pX�O����p��:���W��`H�UAG���H&� #��A�L8��	���R
ۅNt�Z�d22	4Ȅ���pX���W���Gi�*�h��dd8h�	1�O�u@_�*�hGĽ鳠��zP$S�D&��O��O��k�|*�l�eς���d��ȴ�f�?�>	�����)��Q��h=&��| ����D6�������Z��"{t�Y�D2"	Ȅ����LTﭾ^wwA�V��s"�@��L2��"&d���T_���փ"�@��L2��#&|�V�ԣ\'P��Zt�Z�d22m� 2��#&|t�O[k!G	>:Z�E2��d��GL8�h\FL������z�F�1�D AB"a��lp�=AE�i�����M�s�X����xA��xh0m�s�"�/t9��Y������DVz�D�ң­�p+��V���ˈ�.�~�c"���(�nx9���p�!*ܚ�:C0�=���gAG���H&�@J&���p�>��k�:90�ժ���zP$h��I�A&|D�'�Q�oK�^��R���V�A�L�AF&���p�>j�	r�GT�UAG���H&� #��A�L8��	��\��s�Zt�Z�d22mx2��#&|��{]h^[&��Zt�Z�d22	4Ȅ���p��z��g�cP]��j=(�	4��$p� >"����?����V���V�A�L�AF&���p�n}��3�s�5䀆gAG���H&�AJ&�[R��GT���u�������Q��F?�DY	2mv�d����l�����G+1�uAG�ՐH$+@J$�� �p0^���<��~���6W�
/z��>S�>]�1�z�%���I�n&,J�7�h���V�s��:Z�E2����f���L8�t}�7g�vAi�*�h��gd8h�	1�ࣵƨ�~���,�h��dd�p��
1��s�՚�nTAG���H&� #��A�L8��	�o�jn-d;�gAG���H&� #��A�L8�������v����,�h��dd8h�	1��G���k�.sR��j]��j=(��j��I�V�T��n}T���N�A/^u��[��d5Hɴ��Qn}D�[��|楒��eH�UAG���H&�AJ&���p�>*׍�ˇ=�Zt�Z�d22	4Ȅ���p�eB������{=Gķ	ς�V�A�L���?	/������{�#�!kן�m��/��?	??	~��z�?�,�z|�ՏA�L����� n}D�[Q^���jU��j=(��j���~�e��U��^*�Lt���n�2ad8Ȅ	�_5��W�^���R�!�ς�V�A�L F&��L�p�U3�pq�^�\;�/��փ"��↑I�A&|Ą��֒��(%d��gAG���H&� #ӆg� >b��G�KG?��V���V�A�L�AF&���p�>Zg%��E�x��,�h��dd8h�	�|��T�y�$pU��j=(�	4��$p� >b­��E�����Z��uAG���H&�AJ&�[R��GT��ѵ�:}��!���o�cP$�� %ӆW�A*���
�>���-��B�,�h��d5H�$p� >b��G����zi�:Z�E2��d��GDx��9�q�:���z�F�1�D AB"a��lp�*�ϯi��\{�z�.��@�?B a���l�����i?��邎6��H�>F��>&,Ą����2�<�u�{H�UAG���H&�#���L8��	�$����/�ժ���zP$H��I�`A&|D���up��*�HA�Ҫ���zP$h��I�A&|Ą[�up�(���:�պ���zP$�� %����p�#*�����/y�3�K�{�я1�DV��D�=��l�"&۪�Z�|�[�5d������jH$� %��A�L8��	��mA+-�R�,�贞�dd8�	�L���,�9��    	iH�UAG���H&� #����L8��	��9�()d��gAG���H&� #��A�L8��	�u>hg�B]��j=(�	4��t�,����GL8��\烎��|��,�h��dd8h�	1�ࣵ^']w�A�V�փ"�@��L2��#"���.i�ZBJ{t�Z�d22	4Ȅ���p룺>�?zI%�fKt�Z�d��d�� n}D�[�t߽�k�pH�U��[��d5Hɴ���
�>b��!_[�gͳ�~��)�&�u>u+�Z�2���cH��K&+pJ&����p0)&]T���=CZ�
:Z�E2���g���D8|�P�!<G�3�kj]��j=(�	��$p� >b��G���g��]��j=(�	4��$p� >b��G}��K9�y��փ"�@��L�&P��#&|tg��Pς�V�A�L�AF&���p�>Z���ԯSBZ�
:Z�E2��d��GD8|�Pת�:Z�
Et�Z�d22	4Ȅ���p�v��6���Ot�Z�d��d�� n}D�[��Eg?k�1�_u��[��d5Hɴ��O˘p룶��s䠥R����zP$�� %��A�L8��	�m���*�ժ���zP$h��I�A&|D��g
������/�CZ�
:Z�E2��d��GL8���{��ן��V���V�A�L�AF&���p�>b�6{�ժ���zP$h���~���&���B��TA�?���L F&��L�p�U3��^?�<kȑ�ς�V�A�L F&��L�p�U�俭ca�9S�U���V�A�Lpq��$p� >b>z�F���K��Q�|���փ"��'2	�~~>,|��)G����Y��V�A�L��nx7K����Ix����u��jU��j=(��|"��A�L8��	������2ؐV���V�A�L�AF&���p���Gk��:gY��,�h��dd8h�	1�ࣵ-Tyݧ��zt�Z�d22	4Ȅ���p�ѽ/T?S��gAG���H&� #ӆg� >b��Gk��1J	���Y��j=(�	4��$p� >b��Gc]�^��Zt�Z�d22	4Ȅ����>��6��=�.�h��
��<����!�Z�:�/}ճ��큟M�Id�GI$p�>*�:�
��N��_G9J�����o�cL$�U%ӆW�>*�z�
�&Jk�����b�{낎V�A�LV��L	2��#&|��;�Q�V�փ"�@��L2��#"���֦鵼.d#�	~t�Z�d22	4Ȅ���p��z1�#���n]��j=(�	4��$p� >b��G�9D���J�z�F�1�D AB����@"\Dd���G��|�,���dD8�	1�`��"�y����,�贞�dd8�	�'��~q"tG�
:Z�E2��d��GL��Q^O#�<��������zP$�� %����p�#*���>Up-�1Wd����~�d��d��a5H�[Q��G�T�r���.��փ"��)�d��GL8�h�+x��1OtAG���H&� #��A�L8�����p��c>?t�Z�d22	4Ȅ���p��>`�(!�J<:Z�E2��d��G<��|����Ik�r��8����&\'#^u�r�ĳ�cD��K&�7#�n|�@��G��l���?��Վ�jS��j=(��|"���'��pk�{�w�E<�5�l��d-H�$��I�� ��a�������$���V�A�L��d��GL8��^��{*��MAG���H&� #��A�L8��	��̳D�l
:Z�E2��6<��p�>Z��Zmg��jU��j=(�	4��$p� >b��G�>������t�Z�d22	4Ȅ����>����}j��jU��j=(�	4��$p� >b­���y����t�Z�d��d8<+c��an}��W\g=b�����ǘH"+AF�ͮցL�u�mU�W�g=�~Ъ���jH$� %��A�L8��	݋�k̺S��i='�	��$p ."��h��:����6�ժ���zP$H��I�`A&|Ą�������w=G���H"� ��"���Y0��3bcS�����%��i�;��	�_4��s-�L�Gpj
::��D2�H�"a��7̈́Å͸��K�1ϏuAG���H&��ad8X�	�'�h���y����t�Z�d22	4Ȅ���p����u�_���#Z�:Z�E2YR2	�j�
�>�­��:
��y}��jU��V?E2YR2m���­��p�k1����8d�Y��h=&��J��H��@"\Dd���)0m���At�Y�D2"	Ȅ����	&Z������iU��i='�	��$p .b�AFmݷ�Q�ܭ
:Z�E2��d��GL8�����֔b�YꂎV�A�L�AF�>� >b��G����[z+�:Z�E2��d��GL8�h�|�q�)�h��dd8h�	�P>��k�c���B����zP$h��I�A&|Ą[�udr;F�k��r�6�!�<V��<��$����hk���B�qm����z�X����c��lX�d[�0�V?��'$�,�W�=V"������#��>D6����q.�)�h����cD8��	� ��g�ͨ������zN$菑I��?&\Ą����ȯ�+TH�UAG���H&� #����L8��	�g��:��J���V�A�L�AF��5�T8��	�C�Sm�y֭:Z�E2��d��GL8�h={8�1[�:]��j=(�	4��$p� >"�aY|Y�r:rT�UAG���H&� #��A�L8��	�>��9��G���փ"��)�n5H�[Q��G5]��WՈ��L��[��d5Hɴᰨ�
�>�­�jާFl����h�Id%�H$lp �."�AE姣�%� w���>�!�H @F$�� �p0��k]�-���R�}�S"�@�D����!{�ý��Ƨ~�>3s��="|�7�]W�ΐ9MAǄ��H nF$����pp(]��GZ��9���D��	�6�?`���D6tm�ԯc�c�낎>�!�H nF$����p0&��xj}�����s"�@��L2��"">A���S�A�V�փ"�@��L2��#&����]��Ҏ��Bt�Z�d��d�� n}D�[���S}�M����1(��j��i��'T����k���g�8$�t�Z�d��RF&���p�>Z��<�v���փ"�@��L2��#">Ah�;G.A�낎V�A�L�AF&���p�>Z;���u��.�h��dd8h�	1���H��rv�)�h��dd�p�"�
1��smI�Z��[t�Z�d22	4Ȅ���p��z&�Jm9湨.�h��dd8h�	��i�}�[�V�փ"�@����_��^o���ӑr�;�gA�?���L���?	??	�>lK-����g�w[mE2�Od�p���O��g_[>�����,�h��T>�I�A&|Ą��֞O����γ���zP$h��I�A&|D�7�Q]�Q_f��w=G���H"� !����D6������W)�Q>!mV}VC"�@��H2�`"&L�vEu�q��,�贞�dd��d��EL8�h��t���jU��j=(�	$��$p� >b��GkW��J	y�,�h��dd8h�	�'�h^����Ey	�,�h��dd8h�	1��Gi=����=���փ"��)�n5H�[Q��Gi=�裦��Y��V?E2YR2m���­��p룴>nK����'����zP$�� %��A�L8��	���z���Y��j=(�	4��$p� >"�'�h�Ur/9�E�.�h��dd8h�	1�ࣵ7��9�|L�,�h��dd8h�	1�ࣵT�g��*�h��ddZ�r�A&|Ą���3�1�j�    *�h��dd8h�	1�ࣵT�s�|��,�h��dd8h�	�	|��I��jԨG(����zP$h��I�A&|Ą[�{��\���z�F�1�DV��D¶d����l��� �q�3�ϴ.�~���H$+@J��V�T�5nM��ӈ��1�~Ъ���zN$� %��A�L8��	���K�5�ĬgAG���H&� #����L8��/ࣵ��у�}�s�Y���xA� yh�Z{��s��At4�{@$���H�>&Ą+�ߦ{Um�J�u��'�������zL$��D��K����Ix����`����Z�
��j�T>�I�����Ix��z=B�G??��Y����zP$���L2��#<���zQ�������ˀt��r"�h`���c������1SC�G\2���g���L8�t��H�L��{7Kt�Z�d�32	΄�I�p0�:��>�����w�TAG���H&8#ӆw� >b��G��^?����w=G���H"� !����D6����#Xj*��K��N���>�!�H @F$�� �p0~Z]�_W{�HGL�uAG���H&+@J&�[R��ET��Ѻ���=�#毴.�~��"��)�n-H�[Q�p�x?G�s~���Y����zP$ܗ22m� 2��#&|��u�1�(����V�A�L�AF&���p�>Zo<r���G~��R�փ"�@��L2��#"|���;�yԚ>�U��,U��j=(�	4��$p� >b��G}�l#~ѻY����zP$h��I�A&|Ą����������f���V�A�L�AF&yd��GL8�h��u���Ut�Z�d22	4Ȅ���p���[�V�փ"�@��7������׻ǘ߆.����ܒ�ʄ�I�V&T��US��W�6��z��=���o�cP$��	%���L�p������M^w����v�f���V�A�L�→i�3h�	1�ࣲ�^^��4���m�C"y@�?�G� @<�C����|��J�]���h��xH!���{D6���.��up�Q[�1K�tAG�ՈH$�#��AzL8�	����s�1O�tAG���H&�#��AL8��	���q������zL$(��h�a]?�."�AE��_"	i�*���dD8�	1�`���!�&w�JttZωd22	Ȅ���pX_ֳ�:�y�|��:Z�E2Y	R2	�Z�
�>�­��Z�2[�#�W����ǠH&�AJ&�[R��GT��QY�ʙF	�U���V�A�LV��LK�p�>*��3b+��Y����zP$h��I�A&|Ą���È2�<cVb邎V�A�L�AF&���p������1���!�V�փ"�@��L2��#&|��G�1��o{7Kt�Z�d22	4Ȅ���p��z$q�҂�T�s4Z��$	m6��g��ED6�h=���u��gZt�Y�D2"	Ȅ���p0��.l�ڣ��UAG���H& #��A�L8�����u=�ȭ��j]��j=(��J��I�ւT��n}T�ӈq���yE����ǠH&�AJ&�[R��GT��Q]O#r�e�|Ʀ:Z�E2YR2��K�p�>�߅���!�V�փ"�@��L2��#&|T�N+g�����փ"�@��L2��#"���4��҃�C��փ"�@��L2��#&|��Gg�A+�uAG���H&� #��A�L8��	����я��ￗs�Y���x�����D4x���'�O#hՉ.�h���H"P#��A}L88�7����o5�.c�������c�����m��=�tAǈ��L mF&����p0(�$����tg��9��DYu3	ۺ�ɶe��D�zxr�Y{�e�.�~���H$�nJ$�[R��DT�5Qۻ=�3�FA�s�YO�$��c$�l�������I{]�\Cꂎ>�!�H�?F$����p0&�W�}��::��D2� �d��ED8|���S����)�.�h��dd8X�	1����$��_� ���V�A�L�AF&���p�>ZK�F��)�!�V�փ"�@��L�#P��#&|��C�nUf	��R�փ"�@��L2��#&|t�1:F��jU��j=(�	4��$p� >"����;��a�q�%bI볠��zP$S�D&��O��'���~�P��s�γ໭6�"��E#���'��'����#�^Zy�,�h��4?�I^8��p�>b�*{�ժ���zP$h��n~�u���r�oCt���n�2ad8Ȅ	�_5>�W�v~�Ԩ�U��j=(�	d��$p�	�j&.n���<���BZ�
:Z�E2��#��A�L8��	�k��Rb��{t�Z�d22��r��p�>����Ẑ�gAG���H&� #��A�L8��	�ua8ӵ�DH�UAG���H&� #��A�L8��O�G�I|�]��D��}t�Z�d��d�� n}D�[��.i�QC�fx|�ՏA�LV��L��­��p�us��k�!�-��փ"��)�6<��p�>*�v��b�uAG���H&� #��A�L8��	����?�
\t�Z�d22	4Ȅ����>jk���ڃ5�ժ���zP$h��I�A&|Ą����������Zt�Z�d22	4Ȅ���p�ѹ.i��A�PtAG���H&� #ӆW� >b��Gc���_�b�l邎V�A�L�AF&���p�>Z�`Rn%�O�w=G���H"� !����D6���nVE��嫕�����,���dH�$p+@*ܚ�
�&�N,_���c�u��;���dH�$p+@*ܺ�
�2�N�_�<�g!�V�փ"��)�6���p�>Zߋ����v��փ"�@��L2��#&|T/��{T���9��D�	��$��E<6����>4�_�C��Yt�Y�D2"	Ȅ���p0ѽ�S3��a邎N�9�L @F&�� �pp2Z_��x��
i�*�h��dd�pX,O����p��z1�9�^\ꂎV�A�L�AF&���p�>Z#r?c��s�Y���xA� yh����zq��F��n]�����DV}�D��­��p���4�8b�����ǘH&�>J&�[�Q��CT�5QYO f���!�V�փ"��)�nx���T8����z�Ϋ5�������r&>J(����gw}�2���cH��K&8#��A�L8��	�޻��t���jU��j=(�	��$p8&%�ᣄ���sm����z�F�1�D�oB"a��lp�*ZO^���!mV}VC"�@��H2�`"&L�������Y��i='�	�ȴ��9.b�AFk��v�:�.�TAG���H&� #����L8��	��(3���v��փ"�@��L2��#">G�O?������փ"�ྔ�I�V�T��n}�O?l}1�}t��[��d5H�$p�A*���
�>�O?<S�Q�V�փ"��)�6>G���GL8�h�\u�:{P�UAG���H&� #��A�L8��	���Q[KꂎV�A�L�AF&���p��$Եs�uzmj�*�h��dd8h�	1���Lb�-=B��փ"�@��L2��#&|�xa�V�UAG���H&� �#���z����"�Y�����%Ȅ�I� &~�L8����,b�� Bt�Z�d�02	dï������|�+�>G�_<]��j=(��^�P2	�j�
�>�­�������zZ
����ǠH&�AJ&�[R��GT���u@_��Ǒ�6���փ"��)�6�S��#&|T�C�G�1�uAG���H&� #��A�L8��	���[Yۘ��Zt�Z�d22	4Ȅ���pX���W�r�a�UAG���H&� #��A�L8��	��ү���낎V�A�L�AF&���p�>Z��_��Q�V�փ"�@��L7��b*|Ą���3����Y��:Z�E2��d��GL8�h�^�H)�
\t�Z�d22	    4Ȅ���p�d����W�G��ς�V�A�L���?	/��WO��s�l|�,�n�͠H���L���_�����-�ueG�S��1^��Q�K�Azm�%���������D�c�}Oy��.h�d:��WҠN>R��G����R2��Zm�H��L���p�N>����2Δg�V�AA&Ҡ"�A%�|$�7��:-�3�C��V���V�AA&Ҡ"�A%�|������V���Zf
Zm�H��L���p�N>��`߮�)�6����L�AE��I�J8�H	'�W�>����z�F�1A"�� ��@!�\$d������y����.�D"*"NT��DB��&�n�V�֖��t��	2yJ2�(�{I�^Fe��~�uK�X�]��V���%� ��½��p�R�]�G�YeV�AA&�AI����p�N>j��z�qL���z�F�1A"�� ��@!�\$d���i���$�xI�.�D"*"NT��DB�$�c��U�g�a_N�9A&�"�$@%�\�����N��C��P���V�AA&��"�dA%�|����l����:�����N����1ɤkw^���44eHM����G&�"�?H�J8�T	'����q�{�F�w�@��� 	\�	p�N&U�ɤkw^ُ�$��c
Zm�H��L���p�~z�u"�,��\9������L^��L�{J��GR���u���m�[��u[��V���%� ��½��pN�ڏ�u0�զ`��vP��kP�������p�N>Z'B�>��z�U�M�@��� iP�	pҠN>R��Gkw^�3鷖�h�$"	
�M��E:6�M��0��)�r���`��fH���8	P	')�d�}}é�֓��6��s�L$@E&�I�J8�H	'�{}�ruS/�h;&HD
$zش/A�&	٤��%�����`��fH���8	P	')�d���hl���I�ML�@�� 	P�	p�N.�iSB[�Wퟯ�G�c[0�j;(��%(�����}$�{I^�}�jS��V����������4Ӝ�W�0��F&/I�NK��p�T+���n���#�Z�W�@�� �D�l2��M�h!�V"�_��9Ϭ6���>�!A$Z���8	P	'	ᴴ���o��1j�Y�m:m��H��L�� �pr�N2��߷Y���6����L$AE&�ɂJ8�H	'�u�q\�d�����
2��8-���GJ8�h�z9Zُ��M�@��� iP�	pҠN>R��G���x�'}�M�@��� iP�	pҠN>�i��}C_?��r)�7������L^��L�{J��GR��Q_�?soI��m��[�d��d�kP
�>�½����vl�uPmJ�M�@��� �נ$��e�R8�H	'�gc�9o�l�@��� iP�	pҠN>R��G�:@d��R�|��
2�� '*��#!�-���"�����s������L�� ����_»���~g;2�.�m�ݠ ��E&��_��/ᇃ������)GK�Zm��_d��s��½��p��:G+)g��~��נ �נ$�^�R����}T������*�w��`��vP��kP�	pҠN>��h�[�sO9��]0�j;(�DTd�4�����p��:l����I�6����L�AE&�I�J8�H	'��vg�%e_Ļ`��vP��4����+iP	')����ݳ���f�]0�j;(�DTd�4�����p�Ѽ*�ۺ�*�զ`��vP��4��8iP	'	�|t\�s�q���C�����L�AE&�I�J8�H	'�g�gI�����
2�� '*��#%���>����$}�����L^��L�{J��GR���u �u�J�5�i�-�}�_��L^��L�{J��GR���} G=KO9��]0�j;(��5(�8iP	'	�|�֓�m�lO~��
2�� '*��#���|���߮��y��A��t�9ȣ��*>��Z�2��``D�#�[�	pҷNU�ɣ�8�v�A�8�]0�j;(�D�Vdz�;�[	'�*��ѵ���Ɩ�~�]0�j;(�D�Vd�4�����p�Ѻ�~�v֌])V�AA&Ҡ"�A%�|$�O�����L���U/�h;&HD$�(d���lR���Q�'=˵}6C�H$@E$�I�J8�H	�&j����ͣ��+m:m��� %���(�{I���p�2��{·���կAA&�Y������}$�{]G���2�KU)�6����L^��L���p�N��Zs4ڙ����4ڎ	���&
��"!�Tԯo�ۼ��i�)�D"*"NT��DJ8�h]�5J��}��\��vF������<Ϥic�MҡIA��٘��=�ئ`����H|�D����pr�NZ+�f�F�q�F�1A&R�"�>%�<$�Ӗ�۱�:��^��vL��(|�H�H������L�������D��8�D	�O��?�}V;�v�ڂ�N�9A&/I�N���p�����ך�.l���Rn��կAA&��F�	poA)��H
�>��)e�G��6V�AA&�AI&�I�J8�H�E�}�ھ�zݩ��jS0�j;(�DTd�4�����p��Z��k�#�%�-h�d"*2NT��GJ8����v{�/S[0�j;(�DTdzന_
')�䣵֥�����.h�d"*2NT��GJ8�h�[˙s`�`��vP��4��8iP	'	ᴨ��mǶ%-�6���c�D$AA"�ɁB6�H�&����ͤ�Ҷ`��fH���8	P	')��Dc��t�H9�]0�i;'��(���iY��]$�{��4�n=��-�}�_��L^��L�{J��GR���X�]f-Gґ6�`��vP��kP�	pҠN>�iY���Fu�܉�.h�d"*2NT��GJ8�h�z��h5�D[0�j;(�DTd�4�����p�Ѻ�j�c&�Ʋ����L�AE�N��p�N>Z'?}�t9�5�Zm�H��L���p�N>�k��9Τ3�l�@��� iP�	pҠN>�iq�8�7{o9�]�Zm�H��L���p�N>ZW�~�������
2�� '*��#%���Ͽ|�}�r�Y�.h�d:�醟n��^~	����9��[�߶�
2�_d��>~	�=|=�[�%�ֻ`��vP�i�"�A%�|$���z&QZ�~������
2�� '*��#%�|�����r���`��vP��4��8iP	')���L�\� �>զ`��vP��4����+iP	')�����?�-�P�w�@��� iP�	pҠN>R��G��)GI�Zm�H��L���p�~��򺮅-����.q9�_��72�z��޾e<�y�qd"�+2NW�ɤJ8�t=Mُ-eCʫ^��vL���-H6�[�&�
�^E�U���lc�\;�.�D��Dz��P
�&�½�J�ol8jʉ���w�5'��(����]$�{����q���]0�j;(��%(�8YP	'	�|����>��RZm
Zm�H��L���p�N>�k)J�����6����L�AE&�I�J8�H	'��(������]0�j;(�DTdz�;iP	')�����Ҷғ�؂�V�AA&Ҡ"�A%�|������J+-c���`��vP��4��8iP	'	�|���}�o3匇w�@��� iP�	pҠN>R��G�#��)�������L�AE&�I�J8�H	����3�}I�Dm�@��� �.Pd»�A)��H
�>���޷�r�؂߷�5(��5(��נ�}$�{I^�}�jS0�j;(��5�|	xҧz��߷�:�9�f
�`�ύL$E&�I&J8}��p�T߿��s�9"l�@��� �D�	p��N�j%��ܬ_���6<�վ-h�d�/7�L�7Z�/����p��~��8s6ѿZm�H��L���p�N>�W��q�.��jS0�j;(�DTd�4�����pZ�_�50{����z�F�1A"�� ��@!�\$d���    ��^ʞ�j���	"� � '*�d"%ܛ��=n�g��N�9A&/@I�N���p�")�˨�+`JI�jm��[�d��d�[P
�>�½����r�#��-h�d��d�4�����pZ,��0��o��f���
2�� '*��#%�|t?��m��5�.h�d"*2NT��GJ8��:}�o�������
2��8-����GJ8��~q�����V���V�AA&Ҡ"�A%�|�����0}�3�k�)h�d"*2NT��GB8-�o�
�Y��W��`��vP��4��8iP	')��{�G�#S0�j;(�DTd�4�����pV�lǙ�\���
2yJ2=pZ2/�{I��G�y&1J��m��[�d��d�kP
�>�½��Z��ZҎ��m�C�<^��<@� uh�M���g�3��-h�A"R�"�>%������s�Rk�I�6��c�L�>E&�I}J8yH	'=�!f�so[0�j;(�D
Tdz�L^
')���9�~�����`��vP��4��8iP	')���b���\��.h�d"*2NT��GB8-�/{�9�M�@�� IP�lr��M.�IE�����3�ͦ`��fH���8	P	')��D�݂�o8{�#�_i[0�i;'��(�t�-�½��p/��n��w-��>[��V���%� ��½��p�To_'���V��_Ϙ����)�q���_�R���qd��d����I�pڔp݊xm�5�t[0�j;(�DWd����I�p2i��̬��`��vP����8iP	')�䣱^T�=k��-h�d"*2=pښ ����p��~������6����L�AE&�I�J8�H	'�u/�س^�ڂ�V�AA&Ҡ"�A%�|$��ք�V��WK�aж`��vP��4��8iP	')��s��<[M�B���
2�� '*��#%���}݊8��l-���`��vP���E��&��������J�mG�3>��߶�
2�_d���)���L	�=��_/c����]0�j;(�4�	pҠN>���]_i?�j|~�����
2�� '*��#%�|������ERV�AA&Ҡ"�A%�|�����Z|�PHj�)h�d"*2��iP	')��#�k��Zm
Zm�H���������u�o������H&�L��L�p�T�>��}u�1Svh�Zm�H&�L��L�p�T+����\_[)�a�Zm��ˍ"�A%�|��{]����c�-�2�w�@��� �נ$�?��p�#)��躧���sے>ն���~
2yJ2�5(�{I��Ge�����3V��Zm��%� '*��#!�$�����SZm
Zm�H��L���p�N>�넷֏3�Sm
Zm�H��L���p�N>ׯ��utջ`��vP��4��t��FT��GJ8�h=�8���$��jS0�j;(�DTd�4�����p��\?T��I�l�@��� iP�	pҠN>��踶*��9Μ��`��vP��4��8iP	')���L�����Zf
Zm�H��L���p��}T�gǺP<�ն`��vP��kP��W�A)��H
�>��3����E��o�kP��kP�	p�A)��H
�>�n����в����L^��L���p���G��hu��;������L�AE&�I�J8�H	'�g�(��ٲ����L�AE&�I�J8�H	'�{��G&I�jS0�j;(�DTdz�p^
')��}݇��������L�AE&�I�J8�H	'�g�Y��)�6����L�AE&�I�J8�H����5��Yږ$pS0�j;(�DTd�4�����p�ѽ���3�k�)h�d"*2NT��GJ���}����t��P�.h�d��dz�p^
�>�½��z&q~~�&�Ĵ�o�kP��kP�	p�A)��H
�>����W�Zm
Zm��%� '*��#!�����L���1sV�؂�V�AA&Ҡ"�A%�|�����Z�Q�u,uJ�M�@��� iP�	pҠN>R��G�3�#��w�@��� iP��Ӓ)�|��7z�7������ex��|Bx��
m=M��%=�CjG�H��L����p2�N&]'�o�g�������L$pE&�I�J8�T��
m=M�sK��a
Zm�H��L���p�N>ZOS�2�#gQ�-h�d"*2NT��GJ��Q__F;���������L^��L�7'ڬ �{I��G}=M9���_#�զ���~
2yJ2�5(�{I��G}=MimIg�؂�V�AA&�AI&�I�J8�H��
}=M���{΋[0�j;(�DTd�4�����p���4e+g֧���
2�� '*��#%�|t?��sZm
Zm�H��L�6+H��#%�|��I�m���E�`��vP��4��8iP	')�����Y���l�@��� iP�	pҠN>�i�B_�$���[��W[0�j;(�DTd�4�����p��y��:Zҷ����c�D$AA"�ɁB6�H��*��1��Ͷ`��fH�P�������>?�F;�>�����_��F$� ����'Z
��~�ϙ����4ڎ	y�(�M"���c�:��Ҿ��6xI��m�@�͐ ��PD������p2Q_7���$u���N�9A&�"�$@%�\�����u��^ˬ9�Hm�@��� IP���2)�|������a�>�f���������L�AE&�I�J8�H	'����ѳ�{���V�AA&Ҡ"�A%�|$��2��ξ���=���r�6�!AR��4	P�&��d�s�{�ߑ���4�߀ �O�pR�NR���랾���1GF����c�L�/2=p��_/��W/�u�<z�.�w�o[���/2�	���^�_,�ZД�jS0�j;(�4�	pҠN>�O�Q�~��r�\��.h�d"*2NT��GJ8��__e���%�$pS0�j;(�DTd�4�����p�ѸO�8�=���`��vP��4��t��FT��GJ8�h_'K\��t�_�@�� IP�lr��M.�IEs�+��9��m
�l��H��H�� �p2�^�D�:���3��w�@�� 	P�	p�N.R�IF�}|H�g�woS0�j;(�DTd�,�����p��n����g����z�F�1A"/AE��]��l�"%۫���,��XT�.�}�� ��$�^�R�7��MT�s���5��w�@�� ��$�$@%�\$�7��zq3뫷-h�d"	*2NT��GJ8�h=��vd����
2�� '*��#%�|��D��Ԍȯz�F�1A"�� ����@!�\$d���s�q]���fS0�g3$�DTD������p2�\o��~��8yt��	2� � '*��"!|���s��֙��6����L$AE&�ɂJ8�H	'���z�-堧w�@��� iP�	pҠN>��=ۻN]�ṷ�w��/���&��1�����ybo�Ԏ82y�K2=��\
�&�½I��??�?.�r~ ڂ߷�5(��.����M*�{]7!nsn{��BiZm��%� '*��#!|���:�s̙rٻ`��vP��4��8iP	')���$床<e�ѻ`��vP��4��8iP	')�䣱6u����[�.h�d"*2=pښ ����p�ѾV&l[�"8[0�j;(�DTd�4�����p��z�2��I7����L�AE&�I�J8�H��	u=M9���6���
2�� '*��#%�|��I��Ϥ��z�F�1A"�� ��@!�\$d{]� ��m�=i�-�D��Dz�ZҦ)ܛH
�&��@����^_=R:m
~��ל ��$�^�R�w�N�������m�9km�@��� �.Pd�,�����pښ��ӈϟ�Ԝ�Y�`��vP��4��8iP	')��#���Zm
Zm�H��7���^���6Z��S?��s#�D���)�>�J8}��o�ZJO9��]0�j;(�D2Qd�d��ӧZ	�/7�    7�<��t�-h�d�/7�L���p�N������_c�I�jS0�j;(�DTd�4�����p���a������.h�d"*2NT��GJ��Q_���{�O[0�j;(��5(���i���}$�{����<Z��%�z�7�5&H�%�H�w���]�d{����ڛ������	"yJ"NT��DB8-�n���9w��:m��H��L�� �pr�N2ZO#Ʊ�3�u�-h�d"	*2NT��GJ8�h܇��[I�T���V�AA&Ҡ"����R8�H	'���}��&}�M�@��� iP�	pҠN>R��Gs�D�[O���]0�j;(�DTd�4�����pZ,��ӈ���ݗ�`��vP��4��8iP	')��s�H=���)�զ`��vP��4��8iP	')��G�M��o�Κ�����
2yJ2=pZ,/�{I��G�]��՜�_��o�kL��KP�l�@%ۻH��*�n��=�<[0�g3$��(�8	P	'	�L��I�z�۶3绷-贝d"*2NT��EJ8ɨ�-g/I�ڂ�V�AA&��"�dA%�|������e��^[��,[0�j;(�DTd��;-����GJ8�h��{)�~ϔV���V�AA&Ҡ"�A%�|�����+���%g��K[0�j;(�DTd�4�����pZ,�)����f]�fZm�H��L���p�N>:�F�vn%鷖)h�d"*2NT��GJ��GǺU�\���~��
2������21��^=��-��*�.�m�ݠ S�E&��/�������C��{�3�w�@��� ��E&�I�J8�Ho��Lb�ZF���w�@��� iP�	pҠN>�O�_^�u������~��r%��I�:�q-��ϻ``H�#	\�	p�N&U�ɤ�>��֞���]0�j;(�DWdz�����I�p2�n9�|O�>�.h�d"�+2NT��GJ8�h�'�5e���`��vP��4��8iP	'	�|t���{Oُ�h�$"	
�M��EB6��q;r��l��H��H�� �p2��Mt_���r�w�N�9A&/@I��{J��ER8�@\g���Z��Pۂ߷�5(�D�K� ��½��p�L?Z?j�?Ӷ`��vP��kP�	pҠN>�'�h�5�>S�|��
2�� '*��#%�|t�b��ղ'�[m
Zm�H��L���p�N>�O�:�v�<<�����L�AE�~��p�N>Z���6���d�`��vP��4��8iP	')��u��QΜ̈́V�AA&Ҡ"�A%�|$���#���Zm
Zm�H��7���^gA��)�/�0��F&��"�$%�>�J��T�u�(�H�kZm��L$�n�ܼL�p��������5k�9��߷�5(���H2�5(�{I��Gu���zo��jS0�j;(��5(�8iP	'	�|t������]0�j;(�DTd�4�����p���e�os���z��
2�� '*��#%�|t�Uz�9��m�@��� iP���b)�|�����/����&�[m
Zm�H��L���p�N>�O���I�jS0�j;(�DTd�4�����pZ���7������gN�`��vP��4��8iP	')��u"Ծ��w�^��vL��$(H69P�&	�^Emmrk{�I�}6C�H^��H��K��DR�7Q[{ܶ��,��W��.�fy��y�������h���v���������4�߀ ��$�d>%�$����v���9��L�@�� �O�l�M�IB���Z����b�l��H~�H����p2�N&Z�?�{�w0S0�i;'�DTdz�@^
')�$�}���m&����
2�� '*��#%�|4�-��ܒ�q���V�AA&Ҡ"�A%�|$����>��7k�gҧ���
2�� '*��#%�|t��v��Fҧ���
2�� '*��#%���Q��m�������
2yJ2=pZ /�{I��Gץ���19Τ�E����~
2yJ2�5(�{I��G׭��_/t�yJfZm��%� '*��#!��_�
���dߎ$����V�AA&Ҡ"�A%�|������k%VJ���4ڎ	���&
��"!�T4�{��]�&�����	"� �n�A���p2�N&��C�Z0��iS0�i;'�DTd������p��:Ai�-�'S/�h;&HD
$�(d��tl�p�\Ǯ��:�r�g[��{Z8�D�.Alc�-�&�w�����F$R�"�n%����C�.���י�)�6��s�L�nE&�I�J8YT	���b�I��m�@��� �׷$���R����}4��b�h����Zm
~��נ �נ$�^�R����}4�.�2FOZ2cZm��%� '*��#!�6"�vo�l�������
2�� '*��#%�|�v�{�I_�m�@��� iP�	pҠN>R��G�y�1�v}ۂ�V�AA&Ҡ"���R8�H	'����'^�B
[0�j;(�DTd�4�����p�Ѽ���g����`��vP��4��8iP	'	�5a�cҷ}^O�����
2�� '*��#%�|�����-��)h�d"*2NT��GJ8=��.�9{��w�@��� �3Pd�����Kx��{��y�\��.�m�ݠ S�E&��/�����o;j�)h�d�����'}��}Jc9R%��`�ύL$E&�I&J8}��p�T��;�#�3�a
Zm�H&�L��L�p�T+���f��7g�Ό_�V�AA&�r����ҠN>R��G�V��m׆��V���V�AA&Ҡ"�A%�|�������֜�����L�AE&�I�J8�H?�G�}-��\�.h�d"*2NT��GJ8����%��jS0�j;(�DTd�4�����p���:��=�V�AA&�AI�~n^�R����}T�/��m9�˽~��נ �נ$�^�R����}T�3�2zK��]0�j;(��5(�8iP	'	�|��I����������
2�� '*��#%�|��I����$pS0�j;(�DTd�4�����p��X���y�ݕ�jS0�j;(�DTdz��4�����p��z&1�^fҿզ`��vP��4��8iP	')�䣵i��q���jS0�j;(�DTd�4������F>Z�$f]gK�����
2�� '*��#%�|t���m����`��vP��4��8iP	')��Gu��V��\��.h�d��dz��kP
�>�½��[?�^�Q���	����~
2yJ2�5(�{I��G׭��o+I���Zm��%� '*��#!|��֙P�:9I�`��vP��4��8iP	')�䣾����v&����
2�� '*��#%�|�΅�m���.h�d"*2=pZ8/����p��:���:'�զ`��vP��4��8iP	')���L�h�%-#�����L�AE&�I�J8�H�%�u�������V���V�AA&Ҡ"�A%�|�����K��S������
2�� '*��#%���=�$Ƒ�f���
2yJ2=pZ�/�{I��Gm=�8�>�V�ڂ߷�5(��5(��נ�}��������>?}�׻5?l�.c�(ޣm=K���4��``D�#�׷$�o%�<*��V�����{Қ8[0�j;(�D�Vd�����G�p��z����lI��m�@��� �[�	pҠN>R��G�Y�u"k�Y�V�AA&Ҡ"��)mU���GJ8�h�����P���
2�� '*��#%�|t�<:{�I_�L�@��� iP�	pҠN>�i�B[+����-�զ`��vP��4��8iP	')�䣵�����<���
2�� '*��#%����g)��m?RZmZm��%�8mU�½��pNR/}���4����~
2yJ2N�ʔpzX��{]�$n�����eZm��%� '*��#!��*�u��<�������c�D$AA"�ɁB6�H�&�C�g%� �w�    @�͐ 	P	p�N&R��D���ʰ#�m
:m��H��Lx�DT��EJ8�H��N��h�$"
�.�Gz�WjcԖ�������H$�H��H�p�D�����o��}��A�t�t��	2�H� '�(���V��{͹�����*�`��vP����(2NT��GJ���X��~e)7T�Zm��%�8-�½��p��ξ���H���]��V���%� ��½��p��ξ�W����w��`��vP��kP�	pҠN>�i��ug_�;�h9?LM�@�� IP�lr��M.�IE}-���Lz�`�l��H��H�� �p2�N&�S�clI�6��s�L$@E�N��pr�N2��!����1g��-h�d"	*2NT��GJ8�h�����s���`��vP��4��8iP	'	�P���o[W<�����
2�� '*��#%�|t��>�#��9[0�j;(�DTd�4�����𗏮������s&<�v����L�/2���^(���_«����K�觴����~P���"�����K����>N�du�_�@�� ��A"�ɁB6�H�.��v�pI9�]/�e3"D���'d���l�P_���<�u���l&�H|�@`���l2��M�)_�ѓ�l
�lf�H|�H����p��NZ{�α��6q��s�L�?E&��J8�H	'�=a��R3��gS0�j;(�DTd�,������F>Z'?]/z3�u����L�AE&�I�J8�H	'���FsOXC�
Zm�H��L���p��}t�*8���τ%d�z�F�1A"/AE��ݽ�l�"%۫�P���mΚ������vH�P	p/@)ܛH
�&��,�Z&����t��	2yJ2NT��EB� �u��h-c��+h�d"	*2NT��GJ8���vm����eZm�H��L���p�N>Z�®}
[R�M�@��� iP���A%�|��Wz���*��������>!��dҵ���8���1CjG�H��L����p2�N&]{���,���V�AA&�"�$p%�L*�O2�ڋ���2n�s����L$pE&�I�J8�H	'������Hz `
Zm�H��L���p��}T�xg�#��-h�d��dz��נ�}$�{�{/�����<���o�kP��kP�	p�A)��H
�>�k7^)��Mj�)h�d��d�4������|�6����\�
Zm�H��L���p�N>�w�mm��@�
Zm�H��L���p�N>댛��]�@��� iP��y�E��p�N>Z;��1Sn�q����L�AE&�I�J8�H	'�;��Y3N�s����L�AE&�I�J8�H��	�M����IKm�@��� iP�	pҠN>R��GkG^}�I�jS0�j;(�DTd�4�����p�#�k�oZmZm���������m����A[��?��ύL^&�L�{�H��S-��Ou[��Ϲ�#�ש-h�d�2�d�d��ӧZ���׽|��{�\������
2ъE&�I�J8�H	'�_�g�{ֿx�`��vP��4��8iP	')��u
T?Jֲ[0�j;(�DTdz��_
')����X��=I�`��vP��4��8iP	')����~�q��o�`��vP��4��8iP	'	��������Z���jS0�j;(�DTd�4�����p���e�r���e�`��vP��4��8iP	')��G}��'�����
2yJ2=pZ�/�{I��G׽|�oke�9�.m��[�d��d�kP
�>�½��}�8J�y	�`��vP��kP�	pҠN>�i�|���gƕ;�z�F�1A"�� ��@!�\$d����ZO�8�_�@�̀ ����ԧC��th��:���H�qez�o>����H��i��N�Q�I@�}��yf��l
m��H|�L����p�N&Z�@�}ۓ�9ق�V�AA&R�"�$A%�|$��2���@��8Z�*K[0�j;(�DTd�4�����p�ѹ~�~�Xr�xۂ�V�AA&Ҡ"�A%�|��{]7	��ރ�5'�`��vP��kP��7Z&/�{I��Gc=����Oڽh~��נ �נ$�^�R����}t�%��}��'=1�����L^��L���p�N����k�A���^��vL��$(H69P�&	٤�~�p��cdܦ�
�l��H��H�� �p2�N&ZO#�r�I��W/�g;%HD�$zؕ�'d���l��~�d�~�ל/�`��fH����8�O	')�d�y�b������b�`��vN����8	P	'	ᴨ�s��ѷ��jS0�j;(�DTd�,�����p��:)�ֺ��W�`��vP��4��8iP	'	���^yN`=�v-�����C�ێP���o�y����CjG��_dz�n;�^~	�^։���S�~�j7(��~�	��K��%���q�;>����2�����L^��L���p�>�G����?�6�|�x��
2�� '*��#%�|��l}og�+�w�@��� iP�	pҠN>R��Gc����s)�6����L�AE����p�N>��Y�������V�AA&Ҡ"�A%�|�����Ӕ}�q$}�M�@��� iP�	pҠN>�'��~��گ��RZm
Zm�H��L���p�N>Z�$�O�,����V�AA&Ҡ"�A%�|��{=�"��x+������L^��L���½��p��V����Α#p[��V���%� ��½��pzz�nE�G��C�����
2�KE&�I�J8�H?�G�V�ϧ=�m��h�$"	
�M��EB6�H���6���>�!A$��#}����<Z΃E[0���md"�(2��F�p�D+���^w�m{9[�K@[0�j;(�D*Qd�\��ӧZ	��5�n����H�Zc
Zm��k�"�A%�|$��Ѻ����ǘKE�����L�AE&�I�J8�H	'��N�-�αW�@�� IP�lr��M.���궮؎���r��A/?A�]���ho !��羋�3�9眼~�c3H�'I�����#�{]7�}~�Թ9?Em�@�� ��$�d>%�<$�72Q��~N�M�@��� )P�	p��N>R��G�T�kDҢ;[0�j;(�DTd�4�����p�Ѻ��8K��z�F�1A"�� �æ��J6�H�&ݗ���V���6}6C�H$@E$�I�J8�H	'���K=�#��-贝d"*2NT��EB8-���̧Z�1�~d���V�AA&��"�dA%�|�������?�ǜX�^��vL��$(H69P�&	�^Em�^f�I�}6C�H^��H��K��DR�7�}o��>�Ӧ���~�	2yJ2�(�{I�^Fm��^�uoIN�M�@��� ���$�dA%�|$�Ӣ������m��g���
2�� '*��#%�|�כ�Z�˦RZm
Zm�H��L���p�N>� �넵�u&�`��vP��4����iQ�N>R��G�l���v*����m��H��D`��lr��M*ZǢ�ڳ��؂�>�!A$�"�$@%�L$��r��������ݰ��s�L$@E&�I�J8�H	'�S�۾��j�����L$AE&�ɂJ8�H	�>�o<�I�l�@��� �נ$���_
�>�½����^ŜO�-�}�_��L^��L�{J��GR��Q_�׳�)�{�Zm��%� '*��#!����ur�V��rcֻ`��vP��4��8iP	'	���:u�������u�'|J8mD��\���0���؂�!�#�L$pE&�I�J8�T	'��3�>���1RZm
Zm�H��L�6%H�dR%�L��v��άzL�@��� 	\�	pҠN>R��G��O�8���ق�V�AA&Ҡ"�A%�|$��ք��|��}�IOL�@��� iP�	pҠN>R��G�̧�w��3bM�@�� IP�lr��M.����:�麭6��w�@�͐ ��$��}	R�7�N?����Z=���n[��N����w�"�^�R�w��e    4�˶em �����L^��L���p�N[�:�i�2{R�M�@��� iP�	pҠN>R��G�>E�]7P�����
2�� '*��#%�|4�S�����^���c�D$AA��M��lr��M*ZU��K��M�@�͐ 	P	p�N&R��D��2������t��	2� � '*��"!��&H^�}�jS0�j;(�DT�[�O��ܶo���&�f
�`�ύL$E&�I&J8}�����������V��rq�`��vP���E���������kw[�����w�o[���/2�	���^�Z���)��Zm��/2NT��GB�I>j�y��LY��.h�d"*2NT��GJ8�h�n+׻ӌ�MV�AA&Ҡ"�A%�|�������2S�C�Zm�H��L7|�H�J8�H	'�-n���iJ�M�@��� iP�	pҠN>R��G������X��.h�d"*2NT��GBx!�=ne���������L�AE&�I�J8�H	'�g��j�]0�j;(�DTd�4�����p��m�-��-h�d��dz��kP
�>�½��z&1��٧�.�}�_��L^��L�{J��GR��QYgA��<R�Zm��%� '*��#!����g��-�i�-h�d"*2NT��GJ8�h=�hc�)'ݼZm�H��L���p�N>Z�$ι�g��2S0�j;(�DTdz��4�����p��z&���SNJx��
2�� '*��#%�|�N���m�\��.h�d"*2NT��GB� �gG9g��[0�j;(�DTd�4�����p��:��uAJ�M�@��� iP�	pҠN>R½�����ַ��<�����L^��L|��½��p�Z��۞�	�]��V���%� ��½��p���o;j�9/1m�@��� �נ$�A%�|$�O�Q��ngʦ�w�@��� iP�	pҠN>R��G�>B��5�k�-h�d"*2NT��GJ8�h����L�Ym�m��H��D���+��"!�T�G�>���ڂ�>�!A$�"�$@%�L�����ӈ�������t�����{�I}:49H���\��z?�󙀓���;mM��B<�hg��V[00���H؊D����pr�N�\�.�~����F�1A&R�"�l%����{ݷ!��^R��|��
2yJ2=���	R����}��''�d�U��o�kP��kP�	p�A)��H
�>�oC�m]F��jS0�j;(��5(�8iP	'	�1��ON�9S��z��
2�� '*��#%�|t?;鵷�O�)h�d"*2NT��GJ8�h�?��q&����
2��8mL���GJ8�h��^��Z��jS0�j;(�DTd�4�����p��z"1[o%��-h�d"*2NT��GB8mL��Dl�5j�qtV�AA&Ҡ"�A%�|�������<���e�`��vP��4��8iP	')���~���(g��ZfZm�荁"^GxJ��GR���u'�����,g�-�}�_��L^��L�{J��GR�����W��W/�h;&H�%(|�Hk�����z-�;[0��2lD"�("N"Q�����'���m?�ۂ�N�9A&�"�$%�>�J8}�Y��y���s���
2�E�NK��p�N>�/��>_
��ؘ��V�AA&Ҡ"�A%�|���������th�-h�d"*2NT��GB8-��o��|)<��׀�`��vP��4��8iP	')�������T?�~����V�AA&Ҡ"�A%�|��{]7����<���Tۂ�V�AA&�AI�N���p�#)��辡�ڶ�rw����~
2yJ2�5(�{I��Gc�����du�_�@�� ���"��@!�\�c�j��r��w~\�'����	"� � '*�d"%�L��Q_Ǒ���t��	2� � '*��"%�d4.�qԞ�X���
2��n�A��p�N>�ח��Y�L���V�AA&Ҡ"�A%�|�����i���5K�`��vP��4��8iP	'	�\~��b�V[�y�mZm�H��L���p�N>���L��dZm�H��L���p���Q[w��R枲a�]0�j;(�t�"�w�����Kx��uZ��9OO��m�dj��x�%|��{��+V��s���`��vP�i�"�A%�|$�7��sZz�����m��H��D`��lr��M*��l��lK~��	"� � '*�d"%�L��I��)W��:m��H��L�� �pr�N2��I�{ߒ>Ԧ`��vP��$��8YP	')���D�>��)�6����L�AE&�I�J8�H�{��h{�m;V�AA&Ҡ"�A%�|�����.�Z�6���6����L�AE&�I�J8�H	�>*���v?�ZmZm��%���5(�{I��G׭�����v��jS��V���%� ��½�����uNC�z9��o����	���MZ�	��jȞ����qd��d����I��I&����}~u��}��
2��� '�+�dR%�L�N����%�Sm
Zm�H��L���p�N>���mߒ�P���V�AA&Ҡ"�?H�J8�H	'�/{k[�ygZm�H��L���p�N>�����H9��]0�j;(�DTd�4������|���\�TRv��Zm�H��L���p�N>Zg��%�d�����L�AE&�I�J8�H	�>��Clǵ<�Ӧ^��vL��KP��yıy*��EJ�W�}�9g)Im6��D��D���)���L	�&��iD�[)9_�l�@�� ��$�$@%�\$���z1Ϥ�X�����L$AE&�ɂJ8�H	'����G��V�AA&Ҡ"�A%�|�����ӈ��k�\J�M�@��� iP�	��H�J8�H	')^�}�jS0�j;(�DT�a�O�Z�3�LZl��\���D�A"��$B6}�ulZ�_����o=i��-�D"�("NQ����ך�j������`��vN����(2NT��EJ�����+����w�p��������V�AA����]毇�_«�����u��Ͽ�ޭ6�j5
2�_d��>~	�=�XG~�:~��,S0�j;(�4�	pҠN>���\�y]����i��,S0�j;(�DTd�4�����p�k�m���mgN�m�@��� �נ$�^�R����}T�N���8rn~��נ �נ$�߽�p�#)�������G�9��m�@��� �נ$�A%�|������&��_V�M�@��� iP�	pҠN>�'�h�o��^���n�)h�d"*2NT��GJ8�h�o2k�����n�)h�d"*2NT��GJ8�h_`j��������
2���AT��GJ8�h��1���ױ��2����L�AE&�I�J8�H	'k������e
Zm�H��L���p�~���3������n�f���V�AA&Ҡ"�A%�|��{��L��r�~U�j�-h�d��d�kP
�>�½��z&��[�I�6�o�kP��kP�i��m��½��p.99>?T�����2����L^��L���p�N>�o<���3��f���V�AA&Ҡ"�A%�|$���:���%�͟��7���
2�� '*��#%�|�V�ֳϚ�c���
2�� '*��#%�|�V�ֱ��U(�`��vP��4����+iP	')�䣵�������,w�L�@��� iP�	pҠN>R��G�j�vnIKl�@��� iP�	pҠN>���^m{�י�)�6����L�AE&�I�J8�H	�>jkk��F�m�@��� �נ$�^�R����}���c�{�2B[��V���%�8-��½���B���:?����_��|���\�h�B����ޮ��)Cj
�Ԏ82y�K2NW�ɤJ8���k�Zَ�V���V�AA&�"�$p%�L*��f�����r��+h��!y��<@� uh�MZ+    ��r��_�`����H}�D����pr�N��m���Qtw�L�@�� �O���)�<������F���>�`��vP����8IP	')�䣵��h#��Y�`��vP��4��8iP	'	�A���Fc��N���z�F�1A"�� ��@!�\$d{��Ԩ�6j�6[0�g3$��(����M$�{�{��<��/b�;m
~��ל ��$���	R8=S½���Ҩ���G���2����L^��L���p�N>Z+��<�=�٘-h�d"*2NT��GB8mM�k�Q�zoI7����L�AE&�I�J8�H	'��F�l5�g�-h�d"*2NT��GJ8�H��V���V�AA&Ҡ�"}��z�}֖�������H&�L��L�p�T+���^�uZ?���=���
2�L� '�(����i�_�u�v�Y�!L�@��� }�Qd�4�����p��V���Զ`��vP��kP�	p�A)��H
�>�m�GI"l��[�d��dz��_
�>�½�F];�k[R�M�@��� �נ$�A%�|�����YP}�#�RZm
Zm�H��L���p�N��Z���d̑��6����L�AE&�I�J8�H	'������ĜV���V�AA&Ҡ"�A%�|�������K����[��f���V�AA&Ҡ"���R8�H	'���ͽe}�M�@��� iP�	pҠN>R��G��륟sΜ'ɶ`��vP��4��8iP	'	�L~���K�e&	���
2�� '*��#%����>���o)�E_���c�D����Ȟ?d�]���ھ'������!A����[,/��_«����e?����w�@�� S�E&�I�J8�H	'�b�=��ɻ`��vP��$��8YP	'	�|�ח�֮�w)�6����L�AE&�I�J8�H	'�u�[�}O�T���V�AA&Ҡ"�A%�|��������v[������
2���AT��GJ8�h�D��Yj�.h�d"*2NT��GJ8�h�08��S�9}��
2�� '*��#!�$�kQ�$	���
2�� '*��#%���a��DT��#������L^��L�{J��GR���}�`?��d�%�.�}�_��L^��L7�n^�R����}T���_x-�ǖ-h�d��d�4�����p���/v�YϤV���V�AA&Ҡ"�A%�|$����/v�:Μg�`��vP��4��8iP	')������{ʲ�w�@��� iP�	pҠN>��9_['6�c����~���\	/d�u��V���!5CjG�H��L����p2�N&�����Z3��jS0�j;(�DWd����I�p2�ڣW�cOz�cZm�H��L���p���G�?�m̌=�V�AA&Ҡ"�A%�|��{����:{�=V�AA&�AI&���p�#)����=z�}���|�m��[�d��dz��kP
�>�½��G�l)�ӽZm��%� '*��#%�|t����Y�Zm
Zm�H��L���p�>�G��E��3�p�w�@��� iP�	pҠN>R��Gk��1KK���]0�j;(�DTd�4�����p��ڣ��v$mI�����L�AE�N��p�N>��ƭ1���6����L�AE&�I�J8�H	'����-�V�AA&Ҡ"�A%�|$��f��n��W��()W��Zm�H��L���p��}$ym�M�m�@��� �נ�=��T_��տ~̖r����ן��L$�8-�����Z
���V��g��![0�j;(��e"�8�D	�O�NK�z
_JK9��]0�j;(�D+"� '*��#!���_7�}��2��lق�V�AA&Ҡ"�A%�|������u�=i�-h�d"*2NT��GJ8�h��Σ^������
2��nx�%�R8�H	'��Ǭ_��`��vP��4��8iP	')��c=�_��sZm
Zm�H��L���p�NK��Z�]�`������L�AE&�I�J8�H	�>�k����~�lϳ����L^��L�{J��GR���us_�;�v}�Mi�)�}�_��L^��L��K��GR���us���Ζ����
2yJ2NT��GJ8�h����h#I�`��vP��4��8iP	'	�p�����ڒ�������L�AE&�I�J8�H	'�����6���6����L�AE&�I�J8�H	'��-���.0�զ`��vP��4����i�N>R��G�ޛ~)���Zm�H��L���p�N>Z�`�(�L�Zf
Zm�H��L���p�N�;���HڥlZm�H��L���p��}4�<�������
2yJ2�5(�{I��Gc��9j���Ji�)�}�_��L^��L��K��GR���X+xf)Yg�ڂ�V�AA&�AI&�I�J8�H	'�ӡڹ�[ΊQ[0�j;(�DTd�4�����pZ8?��Pu+#ii�-h�d"*2NT��GJ8�h�S��c$�g�����L�AE&�I�J8�H	'�늛������c�D$AA��M���lr��M*Z�+�y\5�����	"� � '*�d"%�L�NW*�<�~f���N�9A&�"�$@%�\$��b�q��T�HZjdZm�H��L���p�����Nf���k������ۦ��e��R�~�������Ԏ82�_d|�%|�~xx��m��c�%�զ෭v��L�/2=<�MA/��{=�%֣��}fV�AA&�AI&�I�J8�H	'����r���Zm�H��L���p�^�G��r~>'9��W/�h;&HD$�(d���lR�Zpt�q�qOi�)�D"*"NT��DJ8�h����8Rޮ�:m��H��L�� �pr�N2�k��Q���)�6����L$AE&�ɂJ8�H	'��F}?R��{�4ڎ	���&
��"����Fm��m(>�!A$�"�$@%�L��{]7$�����]����s�L^��L�{J��ER��QY+�Fǖ�jS��V���%�x��½��pzb�V����������L��@�	pҠN>R��Gk�Q��H9<�]0�j;(�DTd�4������A>R�*��զ`��vP��4�|�H��u�(gM���]0��nd"�(2N2Q��S��ӧz-ک��>�a
Zm�H&�L|'�(���V����:�e9�l�@��� }�Qd�4�����p��:���82֓�Zm�H��L���p�>�Gk�KI�������L�AE&�I�J8�H	�>����e^O�2ZmZm��%� ��½��p΂��y�9Kul��[�d��dz��נ�}$�{��,������V�AA&�AI&�I�J8�H	'�gA�^R�!{��
2�� '*��#!�$��+��ל��`��vP��4��8iP	')��u[�,�u-)��W/�h;&HD$�(d���lR�:�����U�M�@�͐ 	P�Z&/����p2�:���{R�M�@�� 	P�	p�N.R�IF���jIz�a�m��H��D`��lr��M+�+��߬m�I��L�@�͐ 	P	p�N&R½��Z�R��5�m:m��� %� ��½��p/��n��w��8s>Զ���~
2y	J2=pZ&/�{I��Gm��H��\��.h�d��d�4�����p�Ѻ[��Rs�O�z�F�1A"�� ��@!�\�c�2��v��u�R%{��	"� � '*�d"%�L4�����,u���N�9A&�"�$@%�\�����u��<j��[0�j;(�DTdz�L^
')�䣵^g;��t��-h�d"*2NT��GJ8����kKzJ�M�@��� iP�	pҠN>�i��u�`�k}�5gy�-h�d"*2NT��GJ���}��6�?y������L^��L�{J��GR���}��6G֢A[��V���%�8-�½��p��V��1b�I/.m�@��� �נ$�A%�|������J{�    ��ز����L�AE&�I�J8�H��!�u��wξ��[�vz�'�ښ��Z�R����CjG�H��L����p2�N&]ˍ�m+I/^m�@��� 	\�	p�N&U�ɤ��x��GҎQ[0�j;(�DWdz�5A
')�䣹^|lG�I�L�@��� iP�	pҠN>R��G�}�vI/jl�@��� iP�	pҠN>�ik�u�u��lI�Pl�@��� iP�	pҠN>R½����_���r6ۂ�V�AA&�AI&���p�#)�~&����ϷڜV��߷�5(�D�N�����AA
�>�½��ZwT{Y�jS0�j;(��5(�8iP	')��u^�<�#�Sm
Zm�H��L���p�N��Z{��d��K������L�AE&�I�J8�H	'��G�ܮ�)�6����L�AE&�I�J8�H	'�3�Ϲe�S��^��vL��$(H���@!�\$d����l�����`��fH���8	P	')�d"���:m
:m��H��7��������g�u&�`�f���D*Qd�\��ӧZ	�Ǻ�����v�W�}������L�� �	���^��O[�#�W�o����$z�n���]~Ȯ���u�Rn>x��	"�_D������p2����9S�:m��H��L�� �pr�>HF��2�yd�q�.h�d"	*2NT��GJ8�h����^�zRZm
Zm�H��L���p�N>Z{�Jk-eK��`��vP��4����wҠN>R��G�7���H�R��^��vL��$(H69P�&	٤���m;�ْ��6}6C�H$@E$�I�J8�H�d��9Ĩ5��w�@�� 	P�	p�N.R½�������ZOzBfZm��%� ��½��p��n�~�3eGĻ���~
2yJ2=��kP
�>�½��Z�rm$=&�����L^��L���p�N>Zw�m��)/z�����L�AE&�I�J8�H?�G�iD����=�զ`��vP��4��8iP	')���i��Τ���4ڎ	���&
��"!�T�߷���nS0�g3$�DTD��s#*�d"%�L4�E�H9��]0�i;'�DTd������p��z1fm)ˋ�����L$AE&�ɂJ8�H/���\0ΔX�����L�AE&�I�J8�H	�>��i�l�H9u�]0�j;(��5(��נ�}$�{]�	^�+�3�ᷩ�}�_c�D^��D�z*��EJ�WQ]@m}�kJi�)�D��D������p2�z��c$�����s�L$@E&�I�J8�Ho$��6�m�I�J���V�AA&��"�dA%�|�����%;�&�,���V�AA&Ҡ"�A%�|������]��3�C��^��vL��$(H��ii��M.�IE�y�{m�Mi�)�D"*"NT��D:�A���W�>�������	�6%��^�to�``F�#�[�	pR�N�iSB]'?�yG��L�@��� �[�	p�N&U½���Y������
2yJ2�5(�{I��Gm��^Ǳm9/gl��[�d��dz�)A
�>�½����߱�%i�-h�d��d�4�����p���eK�+jZm�H��L���p�N[���_��9��m�@��� iP�	pҠN>R��G�~dVϖ�jS0�j;(�DTd�4�����p��:'������n
Zm�H��L��&H��#%�|to�;׈�����
2�� '*��#%�|t�k�������
2�� '*��#!��&\�!���׾�<-�����L�AE&�I�J8�H	����>�r�g��-h�d���L�{J��GR��Q_礗ٯ��)�6�o�kP��kP�	/���p�#)��H���V���V�AA&�A�{F�T�u��~�-�_<[0��nd"�(2N2Q��S-��2�������_��`��vP��d��8�D	�O�N_n��|�u�OΪ2[0�j;(�D_n� '*��#%�|������&}�5����L�AE�N+��p�N>Z�`zM{?dZm�H��L���p�N>:��n/�L�)h�d"*2NT��GB�[�_�����g?3�T�Zm��/2^	o��w_w \�US��~�j7(�4~�	�����K���넷�>�V���V�AA���x'*��#%��������jʡV�AA&�AI&���p�#)�����m�,=����}��ט ���"�ށJ�w��=���%��ɻ`��fH�P	p�N&R��D��2�����_�}�S�D�?A"��~B6yH�&�M����Y�gS0�g3$�D�SDz�;�O	')�d�뮢�VΒr`�`��vN����8	P	')�$�����ZZ��w�@��� IP�	p��N>�'�h^"��F�ՠV�AA&Ҡ"�A%�|������]�)�_���c�D$AA"�ɁB6�H�&����uhY��hS0�g3$�DTDz�	P	')��Dc[�re��Nۂ�N�9A&/@I&�� �p�")��h��lc?[��6�߷�5(��%(�����}����Gc�~�Kmg�j[0�j;(��5(�8iP	')��uKhG�r�ق�V�AA&Ҡ"�A%�|�������v\o	RZm
Zm�H��L7��H�J8�H	'����Q����V�AA&Ҡ"�A%�|�����o�QK��ImZm�H��L���p�^�Gs��oY���+h��!��<@� uh�MZ�=�vΤ��`����H}�D����pr�N:��6��z$}�6��c�L�>E�^I}J8yHw���u��ձ�����t���p��o��V6�QJ�S�w����G���L��_��/ệ�u�V�c�x��.�m�ݠ ��E&��_���ѶNu�GI9��U/�h;&H�%�H69P�&	٤��.V��I�6}6C�H$@E$�I�J8�H	'�u��V�ۢS:m
:m��H��L�� �pr�N2�M�cK���]0�j;(�DTd�,�����p��~�Ͻ�)7+�Zm�H��L���p�>�G�f���g#�C��^��vL��$(H69P�&	٤�u���ZM�<���"�	�M���BB6Ih]gڎ���7�6�A$R�"��I}J8YH	�*��m�zʩ�N�9A&�?I&����p�")�˨�+M?�Z���P�߷�5(��%(�����}��OzV�v�]��[~��
2�+E&�I�J8�H	'�Mx}�{�yjZm�H��L���p�N>R�$��զ`��vP��4�|�H�굗�<���������H&�L��L�p�T+���^{َm�3�x�w�@��� �D�	p��N�j!��/7k7ۜ�z>�զ`��vP����(2NT��GJ8�hmh���$pS0�j;(�DTd�4�����p�Ѻ7���L���]0�j;(�DTdZ�m�A%�|��{��>b�������
2yJ2�5(�{I��G��#�^K�K![��V���%� ��½����}T�z^w��r��`��vP��kP�	pҠN>R��GkG�y���m��l�yH��=�$@�<�C���{f�{�g�4�߀ �O��WR�NR��Ac� �|L�\�]0�h;&�D�Sd�ԧ����p2���֏cOz�kZm�H��L���p�NK�뽟m�=��w�@��� iP�	pҠN>R��GkG[�gʝ�z�F�1A"�� ��@!�\$d��֖�޶��e�]0�g3$�DTDz�4^
')��Dm��G�)j���s�L^��L�{J��ER��Q[{�J�ΔM��߷�5(��%(�����}����������Z�zZ[0�j;(��5(�8iP	')��u�1>es~fق�V�AA&Ҡ"�A%�|�������3>�n�|+�����L�AE�N��p�N>�o�cK93�]0�j;(�DTd�4�����p��~}���ܒv,ڂ�V�AA&Ҡ"�A%�|$��2�v���k��)���Zm�H��L���p�N>Z���q���l�@��� iP�	pҠN>R��G�"r��SZm
Zm    �H��L�N����GJ��Q_�u�դ�/�`��vP��kP�	p�A)��H
�>��~wp��W��o�kP��kP�	p�A)��H	��}��>g�Z�oZ��y;�$ו݈N�#P��"y�����bQ`'�?v
�?߈���j>�U8(��kP���A%�|�������ǖ���2��NT��G:�J����W���?�KY�9��Ч	m�:Z�1jҐB����[&�"ӧ�i�N&U�ɤs��m�K�a�ς�V�X&�"��I�J8�T	'��󾫯GOZ����A�L$pE&���p�N�&���h�[�*),h5�e"*2�4�����p���
jێ���X0�j�DTd28iP	')�䣹T��^�~�P0�j�DTd���3)�|��{�g ����^;rU`�@�qP,�נ$����p�#)�n�^P��cIj5��ՏA�Ltw��dp�A)��H	����6�}Izz���A�L^��L'*��#%�|T�v	��5�.��2��NT��GJ8�h>�8��%}����A�L�AE��>V���GJ8�h��8ڲ%��Â�V�X&Ҡ"��I�J8�H	']� �����j(h5�e"*2�4�����p�X�:q,���G(X0�j�DTd28iP	')��#�k��Z��A�L�A�{F�U�;�>z)I�`���-�D���%�R8�����z�g����ڒ���`��8(���"���/��������~�KʆF�z�6ڍ�%�~��������n��>��k�R�e\�>�Cb��_D28	P	')�d��������W�Y0�i��DTd28	P	')�$��=�x��}��q��,h5�e"	*2}�;YP	')�䣹gz����&��P0�j�DTd28iP	')�䣹g����v��m�!�<���ch�M����6R龜�C�@������N�S��AJ89h����~>6Ki44��2���N�S��CJ8�蘗�ui%�6
Z��b�H��L�,$A%�|��{�s/��8ƚ�:�Y0�j��5(�dp�A)��H
�>Z�^P�����Y��V?�2yJ2�kP
�>R�W�u�U�Uk�ʍg�@�qP,�נ$��I�J8�H	'��bhݶ������A�L�AE&���p�N>�{Am���s���A�L�AE����p�N>�s��6��Li5��2��NT��GJ8�h�/?�>�97[X0�j�DTd28iP	'	�|4��:�ҳZ��A�L�AE&���p�N>�{A����Sl���A�L�AE&���p�N>:�ƻKْ�fa�@�qP,iP��o�A%�|��{����z���Ƃ�V�X&�AI&�{J��GR���u��q��%��o�cH,�W� ��� �h�!�{]�	�}K�1�Q/���<^|�<�&�	��!��s}���Yr.��`��0"����dp��NR��A͎vN���W/�g�KD�$���O�&	�䡹D��VSN>|���"���N�S��DJ8�h�ө�A 9�A�`��8'����dp�N.�i5�y�`}��˦�?�P0�j�DTd28YP	'����絹R�]���6Tz��c������ڗ�&]^`����[&�oI&�{}K�ޣR��h�V��Hٵ�Y��V?�2y}K2}����=*�{�i�ֶ�<���V�X&oAI&���p�N>��7ֶ���$���A�L�AE&���p�N$\���m)��<Z��b�H��L'*��#%�|4�o������P0�j�DTd28iP	')��Gm~+_εd9�Ƃ�V�X&�AI���}� �{I�t�x�Y|n�����m~����ch/@!�{H��js�%�M�K��'IdpR�N��3�6�s��᎔38���1�L�>E&����p�N&��:�<`,�����1�D$@A"c��lr��M*�G/{?�`��0$�����R8�H	'͵�m�%K�P0�i��DTd28	P	')�$�k/�u��m����A�L$AE&���p�N (ޑ}��z�F�X"����"���M�ڎ�H�k�^��m�H$�H'�(��V��/�����ν03:���9�L^$�L8-���oZ
�6�����1��/a��[���/l$��-(�{I��G}�Ϗ���տX0�j��5(�dpҠN>�i1�u���ǚ�&��cb�H��D�&
��"!�T��*�u-��1���"� �NT��DJ��D��=j9^��SF����9�L�/2}�nE���^<��`�5��s��,�m�ݠX���Lo���_����mK��3�,h5�e�~���A%�|$�o�G�7��k�s�������A�L^��L��½��p����1���a��o�cP,�נ$����p�#)�������Z��b��%�>�4�����p�Ѽy���!�o5��2��NT��GJ8�h�&o��{��j(h5�e"*2�4�������|t}�z��@��VC�@�qP,iP���A%�|�����7$elk�9�ς�V�X&Ҡ"��I�J8�H	'��=Kk��)����V�X&Ҡ"�oiP	')���:al=!��
Z��b�H��L'*��#%�|4�:8�5N���Y0�j�DTd28iP	'	��Q]�am�)��<Z��b��%��5(�{I��Gu�oY^G�ϳ�RZ�o�cP,�נ$����p�#)��������K��j,h5�e��d��iP	')��y�AǶ�<-Â�V�X&Ҡ"��I�J8�H	']g�^R6}��2��NT��GBx%��`�*��P0�j�DTd28iP	')��1�/�%=-Â�V�X&Ҡ"��I�J8�H	'];��v$�k��4����>�F��EB6�h>���ޏ�?�P0�g�DTD28	P	')�d��vg=j_��JC�@�qN,	P���$@%�\��wz�w����u��?�x��Oo�k���(��K��\,Rq���-�dp�o)ܛT
�&=��Y_e-���ՏA�L^��L��½I�p�kǘ�l{�"��2yJ2}����N>R��G�i���s^�C�@�qL,IP����@!�\$d���#����H��6C�@�aH,	P��$@%�L$��
m>A)Ƕ��z�,�4Ήe"*2������p���e�s	uJ��`��8(��$��dp��N>R��G�1J9��'�
Z��b�H��L8}� ����p���u�Z��'�X0�j�DTd28iP	')���i�������4�����M��E:6}��烈����-�`��0$�P��^�R�7��M��s�u�7)����w�1'��P���^�R�w�NO�˵��s�۳`��8(��^(2�[��N>R��G�J������`��8(��4��dpҠN>R��G�We_�
Z��b�H��w�����{��m|O������2�L�N2Q��W��ӯz������xP0�j�D2Qd28�D	�_�N7��ҒV�`�@�qP,]�(2}��_
')���Z�/)G�=Z��b�H��L'*��#%�|�����J��k��z�F�X"�� ��ɁB6�H�~��?����@s��w�Y/�e�� ������������K9���t
~�f7#��"���/��/ệ�~���1��P0�i��t�"���J8�H	'�y�}#�[KW0�j�DTd28YP	')��6��X�-�W0�j�DTd28iP	'	�|ԯצ�ޒ�TC�@�qP,iP���A%�|�����yϲ�#co�g�@�qL,IP����@!�\$d�����t�σ�R�}�!�H$@E�|� �p2�N&��g�82��u��9�L$@E&�� �pr�N2:�ͳ�-c�+h5�e"	*2�,��������h]�]��&��t��A�L^��L��½��p�u>���~.�Oi5��ՏA�L^��L��½��p�u��喜�o,h5�e��d��wҠN>R��G�iĨ��S/��P0�j    �DTd28iP	')���4b��9�eX0�j�DTd28iP	'	��h>��eY��b�@�qP,iP���A%�|�����G�82ίu��A�L�AE&���p�N>��$z���x9����V�X&Ҡ"�iP	')���Lb]�Hz����A�L�AE&���p�N>��$��G��PW0�j�DTd28iP	'	��Q��I$���
Z��b��%��5(�{I��Gש�[]���~��ǠX&�AI&�{J��GR���u���K�)��`��8(��kP��/�A%�|�����geͺǂ�V�X&Ҡ"��I�J8�H��|u���k��?_�s>%��I��)�<�!eH�``Hq�-	\���$p%�L*���	e>Mi�~$=�ł�V�X&�"��I�J8�T	'���@۲��J(h5�e"�+2�4�����p��|�R�J���ٿj5��2��>p�LA
')��y!��%�����V�X&Ҡ"��I�J8�H	'ͧ)��uOj5��2��NT��GB8}�P�'z�k���`��8(��kP���^�R���N���iʲ�#c�W��V?�2�ݩ"����p�#)����o���9{�yQ���A�L^��L8}� ����p����h�� 5��P0�j�DTd28iP	')�䣹�Um}K�&Z��b�H��L'*��#!�>V��Cܷ�&�`ǂ�V�X&Ҡ"��I�J8�H	'�g�l5�`?W0�j�DTd28iP	')���Ĳ��$p(h5�e"*2��'ҠN>R��G��f_�
Z��b�H�������w�G��|݁�`��m�H&�L'�(����i��V��r$]�b�@�qP,���$���L�p�������yB_}�[��N����я1�D��F���ށJ�w���Ut��^�a�I{�`�@�aH,��$��h��N&R��Dun�ٗ���:�sb�H��L'*��"%�d��9�m�j�
Z��b�H��L'*��#!���'���󄧤ko,h5�e"*2�4�����p��8�}+�JϔVC�@�qP,iP���A%�|�����y�r���.��P0�j�DTd��i��N>R��G�<M�}e�qL�+h5�e"*2�4�����p��q���ˡ���P0�j�DTd28iP	'	�d�<����޷�Vc�@�qP,�נ$����p�#)����G[_��� ~��ǠX&�AI&�{J��GR��Q��$jKz/�W.�f��(��A�by!�<�C����KkIcX0���D�>E"����pr�N��!Εl[��;,h4��e"�)2�ԧ����pZ&�*x������X0�j�D
Td28IP	')����ն%���^��8&��$(Hdlr��M.�IE�#��}�����Cb�H��H8-����DJ8�h>���<�+��P0�i��DTd28	P	')�$�cn"r�����2��NT��GB�[ �Σ�km�8����2���d��Kx�%�y�:oP���\{?~�j7(���"���/��/ệ_�u�Q��?Z��b��_d��wҠN>R��Gs�N9��rpó`��8(��4��dpҠN>R��G�i�8Z�8k���2��NT��GB�A>���G�&1ς�V�X&Ҡ"��I�J8�H	'�G�������Y0�j�DTd28iP	')���z�����8���A�L�AE��/�A%�|$��_^�u��V����_��/dҹ����i�Nt�z���D�oA"c���l���M���Z皾�6C�@�aH,�[��n%�*���Dש�G�{Ϲ����N�X&/@I&�{J��ER���:���I�Ƃ߷�1(��KP���ނR����}��K���Kҥ$��2yJ2}��4�����p��\o4��D,h5�e"*2�4�����p��|�R������ς�V�X&Ҡ"��I�J8�H���߲����-�	��2��NT��GJ8��z�r%��P0�j�DTd28iP	')���<b�G��Kς�V�X&Ҡ"��H�J8�H	'�UGe?��t��^��8&��$(Hdlr��M.�IE�u���
Li3���"� �NT��DBx�'�s�Q/-e��^��8%��^��O��R������׺�c�Y���3�E���D2����M$�{I^�}�i(�4Ήe���U�����SYrN�{���?�e"�(2�\��ӯZ	�_���ߏ>R��x��2�L�N2Q��W-���2���1Z�yы��A�Lta��dpҠN>R��G�(�K]R6�x��2��NT��GJ8�h�'z[M9��Y0�j�DTd��i��N>R��G�u&H�.�z�F�X"�� ��ɁB6�H�&����k����"� �NT��DB8-�?��k���G�t��2yJ2�P
�.�½�����y"vK9��Y��V?�2y	J2�[P
�>�½�������nKz����A�L^��L����R8�H	'��e���z�F�X"�� ��ɁB6�H�&��Jv-���MJ��`��0$���dp�N&�i��y_y���-e+�g�@�qN,	P���$@%�\����Ƽm��FR��`��8(��$��dp��N>R��G�<��h��y����A�L�AE���K��#%�|���5{ݓ�fa�@�qP,iP���A%�|�����ӈ�n�H�����V�X&Ҡ"��I�J8�H���)��U�6��_5��2yJ2�kP
�>�½�΃�7�����)���߷�1(��kP���^�R����}t�$��J�{�w�X0�j��5(���y)�|�����G��5�E,h5�e"*2�4�����p��|&QK=�{Ii5��2��NT��GB8-�o��Vǹ'qJ��`��8(��4��dpҠN>R��G�Dcm9KO�`��8(��4��dpҠN>R��G�ľ����Z��b�H��L8��%����p��|&����mJ��`��8(��4��dpҠN>R��G�<๏�rH�`��8(��4��dpҠN>�i��y�����>����X0�j��5(�dp�A)��H
�>:O<�^���bJ�����~�e��d2�נ�}���{�W���Տ���_�7|�|;���4�s��8r޴c����[&/pI��>P��ɤJ8���o�ײ$]Pb�@�qP,	\���$p%�L���I��Hy�5�3,h5�e"�+2�4�����p�L�<�<����Z��b�H��L'*��#%�|4/�uM9~�Q/�hKD$269P�&	٤�m��n��r ��P0�g�DTD�\�
R8�H	'����l%�mt��2� �NT��EJ8��������(��2��NT��GB��L����7��<?���~��2���d��Kx�%�y�:�[ƚ��޷�vcb����	��L��=�̗�G��Z�Y0�g�t�"�^H�J8�H	'����Cooן��9�L$@E&�� �pr�N2��!zmKʵ��`��8(��$��dp��N>�+�h>�8������,h5�e"*2�4�����p��U�W����V�X&Ҡ��"���>P벟�zS���`���-�D��o$%�~�J8���-Ǘ�e��z�F�X"R� ���$B6���l����o�~��Ji3���"�e�"��I�J8�H��D��r|�ȹ��z�>�X"�?E"c{�)��CJ���u �V����ς����"y�I"��O
�&�½����J9�%��:�sb�� %�>�AT��EJ8�h��wn�ْZ��A�L$AE&���p�N>z�͏ײ-K�"�g�@�qP,iP���A%�|$�o䣹O�6�yRNJ��`��8(��4��dpҠN>R��G�<����$�
Z��b�H��L'*��#%�|4�D����
Z��b�H��L�NT��GJ8�hn��ۚ%p(h5�e"*2�4�����p��1Ϲ{�>�z�F�X"�� ��ɁB6�H�>����(�}�R��}���"yJ"�P
�&�½��    �yi�R6�~��ӏ9�L^��L��½��p/�������r �`��8(��KP�邯YP	')��z��v�ո&���`��8(��4��dpҠN>R��G��o�[�F�s2,h5�e"*2�4�����pZ �'x��%=�Ƃ�V�X&Ҡ"��I�J8�H	'��{{��ς�V�X&Ҡ"��I�J8�H	'ͧG9w�t��^��8&��$(H�a�y%�\$d����kmI�Ȱ`��0$���dp�N&R��D�a���~���`��8'����dp�N.�i��u�`��Ȏ�Vc�@�qP,���$����p�#)���Χ��zү~��ǠX&�AI&�{J��GR��Q�O#�1��ySZ��A�L^��L8-���GJ8�h>�8?�OzN���A�L�AE&���p�N>�O#�cے>FƂ�V�X&Ҡ"��I�J8�H��ש����sK���A�L�AE&���p�^�	��?�1Wؾ/iG�ʿ�Q�'|j�J�'��$�.c��4�,Rq�DWd28	\	'�*��c��sS���z��?n����`��8(���E�?P���_½�νb��6����W�n���W��A�L^��Lo���_���b�볟_d\����m�ݘX�������}���-�=7S������
�Cb��_D28	P	')�d����>ƺ�����+(�4Ήe"*2������p�Q;/���JM�QC�@�qP,IP���ɂJ8�H	'�yy��G]͂��V�X&Ҡ"��I�J8�H	'��`t�������A�L�AE&���p�~����K�����P0�j�DTd28iP	')�����vԣ�����YP0�j�DTd28iP	')�䣹��Җ�%���`��8(��4���y@���p�N����{�Ɩ�Z��b��u�"����p�#)������e�sn�����~�e��d2�נ�}����G�f_�
Z��b���o�W=w�i�-�!2���?�e"�(2�d��ӯZ	�_�ܝ�8Z�����YP0�j�D2Qd���D	�_�N7���r���X,h5�e��E&���p�N>�������O�����V�X&Ҡ"��I�J8�H��m~�n���_͂��V�X&Ҡ"��I�J8�H	'�͕ǲ��?����1�D$AA"c��lr��M*���,���������"� �>�FT��DJ�7Q��+�z~���i,�4Ήe��d2���]$�{�yO�{�G��,�}��b��%��-(�{)����\�+��9O��`��8(��kP���A%�|��������z����A�L�AE&���p�N>j�겜�SZ��A�L�AE�|��p�N>�O#����h{5
Z��b�H��L'*��#%�|t=�(�HZ����A�L�AE&���p�N+��|q_3�.ˠ`��8(��4��dpҠN>R��G�D=/�s�a�@�qP,iP���A%�|�����g�ԤW�X0�j�DTd��iɼN>R½���c�#P~���l��2yJ2�kP
�>�½�νc����rF�}5��ՏA�L^��L��½��pZ2�3^Kk���C����A�L^��L'*��#%�|T�E����+h3��!�{C� uh�Mjs3�u{�_h(h�߀X"R�"���L^
')����X/Km9W�X0�h�D�Sd28�O	')�d�����G�Y%���1�D$@A"c��lr��M+���y�ڷ�o#zu

�Cb�H��H'*�d"%�L�ϥ����t
:�sb�H��L'*��"%�dt��*�؏�+n(h5�e"	*2}ഴ_
')��Gm������gZ��b��%��5(�{I��Gmn�4�^{�,�}��b��%��5(�{)��T���b�_9Tz�'d�'	m�3j��$�6�#�n���%�N�V�ɣJ8y�Zgt��r�Ea�@�qP,�[���o%�<���G���*{O8��j��2���>p�0A
')�䣹�r_��'�
Z��b�H��L'*��#%�|4wU��Fҝ��2��NT��GB8}�ж�Q��N�:e�M��4$�Ncd���L��I@��׮��M���{��!��{C��th��M�9棱��{��
:�7��D�H�ӧR8yG	����i���tOZ����1�L^z�L�ޓ½��po�>�:����ܱ���~�e�
�d28=S��q�N���?���N�"e,h5�e��d28iP	')��:��-�'����A�L�AE&���p�N>��S�r��M��_�@�qL,IP���7��lr��M*�����y݆}�!�H$@E$�� �p2�N&�����}�)�D�?�;>Z���Gkc��yǇ�^��m�H#�H'�(��V��}}���4���Sb�H#�D�&���{��jf~�V�X~��)(�3�E��E��h�N&R�&:��_�-�.GW0�i��T������K���u~�8�O��_�/���m?Hd�����w����y.��v��sYJ��`��0$i�E$�� �p2�N&��y�Z�s�R:��9�L$@E&�� �pr�N2j��z��̕�VC�@�qP,IP��/dA%�|����暖Z��uA��A�L�AE&���p�N>�+[�e�8��Y/�hKD$269P�&�ؕT4W����[򻂁>ÐX$�"��I�J8�H	'͕.��[�'v�`��8'����dp�N.R�IF�\O>�y�xJ��`��8(��$���7��N>R½��߾W)뒱V���2yJ2�kP
�>�½�γ��W?J��P,�}��b��%��5(�{)����<%p}�2R�渂�V�X&�AI&���p�N>��]���ƥ�
Z��b�H��L'*��#%�|4���j��_�
Z��b�H��L� *��#%�|4׼�eYF����2��NT��GJ8��\g�����V�X&Ҡ"��I�J8�H��Gs�N/ےqT�+h5�e"*2�4�����p��\���e�R�
Z��b�H��L'*��#%�|t�~��Q-I���V�X&Ҡ"����p��}dg�-+3Z���A�L^��L��½��p�2��G��G��߷�1(��kP���^�R���~x]gn�X3��s��A�L^��L'*��#%�|4��Ծ����
Z��b�H��L'*��#%�|4��,}�3��q��A�L�AE��ҠN>R��G��6ƒ�Z��b�H��L'*��#!���{����]=��^�uzΧ��
�Zzt�-� ��(�%"}��-d�Eul�@��eG[[�5����"���N�V�ɡJ89t�:ju���]�@�qN,�[���$@%�\����檣�fl�(h3��!�{��>M��C:��P�k��r���B,h�߀X"�>I"�{�I��AR�wP��4Z���X��F?��2y�I2ܫO
�R��˄�\�r�\��Ƃ�V�X&�@I&���p�N>�sݱ�=g���2��NT��GJ8���ͽ�Sis�`�@�qP,iP���O�p�N>�'�Ov����X0�j�DTd28iP	')��y�a9FַeX0�j�DTd28iP	'	��i�y����ek[K�UC�@�qP,iP���A%�|�����S��m�]�@�qP,iP���A%�|�����)�u�+�S:�W/�hKD$��L�@!�\$d{Iޏ}�f,�3�E���O���봽���5ga�����m��F$��5"��_�NK���]��dl��
Z��b��J$�N.Q��W����y�^y��-I����V�X&Z���dpҠN>R��G󻶭�%�i��2��>pZ�/����p�Q?/
۹]p�%,��2��NT��GJ8��}O���c�k���`��8(��4��dpҠN>�ii��k{��e]�A�@�qP,iP���A%�|��������r$�.���V�X&Ҡ"��I�J8�H	'    �ڲ-IK��`��8(��4��t�-���GJ��Q�ߵ��VJ�3d,h5�e��d2�נ�}$�{�g���X�u��~��ǠX&�AI&�{J��GJ8���峍_��:P/�hK�%�Hdlr��M.�IE�qıY�Pc�@�aH,	P��$@%�L�����y����Z3�s��9�L$@E����K��"%�d��K��դ�:X0�j�DTd28YP	')��q��m�'����V�X&Ҡ"��I�J8�H����Y����e�yQ���1�D$AA"c��lr��M*��-�>�^q@�@�aD,�O��d?!�,$d���O��՚dm(h3̈E"�)"}�@^
')�O��S�6���ς�N�X���Lo���_��s3�z�Gƭ��෭v�b��_d2��K��C�[ �~N<5e�
Z��b��_d28iP	')��z^�n����
Z��b�H��L'*��#%�|���lk#��`��8(��4���ҠN>R��G��uYK���`��8(��4��dpҠN>R��G�o�oI����V�X&Ҡ"��I�J8�H��G�����l�8���A�L�AE&���p�N>گ��GٓZ��A�L�AE&���p�N>:�����g��z��2��>�4����t����2W̯�q.k�W��/���M�.sUp����|X00�8��\���^�R�7��Mz��������D,�}��b���%��.�{�*���u��#�=ͳ`��8(��kP���A%�|�����.Pm)��\Pb�@�qP,iP���A%�|�����Ӕ��Ô3:���A�L�AE�ϕ�BT��GJ8�h>M饦����h4��%"	
�(d���lR�� j]����`��0$���dp�N&�W2�|����ؒ.Ƞ`��8'����dp�N.R�IFs���Tʡ�ς�V�X&��"��ɂJ8�H	'�u,[K�7��2��>�BT��GJ���y��z_����Vc�@�qP,�נ$����p�#)������J�[�1;ς߷�1(��kP���^�R���^���-�X�22�Mx��2��E&���p�N>���K�A�ς�V�X&Ҡ"��I�J8�H	')^�}�j(h5�e"*�0ү��W�K_R��~���?�e"�(2�d��ӯZ	�_��G�������b�@�qP,�D���$%�~�B8-�?O�;�T/{�y����A�Ltq��dpҠN>R��G�\R]����P/�hKD$269P�&	٤��Ա�uOj3���"� �>pZ�/����po�:�*�.�9ϐ�`��8'��P���^�R�w��eT�P�67�Ni5��ՏA�L^��L��½��pZ�_�7n�2�$c�@�qP,�נ$��I�J8�H	'�yL����fZ��b�H��L'*��#%�|����qk��,h5�e"*2}��_
')�䣹��BsK��Z��b�H��L'*��#%�|4�Kھ�-ii��2��NT��GB8-�?�櫯�-#i�,h5�e"*2�4�����p��~m�[kʎ�ς�V�X&Ҡ"��I�J8�H	'���q���RZ��A�L�AE���by)�|��{��#Ԟ����cb������d{)�^Em�n\��Ø�6C����Cb�� %��(�{)�X����l�17����y�)��'d���l���<lK�f�ς�>ÐX$ҟ"���J8�H	'����C{�so,�4Ήe"*2}�L^
')�$��^gi9G�=��cb�H��D�&
��"!�T4?{���H����>ÐX$�"��I�J8�H��m��i�)��P0�i��DTd28	P	')�$��\g]�:r�b�@�qP,IP���dA%�|�����>�Ғ�{c�@�qP,iP������p��}t�$X^���\{c�@�qP,�נ$����p�#)���ϯÎRڞs��o�cP,�נ$����p�#%�����	��Q�s{��VC�@�qP,�נ$��I�J8�H	'��{�r)����V�X&Ҡ"��I�J8�H?��2w`������o����e�ubYk���ς�!��L$pE��>M��ɤJ8����m���Z��b�H��L'�+�dR%�L:�B�G�I_�`�@�qP,	\���A%�|$�ӧ	ׁ�u���,��V�X&Ҡ"��I�J8�H	'�����|P/�hKD$269P�&	٤�y �v��tE}�!�H$@E��>J���DJx���y�[�%cc�g�@�qN,S�E&��_��/�t�x�5Zϛ��N�����nL,ݕ
{�!���}�P�Q��r�B?�Cb��_D28	P	')�d�y���5MY�,�4Ήe"*2������p��u�8?��i5��2��>�+��N>R��G�I���I�c���1�D$AA"c��lr��M*���Ԍ�g�@�aH,	P��$@%�L$��d��Cz;�s���NC�@�qN,	P���$@%�\����/ɾj5��2��o�W����Ҭ���`��m�H&�Lx!�(���V���z����v���j,h5�e�2�d2�����R���Y�z�u=��5'ς߷�1(��_�H2�kP
�>R«�Ѽx�u�GƋ�g�@�qP,�נ$��I�J8�H	'͝�꾖��Z��b�H��L'*��#%�|��?~K=rVn<Z��b�H��Lx#*��#%�|4w~��)gP=Z��b�H��L'*��#%�|4w~�����
Z��b�H��L'*��#!������ƺ�I���V�X&Ҡ"��I�J8�H	'͝��s@������cb�H��D�&
��"!�T4�}:��]j��,�3�E"*"}�������po��<���RR>�}t��2yJ2�P
�.�½�����W[�H9S�Y��V?�2y	J2�[P
�>R�7�rm����΁���P0�j��5(�dpҠN>R��Gs��~��'�
Z��b�H��L'*��#%�|t� ��ђZ��A�L�AE�|'*��#%�|ԯ#.֑��ѳ`��8(��4��dpҠN>R��G�$�u�˖t��A�L�AE&���p�Nk寳�ҳ^t`�@�qP,iP���A%�|�����.P��SU)����V�X&Ҡ"��I�J8�H	'�]��R[ҏ��^��8&��$(H4��B+�lr���UT�P���r�ʳ`��0$�P��^�R�7��Mt�$���4`J�����~̉e��d2���]���b�:���R֜�,,h5�e��d28YP	')�䣹��}K�0�Y0�j�DTd28iP	')��6_�u,9/:�`��8(��4����2)�|�����ӈ}�K�iς�V�X&Ҡ"��I�J8�H	'�]����I��`��8(��4��dpҠN>�i��y����ʨI���h4��%"	
�(d��tlz���1�e9�������J8}�P��O���r^��``Ba�-�[��n%����C��խ/It�`��8'��ԭ��ӧ	R8YT	�::�@<�.}�y���A�Lނ�L��½��p�6�����r�rł߷�1(��kP���^�R���N�&��]^}_z'�o���A�L^��L'*��#%�|4��l�8r��A�@�qL,IP����@!�\$d����뱷�s�}�!�H$@E��>M���DJ8���!�Ԗ��(t��2� �NT��EJ8�h>C��֖�2(h5�e"	*2�,�����p�4�͇�Qj��V,h5�e"*2�4�����p��|1zݲ�Ƞ`��8(��4��dpҠN>R��Gs�����K9R�Y0�j�DTd����)�|���c��4b;��r�ʳ`��8(���(2�kP
�>�½�Γ۫����53X��V?�2yJ2�kP
�>R�������Z��A�L^�������@�sc���X0���ܖ�d��dp��N�j%�~���0,}/I"�`��8(��d��t�WZ�/�ӯZ	�����Q����Z��b���F���A%�|�����.Pm����)���    �V�X&Ҡ"��I�J8�H�e�׹|��J��E�h4��%"	
�(d���lR�����R��Ƞ`��0$���dp�N&R��Ds��jٓ���9�L$@E���K��"%�)��9�o��62{��2�_d2x�%��><�:�on����z�6ڍ�%�~��������nm��WF+[���g�@�aH,���H'*�d"%�LT�W�cK9��Y0�i��DTd28	P	')�$��\�u�#��Y0�j�DTd��YP	')��y4߲�G��rς�V�X&Ҡ"��I�J8�H	']g�eI��z��2��NT��GBx'ͳ�J����T�Y0�j�DTd28iP	')��y��R�sUaJ��`��8(��4��dpҠN>R��GǼy)�藔VC�@�qP,iP���A%�|��{]�
���#��X0�j��5(�dp�A)��H
�>Z�n�}o5�	
��ՏA�L^��L��½�����h-so�e�y��,h5�e��d28iP	')���yx���,h5�e"*2�4�����p�Q���������1�D$AA�{'
��"!�T4G��)��<�Cb�H��H'*�d"%�L4�K߱Ԕmݟ��9�L$@E&�� �pr�~���ӈ}Ԓ�г`��8(��$��dp��N>R��G�iD?ϯI�����V�X&Ҡ"��I�J8�H	'����r�%I�P0�j�DTd��e!*��#%���̧��K�'mς�V�X&�AI&�{J��GR���y�`9o�%g� ��ՏA�L^��L��½��pz�w��Z���}�Z�	��z��r�E�ה�ʟC�#n���%�NW�ɤJ8���m��s�۳`��8(����dp�N&U�ɤ�:������h4��%"}}؅(d���lRQ�w�}l=�a ���"� �NT��DJ8�h>G�J?7H�4t��2� �NT��EB8}�p��ؗ޳��`��8(��$��dp��N>R��G���:?FNz���A�L�AE&���p�N>��QZۏ�+2(h5�e"*2}��N>R½��|���:��c�@�qP,�נ$����p�#)���Χ�����|�j(�}��b��%�N�ɔpzN&��
��G��Z��b��%�NT��GJ8�hn�^kIW�X0�j�DTd28iP	')��k��emY���V�X&Ҡ"��� *��#%�|t��ؗ�����1�D$AA"c��lr��M*R�+���P0�g�D������|Gߏ������/�׶L�E&��F�p�E+�����ߖ�����A�L�E&��K�p�U+�tY3�_ڱ%}����A�LtY����)�|��{]g�m}���Vc�@�qP,�נ$����p�#)����=����q$�
~��ǠX&�AI&�{J��GJ8-�os���}�Sr�Vc�@�qP,�נ$��I�J8�H	'���Ǩۚsǂ��A�L�AE&���p�N>����{�F�*,h5�e"*2]�J���p�N>�s���K�ǗX0�j�DTd28iP	')�䣹_�~��N8��2��NT��GB8-�?O諯u����)����V�X&Ҡ"��I�J8�H	'�=ӏe�SΎ|��2��NT��GJ8�h�>�Rk�e��2��>pZ//����p���Z��/9ǂ�V�X&�AI&�{J��GR��Q�6N?����7��ՏA�L^��L��½��pZ2����k�G�7�X0�j��5(�dpҠN>R��Gs��q����*��2��NT��GJ8�h�^�r<ޣ^��8&��$(H�a�zy%�\$d����m-�F,�3�E"*"������p2��3��u[�.Ƞ`��8'����dp�N.�i�|�_��}�֜7�X0�j�DTd28YP	')���:׻n-��P0�j�DTd28iP	')�䣹g�����)����V�X&Ҡ"�N��p���Q���{A-)�6<��cb��������=?{�������
~���X�����~����y�`y�o4���Yς�N�X���NT��EJ8�h��k�)�8���A�L$AE&���p�N>�*۲�l��,h5�e"*2}�;iP	')�䣹�R?��ow���1�D$AA"c��lr��ݚ��.s���}���p�.�ם$:��V#�P�g����x[$R�"��I�J89T?ȡ�	�v��u� ��9�L�nE&����p��N�����k�fl��,h5�e"}+2�,�����p���.om�d� B�@�qP,iP����t!*��#%��h�+��������Y0�j��5(�dp�A)��H
�>:�C�^K�zIj5��ՏA�L^��L��½������:��/WS^�>Z��b��%�NT��GJ8�h���87Ii5��2��NT��GJ8�h��Z;�~Ii5��2��>�BT��GJ8�hn��ᒲC�`��8(��4��dpҠN>R��Gs�������Y0�j�DTd28iP	'	�|4w��ꚲӣ^��8&��$(Hdlr��M.�IEs���z�z,�3�E"*"������p2�1Wײ���,�4Ήe"*2�['�N.R½�$�˾i5��2y	J_0�_�y6_}m�\|����W����?�%�*Q$2�7����Bv�?��X��ZGm)�U=�Cb��F$�NQ������j�y7?�uM��Y0�i��D� �NT��EJ8����XkR��`��8(��$�����N>R��G�n~���|�e��2��NT��GJ8�h�S��:Fҟj(h5�e"*2�4�����pZ�_�-}=_�&	
Z��b�H��L'*��#%�|4׾���=�>
Z��b�H��L'*��#%�|4���z��U�X0�j�DTd��i��N>R½�γ��k����SF��`��8(��kP���^�R����}T�#���%i���ՏA�L^��L��½��pZ,������=����1�D^��D�&
��"!�Tt���v�|r�}�!�H$@E$�� �p2�N&:?n{��%���`��8'����t�;-����EJ8�h>��}��mݟ��A�L$AE&���p�N>��^�q%g���2��NT��GB8-��N|߷lk�5��2��NT��GJ8�h>������VC�@�qP,iP���A%�|�����ӈZ��&	
Z��b�H��L8-����GJ��Q�O#���z����A�L^��L��½��p�6�G�}]��^Ă߷�1(��kP���^�R���N���|$��m$�Wc�@�qP,�נ$��I�J8�H	'�geԑ��Z��b�H��L'*��#%�|4�Il{=��Li5��2��>pZ,/����p�Q�6�=�5�8��2��NT��GJ8�h�^�vĂ�V�X&Ҡ"��I�J8�H���m>�(�~^���
Z��b�H��L'*��#%�|4�I���m�SZ��A�L�AE&���p�N>��$�}�I���`��8(��4����b)�|�����ϝX�k=�����}��|Rx���|����f9z�``Hq�-��$�����poR)ܛ���o}9������~�e��d2���M���g
}>Mi�R�_5��2y�K2�4�����p��|������.(�`��8(��4��dpҠN>R��G�iJ/�<�7��P0�j�DTd���3)�|��������I��`��8(��4��dpҠN>R��G�:*oI�_�`��8(��4��dpҠN>��3��T��u�I���A�L�AE&���p�N>�vNcͺ���V�X&Ҡ"��I�J8�H	''|��y!��j(h5�e"*2}h��
R8�H	��ϣ˫�r�%��V?Z��b��/2���	~�=*eooڟ�m�˴�"���_�Wzz}��G��ֳ`��8(��^(2�4�����p�ѵ�ն����,h5�e"*2�4��    ���p���W����V�X&Ҡ�=#��Ͽ"�����XF�,��n�D2Qd28�D	�_�N������jΆ	ς�V�X&��"��I&J8����J7sO?�r$�
Z��b���F���A%�|�����.�Kk9[N>Z��b�H��L'*��#%�|t�u�kʁEς�V�X&Ҡ"��H�J8�H	�>Z��ݎ���c�@�qP,�נ$����p�#)��h�w���5e��g��[���5(�dp�A)��H	��G��E}Y��p�h4��%�T$269P�&	٤�yS��7+)���}�!�H$@E$�� �p2�N&���;J]r^`�@�qN,	P��$@%�\������ǖs��W.�f�C��<�&����!�,4�A��j֔��M�KD�S$28�O	'	�9h��*�<�%��P0�h�D�Sd28�O	')�d����]F=�Ji5��2��NT��GJ8���W^J�a�ς�V�X&Ҡ"����p��}t�.x�T��Z��b��%��5(�{I��G���;�}�9:�Y��V?�2yJ2�kP
�>R�����ZK;����
Z��b��%�NT��GJ8�����mĒ��Z��b�H��L'*��#%�|��ƻ�����A�L�AE��-�A%�|�����<�jn֘��z�F�X"�� ��ɁB6�H�&��A���wKr7���"� �NT��DB8-�?�|ߢ��r���^��8%���'Hdl���M��C��5�v�9OC�`��0$����dp�N&R��D�9Ĺ��<!Â�N�X&�"�^H�J8�H	�2:Ol�s�]�U7��2y	J2�[P
�>�½��|qn���~��ǠX&�AI&�{J��GJ8-��9D�}M�S���A�L^��L'*��#%�|TϷ��^z��ς�V�X&Ҡ"��I�J8�H�g{�z�����7z������]�Ͽ���\Pb����[&�"�N�#H�dR%�L:�l��s`�@�qP,	\���$p%�L���I瓔��sAYJ��`��8(����dpҠN>��s�:�m{I�r�`��8(��4��dpҠN>R��G�B��q�IO�`��8(��4��dpҠN>R��G�i�~�#��A,h5�e"*2}��Q�N>R½��|�Rk۶��QX0�j��5(�dp�A)��H
����:T��ڄ~��ǠX&�;Ud2�נ�}���
m>M)�%�kA�@�qL,���"��ɁB6�H�&��c�-i�:���"� �NT��DJ8�h>�x߶Ԕ�w���9�L$@E��>M���EJ8�h>�u�)��<Z��b�H��L'*��#%�|4�F,�Z���W/�hKD$269P�&���UB�"F9��P�`��0$���dp�N&R��D�We_u
:�sb�H��w�����k�KOZ ��`��m�H%�L|���R8���p����n~�9+O�`��8(���D���^&R��UK����_ߵձ��<~��ǠX&a#�dp�A)��H	��}n�tn��r0ѳ`��8(��kP���A%�|�����wm���x��cb�H��D�&
��"!�T��
��f]�a�@�aH,	P���R8�H	'����v����:�sb�H��L'*��"%�d4��֭&�'���V�X&��"��ɂJ8�H��}�ӯ�z$^���A�L�AE&���p�N>�{����g"��
Z��b�H��L'*��#%�|4�F,}k)g�?Z��b�H��L8-����GJ��Gc��W_m%e���`��8(���"���/������sw�c=��U�ς߶��e�~����/���n�����WeOY&�,h5�eZ���A%�|��������)_�=��cb�H��D�&
��"!�T47H��K��qE�,�3�E"*"}�������p2ѵKz_z�^�ς�N�X&�"��I�J8�H	'ͧ�l��t)����V�X&��"��ɂJ8�H��G���}}�f��|��2��NT��GJ8�h>�(�8z�#�g�@�qP,iP���A%�|�������y�Xƞ'ς�V�X&Ҡ"����p��}�Χ�-%�k�g�@�qP,�נ$����p�#)���:Ep��R��<~��ǠX&�AI&�{J��GJ��}�9Hp+箙)����V�X&�AI&���p�N>�N�&]�a�@�qP,iP���A%�|�����3����c)����V�X&Ҡ"�?ҠN>R��G}n㷗��yс��A�L�AE&���p�N>:_	��E[��ς�V�X&Ҡ"��I�J8�H_�G۵��S��x��2��NT��GB8=�;�a}ߨ���y[����)�+�t����Y#��QpKD�$26�[�&�
�$�� �Թ�qJ��`��0$�ԭ��R�NU½�Γ�yjqƇ(X.�e����'�ch�>!�;H��
*��ɺ���Yǂ�7�y�IܛO
�R«w�y�a}�Pokʖ6ς�F�X&�>I&����p�N&��9,m�0�,h5�e"*2�$�����p�Q�kσ̒Z��A�L�AE����p�N>�s��s�n��?��2��NT��GJ8�h\�4�՞t���A�L�AE&���p�N%�!��4��tr��`�@�qP,iP���A%�|�����c���m�,����V�X&Ҡ"��I�J8�H	''�����VC�@�qP,iP���O�p�N��y2u�c�Z��b���"����p�#)���<	��j/�$)���߷�1(��kP���^�R���N�&H^�}�j(h5�e���U�_���/�ҳ~P0���ܖ�d��dp��N�j%�~�m.�\���k�^��8&��T"H�a��~%�~�B6]�̛���e�YD�}�!�HtY��dp�N&R��Dc���Ǟ�
:�sb�H��L'*��"!�����������<XĂ�V�X&��"��ɂJ8�H	'��Iݶ��GX0�j�DTd28iP	')��c>��{�����cb�H��D��.-���FJ��Q�w��k�u�Y���9�3y	J2�toA-��HK�B:��;���خ�nC�����;��$�M�&�ҽ��tZ��̷�η�9O��^��8*w$/CE�N2T��JJ8I���ڹk�l,�5ʝ�T��t�I�R:9IJ''�'�2��7�X0�m��;�P��^^K'+I餥~�f\k͹��v�ܡH��P7�|(����t2Ӹ�F��+�z�f�ܑH��H7�l�����pZ@�����#��K,�5ʝ�T��t�I�R:9IJ''�ח���IqJ��`��8+w(R�"�M'J�d%)��4��,��,�C�@�qX�P�CE(��Jz-��$�{3���b�C����,h7��Q�{!j��LZ�7S_�;�}M�9��ݏa�Cy!JB�t/D-ݛIJ�5�}.�i�/{�̱`��8,w(/DI��NB���LR:�i.�Y{ے.̱`��8,w(�"�M'!J�d&)��4�X��}o��n(h7����etZ]�����t2�|f�׭d�톂�v�ܡH��P7��(����t2�|lQ�V��)Ă�v�ܡH��P7��(����tZg��s���u�YL���a�C��n:	QJ'3I�d���b]���a3���EBT���$D)��$�����^�u$݈A�@�qX�P$DE(���Z:�IJ�i�����}�r���`��8,w���P7����J�~�ڷ�+�G�o��F厴� ��	?~_��/_�eO߶����''���+���a��+���v��|��,�S�;���L7�4.��O�t��zi�[�xZ�,�6���4�u�I�R:UJ'���~nFS3�'���a�C���̶�PJ'3I�d�k�R)-�-��`��8,w(�"�M'!J�d&)��4�n�[9�Ii7���EBT���$D)�̤��d�����uԤ��P0�n�;	Qꦓ�t2��Nf�O[jkۑt���a�C��n:	QJ'3I�d���e;���^���a�C���^H�R:�IJ�fZ���Ɩt����a�Cy!JB�t/D-ݛIK�fZ�c    ��oc�\�a�����;��$�M��jR:=TSҫ7�:�[�G]SNn����(	u�I�R:�IJ'3�����e��,h7����u�I�R:�IJ'3]���9�̱`��8,w(�"����(����t2S�ndϗ�9톂�v�ܡH��P7��(����t2��u�W톂�v�ܡH��7��~�۵0�֔SV��d�~�"�(B�t���N�n)�~�������`��8,w(��"�M'�H�����rg~]����I��P0�n�;]�(B}��t2����T����#e��g�@�qX�P^��P7�QK�f�ҽ���}�GKYr�,�}��r��B����^�Z�7���y3]�	.9�Vc�@�qP�@^��@7�T(d���l�Q�6�n%��g�@����DTD��$A)�l$����A�법-��!4G�ET�2�N���HR:9in�>��)��=ڍ�r�"*B�tҡ�Nf���L�@��8?MJ��_�@�qT�H�CA�N6T��JB8-ķ��mM����A�3�
�n:�PJ''I��y�`Y�y�vJ��`��8+w(R�"�M'J�d%)��t\��eOz����a�C��>�B�t2����t-x�aS��_7����(	uӽ�to&-ݛ�<^�}��Y�n,�}��r��B����^�Z�7��N��|^ю�%}ً��a�Cy!JB�t��Nf���L��Q˚����h6���t(�t�ɆJ8YI	')��yjI�@�@�aL�D$BA"���{)�|�����y�z� �����`��0'w&��"�M'	J��#)��4�[���G�%�����Y�C��n:�PJ'+)���<*q}}�֤vC�@�qX�P�CE��N>���LR:�i�����ג�n(h7����u�I�R:�IJ'3�z�ړ^wb�@�qX�P$DE(���{-��$�{3��&��uo9�Ƃ�v�ܡ�%�n����ͤ�{3��&����^rn�����~��Q�{!j��LR:}8��hﻍ^r��a�@�qX�P^��P7��(����t2���h���GiX0�n�;	Qꦓ�t2��Nfj׆?md�
ڍ�r�"!*B�>���LR:�i��9��>r^zb�@�qX�P$DE��NB���LBzuO��cl{-�(��zuO��B=\�?�o�/If���Q�A�C���n:�\J'�*���CۮO����
ڍ�r�"�+B�t���NV��ɪ��|�s�S���a�C���n:	QJ'3I�d�c^�ץ���X0�n�;	Q�����Nf�ҽ��\�4�^���Ƃ�v�ܡ�%�n����ͤ�{3��z����ra�����;��$�M�B�ҽ��t�
���KGy;+��&,h7��Qꦓ�t2��Nf�������$s(h7����u�I�R:�IJ'3ͅLGk��< Ƃ�v�ܡH��P� �>����LR:�i�dj}[�.ՠ`��8,w(�"�M'!J�d&)��4�;��X�%�ڍ�r�"!*B�t��NfR��3�>W2�>��te��a�C��n:	QJ'3I�d����(�\���n(h7����u�I�R:�IJ'3��ǹz7��P0�n�;	Q�~gEB���LR��L��m��v?ڍ�r���uӇ���r�,G���<~�O����P�/B������t�Y�>�"쯾�s3הvC�@�qX�P�/B�t���N�n)��7�sǋ��9o����a�CѪ
E��NB���LR:����ml[�Yzς�v�ܡH��PFo$D)��$����|+�z�I��`��8,w(�"�M'!J�d&)��4���g|����A���=��&
�d$������䱵=��g�@����DTD��$A)�l$���N)��ZGIj64G�ET���$A)��$����y7��-�ȥg�@�qX�P$CE(�ҡ�Nf�ҽ���]]ߏ���Y0�n�;��$�M�B�ҽ��to����R��\�c�����;��$�M�B�ҽ���͛�<p}�1<r��{����(	u�I�R:�IJ'3չ�dYڑ$s(h7����u�I�R:�IJ'3�������_7���EBT�2�NB���LR:�i~�V��%��
���EBT���$D)��$������Q��_7���EBT���$D)�̤�d����d�-��P0�n�;	Qꦓ�t2��Nf�g'�G=r�A�@�qT�H�CA�N6T��JJ8Ii.�Y�v�l��,�5ʝ�T������T(����t�������[��8tg��U(	uӽ
�to%-�k�̯����x��qc�����;�ס$�M�>�ҽ���՛�<?��j�9�<ڍ�r��B����$D)��$�������������a�C��n:	QJ'3I�d�6?S��H9�Y0�n�;	Q�腄(����t2S��)�����,h7����u�I�R:�IJ'3ͯ�z9�L>ڍ�r�"!*B�t��NfR��#���������I����a�C��n:	QJ'3I�d�sa�kۖ�&]�C�@�qX�P$DE��NB���LR:�i���nu�|�,h7����et��@K'3	非^;Ȟ����P�NO��F@��5�X�t����~��2����^�Z�����Z�׀c�5�-��ݏa�Cy�KB�t/s-�[UJ�O!j�v��Kʹ
ς�v�ܡ�%�n:	QJ'3I�d�z�K�� ��vC�@�qX�P$DE��NB���LR:�i~8��(9���`��8,w(�"���c-��$�����k[�5c�g�@�qX�P$DE��NB���LR:�i~؏mI2���v�ܡH��P7��(����t���/ױ��m����a�C��n:	QJ'3I�d�y���ԔvC�@�qX�P$DE��NB���LR:�i~�o�^rV�`�@�qX�P$DE(���Z:�IJ�f�΃leے�Â�v�ܡ�%�n����ͤ�{3��A�ױ�[��ς߷�1,w(/DI���{3I��1D�|�-)'�?ڍ�r����P7��(����t2�<���H9��Y0�n�;	Qꦓ�t2��NfR�o���P0�n�;	Q���~�}���MZx��d�~�"�(B�t���N�n)�~����}˼&�B�`��8,w(��"�M'�H���V�鳂�\¾���vC�@�qX�Pt��u�I�R:�IJ'3�ԗ>J�sg,h7����u�I�R:�IJ'3'}�AK�����v�ܡH��PF��
�t2����t�KX�r���,h7��Q�{!j��LZ�7S�+jj{���X��v?���(	uӽ�to&)�>+�׊�Q���놂�v�ܡ�%�n:	QJ'3I�d�z=�kG�ỳ��a�C��n:	QJ'3I�d���f����xڍ�r�"!*B�>+���LR:�i~c���o&��P0�n�;	Qꦓ�t2��Nf�+j���I�b�@�qX�P$DE��NB���LJ:-��sEM��?ZJ��`��8,w(�"�M'!J�d&)��t��Y�s�bJ��`��8,w(�"�M'!J�d&)��4W���|��n(h7����etZ����������y�b-�����,ς�v�ܡ�/B���Sz�)}x��	��uK���Y��v�a�Cm�u���ҏ_����c��X^K�{���ς�v�ܡ�_���$D)��$������p)eI�n(h7����u�I�R:�IJ'3��@�}3�d<fy���EBT�2�NB���LR:�i��.ˑqa��h6���t(�t�ɆJ8YI	')]���eO���Y0�k�;�P�馓
�tr��~���:���5e�ڳ`��8+w(R�"�M'J�d%)����_ok۳n��`��8,w(ҡ"�M'J�d&)��4��ڏ�)���a�C��>����t2����t�����]�z�̱`��8,w(/DI���{3i��L�Ɋ�Uױ�9WjX��v?���(	uӽ�to&)}�fZ��m�e�X��,h7��Qꦓ�t2��Nf�O,J[��aς�v�ܡH��P7��(����tzX�ĵ������J�+Y�]�AKM٦�Y00�8�w(��"���\J'�J�d��ȥ�˲%�	
ڍ�r�"�+B�t���NV��ɪc��:����a�C���n:	QJ    '3)��t}XzK��Y0�n�;	Qꦓ�t2��Nf�������,h7����u�I�R:�IJ'3ב$m�*���a�C����H�R:�IJ�f*���V�-eωg�@�qX�P^��P7�QK�f���&r�9����~��ǰܡ��U�{!j��LRz�f:φ��y�Y���h6����P醓�p��NR���Yj�{[,�5ʝ�T��t�I�R:9IJ''��gےn��`��8+w(R�"���PJ'+I餥�M��J$��)���E:T����C)��$����7���랳(ڍ�r�"!*B�t��NfR��3��T���_��ӏz�f�ܑH��H7�l�����p���%�W����^àܙH��ג������~��e�X0�/���w(҉"���#����܎]Wvd�J[� �A�ڎ�ߖV�r��/a��q��ع �)R����-����}/acO9�Y0�n;,�"	�w���?�Z���S�	ꥎ�r9�����~B�/:�P�{!j��LR:m%��΃��H��Y0�n;,�(	:	QJ'3I�d�������^�,h��"!*B�NB���LR:����lJ{�S�-h��"!*B��O�J�����t2�:��l�lI�6��ÂP$DE(�I�R:�IJ'3�}u�˕�����B��@'!J�d&%��Ե��֫Ԝ%}�`��vX���:	QJ'3I�d����sΧהv���v�aA(�"�$D)��$����}�U+IGs؂�v�aA(�"ԇN��t2������*��s�}��By!JB��{3i��Lm�����H���Y��v?���%�@�B�ҽ��tZ���R�q�}������ÂP^��P���t2��Nf��T�3���ÂP$DE(�I�R:�IJ'3��{����r7��`��vX������r|-��$�����׵I_�L�@��� 	Q
t��Nf���L�:��M/I_�L�@��� 	Q
t��NfR�i9��Sq�1��-gy�-h��"!*B�NB���LR:��Z�W�Z�N[���ÂP$DE(�I�R:�IJ'3��Ԉ�Z�/b�`��vX�����Ӣ|-��$�{3�����%-o���ÂP^��P�{!j��LZ�7�}��cߓ�m
~��ǰ ��$�^�Z�7��N�����%�>�g�@��� ��$�$D)��$����j�q���y�f�m��H��P���t2��Nfj�K���1[0�n;,EBT���i[��Nf���Lk5��ߏ�9b�`��vX���:	QJ'3I�d��^��g� j�m��H��P���t2��N�
杊���w�+�'[0�n;,EBT���(�������F�l���j�:�y���|!����!��uLuԽ� ��j��H�P��ʥtr��NN�7��ɮ+�&�g�@��� �\���Z:9UJ�^��:���5㍁+h���>������J?<�^�4�&|�p�l��:
����K�sC�:�n��~��	{W]�@��� ���P���t2��NfZ똶Z���>���B��@'!J�d&)���&}.c�8���B��>�BB���LR:�i�cj�hG�W5S0�n;,EBT���(����t2ӱ��ֳo	�]�@��� 	Q
t��NfR�+�i[�՞�.�Y/�l;*�D:D�l�����p��Z�t����<n
zm�H��L��
�tr��NN�W0{�}g�`��vV�T���7R��NV���=�����ytlF�m�@��� ��������ͤ�{3�k�5Δ8����~By!JB��{3I�ݛI�C�W���^��vT��P��$}��}���e���
���?72�P�@'�H��ɖ�铽NPo圇��t�t��
B�P�>�"��g[J��:}~U��c���v�aA(���:�PJ'3I�d�c����rdl�@��� 	Q
t��NfR�O2ӹ��խ����`��vX���:	QJ'3I�d�k]/[�ғ>ݦ`��vX���:	QJ'3I�d���@�g�9��m�@��� 	Q�C�H�R:�IJ�fZ�ϼ�uc���v�aA(/DI(н�to&-ݛi�I���9J�NW��v?���%�@�B�ҽ�����4�%�_�~�+g�-h���B����(����t2S]O6GmWҧ���B��@'!J�d&)���֓M)=�^=W0�n;,EBT������(����t2ӽ����3�ݦ`��vX���:	QJ'3I�d�������l9/Qm�@��� 	Q
t��NfR�iA~�w��fΤ�j�`��vX���:	QJ'3I�d��;��U�9�Al�@��� 	Q
t��Nf���L����j�Yb�m��H��P:-����LR�7ӼYq�? -�B
W0�n;,�(	����ͤ�{3յ;��׼�4�ݦ���~By!JB��{3I� ���i�~d-����ÂP^��P���t2��Nf�����lI��l�@��� 	Q
t��Nf���LkwZi)�-=��mG�H��H8�ɗ��JJ8I������	���6c�D$BA"�ɃJ8�H	'���rlI�m�@�͜ IP�	t���N>R�i)��M�x�{mI��m�@��� �P
t2��NV��IK׺^�=�Y���B��@'J�d&)��4֦Ӿo�ι��v�aA(�"ԇN[	�t2����tߦx\�JZll�m���%�@�B�ҽ��to�y��}�Oҫ[��v?���%�@�B�ҽ����g����u#�Y����1o��oҷA� �M���r���PaF�:By�KB�N2��ɪR:Yu�QՏ^���Ҷ`��vX�d�:�\J'�J�d��&���j��5���B���>t������t2S_��n}`��nS0�n;,EBT���(����t2���TV3.�p��ÂP$DE(�I�R:�II�mmT��Z�NZ���ÂP$DE(�I�R:�IJ'3����U���El�@��� 	Q
t��Nf���L뤪y����c���B��>OƴBK'3I��L�&���c�=�}�-h���B���QK�f�ҽ�� �~֑�F���ݏaA(/DI(�魚�NoՔt���{�skY/�m�@��� ��$�$D)��$���꺤���)�6��ÂP$DE(�I�R:�IJ'3��WiY��m�@��� 	Q
?b��t2��NfZ�-��Ӓ�����mG�H��H���p��NRR���U�M�@�͠ �P��$m"����%}8L������F(҉"�)�>�R:}��S�u�gM�����v�aA(R�"��)�>�R:}�YO��ַ��m�@��� }�Q���i��Nf�ҟf��m����J�^�Y0�n;,U
��Sz�)������{�G�̟�m��:
����K��P����k���lz��B��:	QJ'3I�d����˾�'=�m��H��P���t2��Nfj�Ʋ��]P)�6��ÂP$DE�� !J�d&)�̴�[)�{�Ͷ��H�CA$�ɆJ8YI	')�W�y�3]SZm
zm�H��L��
�tr��~���7[m%�m
�mg�H��P��
�t���NZ��X�����v���v�aA(ҡ"��C)��$�����>O����Ͷ��H�CA��"*�d%%�K龈p^;�r�γ`��fP�ɫP�	t�B-�;IK�N�o"�*R.�{��ۏYA(�BI(н
�to%)}x-��(^e���,h���:���|(����t2ӺG��ғ�m�@��� 	Q
t��Nf���L�ų�5���g�@��� 	Qꏾo$D)��$����=���k�y�b�m��H��P���t2��Nf:&�<�\)��nS0�n;,EBT���(������̴�Q,G�)g�=�m��H��P���t2��Nf�&��y���i�m��H��P���t2��Nf�w�#雹)h��"!*B}腄(����to���[��j[�yς�v�aA(/DI(н�to&-ݛi���*�P9�M���D�:TD��P
�VR«�RY�,�RZ�y��6��L^��L��
�tr��NNZo,�v̓�S�m
�    mg�H��P��
�t���NZj뤈����,�`��vX�t���7�Nf���L��({ʖ�G�@��� �P	p��NV����;6v��?_>����t%}�����y��9�v���)�S3��DWd�4.��O�t��p����R�����`��vV�4�:i\J'�J��kҏ������v���v�aA(R�"��C)��$����{�q]�Hj�)h��"!*B}��AK'3I��L��ǵudO��b�m���%�@�B�ҽ��to�y�㾶}&=>ڂ߷�1,�(	�����$���y�cy�m�F�!�ς�v�aA(/DI(�I�R:�IJ'3�� ��������B��@'!J�d&)�̴޷��:��lm�@��� 	Q�C�=Z:�IJ'3�� ;�V��v���v�aA(�"�$D)��$�����U������,�`��vX���:	QJ'3)�b����<I�m�@��� 	Q
t��Nf���L���_�+i=�-h��"!*B�NB���LR:�i���{[҃�)h��"!*B�'+��Nf�ҽ�$?�}�n[0�n;,全������U{�rD[����?8By�HB���O��N[	�]��un#i���h�D�JQD�����'[	���£��tR�-�d���L��
�tr��NNj��r�)�7<�mg�H��P:m"���JR:i����3i���h�D"
"N2T��JJ8IiN�Gm{��	[0�k3(�D*Td�T(����t�C�w���H�)趝�"*B�N*���JR:ii�Nu��I_�`��vX�t�:�PJ'3I�d����:�+�]�-h��"!*B}贉@K'3I��L}��{/)��?�m���%�@�B�ҽ��to��6�խ�I�Fl������B���QK�f��i9��p���ˑ�f�m���%�@'!J�d&)�̴vԝ�ֲdn
�m��H��P���t2��NfZ�h�q���l�@��� 	Q�C���Z:�IJ'3�U4u�������B��@'!J�d&)�̴�����]7gI�-h��"!*B�NB���LJ:-��ko��Zm9�Yl�@��� 	Q
t��Nf���L���{��f=����v�aA(�"�$D)��$����*����/뫚)h��"!*B}�_K'3I�O3�u�������2��B�_�����J?<}ު�j���e��|���nX��E(Я���/�n9~Y7(���FO�J�,h���
t��Nf���Lu�~<ƙ�س`��vX���:	QJ'3I�d�6��l���&����4ێ
"����u#*�d%%��������J�����
2�
�@'J��$)������{�a�ς�n�YA(R�"�B)����拉{О���Y0�n;,E:T��|(����t2�}��6����m
�m��H��P���t2��NfZ�(���ׄ)�6��ÂP$DE����t2��~���yz�^�y&�?����^�U�; �~-���``T��#���$�^�Z�����5L[�W�;ς߷�1,�e.	�����*�Wo��; �2�������By!JB�NB���LR:������R�m
�m��H��P���t2��NfZw@�m��nS0�n;,EBT���	QJ'3I�d�u���Y�e��6��ÂP$DE(�I�R:�IJ'3�; ��|�1�ݦ`��vX���:	QJ'3)��t���kO��f
�m��H��P���t2��NfZ����(�pU�W�6��ÂP$DE(�I�R:�IJ'3����z����ς�v�aA(�"ԇ~��t2����T��2���R��4ێ
"y*"�m(�{+I�^Je��;��7,�������L^��L�{j��IR�Io�׹�m{Ó>ئ`��vV�~`P��T(����t��:�}�q�\ �,h��"*B�N>���LR:�I�K�W�6��ÂP$D鏓����β�<���ڂ�2��P$E(�I*R:}��t�t���ϫn)G�>�m��H*�P��T�t�t+鴕�����~ו�nS0�n;,E_w�@'!J�d&)���~�?^�(9�|���ÂP$DE(�I�R:�IJ'3���:�Li�)h��"!*B���&���LR�7S]�_�֓�m�m���%�@�B�ҽ��to��N~/�:R.rx��ݏaA(/DI(н�to&)�6Ե��j��9b�`��vX�Q
t��Nf���Lu~�ُ�G��M�@��� 	Q
t��Nf���Lm~�s�bR�M�@��� 	Q�C�Z:�IJ'3��R�W�y,ς�v�aA(�"�$D)��$�����q���)�6��ÂP$DE(�I�R:�II�E�u���[?����W/�l;*�D:D�l�����p�ҵ���|���jS0�k3(�D*Td�T(����tr��,��I7ݶ��P�BE����k�d%)�kiޣ�_��W�~[0�n;,�u(	�����ͤ�{3͛�������ݏaA(/DI(н�to&)������r���٦^��vT��P	p��NVR�IJkͶo����V���^�AA&R�"�B)��$���ֹ�}����%�t��
B�
�>tZ������t��:�}?�Ys�m�@��� �P
t�Nf���L����܏#��m
�m��H��P���t2��N���:�}?�֓�t���v�aA(�"�$D)��$���֮�clcKz3��ÂP$DE(�I�R:�IJ'3�}i�Ւt��-h��"!*B}�t��Nf�ҽ��zc1zO����4ێ
"y*"�m(�{+I�^J}mKk�ܺ��jS��^�AA&�BI&н
�t�$)�6��(�����*�t��
ByJB�N*���JR:ii��������l�@��� �P
t�Nfһ{X�Ύ�g\;�ݻ@-����v�Z���C�``T��#�\��� ���U�t��z�r��,If2��ÂP$sE(�I�R:YUJ'��݀}�=�%[0�n;,E2W���(����t� ��n���GO��)h��"!*B�NB���LR:���>ʖ�����B��@'!J�d&)��4�K�~�����ÂP$DE��6@h�d&)����k7���m�3�v?�m���/B��~J�?��C�������Y��v�aA(zvU���)}���6@Կ; �u�);џ��ÂP�/B�NB���LR:��ί���cd|3��B��@'!J�d&)����B�y�HƢ�g�@��� 	Q�Co$D)��$����n����q�ĳ`��vX���:	QJ'3I�d����מ����`��vX���:	QJ'3)��t�'���)*���ÂP$DE(�I�R:�IJ'3)~o��ݦ`��vX��(���>�c��:�#��g��?��G(��"ԇ~�T�t�tK���=o$���ڎ���ς�v�aA(/I(нT�t�����םy#a{�3�ގg������_w$�@�B�ҽ���ӛ龑���%�����ÂP^��P���t2��Nf��_���r˳`��vX���:	QJ'3I�d�v]l#齳-h��"!*B}�	QJ'3I�d�������2~3~��B��@'!J�d&)�̴V����zΛ)[0�n;,EBT���(�����AfZ+j�)���^��vT�t(�8�P	'+)�$�����RRnaz��
2�
�@'J��$)������Ք+֞ݶ��P�BE�?���
�t����T�Z�}�m��Nn�m���%�@�>�ҽ��to�y#ay][=ZR�M������B���QK�f��wo������玤����By!JB�NB���LR�1��?�9��o�c�}���s��ÂP��@?~J?J�<�ސx����jw�M����ÂP��>t�(��������p�������v�aA���P���t2��Nf�'�۹�|���0S0�n;,EBT���(�����Jf�'۶߿d���_�@��� �P	p��NVR�IJ�Z�~�����w�L�@�͠ �P�	tR��NN���I��yZ�Q~����)趝�"*B}�T(����t��zA��k���ڦ`��v    X�t�:�PJ'3I��Lm-�������[���ÂP^��P�{!j��LRz�fjk��>������ݏaA(/DI(н�to&-ݛi���^��$�ۂ�v�aA(/DI(�I�R:�IJ'3�k3�R��_ u7���B��>�(����t2�Z�U��:~�[��0S0�n;,EBT���(����t2S_�%n}�I�nS0�n;,EBT���(�������t���u�/��nS0�n;,EBT���(�����ӽ��!!�u�s�2�gz�!�8ɪ뢂����k)�a3�j�H�P��̥t���NV]�1.ɹf
�m��H�P�E2��ɪR:Yu��W���W��3��ÂP$sE(�I�R:�IJ�f���y��&�ݶ`��vX�Q
t/D-ݛIJ�L}}3��΋S�m
~��ǰ ��$�^�Z�7������-��9H�����By!JB�NB���LR:���,�'� ���ÂP$DE�ϣ�FB���LR:����A���G��3��ÂP$DE(�I�R:�IJ'3��T���W��3��ÂP$DE(�I�R:�II��LǺGln I�t���v�aA(�"�$D)��$����uu�Q����M�@��� �P	p��NVR�IJ�c�[I�`���^�AA&R�"ӇN� �tr��NN����ޭ�n���n�YA(R�"�B)��$�?�t���ߏ��UK���`��vX��E(Я���/�n�藶��m
~�n7,��"����S�~9�t?���?8B�_���"�ӧ[J�O�� �u�m���Zw�L�@��� IE�Co$)�>�R:-lX����_�>ݦ`��vX��S(B�NB���LR:�i�zY)�'=�m��H��P���t2����Lks������;��v�aA(�"�$D)��$�����z����m
�m��H��P���t2��NfZ�����x{7���B��>�(����t2�z�/���a��nS0�n;,EBT���(����to�}=�_��)����ÂP^��P�{!j��LR���4�?Ȟ���{��v��߷�1,�(	����ͤ�{3�����UFҧ���By!JB�NB���LR:�i���{����^ς�v�aA(�"ԇ~��t2��NfZ�-zy�o9���^��vT�t(�8�P	'+)�$�~_�6�F����
2�
�@'J��$%}���޴^�y�IJ�M�@��� �P
tR��NV��IK����u�O���ÂP�CE(�ɇR:�IJ'3��i}�{�oa�`��vX����G�6��Nf���LcNv��%����B��@'!J�d&)ݛi�?8�G��S/�l;*��u�������^J���Ukٓ��ق���
2yJ2��U��{'i��I���5�6O�N�)趝��*���T(����t�R��zz�ە�����B��>�B>���LR:���o={�����`��vX���:	QJ'3I�d��+�����B��a�`��vX���:	QJ'3)���}iￄ�&��6��ÂP$DE(�I�R:�IJ'3��3��~lMz3��ÂP$DE(�I�R:�IJ'3]����9�Om�@��� 	Q�C��Z:�IJ'3�up_�WKj�)h��"!*B�NB���LR�7S]�g�~���U���By!JB��{3)�^�������g��oW�/�Q��TV�'�����Ǡ#���$�^�Z�����Z�ş���3���g�@��� ���$�$s)��*��U�t��)�<��v���v�aA(��"ԇN� �t2��Nfj�k������h�D"
"N6T��JJ8I��w��)7>zm�H��L��
�tr��N �}��vI�m�@��� �P
tR��NV��IKk�R/m>s�����B��@'J�d&)��t��~ֹ0�ݦ`��vX������-��$����n��֑����z�f�QA$ҡ �dC%����{)��z���ړ~ӱ�6��L^��L�ӛ4)�ޤ)����K����|�m������*��ݫPK�V�ҽ���嵗���7i�`��vX��P
t�Nf���Lk�Ҿ�������B����	QJ'3I�d�u���ڼ�'�ݦ`��vX���:	QJ'3I�d&�ol_����B��?K�N��V m�·�2S0�Of�����:IEJ�O��N��s�V�գ����,h��"�(B�NR����-��םkm��'I�6��ÂP�uG�C��Z:�IJ'3�� :z�a��`��vX���:	QJ'3I��L�]��x?�q���ÂP^��P�{!j��LR:m%�k�1��s^U؂߷�1,�(	����ͤ�{3ͻ۫�GҚS/�l;*��u��8�P	'+)�$����n�JZ�czm�H��L:m"���IR:9iP�~�oYlS0�m;+E*T��T(����t��:����tȭ-h��"*B�N>���LJ:-��k�x�p��M�@��� 	Q
t��Nf���Lk�ծғ�m
�m��H��P���t2��Nf�7x���ˑ�nS0�n;,EBT���i!��Nf���Lc�1��O�I�Om�@��� 	Q
t��Nf�ҟf�.���p�y[�,h��:
����K�[�?�=��u�>R�#x���nXj�E(��O������kWZݎre,x��B�_���(����t2�ڗ6�E�,�y��B��>�(����t2�ښVJ�)?�<�m��H��P���t2��Nf�k'b9��{ ���ÂP$DE(�I�R:�IId�c-{~?�mI�M�@��� 	Q
t��Nf���L��ǹe��~��B��@'!J�d&)�̴�]�)7�<�m��H��P7�l	QJ'3I�d���ٶ���Y0�n;,EBT���(����to���V���p�z�f�QA$�CE$���po%%|�R��P<j�'6������vP�ɫP�	t�B-�;IK�N�7(��2wb$u�t��
ByJB�N*���JR:ii�M;J��������@���a2��MF��G����_�=���4��!A$��"�$A)�l������~?�O�����M���Rz!��}�[=rQ>Վ9B���@'�K�dS%��O�>�s��r+�`��vX�D�:�\J'�J�d�s�?��W�<�m��H�P���t2��Nf��N�m�[�B[0�n;,EBT���	QJ'3I�d�u@��bP��m
�m��H��P���t2����4/<^W�F��z�f�QA$�CE$���po%%�{)��di�r���������L^��L�{j��IZ:=>�m�}��+�ݶ��P�Ԫ:�PJ'+I餥�p^2�����ÂP�CE�� J�d&)�̴���mǜ'n[0�n;,EBT���(����t2S_�O�q���i�`��vX���:	QJ'3)��a����k;s~����ÂP$DE(�I�R:�IJ'3��Լu�dN�M�@��� 	Q
t��Nf���Lk�Y�>r��ۂ�v�aA(�"~�"!J�d&)�̤����v���v�aA(���I�����8���Ӟ�d���T$�@�R����[J��u=�����3�ۭ-�}�ÂP^*�P�{�h��ӭ���;����~�#�=�-h���_w$�@'!J�d&)�̴���RKI��)h��"!*B��w�>�����t2S[N�[I�-b�m��H��P���t2��NfZ{��檚�"���B��@'!J�d&%���{/���;��v���v�aA(�"�$D)��$����^�z�-�'c[0�n;,EBT���(����t2�ZK��cO�#�Y0�n;,EBT���i+��Nf���Lk7����� f
�m��H��P���t2����Զ��H��4ێ
"y*"�m(�{+)����t��%�ҨG��;m���� ��½��p��ya��?^ʕ�ς�V�9A&/AI&�I�R:�HJ'!�����zI7ݶ��P$BE���k�d%)���V�\��S�m
�m��H��P���t2��NfZg����C��
���    B��@'!J�d&%�V�u��5��Y3�ݦ`��vX���:	QJ'3I�d�u�{���s^�؂�v�aA(�"�$D)��$���ֻ��G�I�`��vX������r|-��$���־��f*i�-h��"!*B�NB���LR�7Ӽ;���v�U�����ÂP^��P�{!j��LR:-���E?ϖ�>���ݏaA(/DI(н�to&-ݛi^����z^I{�l�@��� ��$�$D)��$����{�k�G��*[0�n;,EBT���i9��Nf���Lm=�l��}Ώ��`��vX���:	QJ'3I�d��7��{�R�m
�m��H��P���t2��N[	�ڛ��Է�$sS0�n;,EBT���(����t2�ڛ��Y�N����ÂP$DE(�I�R:�IJ'3��i��$[��v���v�aA(�"���V-�̤�?���:?������?� S�o�t�1o�k�ǖ4��``T��#�\
t���NV��7L�v�˾���]�@��� ���P�_?��_�wo����z���r����~X�Q
��Sz�)�y��X���8K���B�_���(����t2S�ϳ����+h��"!*B}腄(����t2�����#��m
�m��H��P���t2��NfZ����.�Ji�)h��"!*B�NB���LJz%3ݧ�׵U=�ݦ`��vX���:	QJ'3I�d�s�<�m5��<W0�n;,EBT���(����t2�5�G��R��_�@��� �P�odC%�������: ��q&y���
2�
�@'J��$)�;i}�}mG;[�;�`��vVʫP
t�B-�[IJ�����6O��y��o�cX�~`P���PK�f�ҽ��u���a9s�lۂ�v�aA(/DI(�I�R:�IJ'3)~i��ݦ`��vX��(�q�>�m]�ۏ+�TW0�Of�����:IEJ�O��N��>�-�z��+�\�@��� IE
t���N�n%���;뜪c�=��[W0�n;,E_w�@'!J�d&)�̴Ωz���e���v�aA(�"�$D)��$����I?z�k�R�m
�m��H��P�EB���LR:��>�j?ϬgWS0�n;,EBT���(����to������~��_�m�@��� ��$�^�Z�7��>��潄�5�������ُQA$�CE$���po%)�K��㪎�z����
2yJ2�N*���IR:9i��{�{ƾ0W0�m;+E*T���׍T(����t���W��s�%Li�)h��"*B�N>���LR:�龑��y`zJ�M�@��� 	Q
t��NfR�i9�ߍ��5��:\�@��� 	Q
t��Nf���Lk_�9�-�}�-h��"!*B�NB���LR:�i�M��h�k���v�aA(�"ԇN��t2��Nf��m���[үŶ`��vX���:	QJ'3I��L�m��Gמq��+h���B���QK�f��i9��Mq�����ݏaA(/DI(н�to&-ݛiަX^u^����-h���B����(����t2S��s畳����B��>tZ������t2S[?.���8m���B��@'!J�d&)�̴��ͻ��w�`��vX���:	QJ'3)�(���W�ƞ��6��ÂP$DE(�I�R:�IJ'3��A��kޜ�nS0�n;,EBT���(����t2�ڠ���^9�Yl�@��� 	Q�C��ȴt2��Nf�j�k�~J�M�@��� 	Q
t��Nf�ҽ�楊���rޙۂ�v�aA(/DI(н�to&)�6�u��u�k��tۂ߷�1,�(	����ͤ�{3�u�S;���ON�M�@��� ��$�$D)��$�7z#��������Fo��J�!Z��<g�yg`F�:B���>t����U�t��:��8��8����B���@'�K�dU)���׏��5�����B���@'!J�d&%�6C�uZ�5o��a�m��H��P���t2��NfZ�U�8rњz�f�QA$ҡ �dC%������֦�������-�d"*2}�5�f-��$���֞��]�D��n���n�YA(R�"�B)��$����:��m$xk�m���U
t�C-ݛIJ��}m�F�WΧ���ݏaA(/DI(н�to&-ݛ������oI{_l�@��� ��$�$D)��$�������������B��>t������t2S[?��-k��-h��"!*B�NB���LR:�i��5�ѳ�����v�aA(�"�$D)�̤��f�ydy͋�3nw��ÂP$DE(�I�R:�IJ'3)~i��ݦ`��vX��(�q�>��z:��_���2��P$E��6h������=��Aݯ�'	���B�T�@'�H���ҟ_w�u/a{c+)�>�m���_���)}���6��^��:��'��nS��v�aA���@/?�ןқ��ul���|����,h���:	QJ'3I�d�uf�(���8�Y0�n;,EBT�����Nf���L�̪��a��b��h�D"
"N6T��JJ8I�>��:{��ς�^�AA&R�"�B)������c�m{�ULς�n�YA(R�"�B)��$����iU���$}�M�@��� �P
t�Nf���L�:��m[V�M�@��� 	Q�C�H�R:�IJ'3������Kj�)h��"!*B�NB���LR�7ӾN��[�R.?~��By!JB��{3I�Ûi=ž�v����ς߷�1,�(	����ͤ�{3�����y�ZJ�M�@��� ��$�$D)��$������[��6�ݦ`��vX����G�	QJ'3I�d�������)�6��ÂP$DE(�I�R:�IJ'3�{�i�R.x}��B��@'!J�d&%}'3�Ӫ�~\)g�=�m��H��P���t2��NfZ�Uս�g�s�-h��"!*B�NB���LR:�i�me?��L�@��� 	Q�C/$D)��$����iU�h[O��)h��"!*B�NB���LR�7SY�-F=��#l���ÂP^��P�{!j��LRz�f�+��6櫩�n�W��f?F��� �6�½��p/��{:�+i����$�"T$�<�����p��:��خ��E�Q/�h3%HD
$��P	')��������g����6�6s�L�@E&�ɁR:�HJ'!���y�Ζ��-趝�"*B�N&���JJ:m(k?Z�פ�<vق�v�aA(ҡ"��C)��$����>5���߿���OI����}��ZҤ�W/0�v��T.�8�\	'�*�$Եdi߯#���g�@�͠ i\��C�MZ:�TJ'����n[��ς�n�YA(Ҹ"�B)��$�{-���q&gKz�g�m���%�@�>�ҽ��t��0o�^��S��x���ُQA$�CE$���po%)�K���g_;�SZm
zm��
%�@'J��$)��T�/��<�>ئ`��vV�T���Ӧ-��$����ߛ�=��`��vX�t�:�PJ'3I�d���ڮ3ii�-h��"!*B�NB���LJ:mz��}|?�)'B=�m��H��P���t2��NfZk��Z���f
�m��H��P���t2��NfZk��O#�D([0�n;,EBT���ɦMZ:�IJ'3��J�L=�F�g�@��� 	Q
t��Nf�ҽ��{k_?f����By!JB��{3I��A��W�6�o�cX�Q����t����u$���d���T$�@'�H�����ӽ ��]G�'���B�T�>t�8��ӧ[J�%k�~���l����ÂP��B
t��Nf���L�
����W�`��vX���:	QJ'3)�q��u4u�y�\J�M�@��� 	Q
t��Nf���L�ռ9G�?�m��H��P���t2��Nf��zo����nS0�n;,EBT���i��Nf���Lk-���Jֳ�)h��"!*B�NB���LR�7S_g�_��=�7c[0�n;,�(	�����$��V�y��*�q���ݶ���~By!JB��{3i��L}�M���n9˽l�@��� ��$    �$D)��$���֊�Q�3����4ێ
"��>pZ�/����p��ZL�Zݒ�m�@�͠ �P�	tR��NN���I�T�z���|'�ݶ��P�BE(�I�R:YII������uLѨI7��ÂP�CE(�ɇR:�IJ'3��1EW���h�D"
"N6T��JJ8I����=鯶)�d"*2}�_K''I�䤱~4�J�A��^��vR�D(�8yP	'#)�O#�uab}�z�#cw�`��fP���E&Я���/�n!~Y�%��^��'u����nVj�E(��O������em.nGI9-�Y0�n;,�
t�Nf���Lu~�9ZI92�Q/�l;*�D:D����P	'+)�$��V9��'u��z�N�1A"� ��A%�|����ւ�y��)�ς�V�9A&��"�$A)�|���$�uSb�uK��Y0�m;+E"T��L(����t�Һ)��}�z�2��ÂP�CE(�ɇR:�IJ'3�������0�۸)h��"!*B}腄(����t2�Z�smg����h�D"
"N6T��JJ��Ҿ�u�����w��
2yJ2��U��{'I����X����_���]v��bz�>��{w�^{��g��'�1��5.	�׸��{��wj�Q�r�`��vXʫ\
tr��NV��ɪk�ҹ��r2ó`��vX�d���7��Nf���L��T��#�y���B��@'!J�d&)�̴�*��=㘁g�@��� 	Q
t��NfR�;�i�J5�6_g�����B��@'!J�d&)�̴,��mI?�؂�v�aA(�"�$D)��$������g�kUJ�M�@��� 	Q�C?H�R:�IJ�i��������̐��`��vX��"�����S����A�z?Ǧ^�,�m�ݰ ���P��[5)�ު)�n��X�����/�Ͷ��H�"N6T��JJ8I���x��R��=zm�H��L��
�tr��NNZ�������)�6ݶ��P�BE(�|A*���JR:i�~�ݶ�d)�ݦ`��vX�t�:�PJ'3I�d&�/m_����B��?�t������3e���`�����#IE
t���N�n)�>��c���5�b
�m��H*�P��T�t�tK��u�>:{��r�ҳ`��vX���(B�ѯ��(����to���Ξ�'�lz��By!JB��{3i��Le�}�1��Mi�)�}�ÂP^��P�{!j��LR���Tֱߵ�?/9�m�@��� ��$�$D)��$���ֳ������J�,h��"!*B�NB���LR:�����Z�����Y0�n;,EBT���	QJ'3I�d�~��~�%�)�6��ÂP$DE(�I�R:�IJ'3��G�G�:�g�@��� 	Q
t��NfR�+��O6����j�^��vT�t(�8�P	'+)�$�uR�VGM���,�d"*2�N*���IR:9iL���ޒ�L�@��� �P�Co�B)��$�{-����u;S��=�m���%�@�>�ҽ��to����_G))�>��o�cX�Q
t/D-ݛIJ��LuWT��0�ݦ`��vX�Q
t��Nf���L�̢m���"�ݦ`��vX���:	QJ'3I�d���j�>��Ni�)h��"!*B}�	QJ'3I�d��l����s����ÂP$DE(�I�R:�IJ'3k�󹕔�X���ÂP$DE(�I�R:�II���u]�W�ڒ~���ÂP$DE(�I�R:�IJ'3��רI[�L�@��� �P	p��NVR�IJ����=i=�-�d"*2}�_K''I��Im��K<��m�mg��
%�@�*�ҽ��t���?�9�i���k�}:����ݏaA���@�~J��ۭ7}���)�?�v�����<,��"����Sz���Lsm�����~�W/�l;*��	p��NVR�IJ���g�?&�����
2�
���c#J��$)��t���ʨ��o��~���n�YA(R�"�B)������}^|�_��a����t��.�o��k�ˌql�?P�6S00�v��T�:�\J'�*�;Y��<v����x7���B���@'�K�dU)ݛ�m����tV�l�@��� ��$�^�Z�7�������W=��o�����~By!JB}��QK�f�ҽ�ڽ�am�����By!JB�NB���LR:����v���5��4�n�)h��"!*B�NB���LJz%3��T����|U���ÂP$DE(�I�R:�IJ'3��B�Vƙ$sS0�n;,EBT���(����t2�}
Ok	�j�3��ÂP$DE����t2��NfZ��5�-��h�D"
"N6T��JJ8Ii���k���jS0�k3(�D*Td�T(�����NNZ����m9���z�^�IA$� ��A%�����K�u��ٷ���e�:m���A"���p�#)���ӈ���9/�l��[m���%�������}��{!I~]��ۦ`��vVʋP��$}���3��nXj�?�)�'���EJQ���"�ӧ[I?������*W�_>S0�n;,ERQ���"�ӧ[J��9���ڏ��b���B�WE(�I�R:�IJ'3�+�z-=�'C[0�n;,EBT���/��Nf���L�q��B$��6��ÂP$DE(�I�R:�IJ'3]k��qԑ��6��ÂP$DE(�I�R:�II��}�]�s�\��nS0�n;,EBT���(���������x��/�<j���~��B�_���)��)���ut���3�v?~�n7,5~���m"P���ҋ���&�cKy�,h����:	QJ'3I�d�:���Ք����ÂP$DE(�I�R:�II��L������2ު=�m��H��P���t2��Nf�^���>�ݦ`��vX���:	QJ'3I�d�����5�u\bς�v�aA(�"ԇ^H�R:�IJ'3�#������)�6��ÂP$DE(�I�R:�IJ'3�#l�8ʑ���Y0�n;,EBT���(�����JfZ�i���{�7sS0�n;,EBT���(����to�}�J���_Ğ��ÂP^��P�{!j��LZ�7Ӿ���}ߓ>ݶ���~By!JB}��QK�f�ҽ���h~���t���v�aA(/DI(�I�R:�IJ'3��'�������a�`��vX���:	QJ'3)�̴V͟ʒ~"���ÂP$DE(�I�R:�IJ'3����(I�Yl�@��� 	Q
t��Nf���L�����>��E�Y0�n;,EBT�����Nf���L묧�,5�x�g�@��� 	Q
t��Nf���L׺0�������3��ÂP$DE(�I�R:�II?�Lcm�h�|zMi�)h��"!*B�NB���LR�7S��o�X+�3�m�m���%�@�B�ҽ��to��V�}������}��Ǩ �ס"�~yJ��JB�N/��c���>��M���J�vy�����ֱ�lm{̩rd��d�4.��O�t�i��خ7��<>ڂ�n�YA(Ҹ"�q)����Rj����9�����v���v�aA(R�"��C)��$����	U�q�ߋS�m
�m��H��P���t2��NfZgT�����v���v�aA(�"��m����LR:�i��GO�a�m��H��P���t2��NfZk�����m)�6��ÂP$DE(�I�R:�II��e�W�}KZo�m��H��P���t2����T���դ%8�^��vT��P	poC)�[I
�R��߷�3���g��{m��
%�>t�����4)�;��߯r�$�ۂ�n�YA(�BI(�I�R:YIJ'-��Km�����b�m��H��P���t2��N� ���k�P�rާق�v�aA(�"�$D)��$������^����nS0�n;,EBT���(����t2�⇶��m
�m��H���&�ӽ���m�I[�m��?��G(��"�$)�>�R:}��]��\�������B�T�@'�H���V�i+�����tPʖ�����B��E(�I�R:�IJ�f�w��Q�+�M�-h���B���QK�f�ҽ��:G}[>�.�g������B����i+���ͤ�{3��    ���<U(�ݦ`��vX�Q
t��Nf���L�����#g_�-h��"!*B�NB���LJ:m%��",��R�~��B��@'!J�d&)�̴��W?�^Dڂ�v�aA(�"�$D)��$���־�}lW��`��vX������n-��$����}u���k����ÂP$DE(�I�R:�IJ'3�}u�ľ��j�`��vX���:	QJ'3)�(�����#�v�g�@��� 	Q
t��Nf�ҽ����u�%m����ÂP^��P�{!j��LZ�7Ӽ�p��[�������~By!JB��-��ҽ��to��v�m�*I2���ÂP^��P���t2��Nf��<��j{��M�@��� 	Q
t��NfR�iQ��Y���Qꑳ"���B��@'!J�d&)������%�5�-h��"!*B�NB���LR:��=���U���B��>tZ������t2�9��6����`��vX���:	QJ'3I�d���y�[+S�m
�m��H��P���t2��NK��:����ܤ��nS0�n;,EBT���(�������ƺYq�}�5���`��vX��"��O��O闧��W9F���g�o����/B}��P25}�)�xz����玃�v���v�aA���P���t2��NfZ�-��^gƃس`��vX���:	QJ'3)�̴�[�Zs6=�m��H��P���t2��NfZ�>m}����ς�v�aA(�"�$D)��$������u��h�������]I/���޹鱗��ς�Q���P$sE�� �K�dU)��z����w�-�k�)h��"�+B�N2��ɪR:Yu�s9j+Y�6��ÂP$sE(�I�R:�II?�L��쵏��X��g�@��� 	Q
t��Nf�ҽ��������-�ݶ`��vX�Q
t/D-ݛIK�f��A��r��-�}���@^��@��U�d{#)��G�.�y��y��B�,h�C�H^��H���t���N6Z����笾y4ێ
B��@'	J�d$%}�����o󢤔v���v�aA(��"�C)��$�����ַ�+e���`��vX���:	QJ'3I�d�c�'�FO���Ͷ��H�CA��{͍l�����p�ҹd��d��6�6��L�BE&�I�R:9IJ'']�>�Ҳ�����n�YA(R�"�B)����拉�v�>/*Ki�)h��"*B�N>���LR�7��׵o�m�m����?H�O����������	���'{��#���$ԇ^�T�t���������{;��;|���ÂP^*�P��T�t�tK鴘a���~8K�O��`��vX��P(B�NB���LJz%3�����v%����B��@'!J�d&)�̴Ω��5/EHi�)h��"!*B�NB���LR:��>��l�Ԏ�v���v�aA(�"ԇN��t2��NfZ;��8Gҳ�-h��"!*B�NB���LR:�i�������Wζ`��vX���:	QJ'3)鴉`�I��ƘGq�t��z�f�QA$ҡ �dC%����{)�{��u^I_�m�@�͠ �W�$�^�Z�w���T��^�+i��-�}���P^��P:-��ҽ��t���Vє}�G;�����ByJB�N>���LR:�i��{�9?�Y0�n;,EBT���(����tZ�_�*��X��v���v�aA(�"�$D)��$����:�c�[�oa�`��vX���:	QJ'3I�d�c-�h{oI�M�@��� 	Q�C���Z:�IJ'3��4��kV�M�@��� 	Q
t��Nf���L���ۮ���J�,h��"!*B�NB���LJ:-ǟ�)����r~���ÂP$DE(�I�R:�IJ�f��)��YΚ�f��mG��� �6�½��p/�y�b{�U�I+�l��{m��
%�����k��IZ�wR[+�F)��jJ�M�@��� �W�$�B)��$��������v��6�,h��"*B�N>���LJ:-�okоI?z�z�f�QA$ҡ �dC%�������Z��~
�9��l�@�͠ �P�	tR��NN���IǺ�e��,�=�-趝�"*B}贉@K'+I餥s�q\���qS0�n;,E:T��|(����t2ӵ~E<�_��v���v�aA(�"�$D)�̤��&���V���S�b��B��@'!J�d&%��p�t�ʿF�wu<_���a���^�?��,��A��j����%�@�*�ҽS�t�Ծ޵\�y9�|m������*����i����{�λ��r��'H[0�n;,�U.	:	QJ'3I�d����,�%-����ÂP$DE(�I�R:�II��}}1(�1�YMi�)h��"!*B�NB���LR:��~�r�W�[0�n;,EBT���(����t2�z�_g�ay�`��vX������&-��$����7.���[�����B��@'!J�d&)�̴޸�WIZ�a��mG�H��H���p��N����l�Ӿ����^�AA&R�"�B)��$�?�4_֭7���`I�+趝����S��S�����یב�qW��v�aA��P�s������N��������&���
�m����E(�I�R:�IJ'3�7��ǌ���`��vX���:	QJ'3)��$����n�W/�l;*�D:T�0I��p���;N�3�b��H(�L��P�t�dK���^O�׶׌�\�@��� 	E��7��N�m)����
�1�:��v���v�aA(���:�PJ'3I�d�k~U,�<2�s��ÂP$DE(�I�R:�II��Lc�?�<Z&�ݦ`��vX���:	QJ'3I��L���~\5�bW0�n;,�(	����ͤ�{3�;��خs�|�m������B���Ћ���ͤ�{3�[��x�_�[
S/�l;*��u��8�P	'+)�$�u8�y��RZm
zm�H��L��
�tr��^�I�l�u�Z�b]W0�m;+E*T��T(����t�R_�_��9�%ق�v�aA(ҡ"��C)��$�����%�]Wٓ�����v�aA(�"ԇ�H�R:�IJ'3��B��I_���h�D"
"N6T��JJ8Ii]@x�k�lIi�)�d"*2�N*���IJz''���[O��f
�mg�H��P��
�t����tߝxm�%�I���ÂP^��P�{j��LZ�7�}w��j���-�}�ÂP^��P�ᅨ�{3i��Le��>ʾ��ۂ�v�aA(/DI(�I�R:�IJ'3յ���[�͞�`��vX���:	QJ'3)�'���'���<��mG�H��H���p��NRZǼ�[�~SZm
zm�H��L��
�tr��NN�Oy�Ƒ�*�t��
B�
�>tZ������t��ڍV�e���
�m��H��P���t2��NfZ���-����B��@'!J�d&%�ᗵ#m�g��6��ÂP$DE(�I�R:�IJ�f�kG�V��열��ÂP^��P�{!j��LZ�7S];�Z�2N����ُQA$�CE�?�A;�po%)�K�އ;��sޭ؂�^�AA&�BI&�I�R:9IJ''�I�{�I+Nm�@��� �P
tR��NVR��-`[��ki[��7���i�C]� ۾��K�\����AG(R�"��r)��*��U׹TWْ��z�f�QA$R� �dr%�����P����#CSZm
zm�H�L:mz���IR:9i� ��z&-��ݶ��P�BE(�I�R:YIJ'-�c���0#i;�-h��"*B�N>���LJ:m}��=�;�j���m
�m��H��P���t2�����֛�s�W�y�`��vX�Q
t/D-ݛIK���}T4�y�yh~��ǰ =�*B}��AK�f�ҽ�捏�k�u��t���v�aA(/DI(�I�R:�IJ'3��`���T9�hق�v�aA(�"�$D)�̤���y�cy�2R��w��ÂP$DE(�I�R:�IJ'3��Av��Ϻ��v�aA(�"�$D)��$����u3ܾmW��nS0�n;,EBT���$D)��$����{�y:G�c��h�D"
"N6T��JJ8II�#�W�6�6��L�B���}��S��O�W�    =����6"�L� '�(���V���z^;�_�v�=���-�d�2�d��DK��k-����D��<�H�)�}���P�+�$ԇN��to%-�ki�:����w��{�\�@��� �ס$��C)��$����|"h�Ⱥ����B��@'!J�d&%���������,;���ÂP$DE(�I�R:�IJ'3��Xз��g��M�@��� 	Q
t��Nf���L�:zGV�M�@��� 	Q�~�-��$���ε/z:+�o�)h��"!*B�NB���LR:��Z?���f=����v�aA(�"�$D)�̤��B�y��6�3IW%ڂ�v�aA(�"�$D)��$�?ʹ�����H�"�,h�����S��S������)��>�}�l7*�4~�wk�����������m[ʟ�g�@�͠ S�E&�I�R:9IJ''�S�k���[�ݶ��P�BE(�I�R:YII�������|I{��B��@'J�d&)�̴Ny��j��M�@��� 	Q
t��Nf���L���q�����`��vX�����7��Nf���L��K����nS0�n;,EBT���(����t2�}�{oמ���Y0�n;,EBT���(�����Nf���9���Ͷ��H�CA$�ɆJ8YI	�R��1�n#�G�g�@�͠ �W�$�^�Z�w��ߧ��WIY��,�}���P^��P��U��{+i�^K�����ܬ^�m�q��=ФA�l�C�����׾��$oS0�e���@'�I��!%�$�u�w��+c���`��vR���:�OJ'I�$�u�Sk#�'N[0�n;,E*T��l(����t2�:өn�>�Ln
�m��H��P�EB���LR:�i��~�1RV�?�m��H��P���t2��~ћ��n[z������.z󧤟Yu�GU��_n��jTM����AG(��"�$s)������:�~���,�~��B���@'�K�dU)ݛi�򸽮��XF�m�@��� ��$�^�Z�7����T������rS��f?F���>Z7oC)�[I
�R*k����&�V���^�AA&�BI&�I�R:9IJ''��J}۞�r�t��
B�
�@'J�d%%}'-���Ы�9��ڂ�v�aA(ҡ"��C)��$����}�dm)g�>�m��H��P���t2��Nf:�S$�yC}J�M�@��� 	Q�C�Z:�IJ'3��O���v���v�aA(�"�$D)��$����j��<��Yo�m��H��P���t2��N�ʽ`��H��f
�m��H��P���t2��No��K����Î-h����@�B�ҽ��to�z�[�ʸ2N�|��ݏaA(/DI(�Z全�{3i��L�_ھj�)h���B��8I��h���r���`�����#IE
t���N�n%�6Եh?�Q�>!�`��vX���:IEJ�O��N_wֳ�1�w�R���ÂP�uG
t��Nf���L�:@�X�S�m
�m��H��P:m���LR:�i=��v�-�͔-h��"!*B�NB���LR:��O[�Gʽ�ς�v�aA(�"�$D)�̤�ӆ����[/i3�`��vX���:	QJ'3I��L����jsU~Ϋ
[0�n;,�(	����ͤ�{3͛��eO��m~��ǰ ��$ԇN
�to&-ݛi�<�_�y�������By!JB�NB���LR:��Nz+g=�dn
�m��H��P���t2��N���̓�W�mے�m
�m��H��P���t2��NfZ7�mlI�l�@��� 	Q
t��Nf���L�݃��p�oƶ`��vX����G�._K'3I�d�u��՚r�߳`��vX���:	QJ'3I�d�u{b;�TbJ�M�@��� 	Q
t��NfR�ii�}�bm��r^�ڂ�v�aA(�"�$D)��$�{3���f��i�����By!JB��{3i��L}���zgR�M������B����ii���ͤ�{3�����slR�M�@��� ��$�$D)��$����}jloW�s�-h��"!*B�NB���LJ:-����U�����n[0�n;,EBT���(����t2�:�j�HNډa�m��H��P���t2��NfZgVmg�I�ق�v�aA(�"ԇNK�t2��NfZ�>]g9J���`��vX���:	QJ'3I�d�u����Qsުق�v�aA(�"�$D)�̤�Ӷ�~����H�~���B��@'!J�d&)�i���P<^c�rn||��B�_���)��)���u���#���g�o����/B}�n[�����޶�����d��~x���o��o���2��ˬw.������z�A�c�H�� '�+�dT%��zo�מ��Y0�k3(�DWd�4.��O���|�޶m�)+�ݶ��P�qE(�I�R:YIJ'-��'�׭�I�8L�@��� �P
t�Nf���L�w����q��`��vX�����_$D)��$�������3�v�g�@��� 	Q
t��Nf���L����������ÂP$DE(�I�R:�IId��m˵m)�2���ÂP$DE(�I�R:�IJ�f�w@���ZKj�-h���B���QK�f�ҽ��� ��:FR�M������B�������}��N�Ӥto��>	��Gʾ�g�@��� ��$�$D)��$�����0�\6��h�D"
"N6T��JB�NRZ�,��u!��g�@�͠ �P�	tR��NN���Ik�R�疲��Y0�m;+E*T��T(����tҒ�7���m
�m��H�ҟ%�ӽ��߆�R�>���?8B�T�@'�H������}� ���r�8ۂ�v�aA(��"�$)�>�Jz�o;���c^;��nS0�n;,E_w�@'!J�d&)ݛi�L�_��=e��`��vX�Q
t/D-ݛIK�f����c`ۓ�m
~��ǰ ��$ԇ޼�to&-ݛ龝pnv�rdn�m���%�@'!J�d&)��Tץ�Y��m
�m��H��P���t2����L�v���Τw��`��vX���:	QJ'3I�d�>���;\�Y`�m��H��P���t2��Nf:&}̗��>���ÂP$DE���h�d&)��t�M�e{R�M�@��� 	Q
t��Nf���L�v��fj$����B��@'!J�d&%��߷���s�x��B��@'!J�d&)ݛ龝��E�$����ÂP^��P�{!j��LZ�7S]��F�z҃�-�}�ÂP^��P:-��ҽ��to��f�9�I/Qm�@��� ��$�$D)��$�?���у��()�m[.�j;(t��}��=~�vK��:p��j�F�a�ς�6�1$���"����Sz���D�,4ێ
B�_��$(����trR]�:�>��Ji�)h��"*B����t(����t2S���a�2,=�m��H��P���t2��NfZ/��ҏ��ς�v�aA(�"�$D)�̤��d��������O�)h��"!*B�NB���LR:��~���V3^�>�m��H��P���t2��NfZ/���߿��ς�v�aA(�"ԇ^H�R:�IJ'3�Ͻ�sOj�)h��"!*B�NB���LR��L}7X_�~���$��Y0�n;,u�"��O��t����C���<�<W#�ݦ��vÂP�/B�^~J�?�7O/�5�U{�=�ς�v�aA���P���t2��>���Ѫ�mo���n��]J�dյG��cnwKUS00�v��d���7���NV��ɪ�-��S�y���,h��"�+B�N2��ɪR:Yu]뻍������`��vX�d�:	QJ'3)�̴���ʹ�<D>�m��H��P���t2��Nf:��~�#��m
�m��H��P���t2��NfZ��G�������ÂP$DE�� !J�d&)�̴<���$����v�aA(�"�$D)��$��C�:t��^S�f<�m����U
t/D-ݛIJ?�����e��s��m������B���QK�f�ҽ���u��h92���ÂP^��P���t2��NfZWy�>�F��v���v�aA(�"ԇ~�    �t2��Nf��������Y0�n;,EBT���(����t2�}�`��?����ݦ`��vX���:	QJ'3)�̴�M<����1�,h��"!*B�NB���LR:�I�{�W�6��ÂP$D�O��龯�(g�2�=���?8B�T���e#�H������}_�q�#�`�g�@��� IE
t���N�n)��)�ٿ���弙���ÂP��$�^�Z�7���{3��I��u.�Ki�)�}�ÂP^��P�{!j��LZ�7SY��[/��yi�m���%�@'!J�d&)�̴��K#e��`��vX�������Nf���L�����y�)�6��ÂP$DE(�I�R:�IJ'3͵ۯ�,�伪���ÂP$DE(�I�R:�II�me=���r�I�6��ÂP$DE(�I�R:�IJ'3����3�-��h�D"
"N6T��JJ8Ii���u�5�K�)�d"*2}�(_K''I��1���ޒ��؂�n�YA(R�"�B)��$�{-�:؎��x���ByJB��}��{3I�(���4�h����f~���o�l�ܶ�(��<�+�@�?ޥf�H��,f2�T��Cud���R�q��gl]]�>�J6I��w�W(
 g�"�R!RB	="����KO�T�j��K-��3�[7��J�H	%t"�f���L�Z�z�|!���í�EB��NtX�ϥ���t0S\Kә��a�f�n	Bd�:�J3Q�`��C�u�Zؠf�n	Bd�:�J31�(��;��ز]]0c�u�H("#��A�T:��J3�{V��o�^d�U�����"�@��PB!R�`&*����۶���U�����"�@��P':,����LT:���4��E��;/�1ܺY$�J� D*�D��f��u���E��=/�1ܺY$T*DJ(��B��S3Q�(�O�-�ynU���5��J�H	%�T�\zj&.=5�����ĭ�g�2[��DJuȈ$p�!Vb�AJU\$���.�1֪Q$����^�V.�D����=�M��WR�2�Zw�D"	<Ȅ���p0R�Vr���.2��z#��D�	�d��GD8���2Eh�E���*�1ԪO$H��I� A*|Ĥ'���{ǆ6n��nz��t���㲥�N7�>�`F��>�P qF(��ũt0*�J�WX����;R�p��í�EB���Nt�����U�t�jo&jL]�r�.�1ܺY$ȜJ� D*�D��f
�
�5]���^�`�V�H���ڐ
O�Ąî�`���][����c�E2�*�dz�B.=u��:)����Q�;O�h����{EB�*��:��J+Q頥�b�m}[���N�n�,
t�u���.�D���⊥:ԡ�S��3�[7��!2B	�H����t0�t����Ue��í�EB���B���LL:l}��i�+�8Z�n�,
��%t"�f���Lq��x�ܖ�Ǧf�n	Bd�:�J3Q�`�v��ڔ�ȭ
f�n	Bd�:�a��f���LqŒ�h
�\�n�,
��%t"�f���f��3 �x��&�8U��n�,�y�PBo_�޽$=��@�����V��Y$�}�PB�)=.�q��ĕ�y��C���
/J� *��T:����Ӂ�)���y�����"�@*�P'zR����M��xo*��2"�3�[7�����PB!R�`&*��M��`�<Ko^0c�u�H("#��A�T:��I��L���E����ABߍ}x�&<�/�w+����������ֿ?��>��l�/�h�ߖ����q������?�ß�0z����j�~u\�������ۏ7�����K���C��.7��k��T��j\%i�����Yu��q���/�o�G���y�;�_�j�6��ۧ��� �uWR~�M\�S/BӸiC�x�r+����ah+Wv���g���g�w�SK_wH�7�0�na90�M5B0݅%��r0��i�ج�چ��N�L�u|�[[ץNkU���>O�
����
T:L�T:L�M���
��ݨ�3�[7���sF�'�7p�@���H��,Ў����M E�[�n�,
d�%t"�f���L]�;~=���nU0c�u�H("#��A�T:��I���Ƨ��Eg��e�ݺ`�p�f�P�)���
�KO�ĥ�f�6,�n�����7��%�$v����Ƴ���Rw/��qpo�7����~:nv���Ⱦ����S�?�盯�~�������r|��7����:��s�a��M�M�:c��v[�5M��-�=܌����}F���p�ލ�^�t������~�����S�_��|��=���w߼���:v���0V<��~�����a�n�_�\jw�[�Wa����rVP��7���n�[�?(��>Y���!F�.�����.��hF�^33II�t���z:gq��ť�s��`Ί+f+_�=�^F+�F�H0c"	&,&�+&�+&����ƅ�7�˭��a�ʛG��_tG�ݿZ�x���W��ë�a���[����&�R�)��n����ſU��^Z����$��l�ԗ!sRL�_��ϳ�0p\�]wf�NѨ����$x��I��}&��S�`*짫��x��"��
f���D	�g�:ȟJ�S�O*����4���5g�2ZI7�D/"	�D�{�Rb��I��J��$Z��T��FR](�@I�LB%Q�$*�D����&�=�E�e�f���D	Rb�:X�I�%*�D����,jk�-�O`^0��t+J(0#���LT:��J3Q�`��u>)VmhȫX�֩ЖR��4kY�y�PZ�Qۦ���y�.H]�t�"�gV;� :��MW����^�r��N�j��S 3 &f�A����g1�c[����b���o�ՁgY�р�#�h��;.#^j*����f�,��9Ι�	��a�e�Y�íl%�ҩ��I���ť����ao�����צ]4�+r�Y��;iև)� ��N Tx:P�O�pPR|��kLS�H�zm�zP���>b�AGD8l���AF�k�,�-�H�y��>RM(��F�LBQ��#*�D���|�7Z=ޏ�H+��et��C�J"D8(�	%ᰛ�
%M;>ƹ���`F#�.�L�$F&����tP�J��AIq;ɸۻ�-t��^ҝ(�@J�PB+Q�%"=��.��9f[��v�ͤ[QB����f���LT:��J3Ż��f��ha=�i���n>/�얽5w����>�m��46ϭR�������7���ۥu�sӁ��Au,�@d��Z ��`����~�d'�,o�g�89~�`��<�`^0/�xKB�T�u��V).�*�L������,|�8{a근��s�g�y|x��|��z�|u|���p��ڷ�JsM���_q��]8�����}X�#������3K����k4�y��
�Ӧ*�7��}�[�k����>�َ_�U�W~��XÙD�:��B�K���n� ?�7_�7�]Q~߇�v�`�z�/���{�|�}߂}2�,=Rr���%�u���e&bU0+�y�Ppv�%t��t�]ƥ�4O���|e�=����Хf4�nE	��]PB	=����t���SOs驙*;.��y���p]��f����J�D	%��LT:���S3q�`��9�ն�]ET��Vҍ(��K�H-1�`%&�Ą���k�.��M�/�����a��f{���m���`����VwR�+u\w�̖T�5�"��h�g��Km���������D���O�v��W��=�ӫ���K�n��zc̪��6�����u۹M�n���ݶ˺��޷�l��g��}�nV���~�V�qrW������#��w������;I<��v�*9p ���GÇ��̷'�`��EN�`%��A�T:h�JCR�H��?h�)�K�`F/�N�P�fF(����t�3�Υ�AL�;��a�M��?���X���j��R�-���^f����J���/����a���8~���]~R7pZ�U՚��׽�6�֮:;�ne���[/�nU��cW�κUo7U��߆��.�� [  ��ܘi��3םi��neW�������w]U��ʅ~�^������C���ڙee������g�$�/�{�n���M����`�Ĵ%�*�PB�Y�J�Y�J�S���8�ʄ"Ϲ���h%݈	�B$�ÔÌB���.*�4=�l�|�2�Rt��FR](�@I�LB%Q�$*�D�����uh��B[�u��^ҝ(�@J�PB+Q�%&�9q� ��}A��:YR3�I���31B	�D����t0���ɛ�mg�ܑK�h&݊*5%��S3q驙�t��ĥ�f�v<	�[��]��f����J�D	%��L\zj&.=5�f���3u����f4�nE	fb�:��J31���K3Uq�~�5��c�ͤ[QB����f���LT:��J3��po|�'Z�ͤ[QB����f���LDz���t0S� �5�*��0]0��t+J(0#���LT:��J3Q�`�x���xC�"ͤ
f4�nE	fb�:��J31��K3ū�U0�/s�I�h&݊
��%t0�f���LT:��}m̢��s�2w��3�I���31B	�D����t�>������U���f4�nE	fb�:��J3Q�`&*=5S�+�_Յ��ͤ[QB�f��zj&.=5�����L!^lB|�l�fR�o�Y+J��L�PBO�ĥ�f��S3q�`�x���5e���ͤ[QB����f���LL:�����L�*`g�R_��ͤ[QB����f���LT:��J3ū��
��z&]0��t+J(0#���LT:��I���\:�)L�Bn\U��T��fҭ(��L�PB3Q�`&*�D����U@:[豸�`F3�V�P`&F(����t0�+��t0S3һ�ZW�I�h&݊
��%t0�f���LT:��o�<޴��C@�ͤ
f4�nE	fb�:��J31��K3M"iBו�:E�h&݊
��%t0�f���LT��L��<��m�qU�in^0��t+J��%B	�}Qz���d8�nS�t��U�'��^�LI+J(���^�(ݿ(=�(��������t�3�I���31B	�D������D����U@���6�n&U0��t+J(0#���LT:��J3Q�`�x�ڍ��+�L�`F3�V�P`&F(����t0��0�f�n�:J\���h&݊
��%t0�f���LT:���hko�Bb:��h%݈	�D�$p�V"�-H�	'���V��s��6R=(�@H�D1�#&lĄ������6u�M��}��P2����:���GL�!Q�`�.~ �ޗ�(7���I�%(�I�$&�Ą����TI����v<}-�G�`F#�.�L��(���*�KO�D�W����TI������e%6��^�K�N�P��(���Z�KO�ĥ�^��ALO����s��3�I���31B	�D����tf���L�}��1En1?/��L�%��J�`&*�D����t0��A�su��K�ͤ[QB����f���LLz 3Q�`�0�m[[�e.v�ͤ[QB����f���LT:��J3�gqW���
����ͤ[QB����f���LLzf���L񡵝>k:W3�I���31B	�D����t0����~�?�Q�(         �  x���I��&��z��� i�tg��q��		5�^�[}%\��~/�	�<	$4������RHXFh�����	%��N�� ~���h��O�����w�iے)�����6��?4J� �%a	��7P0uY�m0Ը��P�PF<�ȳ����������۝3㼮�C��@�"��
^$嵝[u�-Tt_މ���.�\CIJ8�~D�f�?kA?���	g�L�~gn�fKv������X(%B>���Q.��ۆrj߹�h_�s�r��U�}M��YZ��?�W��>D���(��W�{��L��s![�M�ۺ���m9{�r��?�P�r�h.�_،s3�SE0��>��ݪ��Ԯ�u��M�^[��,�YN����;& ������^F��5�Cx�s�s��+��A^�(K`�
6~�U���P;M�p�mjJ�4��+�igi.8��n�H�7Xz�̥���n���ݭ@C}g���n�hS�ˁhʋ����K/��o�>�}(K�Br*��ZV�9��dlWm��}�S#���b���

x2+��Z���k�sܮv��@�Tz�
�Έ��2�%���К5텞#ܻeg9j������"-�I�	j_��i��^ݾp5>ƅ��h�����RH"�lGhV:^��XIWoK�j�r���_��2�h_g�Gۅ��ޖ�@9��a5R���g@�YBL��AW��������pu��RD���"x�D�J;���Z����T�os�k��4Rq*�k9k�i����U�i����d�k������R�S�JI�E�@��/�IH5hQ��u<W��6���i��|�D�"^X�{���v�v�n˗���W�� �G�z-��6Ѝ���E��zݔ�|'����ʒ�?����8�ɪ��aۛג�4|>��=P�%�c1l4��L&�3���\o	�='V�+B���R�hIJr<�"��5��i~�~w�HWިk��K�x(X����v�Ǹ�vkmw�IT��h��(���)��G.ba*ݹ��}�g,���S��)���|�#>\��Hޫ&d���^nZ�i�n'�j����R����X�j<��8o��u���j��Y���
�]��;���5�v�"�+�I���Cm��,Ek�|�% ����V�}/�>�S�n��6T+�S֗*�7WL�L@��%Ls3�c�gW�aR[��S���2�4.X���Q���.A{O�`Թ�4@����+R����)�ϣu4�Qw��+_��	�K�܏+E��T����Ҟ4��s���p�<NW*�ԁ�gh-2�]�(_�2�]��G��O��_T�g��-y[������<�Gm�{T��$P����V������ꎌ�=��٧�,�I$���录xo�9���(������t�U��
=����]pZ�Eb�<�Y��Pƍ��Yf{���,�W~�JA�qs�x��a�W�2�1�8D���,�Z�JBr�r�Ռ��N� Q�9��ӹ�j���*0fb��;Z`l�����j���y��v���Ғ>���%�=�iTb엶��ܭ7�naH�K��] A&y2���-_��G�6m[�[���}���/g��c�&�j#Թ����_&U��.ⵕ���_:P[4y߾zx_M�{H;��:��x��hma+���"%î$J���fz�+�~e�CT�Fk�SѶY�)є�ޕy��-o�640ex��ڢ~ B/L	B��0���U�Ua���UɃi�hmy�T*2lۃ���j�WiVw�E.��߄�-��婅�-E�� Kt阺���G�,8�|�n5R[�/���k�@]��WRs+(�5i�R�W����?i�^H� / Y�^6-��:�\ nN��'�"���J�����ᮖa��UY���^T���ΚY��տx�b�Α::���V8SuS�
��ᶲHm*ͼ�
�S4�Q��ЮجǞ���_hBee���\*�a(
��3���b_^N{�o0#@�0����.��JR�R���F��ݘ�{�5Yy�|����R���'c���j���mΙ|9���F����R���K�v���e�=mΚ�ӵ�
��Ej+PEx���R���z���ʑv��\{�C�:�hmy��)0��+a3��1[�s)F\�aO�Y��(����g��[7��y����k����=�--���M)-��'myzA��ξr�Mv�7�=j��<�?
bae6����L���>�ު���r�!KFK�S�oܙ�y;O�t�֛��������J@FK�~֒�!�!��m<4��>�ʹ;�
��S�L~~�	��      �   ;   x�3�-.M,��W��L�M*���".CNǔ�̼�⒢Ĕ�"�����L�d� $�S         _   x�Uɱ�0�Z�"�|�Y��B���u��7��)3`e�sm�H��߾�v��|��]�����JFx:��e|I�ǫ�U�{��ь�      �   �  x��R͎�0<;O�h���9U�Ê�B 8E����mbGv�����8�^���x�!�)$>��E{���I���a+R��rmQ_�)�8GBA)5��J(���	+��ҞF�!�G۾��`�Mm�9e]X�T�r�@`\�����p�Qg�-2��1�C��b�x�놱
V����}vi��lG��mځq��&pw8��i����ݧ%:���oOھ�g�z�5��ݤx���u�������
ɖ�����:((x�҃�V�
�P��I�8��4��nQ&G��h�3q�������:�!��n,+(.8�frޥ%J�����dj_n��ڗ���u�c ��_�� }���G9� �y������1n(-� E[E���     