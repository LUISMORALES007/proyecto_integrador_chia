PGDMP         -                x            infochia    11.1    11.5 �               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false                       1262    95851    infochia    DATABASE     �   CREATE DATABASE infochia WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Spain.1252' LC_CTYPE = 'Spanish_Spain.1252';
    DROP DATABASE infochia;
             postgres    false                        2615    95855    evento    SCHEMA        CREATE SCHEMA evento;
    DROP SCHEMA evento;
             postgres    false                        2615    104719    reporte    SCHEMA        CREATE SCHEMA reporte;
    DROP SCHEMA reporte;
             postgres    false            
            2615    95852    security    SCHEMA        CREATE SCHEMA security;
    DROP SCHEMA security;
             postgres    false                        2615    95854    usuario    SCHEMA        CREATE SCHEMA usuario;
    DROP SCHEMA usuario;
             postgres    false            ,           1255    104714    f_actualizar_estado_por_fecha()    FUNCTION     �  CREATE FUNCTION evento.f_actualizar_estado_por_fecha() RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    DECLARE 
	
	datos_tabla CURSOR FOR select * from evento.evento ;
	fila_datos_tabla evento.evento%ROWTYPE;
    BEGIN
	 
	  open datos_tabla;
	    LOOP
		
		    FETCH datos_tabla into fila_datos_tabla;
			IF NOT FOUND THEN
			EXIT;
		    END IF;
		      RAISE NOTICE ' PROCESANDO  %', fila_datos_tabla;  
			  
			 
		          
				   UPDATE evento.evento SET


					id_estado= 3 ,
					ultima_modificacion=current_timestamp
                    
					where 
					 
					 id_estado!=5 
					 AND
					 fecha_fin  < current_date ;

		    
			  
        END LOOP;
		close datos_tabla;
    END
$$;
 6   DROP FUNCTION evento.f_actualizar_estado_por_fecha();
       evento       postgres    false    6            �            1259    96315    evento_id_seq    SEQUENCE     v   CREATE SEQUENCE evento.evento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE evento.evento_id_seq;
       evento       postgres    false    6            �            1259    96320    evento    TABLE     �  CREATE TABLE evento.evento (
    id bigint DEFAULT nextval('evento.evento_id_seq'::regclass) NOT NULL,
    nombre text,
    descripcion text,
    fecha_inicio date,
    fecha_fin date,
    hora time without time zone,
    imagen text,
    id_usuario bigint,
    id_categoria integer,
    id_estado integer,
    cantidad_interesados integer,
    cantidad_denuncias integer,
    session text,
    ultima_modificacion timestamp without time zone,
    lugar text
);
    DROP TABLE evento.evento;
       evento         postgres    false    219    6            �            1255    96397 '   f_buscar_existencia_imagen_evento(text)    FUNCTION     �   CREATE FUNCTION evento.f_buscar_existencia_imagen_evento(_url_imagen text) RETURNS SETOF evento.evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.evento
		WHERE
			imagen = _url_imagen;
	end
$$;
 J   DROP FUNCTION evento.f_buscar_existencia_imagen_evento(_url_imagen text);
       evento       postgres    false    220    6                       1255    104682 (   f_cancelar_eventos_usuario(bigint, text)    FUNCTION     +  CREATE FUNCTION evento.f_cancelar_eventos_usuario(_id_usuario bigint, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    UPDATE evento.evento SET

	 id_estado= 5,
	 session=_session,
	 ultima_modificacion=current_timestamp

	where 
	
	  id_usuario =_id_usuario;
 END
$$;
 T   DROP FUNCTION evento.f_cancelar_eventos_usuario(_id_usuario bigint, _session text);
       evento       postgres    false    6            �            1259    96863    mostrar_cantidad_denuncias_view    VIEW     h   CREATE VIEW evento.mostrar_cantidad_denuncias_view AS
 SELECT (0)::bigint AS cantidad_denuncias_evento;
 2   DROP VIEW evento.mostrar_cantidad_denuncias_view;
       evento       postgres    false    6                       1255    96867    f_contar_denuncias(bigint)    FUNCTION       CREATE FUNCTION evento.f_contar_denuncias(_id_evento bigint) RETURNS SETOF evento.mostrar_cantidad_denuncias_view
    LANGUAGE plpgsql
    AS $$
    BEGIN
     return query
	 select count(*)
	 
	 from
	 evento.denuncia_evento
	 
	 where
	 
		id_evento=_id_evento;
	 
 END
$$;
 <   DROP FUNCTION evento.f_contar_denuncias(_id_evento bigint);
       evento       postgres    false    6    227            �            1259    96839 !   mostrar_cantidad_interesados_view    VIEW     l   CREATE VIEW evento.mostrar_cantidad_interesados_view AS
 SELECT (0)::bigint AS cantidad_interesados_evento;
 4   DROP VIEW evento.mostrar_cantidad_interesados_view;
       evento       postgres    false    6                       1255    96851    f_contar_interesa(bigint)    FUNCTION     *  CREATE FUNCTION evento.f_contar_interesa(_id_evento bigint) RETURNS SETOF evento.mostrar_cantidad_interesados_view
    LANGUAGE plpgsql
    AS $$
    BEGIN
     return query
	 select count(*)
	 
	 from
	 evento.interes
	 
	 where
	 
	    habilitado = true
		AND
		id_evento=_id_evento;
	 
 END
$$;
 ;   DROP FUNCTION evento.f_contar_interesa(_id_evento bigint);
       evento       postgres    false    226    6                       1255    96868 0   f_editar_denuncias_evento(bigint, integer, text)    FUNCTION     k  CREATE FUNCTION evento.f_editar_denuncias_evento(_id_evento bigint, _cantidad_denuncias integer, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
	
	 UPDATE evento.evento SET

	    cantidad_denuncias=_cantidad_denuncias,
	    session=_session,
	    ultima_modificacion=current_timestamp
       
	where 
	
	  id =_id_evento;
   
 END
$$;
 o   DROP FUNCTION evento.f_editar_denuncias_evento(_id_evento bigint, _cantidad_denuncias integer, _session text);
       evento       postgres    false    6            �            1255    96526 $   f_editar_estado_evento(bigint, text)    FUNCTION       CREATE FUNCTION evento.f_editar_estado_evento(_id_evento bigint, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    UPDATE evento.evento SET

	 id_estado= 5,
	 session=_session,
	 ultima_modificacion=current_timestamp

	where 
	
	  id =_id_evento;
 END
$$;
 O   DROP FUNCTION evento.f_editar_estado_evento(_id_evento bigint, _session text);
       evento       postgres    false    6                       1255    96849 A   f_editar_estado_interesados_evento(bigint, bigint, boolean, text)    FUNCTION     �  CREATE FUNCTION evento.f_editar_estado_interesados_evento(_id_usuario bigint, _id_evento bigint, _habilitado boolean, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
	
	 UPDATE evento.interes SET

	    habilitado=_habilitado,
	    session=_session,
	    ultima_modificacion=current_timestamp
       
	where 
	
	  id_usuario =_id_usuario
	  AND
	  id_evento=_id_evento;
   
 END
$$;
 �   DROP FUNCTION evento.f_editar_estado_interesados_evento(_id_usuario bigint, _id_evento bigint, _habilitado boolean, _session text);
       evento       postgres    false    6                       1255    96807 k   f_editar_evento(bigint, text, text, date, date, time without time zone, text, integer, integer, text, text)    FUNCTION     �  CREATE FUNCTION evento.f_editar_evento(_id bigint, _nombre text, _descripcion text, _fecha_inicio date, _fecha_fin date, _hora time without time zone, _imagen text, _id_categoria integer, _id_estado integer, _session text, _lugar text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
	
	 UPDATE evento.evento SET

		nombre = _nombre,
		descripcion= _descripcion ,
		fecha_inicio =_fecha_inicio,
		fecha_fin = _fecha_fin ,
		hora = _hora ,
		imagen= _imagen ,
		id_categoria = _id_categoria,
		id_estado= _id_estado ,
	    session=_session,
	    ultima_modificacion=current_timestamp,
        lugar=_lugar
	where 
	
	  id =_id;
   
 END
$$;
 �   DROP FUNCTION evento.f_editar_evento(_id bigint, _nombre text, _descripcion text, _fecha_inicio date, _fecha_fin date, _hora time without time zone, _imagen text, _id_categoria integer, _id_estado integer, _session text, _lugar text);
       evento       postgres    false    6                       1255    96844 2   f_editar_interesados_evento(bigint, integer, text)    FUNCTION     s  CREATE FUNCTION evento.f_editar_interesados_evento(_id_evento bigint, _cantidad_interesados integer, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
	
	 UPDATE evento.evento SET

	    cantidad_interesados=_cantidad_interesados,
	    session=_session,
	    ultima_modificacion=current_timestamp
       
	where 
	
	  id =_id_evento;
   
 END
$$;
 s   DROP FUNCTION evento.f_editar_interesados_evento(_id_evento bigint, _cantidad_interesados integer, _session text);
       evento       postgres    false    6                       1255    96555 1   f_insertar_cancelacion_evento(text, bigint, text)    FUNCTION     i  CREATE FUNCTION evento.f_insertar_cancelacion_evento(_razon text, _id_evento bigint, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    INSERT INTO evento.cancelar_evento

	(
		razon,
		id_evento,
	    session,
	    ultima_modificacion
	) 
	
	VALUES (
		_razon,
        _id_evento,
	    _session,
	    current_timestamp
	);
 END
$$;
 c   DROP FUNCTION evento.f_insertar_cancelacion_evento(_razon text, _id_evento bigint, _session text);
       evento       postgres    false    6            (           1255    96932     f_insertar_categoria(text, text)    FUNCTION     D  CREATE FUNCTION evento.f_insertar_categoria(_descripcion text, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
BEGIN
    INSERT INTO evento.categoria_evento

	(
	    descripcion,
	    session,
	    ultima_modificacion
		
	) 
	
	VALUES (
		_descripcion,
	    _session,
	    current_timestamp
		
	);
END

$$;
 M   DROP FUNCTION evento.f_insertar_categoria(_descripcion text, _session text);
       evento       postgres    false    6                       1255    96861 6   f_insertar_denuncia_evento(text, bigint, bigint, text)    FUNCTION     �  CREATE FUNCTION evento.f_insertar_denuncia_evento(_descripcion text, _id_evento bigint, _id_usuario bigint, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    INSERT INTO evento.denuncia_evento

	(
		descripcion,
		id_evento,
	    session,
		ultima_modificacion,
		id_usuario  
	) 
	
	VALUES (
		_descripcion,
        _id_evento,
		_session,
		 current_timestamp,
	    _id_usuario
	   
	);
 END
$$;
 z   DROP FUNCTION evento.f_insertar_denuncia_evento(_descripcion text, _id_evento bigint, _id_usuario bigint, _session text);
       evento       postgres    false    6                       1255    96598 d   f_insertar_evento(text, text, date, date, time without time zone, text, bigint, integer, text, text)    FUNCTION     �  CREATE FUNCTION evento.f_insertar_evento(_nombre text, _descripcion text, _fecha_inicio date, _fecha_fin date, _hora time without time zone, _imagen text, _id_usuario bigint, _id_categoria integer, _session text, _lugar text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    INSERT INTO evento.evento

	(
		nombre,
		descripcion ,
		fecha_inicio,
		fecha_fin ,
		hora ,
		imagen ,
		id_usuario ,
		id_categoria ,
		id_estado ,
		cantidad_interesados ,
		cantidad_denuncias,
	    session,
	    ultima_modificacion,
		lugar
	) 
	
	VALUES (
		_nombre,
        _descripcion ,
		_fecha_inicio,
		_fecha_fin ,
		_hora ,
		_imagen ,
		_id_usuario ,
		_id_categoria ,
		 1 ,
		0,
		0,
	    _session,
	    current_timestamp,
		_lugar
	);
 END
$$;
 �   DROP FUNCTION evento.f_insertar_evento(_nombre text, _descripcion text, _fecha_inicio date, _fecha_fin date, _hora time without time zone, _imagen text, _id_usuario bigint, _id_categoria integer, _session text, _lugar text);
       evento       postgres    false    6                       1255    96835 /   f_insertar_interes_evento(bigint, bigint, text)    FUNCTION     �  CREATE FUNCTION evento.f_insertar_interes_evento(_id_usuario bigint, _id_evento bigint, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    INSERT INTO evento.interes

	(
		id_usuario,
		id_evento,
		habilitado,
	    session,
	    ultima_modificacion
	) 
	
	VALUES (
		_id_usuario,
        _id_evento,
		 true,
	    _session,
	    current_timestamp
	);
 END
$$;
 f   DROP FUNCTION evento.f_insertar_interes_evento(_id_usuario bigint, _id_evento bigint, _session text);
       evento       postgres    false    6            '           1255    96926 *   f_modificar_categoria(integer, text, text)    FUNCTION     5  CREATE FUNCTION evento.f_modificar_categoria(_id integer, _descripcion text, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
BEGIN
    UPDATE evento.categoria_evento SET

	id=_id,
	descripcion=_descripcion,
	session=_session,
	ultima_modificacion=current_timestamp
	
	WHERE id =_id;
END

$$;
 [   DROP FUNCTION evento.f_modificar_categoria(_id integer, _descripcion text, _session text);
       evento       postgres    false    6            �            1259    96101    categoria_evento    TABLE     �   CREATE TABLE evento.categoria_evento (
    id integer NOT NULL,
    descripcion character varying(25),
    session text,
    ultima_modificacion timestamp without time zone
);
 $   DROP TABLE evento.categoria_evento;
       evento         postgres    false    6            �            1255    96881    f_mostrar_categorias()    FUNCTION     �   CREATE FUNCTION evento.f_mostrar_categorias() RETURNS SETOF evento.categoria_evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.categoria_evento
			
			order by id asc;
		
	end
$$;
 -   DROP FUNCTION evento.f_mostrar_categorias();
       evento       postgres    false    6    217            &           1255    96923 $   f_mostrar_categorias_administrador()    FUNCTION     �   CREATE FUNCTION evento.f_mostrar_categorias_administrador() RETURNS SETOF evento.categoria_evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.categoria_evento
		where
		   id!=1
			order by id asc;
		
	end
$$;
 ;   DROP FUNCTION evento.f_mostrar_categorias_administrador();
       evento       postgres    false    6    217            �            1259    96001    usuario    TABLE     <  CREATE TABLE usuario.usuario (
    id bigint NOT NULL,
    nombre character varying(20),
    apellido character varying(20),
    correo text,
    clave text,
    url_imagen text,
    estado integer,
    habilitado boolean,
    id_rol integer,
    session text,
    ultima_modificacion timestamp without time zone
);
    DROP TABLE usuario.usuario;
       usuario         postgres    false    8            �            1255    96900 !   f_mostrar_contacto_evento(bigint)    FUNCTION     �   CREATE FUNCTION evento.f_mostrar_contacto_evento(_id_usuario bigint) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			usuario.usuario
		WHERE
			id = _id_usuario;
	end
$$;
 D   DROP FUNCTION evento.f_mostrar_contacto_evento(_id_usuario bigint);
       evento       postgres    false    6    201            �            1259    96933    denuncias_eventos_view    VIEW     �  CREATE VIEW evento.denuncias_eventos_view AS
 SELECT (0)::bigint AS id,
    ''::text AS nombre,
    ''::character varying(25) AS descripcion_categoria,
    ''::character varying(20) AS descripcion_estado,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    0 AS cantidad_denuncias,
    ''::text AS imagen;
 )   DROP VIEW evento.denuncias_eventos_view;
       evento       postgres    false    6            )           1255    96961 )   f_mostrar_denuncias_filtro(text, integer)    FUNCTION     �  CREATE FUNCTION evento.f_mostrar_denuncias_filtro(_nombre_filtro text, _categoria_filtro integer) RETURNS SETOF evento.denuncias_eventos_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		
		select distinct 
		IE.id_evento,
		DE.nombre, 
		CA.descripcion,
		EE.descripcion,
		DE.fecha_inicio,
		DE.fecha_fin,
		DE.hora,
		DE.lugar,
		DE.cantidad_denuncias,
		DE.imagen

		from evento.denuncia_evento as IE
		inner join evento.evento as DE
		ON IE.id_evento = DE.id  
		inner join evento.categoria_evento as CA
		ON DE.id_categoria=CA.id
	    inner join evento.estado_evento as EE
		ON DE.id_estado = EE.id
		
		where
		
		(CASE _nombre_filtro 
				WHEN '' THEN 
					DE.nombre ilike '%' || _nombre_filtro  || '%' 
				ELSE 
					DE.nombre ilike '%' || _nombre_filtro || '%' 
				END)
		AND
			(CASE _categoria_filtro 
				WHEN -1 THEN 
					DE.id_categoria = CA.id
				ELSE 
					DE.id_categoria = _categoria_filtro
				END)	;
				
	
		 
		
	end

$$;
 a   DROP FUNCTION evento.f_mostrar_denuncias_filtro(_nombre_filtro text, _categoria_filtro integer);
       evento       postgres    false    230    6            �            1259    96942    detalle_denuncia_evento_view    VIEW     �   CREATE VIEW evento.detalle_denuncia_evento_view AS
 SELECT ''::text AS descripcion,
    ''::text AS nombre_completo,
    ''::text AS correo,
    ''::text AS imagen;
 /   DROP VIEW evento.detalle_denuncia_evento_view;
       evento       postgres    false    6                       1255    96946 )   f_mostrar_detalle_denuncia_evento(bigint)    FUNCTION       CREATE FUNCTION evento.f_mostrar_detalle_denuncia_evento(_id_evento bigint) RETURNS SETOF evento.detalle_denuncia_evento_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		
		select 
		
		ED.descripcion,
	    (UU.nombre || '  ' || UU.apellido),
        UU.correo,
		uu.url_imagen
		
		from evento.denuncia_evento as ED
		inner join usuario.usuario as UU
		ON ED.id_usuario = UU.id  
		inner join evento.evento as EE
		ON ED.id_evento = EE.id 
		
		where
		ED.id_evento=_id_evento;
	   
		
		 
		
	end

$$;
 K   DROP FUNCTION evento.f_mostrar_detalle_denuncia_evento(_id_evento bigint);
       evento       postgres    false    231    6            �            1259    96079    estado_evento    TABLE     �   CREATE TABLE evento.estado_evento (
    id integer NOT NULL,
    descripcion character varying(20),
    session text,
    ultima_modificacion timestamp without time zone
);
 !   DROP TABLE evento.estado_evento;
       evento         postgres    false    6            �            1255    96494    f_mostrar_estados()    FUNCTION     �   CREATE FUNCTION evento.f_mostrar_estados() RETURNS SETOF evento.estado_evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.estado_evento
		WHERE
		    id!=5;
		
	end
$$;
 *   DROP FUNCTION evento.f_mostrar_estados();
       evento       postgres    false    213    6            �            1259    96537    cancelar_evento    TABLE     �   CREATE TABLE evento.cancelar_evento (
    id bigint NOT NULL,
    razon text,
    id_evento bigint,
    session text,
    ultima_modificacion timestamp without time zone
);
 #   DROP TABLE evento.cancelar_evento;
       evento         postgres    false    6                       1255    96824 "   f_mostrar_evento_cancelado(bigint)    FUNCTION     	  CREATE FUNCTION evento.f_mostrar_evento_cancelado(_id_evento bigint) RETURNS SETOF evento.cancelar_evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.cancelar_evento
			
	     where	
		    id_evento = _id_evento;
		
	end
$$;
 D   DROP FUNCTION evento.f_mostrar_evento_cancelado(_id_evento bigint);
       evento       postgres    false    6    223                       1255    96392    f_mostrar_evento_id(bigint)    FUNCTION     �   CREATE FUNCTION evento.f_mostrar_evento_id(_id_evento bigint) RETURNS SETOF evento.evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.evento
		WHERE
		    id=_id_evento;
		
	end
$$;
 =   DROP FUNCTION evento.f_mostrar_evento_id(_id_evento bigint);
       evento       postgres    false    6    220            �            1259    96808    mostrar_eventos_filtro_view    VIEW       CREATE VIEW evento.mostrar_eventos_filtro_view AS
 SELECT (0)::bigint AS id,
    ''::text AS nombre,
    ''::text AS descripcion,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    ''::text AS imagen,
    (0)::bigint AS id_usuario,
    0 AS id_categoria,
    ''::character varying(25) AS descripcion_categoria,
    0 AS id_estado,
    ''::character varying(20) AS descripcion_estado,
    0 AS cantidad_interesados;
 .   DROP VIEW evento.mostrar_eventos_filtro_view;
       evento       postgres    false    6                       1255    96812 -   f_mostrar_eventos_filtro(text, integer, date)    FUNCTION     �  CREATE FUNCTION evento.f_mostrar_eventos_filtro(_nombre_filtro text, _categoria_filtro integer, _fecha_inicio_filtro date) RETURNS SETOF evento.mostrar_eventos_filtro_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		SELECT
			evento.evento.id,
			evento.evento.nombre,
			evento.evento.descripcion,
			evento.evento.fecha_inicio,
			evento.evento.fecha_fin,
			evento.evento.hora,
			evento.evento.lugar,
			evento.evento.imagen,
			evento.evento.id_usuario,
			evento.evento.id_categoria,
			evento.categoria_evento.descripcion,
			evento.evento.id_estado,
			evento.estado_evento.descripcion,
			evento.evento.cantidad_interesados
			
			
		FROM
			evento.evento,
			evento.categoria_evento,
			evento.estado_evento
		WHERE
			
			evento.evento.id_categoria = evento.categoria_evento.id
			AND
			evento.evento.id_estado = evento.estado_evento.id
            AND
			(CASE _nombre_filtro 
				WHEN '' THEN 
					evento.evento.nombre ilike '%' || _nombre_filtro || '%' 
				ELSE 
					evento.evento.nombre ilike '%' || _nombre_filtro || '%' 
				END)
			AND
			(CASE _categoria_filtro 
				WHEN -1 THEN 
					evento.evento.id_categoria = evento.categoria_evento.id
				ELSE 
					evento.evento.id_categoria = _categoria_filtro
				END)
			AND
			(CASE _fecha_inicio_filtro 
				WHEN '2019-03-31' THEN 
			 	 evento.evento.id_categoria = evento.categoria_evento.id
				ELSE 
					evento.evento.fecha_inicio  = _fecha_inicio_filtro 
				END);
			
			
	end
$$;
 z   DROP FUNCTION evento.f_mostrar_eventos_filtro(_nombre_filtro text, _categoria_filtro integer, _fecha_inicio_filtro date);
       evento       postgres    false    6    225            �            1259    96377    mostrar_mis_eventos_view    VIEW     �  CREATE VIEW evento.mostrar_mis_eventos_view AS
 SELECT (0)::bigint AS id,
    ''::text AS nombre,
    ''::text AS descripcion,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS imagen,
    (0)::bigint AS id_usuario,
    0 AS id_categoria,
    ''::character varying(25) AS descripcion_categoria,
    0 AS id_estado,
    ''::character varying(20) AS descripcion_estado,
    0 AS cantidad_interesados;
 +   DROP VIEW evento.mostrar_mis_eventos_view;
       evento       postgres    false    6                       1255    96561    f_mostrar_eventos_finalizados()    FUNCTION     #  CREATE FUNCTION evento.f_mostrar_eventos_finalizados() RETURNS SETOF evento.mostrar_mis_eventos_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		SELECT
			evento.evento.id,
			evento.evento.nombre,
			evento.evento.descripcion,
			evento.evento.fecha_inicio,
			evento.evento.fecha_fin,
			evento.evento.hora,
			evento.evento.imagen,
			evento.evento.id_usuario,
			evento.evento.id_categoria,
			evento.categoria_evento.descripcion,
			evento.evento.id_estado,
			evento.estado_evento.descripcion,
			evento.evento.cantidad_interesados
			
			
		FROM
			evento.evento,
			evento.categoria_evento,
			evento.estado_evento
		WHERE
			
			evento.evento.id_categoria = evento.categoria_evento.id
			AND
			evento.evento.id_estado = evento.estado_evento.id
		    AND
			id_estado=3;
	end

$$;
 6   DROP FUNCTION evento.f_mostrar_eventos_finalizados();
       evento       postgres    false    221    6            �            1259    96891    me_interesa_eventos_view    VIEW     �  CREATE VIEW evento.me_interesa_eventos_view AS
 SELECT (0)::bigint AS id,
    ''::text AS nombre,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    ''::text AS imagen,
    ''::character varying(25) AS descripcion_categoria,
    ''::character varying(20) AS descripcion_estado,
    0 AS cantidad_interesados;
 +   DROP VIEW evento.me_interesa_eventos_view;
       evento       postgres    false    6            #           1255    96895 %   f_mostrar_me_interesa_eventos(bigint)    FUNCTION     �  CREATE FUNCTION evento.f_mostrar_me_interesa_eventos(_id_usuario bigint) RETURNS SETOF evento.me_interesa_eventos_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		select 
		
		evento.evento.id,
		evento.evento.nombre,
		evento.evento.fecha_inicio,
		evento.evento.fecha_fin,
		evento.evento.hora,
		evento.evento.lugar,
		evento.evento.imagen,
		evento.categoria_evento.descripcion,
		evento.estado_evento.descripcion,
		evento.evento.cantidad_interesados
		
		from evento.evento
		   
		
		   
		inner join evento.interes on evento.interes.id_evento =evento.evento.id and evento.interes.habilitado = true and evento.interes.id_usuario =_id_usuario
		inner join evento.categoria_evento on evento.evento.id_categoria = evento.categoria_evento.id 
		inner join evento.estado_evento on evento.evento.id_estado = evento.estado_evento.id
		
		where
		     evento.evento.id_categoria=evento.categoria_evento.id;
	end

$$;
 H   DROP FUNCTION evento.f_mostrar_me_interesa_eventos(_id_usuario bigint);
       evento       postgres    false    6    228            �            1259    96599    mis_eventos_view    VIEW     �  CREATE VIEW evento.mis_eventos_view AS
 SELECT (0)::bigint AS id,
    ''::text AS nombre,
    ''::text AS descripcion,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    ''::text AS imagen,
    (0)::bigint AS id_usuario,
    0 AS id_categoria,
    ''::character varying(25) AS descripcion_categoria,
    0 AS id_estado,
    ''::character varying(20) AS descripcion_estado,
    0 AS cantidad_interesados;
 #   DROP VIEW evento.mis_eventos_view;
       evento       postgres    false    6            !           1255    96876    f_mostrar_mis_eventos(bigint)    FUNCTION     �  CREATE FUNCTION evento.f_mostrar_mis_eventos(_id_usuario bigint) RETURNS SETOF evento.mis_eventos_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		SELECT
			evento.evento.id,
			evento.evento.nombre,
			evento.evento.descripcion,
			evento.evento.fecha_inicio,
			evento.evento.fecha_fin,
			evento.evento.hora,
			evento.evento.lugar,
			evento.evento.imagen,
			evento.evento.id_usuario,
			evento.evento.id_categoria,
			evento.categoria_evento.descripcion,
			evento.evento.id_estado,
			evento.estado_evento.descripcion,
			evento.evento.cantidad_interesados
			
			
		FROM
			evento.evento,
			evento.categoria_evento,
			evento.estado_evento
		WHERE
			
			evento.evento.id_categoria = evento.categoria_evento.id
			AND
			evento.evento.id_estado = evento.estado_evento.id
			AND
			evento.evento.id_usuario = _id_usuario
                                                                AND
			evento.evento.id_estado != 5;
	end
$$;
 @   DROP FUNCTION evento.f_mostrar_mis_eventos(_id_usuario bigint);
       evento       postgres    false    6    224            �            1259    96947    usuario_denuncia_evento_view    VIEW     �   CREATE VIEW evento.usuario_denuncia_evento_view AS
 SELECT ''::text AS nombre_completo,
    ''::text AS correo,
    ''::text AS imagen;
 /   DROP VIEW evento.usuario_denuncia_evento_view;
       evento       postgres    false    6                       1255    96951 )   f_mostrar_usuario_denuncia_evento(bigint)    FUNCTION     �  CREATE FUNCTION evento.f_mostrar_usuario_denuncia_evento(_id_evento bigint) RETURNS SETOF evento.usuario_denuncia_evento_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		
		select 
		
		(UU.nombre || '  ' || UU.apellido),
		 UU.correo,
		 UU.url_imagen
		
		from evento.evento as EE
		inner join usuario.usuario as UU
		ON EE.id_usuario =UU.id  
		
		where
		EE.id = _id_evento;
	   
		
		 
		
	end

$$;
 K   DROP FUNCTION evento.f_mostrar_usuario_denuncia_evento(_id_evento bigint);
       evento       postgres    false    232    6            �            1259    96090    denuncia_evento    TABLE     �   CREATE TABLE evento.denuncia_evento (
    id integer NOT NULL,
    descripcion text,
    id_evento bigint,
    session text,
    ultima_modificacion timestamp without time zone,
    id_usuario bigint
);
 #   DROP TABLE evento.denuncia_evento;
       evento         postgres    false    6                       1255    96862 *   f_validar_denuncia_usuario(bigint, bigint)    FUNCTION     C  CREATE FUNCTION evento.f_validar_denuncia_usuario(_id_usuario bigint, _id_evento bigint) RETURNS SETOF evento.denuncia_evento
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.denuncia_evento
			
	     where	
		    id_usuario = _id_usuario
			AND
		    id_evento = _id_evento;
		
	end
$$;
 X   DROP FUNCTION evento.f_validar_denuncia_usuario(_id_usuario bigint, _id_evento bigint);
       evento       postgres    false    215    6            �            1259    104687    valida_razon_eventos_view    VIEW     �   CREATE VIEW evento.valida_razon_eventos_view AS
 SELECT ''::text AS razon,
    (0)::bigint AS id_evento,
    (0)::bigint AS id_usuario;
 ,   DROP VIEW evento.valida_razon_eventos_view;
       evento       postgres    false    6                       1255    104691 "   f_validar_existencia_razon(bigint)    FUNCTION     q  CREATE FUNCTION evento.f_validar_existencia_razon(_id_evento bigint) RETURNS SETOF evento.valida_razon_eventos_view
    LANGUAGE plpgsql
    AS $$
    BEGIN
	return query
	   
    Select 
	EC.razon,
	EC.id_evento,
	EE.id_usuario
	from
	evento.cancelar_evento  as EC
	inner join evento.evento as EE
	ON EC.id_evento = EE.id

	where 
	
	  id_evento =_id_evento;
 END
$$;
 D   DROP FUNCTION evento.f_validar_existencia_razon(_id_evento bigint);
       evento       postgres    false    6    233            �            1259    96056    interes    TABLE     �   CREATE TABLE evento.interes (
    id bigint NOT NULL,
    id_usuario bigint,
    id_evento bigint,
    habilitado boolean,
    session text,
    ultima_modificacion timestamp without time zone
);
    DROP TABLE evento.interes;
       evento         postgres    false    6                       1255    96829 )   f_validar_interes_usuario(bigint, bigint)    FUNCTION     2  CREATE FUNCTION evento.f_validar_interes_usuario(_id_usuario bigint, _id_evento bigint) RETURNS SETOF evento.interes
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			evento.interes
			
	     where	
		    id_usuario = _id_usuario
			AND
		    id_evento = _id_evento;
		
	end
$$;
 W   DROP FUNCTION evento.f_validar_interes_usuario(_id_usuario bigint, _id_evento bigint);
       evento       postgres    false    6    211            �            1259    104737    mostrar_eventos_activos_view    VIEW     E  CREATE VIEW reporte.mostrar_eventos_activos_view AS
 SELECT ''::text AS nombre,
    '2019-03-31'::date AS fecha_inicio,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    ''::text AS imagen,
    0 AS id_categoria,
    ''::character varying(25) AS descripcion_categoria,
    0 AS cantidad_interesados;
 0   DROP VIEW reporte.mostrar_eventos_activos_view;
       reporte       postgres    false    11            /           1255    104741    f_mostrar_eventos_activos(date)    FUNCTION     <  CREATE FUNCTION reporte.f_mostrar_eventos_activos(_fecha_inicio_filtro date) RETURNS SETOF reporte.mostrar_eventos_activos_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		SELECT
			evento.evento.nombre,
			evento.evento.fecha_inicio,
			evento.evento.hora,
			evento.evento.lugar,
			evento.evento.imagen,
			evento.evento.id_categoria,
			evento.categoria_evento.descripcion,
			evento.evento.cantidad_interesados
			
			
		FROM
			evento.evento,
			evento.categoria_evento
	        
		WHERE
			
			evento.evento.id_categoria = evento.categoria_evento.id
			AND
			evento.evento.id_estado=1
			AND
			(CASE _fecha_inicio_filtro 
				WHEN '2019-03-31' THEN 
			 	 evento.evento.id_categoria = evento.categoria_evento.id
				ELSE 
					evento.evento.fecha_inicio  = _fecha_inicio_filtro 
				END);
			
			
	end
$$;
 L   DROP FUNCTION reporte.f_mostrar_eventos_activos(_fecha_inicio_filtro date);
       reporte       postgres    false    11    236            �            1259    104726    mostrar_eventos_cancelados_view    VIEW     \  CREATE VIEW reporte.mostrar_eventos_cancelados_view AS
 SELECT ''::text AS nombre,
    ''::text AS descripcion,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    ''::text AS imagen,
    (0)::bigint AS id_usuario,
    ''::text AS nombre_completo;
 3   DROP VIEW reporte.mostrar_eventos_cancelados_view;
       reporte       postgres    false    11            -           1255    104730    f_mostrar_eventos_cancelados()    FUNCTION       CREATE FUNCTION reporte.f_mostrar_eventos_cancelados() RETURNS SETOF reporte.mostrar_eventos_cancelados_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		SELECT
		
			evento.evento.nombre,
			evento.evento.descripcion,
			evento.evento.fecha_inicio,
			evento.evento.fecha_fin,
			evento.evento.hora,
			evento.evento.lugar,
			evento.evento.imagen,
			evento.evento.id_usuario,
			usuario.usuario.nombre ||usuario.usuario.apellido
			
			
			
		FROM
			evento.evento,
			evento.categoria_evento,
			evento.estado_evento,
			usuario.usuario
		WHERE
			evento.evento.id_usuario=usuario.usuario.id
			AND
			evento.evento.id_categoria = evento.categoria_evento.id
			AND
			evento.evento.id_estado = evento.estado_evento.id
			AND
			evento.evento.id_estado=5;
            
			
	end
$$;
 6   DROP FUNCTION reporte.f_mostrar_eventos_cancelados();
       reporte       postgres    false    234    11            �            1259    104731     mostrar_eventos_denunciados_view    VIEW     r  CREATE VIEW reporte.mostrar_eventos_denunciados_view AS
 SELECT ''::text AS nombre,
    ''::character varying(25) AS categoria,
    ''::character varying(20) AS estado,
    '2019-03-31'::date AS fecha_inicio,
    '2019-03-31'::date AS fecha_fin,
    '00:00:00'::time without time zone AS hora,
    ''::text AS lugar,
    0 AS cantidad_denuncias,
    ''::text AS imagen;
 4   DROP VIEW reporte.mostrar_eventos_denunciados_view;
       reporte       postgres    false    11            .           1255    104736    f_mostrar_eventos_denunciados()    FUNCTION     E  CREATE FUNCTION reporte.f_mostrar_eventos_denunciados() RETURNS SETOF reporte.mostrar_eventos_denunciados_view
    LANGUAGE plpgsql
    AS $$	BEGIN
		return query
		
		select distinct 
		DE.nombre, 
		CA.descripcion,
		EE.descripcion,
		DE.fecha_inicio,
		DE.fecha_fin,
		DE.hora,
		DE.lugar,
		DE.cantidad_denuncias,
		DE.imagen

		from evento.denuncia_evento as IE
		inner join evento.evento as DE
		ON IE.id_evento = DE.id  
		inner join evento.categoria_evento as CA
		ON DE.id_categoria=CA.id
	    inner join evento.estado_evento as EE
		ON DE.id_estado = EE.id;
			
	end
$$;
 7   DROP FUNCTION reporte.f_mostrar_eventos_denunciados();
       reporte       postgres    false    235    11                       1255    96190 #   f_cerrar_sesion_autentication(text)    FUNCTION     �   CREATE FUNCTION security.f_cerrar_sesion_autentication(_session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
	
	BEGIN
		UPDATE
			security.autentication
		SET
			fecha_fin = current_timestamp
		WHERE
			session = _session;
			
	END

$$;
 E   DROP FUNCTION security.f_cerrar_sesion_autentication(_session text);
       security       postgres    false    10                       1255    96178 R   f_guardar_sesion_autentication(bigint, character varying, character varying, text)    FUNCTION     �  CREATE FUNCTION security.f_guardar_sesion_autentication(_id bigint, _ip character varying, _mac character varying, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
	
	BEGIN
		INSERT INTO security.autentication
		(
			codigo_usuario,
			ip,
			mac,
			fecha_inicio,
			session
		)
	VALUES 
		(
			_id,
			_ip,
			_mac,
			current_timestamp,
			_session
		);

		
		
	END

$$;
 �   DROP FUNCTION security.f_guardar_sesion_autentication(_id bigint, _ip character varying, _mac character varying, _session text);
       security       postgres    false    10            "           1255    104798    f_log_auditoria()    FUNCTION     �  CREATE FUNCTION security.f_log_auditoria() RETURNS trigger
    LANGUAGE plpgsql
    AS $$	 DECLARE
		_pk TEXT :='';		-- Representa la llave primaria de la tabla que esta siedno modificada.
		_sql TEXT;		-- Variable para la creacion del procedured.
		_column_guia RECORD; 	-- Variable para el FOR guarda los nombre de las columnas.
		_column_key RECORD; 	-- Variable para el FOR guarda los PK de las columnas.
		_session TEXT;	-- Almacena el usuario que genera el cambio.
		_user_db TEXT;		-- Almacena el usuario de bd que genera la transaccion.
		_control INT;		-- Variabel de control par alas llaves primarias.
		_count_key INT = 0;	-- Cantidad de columnas pertenecientes al PK.
		_sql_insert TEXT;	-- Variable para la construcción del insert del json de forma dinamica.
		_sql_delete TEXT;	-- Variable para la construcción del delete del json de forma dinamica.
		_sql_update TEXT;	-- Variable para la construcción del update del json de forma dinamica.
		_new_data RECORD; 	-- Fila que representa los campos nuevos del registro.
		_old_data RECORD;	-- Fila que representa los campos viejos del registro.

	BEGIN

			-- Se genera la evaluacion para determianr el tipo de accion sobre la tabla
		 IF (TG_OP = 'INSERT') THEN
			_new_data := NEW;
			_old_data := NEW;
		ELSEIF (TG_OP = 'UPDATE') THEN
			_new_data := NEW;
			_old_data := OLD;
		ELSE
			_new_data := OLD;
			_old_data := OLD;
		END IF;

		-- Se genera la evaluacion para determianr el tipo de accion sobre la tabla
		IF ((SELECT COUNT(*) FROM information_schema.columns WHERE table_schema = TG_TABLE_SCHEMA AND table_name = TG_TABLE_NAME AND column_name = 'codigo' ) > 0) THEN
			_pk := _new_data.codigo;
		ELSE
			_pk := '-1';
		END IF;

		-- Se valida que exista el campo modified_by
		IF ((SELECT COUNT(*) FROM information_schema.columns WHERE table_schema = TG_TABLE_SCHEMA AND table_name = TG_TABLE_NAME AND column_name = 'session') > 0) THEN
			_session := _new_data.session;
		ELSE
			_session := '';
		END IF;

		-- Se guarda el susuario de bd que genera la transaccion
		_user_db := (SELECT CURRENT_USER);

		-- Se evalua que exista el procedimeinto adecuado
		IF (SELECT COUNT(*) FROM security.function_db_view acfdv WHERE acfdv.b_function = 'field_audit' AND acfdv.b_type_parameters = TG_TABLE_SCHEMA || '.'|| TG_TABLE_NAME || ', '|| TG_TABLE_SCHEMA || '.'|| TG_TABLE_NAME || ', character varying, character varying, character varying, text, character varying, text, text') > 0
			THEN
				-- Se realiza la invocación del procedured generado dinamivamente
				PERFORM security.field_audit(_new_data, _old_data, TG_OP, _session, _user_db , _pk, ''::text);
		ELSE
			-- Se empieza la construcción del Procedured generico
			_sql := 'CREATE OR REPLACE FUNCTION security.field_audit( _data_new '|| TG_TABLE_SCHEMA || '.'|| TG_TABLE_NAME || ', _data_old '|| TG_TABLE_SCHEMA || '.'|| TG_TABLE_NAME || ', _accion character varying, _session text, _user_db character varying, _table_pk text, _init text)'
			|| ' RETURNS TEXT AS ''
'
			|| '
'
	|| '	DECLARE
'
	|| '		_column_data TEXT;
	 	_datos jsonb;
	 	
'
	|| '	BEGIN
			_datos = ''''{}'''';
';
			-- Se evalua si hay que actualizar la pk del registro de auditoria.
			IF _pk = '-1'
				THEN
					_sql := _sql
					|| '
		_column_data := ';

					-- Se genera el update con la clave pk de la tabla
					SELECT
						COUNT(isk.column_name)
					INTO
						_control
					FROM
						information_schema.table_constraints istc JOIN information_schema.key_column_usage isk ON isk.constraint_name = istc.constraint_name
					WHERE
						istc.table_schema = TG_TABLE_SCHEMA
					 AND	istc.table_name = TG_TABLE_NAME
					 AND	istc.constraint_type ilike '%primary%';

					-- Se agregan las columnas que componen la pk de la tabla.
					FOR _column_key IN SELECT
							isk.column_name
						FROM
							information_schema.table_constraints istc JOIN information_schema.key_column_usage isk ON isk.constraint_name = istc.constraint_name
						WHERE
							istc.table_schema = TG_TABLE_SCHEMA
						 AND	istc.table_name = TG_TABLE_NAME
						 AND	istc.constraint_type ilike '%primary%'
						ORDER BY 
							isk.ordinal_position  LOOP

						_sql := _sql || ' _data_new.' || _column_key.column_name;
						
						_count_key := _count_key + 1 ;
						
						IF _count_key < _control THEN
							_sql :=	_sql || ' || ' || ''''',''''' || ' ||';
						END IF;
					END LOOP;
				_sql := _sql || ';';
			END IF;

			_sql_insert:='
		IF _accion = ''''INSERT''''
			THEN
				';
			_sql_delete:='
		ELSEIF _accion = ''''DELETE''''
			THEN
				';
			_sql_update:='
		ELSE
			';

			-- Se genera el ciclo de agregado de columnas para el nuevo procedured
			FOR _column_guia IN SELECT column_name, data_type FROM information_schema.columns WHERE table_schema = TG_TABLE_SCHEMA AND table_name = TG_TABLE_NAME
				LOOP
						
					_sql_insert:= _sql_insert || '_datos := _datos || json_build_object('''''
					|| _column_guia.column_name
					|| '_nuevo'
					|| ''''', '
					|| '_data_new.'
					|| _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea', 'USER-DEFINED') THEN 
						_sql_insert:= _sql_insert
						||'::text';
					END IF;

					_sql_insert:= _sql_insert || ')::jsonb;
				';

					_sql_delete := _sql_delete || '_datos := _datos || json_build_object('''''
					|| _column_guia.column_name
					|| '_anterior'
					|| ''''', '
					|| '_data_old.'
					|| _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea', 'USER-DEFINED') THEN 
						_sql_delete:= _sql_delete
						||'::text';
					END IF;

					_sql_delete:= _sql_delete || ')::jsonb;
				';

					_sql_update := _sql_update || 'IF _data_old.' || _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea','USER-DEFINED') THEN 
						_sql_update:= _sql_update
						||'::text';
					END IF;

					_sql_update:= _sql_update || ' <> _data_new.' || _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea','USER-DEFINED') THEN 
						_sql_update:= _sql_update
						||'::text';
					END IF;

					_sql_update:= _sql_update || '
				THEN _datos := _datos || json_build_object('''''
					|| _column_guia.column_name
					|| '_anterior'
					|| ''''', '
					|| '_data_old.'
					|| _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea','USER-DEFINED') THEN 
						_sql_update:= _sql_update
						||'::text';
					END IF;

					_sql_update:= _sql_update
					|| ', '''''
					|| _column_guia.column_name
					|| '_nuevo'
					|| ''''', _data_new.'
					|| _column_guia.column_name;

					IF _column_guia.data_type IN ('bytea', 'USER-DEFINED') THEN 
						_sql_update:= _sql_update
						||'::text';
					END IF;

					_sql_update:= _sql_update
					|| ')::jsonb;
			END IF;
			';
			END LOOP;

			-- Se le agrega la parte final del procedured generico
			
			_sql:= _sql || _sql_insert || _sql_delete || _sql_update
			|| ' 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			''''' || TG_TABLE_SCHEMA || ''''',
			''''' || TG_TABLE_NAME || ''''',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;'''
|| '
LANGUAGE plpgsql;';

			-- Se genera la ejecución de _sql, es decir se crea el nuevo procedured de forma generica.
			EXECUTE _sql;

		-- Se realiza la invocación del procedured generado dinamivamente
			PERFORM security.field_audit(_new_data, _old_data, TG_OP::character varying, _session, _user_db, _pk, ''::text);

		END IF;

		RETURN NULL;

END;
$$;
 *   DROP FUNCTION security.f_log_auditoria();
       security       postgres    false    10            �            1259    96165    autenticate_view    VIEW     �   CREATE VIEW security.autenticate_view AS
 SELECT (0)::text AS correo_usuario,
    ''::character varying(20) AS nombre,
    ''::character varying(20) AS apellido,
    0 AS codigo_rol,
    ''::character varying(20) AS nombre_rol;
 %   DROP VIEW security.autenticate_view;
       security       postgres    false    10                       1255    96873 &   f_validacion_inicio_sesion(text, text)    FUNCTION     C  CREATE FUNCTION security.f_validacion_inicio_sesion(_correo text, _clave text) RETURNS SETOF security.autenticate_view
    LANGUAGE plpgsql
    AS $$

	BEGIN
		return query
		SELECT
			usuario.usuario.correo as correo_usuario,
			usuario.usuario.nombre as nombre,
			usuario.usuario.apellido as apellido,
			usuario.rol.id as codigo_rol,
			usuario.rol.descripcion as nombre_rol
			
		FROM
			usuario.usuario,
			usuario.rol
		WHERE
			
			usuario.usuario.correo = _correo
			AND
			usuario.usuario.clave = _clave
			AND
			usuario.usuario.id_rol = usuario.rol.id;
			
	end

$$;
 N   DROP FUNCTION security.f_validacion_inicio_sesion(_correo text, _clave text);
       security       postgres    false    10    218            *           1255    105118 s   field_audit(evento.cancelar_evento, evento.cancelar_evento, character varying, text, character varying, text, text)    FUNCTION     C
  CREATE FUNCTION security.field_audit(_data_new evento.cancelar_evento, _data_old evento.cancelar_evento, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_nuevo', _data_new.id)::jsonb;
				_datos := _datos || json_build_object('razon_nuevo', _data_new.razon)::jsonb;
				_datos := _datos || json_build_object('id_evento_nuevo', _data_new.id_evento)::jsonb;
				_datos := _datos || json_build_object('session_nuevo', _data_new.session)::jsonb;
				_datos := _datos || json_build_object('ultima_modificacion_nuevo', _data_new.ultima_modificacion)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_anterior', _data_old.id)::jsonb;
				_datos := _datos || json_build_object('razon_anterior', _data_old.razon)::jsonb;
				_datos := _datos || json_build_object('id_evento_anterior', _data_old.id_evento)::jsonb;
				_datos := _datos || json_build_object('session_anterior', _data_old.session)::jsonb;
				_datos := _datos || json_build_object('ultima_modificacion_anterior', _data_old.ultima_modificacion)::jsonb;
				
		ELSE
			IF _data_old.id <> _data_new.id
				THEN _datos := _datos || json_build_object('id_anterior', _data_old.id, 'id_nuevo', _data_new.id)::jsonb;
			END IF;
			IF _data_old.razon <> _data_new.razon
				THEN _datos := _datos || json_build_object('razon_anterior', _data_old.razon, 'razon_nuevo', _data_new.razon)::jsonb;
			END IF;
			IF _data_old.id_evento <> _data_new.id_evento
				THEN _datos := _datos || json_build_object('id_evento_anterior', _data_old.id_evento, 'id_evento_nuevo', _data_new.id_evento)::jsonb;
			END IF;
			IF _data_old.session <> _data_new.session
				THEN _datos := _datos || json_build_object('session_anterior', _data_old.session, 'session_nuevo', _data_new.session)::jsonb;
			END IF;
			IF _data_old.ultima_modificacion <> _data_new.ultima_modificacion
				THEN _datos := _datos || json_build_object('ultima_modificacion_anterior', _data_old.ultima_modificacion, 'ultima_modificacion_nuevo', _data_new.ultima_modificacion)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'evento',
			'cancelar_evento',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new evento.cancelar_evento, _data_old evento.cancelar_evento, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    10    223    223            0           1255    105119 u   field_audit(evento.categoria_evento, evento.categoria_evento, character varying, text, character varying, text, text)    FUNCTION     	  CREATE FUNCTION security.field_audit(_data_new evento.categoria_evento, _data_old evento.categoria_evento, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_nuevo', _data_new.id)::jsonb;
				_datos := _datos || json_build_object('descripcion_nuevo', _data_new.descripcion)::jsonb;
				_datos := _datos || json_build_object('session_nuevo', _data_new.session)::jsonb;
				_datos := _datos || json_build_object('ultima_modificacion_nuevo', _data_new.ultima_modificacion)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_anterior', _data_old.id)::jsonb;
				_datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion)::jsonb;
				_datos := _datos || json_build_object('session_anterior', _data_old.session)::jsonb;
				_datos := _datos || json_build_object('ultima_modificacion_anterior', _data_old.ultima_modificacion)::jsonb;
				
		ELSE
			IF _data_old.id <> _data_new.id
				THEN _datos := _datos || json_build_object('id_anterior', _data_old.id, 'id_nuevo', _data_new.id)::jsonb;
			END IF;
			IF _data_old.descripcion <> _data_new.descripcion
				THEN _datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion, 'descripcion_nuevo', _data_new.descripcion)::jsonb;
			END IF;
			IF _data_old.session <> _data_new.session
				THEN _datos := _datos || json_build_object('session_anterior', _data_old.session, 'session_nuevo', _data_new.session)::jsonb;
			END IF;
			IF _data_old.ultima_modificacion <> _data_new.ultima_modificacion
				THEN _datos := _datos || json_build_object('ultima_modificacion_anterior', _data_old.ultima_modificacion, 'ultima_modificacion_nuevo', _data_new.ultima_modificacion)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'evento',
			'categoria_evento',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new evento.categoria_evento, _data_old evento.categoria_evento, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    10    217    217            	           1255    104839 a   field_audit(evento.evento, evento.evento, character varying, text, character varying, text, text)    FUNCTION     �  CREATE FUNCTION security.field_audit(_data_new evento.evento, _data_old evento.evento, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_nuevo', _data_new.id)::jsonb;
				_datos := _datos || json_build_object('nombre_nuevo', _data_new.nombre)::jsonb;
				_datos := _datos || json_build_object('descripcion_nuevo', _data_new.descripcion)::jsonb;
				_datos := _datos || json_build_object('fecha_inicio_nuevo', _data_new.fecha_inicio)::jsonb;
				_datos := _datos || json_build_object('fecha_fin_nuevo', _data_new.fecha_fin)::jsonb;
				_datos := _datos || json_build_object('hora_nuevo', _data_new.hora)::jsonb;
				_datos := _datos || json_build_object('imagen_nuevo', _data_new.imagen)::jsonb;
				_datos := _datos || json_build_object('id_usuario_nuevo', _data_new.id_usuario)::jsonb;
				_datos := _datos || json_build_object('id_categoria_nuevo', _data_new.id_categoria)::jsonb;
				_datos := _datos || json_build_object('id_estado_nuevo', _data_new.id_estado)::jsonb;
				_datos := _datos || json_build_object('cantidad_interesados_nuevo', _data_new.cantidad_interesados)::jsonb;
				_datos := _datos || json_build_object('cantidad_denuncias_nuevo', _data_new.cantidad_denuncias)::jsonb;
				_datos := _datos || json_build_object('session_nuevo', _data_new.session)::jsonb;
				_datos := _datos || json_build_object('ultima_modificacion_nuevo', _data_new.ultima_modificacion)::jsonb;
				_datos := _datos || json_build_object('lugar_nuevo', _data_new.lugar)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_anterior', _data_old.id)::jsonb;
				_datos := _datos || json_build_object('nombre_anterior', _data_old.nombre)::jsonb;
				_datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion)::jsonb;
				_datos := _datos || json_build_object('fecha_inicio_anterior', _data_old.fecha_inicio)::jsonb;
				_datos := _datos || json_build_object('fecha_fin_anterior', _data_old.fecha_fin)::jsonb;
				_datos := _datos || json_build_object('hora_anterior', _data_old.hora)::jsonb;
				_datos := _datos || json_build_object('imagen_anterior', _data_old.imagen)::jsonb;
				_datos := _datos || json_build_object('id_usuario_anterior', _data_old.id_usuario)::jsonb;
				_datos := _datos || json_build_object('id_categoria_anterior', _data_old.id_categoria)::jsonb;
				_datos := _datos || json_build_object('id_estado_anterior', _data_old.id_estado)::jsonb;
				_datos := _datos || json_build_object('cantidad_interesados_anterior', _data_old.cantidad_interesados)::jsonb;
				_datos := _datos || json_build_object('cantidad_denuncias_anterior', _data_old.cantidad_denuncias)::jsonb;
				_datos := _datos || json_build_object('session_anterior', _data_old.session)::jsonb;
				_datos := _datos || json_build_object('ultima_modificacion_anterior', _data_old.ultima_modificacion)::jsonb;
				_datos := _datos || json_build_object('lugar_anterior', _data_old.lugar)::jsonb;
				
		ELSE
			IF _data_old.id <> _data_new.id
				THEN _datos := _datos || json_build_object('id_anterior', _data_old.id, 'id_nuevo', _data_new.id)::jsonb;
			END IF;
			IF _data_old.nombre <> _data_new.nombre
				THEN _datos := _datos || json_build_object('nombre_anterior', _data_old.nombre, 'nombre_nuevo', _data_new.nombre)::jsonb;
			END IF;
			IF _data_old.descripcion <> _data_new.descripcion
				THEN _datos := _datos || json_build_object('descripcion_anterior', _data_old.descripcion, 'descripcion_nuevo', _data_new.descripcion)::jsonb;
			END IF;
			IF _data_old.fecha_inicio <> _data_new.fecha_inicio
				THEN _datos := _datos || json_build_object('fecha_inicio_anterior', _data_old.fecha_inicio, 'fecha_inicio_nuevo', _data_new.fecha_inicio)::jsonb;
			END IF;
			IF _data_old.fecha_fin <> _data_new.fecha_fin
				THEN _datos := _datos || json_build_object('fecha_fin_anterior', _data_old.fecha_fin, 'fecha_fin_nuevo', _data_new.fecha_fin)::jsonb;
			END IF;
			IF _data_old.hora <> _data_new.hora
				THEN _datos := _datos || json_build_object('hora_anterior', _data_old.hora, 'hora_nuevo', _data_new.hora)::jsonb;
			END IF;
			IF _data_old.imagen <> _data_new.imagen
				THEN _datos := _datos || json_build_object('imagen_anterior', _data_old.imagen, 'imagen_nuevo', _data_new.imagen)::jsonb;
			END IF;
			IF _data_old.id_usuario <> _data_new.id_usuario
				THEN _datos := _datos || json_build_object('id_usuario_anterior', _data_old.id_usuario, 'id_usuario_nuevo', _data_new.id_usuario)::jsonb;
			END IF;
			IF _data_old.id_categoria <> _data_new.id_categoria
				THEN _datos := _datos || json_build_object('id_categoria_anterior', _data_old.id_categoria, 'id_categoria_nuevo', _data_new.id_categoria)::jsonb;
			END IF;
			IF _data_old.id_estado <> _data_new.id_estado
				THEN _datos := _datos || json_build_object('id_estado_anterior', _data_old.id_estado, 'id_estado_nuevo', _data_new.id_estado)::jsonb;
			END IF;
			IF _data_old.cantidad_interesados <> _data_new.cantidad_interesados
				THEN _datos := _datos || json_build_object('cantidad_interesados_anterior', _data_old.cantidad_interesados, 'cantidad_interesados_nuevo', _data_new.cantidad_interesados)::jsonb;
			END IF;
			IF _data_old.cantidad_denuncias <> _data_new.cantidad_denuncias
				THEN _datos := _datos || json_build_object('cantidad_denuncias_anterior', _data_old.cantidad_denuncias, 'cantidad_denuncias_nuevo', _data_new.cantidad_denuncias)::jsonb;
			END IF;
			IF _data_old.session <> _data_new.session
				THEN _datos := _datos || json_build_object('session_anterior', _data_old.session, 'session_nuevo', _data_new.session)::jsonb;
			END IF;
			IF _data_old.ultima_modificacion <> _data_new.ultima_modificacion
				THEN _datos := _datos || json_build_object('ultima_modificacion_anterior', _data_old.ultima_modificacion, 'ultima_modificacion_nuevo', _data_new.ultima_modificacion)::jsonb;
			END IF;
			IF _data_old.lugar <> _data_new.lugar
				THEN _datos := _datos || json_build_object('lugar_anterior', _data_old.lugar, 'lugar_nuevo', _data_new.lugar)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'evento',
			'evento',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new evento.evento, _data_old evento.evento, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    220    10    220            
           1255    105113 c   field_audit(evento.interes, evento.interes, character varying, text, character varying, text, text)    FUNCTION     �  CREATE FUNCTION security.field_audit(_data_new evento.interes, _data_old evento.interes, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_nuevo', _data_new.id)::jsonb;
				_datos := _datos || json_build_object('id_usuario_nuevo', _data_new.id_usuario)::jsonb;
				_datos := _datos || json_build_object('id_evento_nuevo', _data_new.id_evento)::jsonb;
				_datos := _datos || json_build_object('habilitado_nuevo', _data_new.habilitado)::jsonb;
				_datos := _datos || json_build_object('session_nuevo', _data_new.session)::jsonb;
				_datos := _datos || json_build_object('ultima_modificacion_nuevo', _data_new.ultima_modificacion)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_anterior', _data_old.id)::jsonb;
				_datos := _datos || json_build_object('id_usuario_anterior', _data_old.id_usuario)::jsonb;
				_datos := _datos || json_build_object('id_evento_anterior', _data_old.id_evento)::jsonb;
				_datos := _datos || json_build_object('habilitado_anterior', _data_old.habilitado)::jsonb;
				_datos := _datos || json_build_object('session_anterior', _data_old.session)::jsonb;
				_datos := _datos || json_build_object('ultima_modificacion_anterior', _data_old.ultima_modificacion)::jsonb;
				
		ELSE
			IF _data_old.id <> _data_new.id
				THEN _datos := _datos || json_build_object('id_anterior', _data_old.id, 'id_nuevo', _data_new.id)::jsonb;
			END IF;
			IF _data_old.id_usuario <> _data_new.id_usuario
				THEN _datos := _datos || json_build_object('id_usuario_anterior', _data_old.id_usuario, 'id_usuario_nuevo', _data_new.id_usuario)::jsonb;
			END IF;
			IF _data_old.id_evento <> _data_new.id_evento
				THEN _datos := _datos || json_build_object('id_evento_anterior', _data_old.id_evento, 'id_evento_nuevo', _data_new.id_evento)::jsonb;
			END IF;
			IF _data_old.habilitado <> _data_new.habilitado
				THEN _datos := _datos || json_build_object('habilitado_anterior', _data_old.habilitado, 'habilitado_nuevo', _data_new.habilitado)::jsonb;
			END IF;
			IF _data_old.session <> _data_new.session
				THEN _datos := _datos || json_build_object('session_anterior', _data_old.session, 'session_nuevo', _data_new.session)::jsonb;
			END IF;
			IF _data_old.ultima_modificacion <> _data_new.ultima_modificacion
				THEN _datos := _datos || json_build_object('ultima_modificacion_anterior', _data_old.ultima_modificacion, 'ultima_modificacion_nuevo', _data_new.ultima_modificacion)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'evento',
			'interes',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new evento.interes, _data_old evento.interes, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    211    10    211            �            1259    96023    token_recuperacion_usuario    TABLE     �   CREATE TABLE usuario.token_recuperacion_usuario (
    id integer NOT NULL,
    user_id bigint,
    token text,
    fecha_creado timestamp without time zone,
    fecha_vigencia timestamp without time zone
);
 /   DROP TABLE usuario.token_recuperacion_usuario;
       usuario         postgres    false    8            1           1255    105120 �   field_audit(usuario.token_recuperacion_usuario, usuario.token_recuperacion_usuario, character varying, text, character varying, text, text)    FUNCTION     S
  CREATE FUNCTION security.field_audit(_data_new usuario.token_recuperacion_usuario, _data_old usuario.token_recuperacion_usuario, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_nuevo', _data_new.id)::jsonb;
				_datos := _datos || json_build_object('user_id_nuevo', _data_new.user_id)::jsonb;
				_datos := _datos || json_build_object('token_nuevo', _data_new.token)::jsonb;
				_datos := _datos || json_build_object('fecha_creado_nuevo', _data_new.fecha_creado)::jsonb;
				_datos := _datos || json_build_object('fecha_vigencia_nuevo', _data_new.fecha_vigencia)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_anterior', _data_old.id)::jsonb;
				_datos := _datos || json_build_object('user_id_anterior', _data_old.user_id)::jsonb;
				_datos := _datos || json_build_object('token_anterior', _data_old.token)::jsonb;
				_datos := _datos || json_build_object('fecha_creado_anterior', _data_old.fecha_creado)::jsonb;
				_datos := _datos || json_build_object('fecha_vigencia_anterior', _data_old.fecha_vigencia)::jsonb;
				
		ELSE
			IF _data_old.id <> _data_new.id
				THEN _datos := _datos || json_build_object('id_anterior', _data_old.id, 'id_nuevo', _data_new.id)::jsonb;
			END IF;
			IF _data_old.user_id <> _data_new.user_id
				THEN _datos := _datos || json_build_object('user_id_anterior', _data_old.user_id, 'user_id_nuevo', _data_new.user_id)::jsonb;
			END IF;
			IF _data_old.token <> _data_new.token
				THEN _datos := _datos || json_build_object('token_anterior', _data_old.token, 'token_nuevo', _data_new.token)::jsonb;
			END IF;
			IF _data_old.fecha_creado <> _data_new.fecha_creado
				THEN _datos := _datos || json_build_object('fecha_creado_anterior', _data_old.fecha_creado, 'fecha_creado_nuevo', _data_new.fecha_creado)::jsonb;
			END IF;
			IF _data_old.fecha_vigencia <> _data_new.fecha_vigencia
				THEN _datos := _datos || json_build_object('fecha_vigencia_anterior', _data_old.fecha_vigencia, 'fecha_vigencia_nuevo', _data_new.fecha_vigencia)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'token_recuperacion_usuario',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new usuario.token_recuperacion_usuario, _data_old usuario.token_recuperacion_usuario, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    205    205    10            +           1255    104802 e   field_audit(usuario.usuario, usuario.usuario, character varying, text, character varying, text, text)    FUNCTION     �  CREATE FUNCTION security.field_audit(_data_new usuario.usuario, _data_old usuario.usuario, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text) RETURNS text
    LANGUAGE plpgsql
    AS $$

	DECLARE
		_column_data TEXT;
	 	_datos jsonb;
	 	
	BEGIN
			_datos = '{}';

		_column_data :=  _data_new.id;
		IF _accion = 'INSERT'
			THEN
				_datos := _datos || json_build_object('id_nuevo', _data_new.id)::jsonb;
				_datos := _datos || json_build_object('nombre_nuevo', _data_new.nombre)::jsonb;
				_datos := _datos || json_build_object('apellido_nuevo', _data_new.apellido)::jsonb;
				_datos := _datos || json_build_object('correo_nuevo', _data_new.correo)::jsonb;
				_datos := _datos || json_build_object('clave_nuevo', _data_new.clave)::jsonb;
				_datos := _datos || json_build_object('url_imagen_nuevo', _data_new.url_imagen)::jsonb;
				_datos := _datos || json_build_object('estado_nuevo', _data_new.estado)::jsonb;
				_datos := _datos || json_build_object('habilitado_nuevo', _data_new.habilitado)::jsonb;
				_datos := _datos || json_build_object('id_rol_nuevo', _data_new.id_rol)::jsonb;
				_datos := _datos || json_build_object('session_nuevo', _data_new.session)::jsonb;
				_datos := _datos || json_build_object('ultima_modificacion_nuevo', _data_new.ultima_modificacion)::jsonb;
				
		ELSEIF _accion = 'DELETE'
			THEN
				_datos := _datos || json_build_object('id_anterior', _data_old.id)::jsonb;
				_datos := _datos || json_build_object('nombre_anterior', _data_old.nombre)::jsonb;
				_datos := _datos || json_build_object('apellido_anterior', _data_old.apellido)::jsonb;
				_datos := _datos || json_build_object('correo_anterior', _data_old.correo)::jsonb;
				_datos := _datos || json_build_object('clave_anterior', _data_old.clave)::jsonb;
				_datos := _datos || json_build_object('url_imagen_anterior', _data_old.url_imagen)::jsonb;
				_datos := _datos || json_build_object('estado_anterior', _data_old.estado)::jsonb;
				_datos := _datos || json_build_object('habilitado_anterior', _data_old.habilitado)::jsonb;
				_datos := _datos || json_build_object('id_rol_anterior', _data_old.id_rol)::jsonb;
				_datos := _datos || json_build_object('session_anterior', _data_old.session)::jsonb;
				_datos := _datos || json_build_object('ultima_modificacion_anterior', _data_old.ultima_modificacion)::jsonb;
				
		ELSE
			IF _data_old.id <> _data_new.id
				THEN _datos := _datos || json_build_object('id_anterior', _data_old.id, 'id_nuevo', _data_new.id)::jsonb;
			END IF;
			IF _data_old.nombre <> _data_new.nombre
				THEN _datos := _datos || json_build_object('nombre_anterior', _data_old.nombre, 'nombre_nuevo', _data_new.nombre)::jsonb;
			END IF;
			IF _data_old.apellido <> _data_new.apellido
				THEN _datos := _datos || json_build_object('apellido_anterior', _data_old.apellido, 'apellido_nuevo', _data_new.apellido)::jsonb;
			END IF;
			IF _data_old.correo <> _data_new.correo
				THEN _datos := _datos || json_build_object('correo_anterior', _data_old.correo, 'correo_nuevo', _data_new.correo)::jsonb;
			END IF;
			IF _data_old.clave <> _data_new.clave
				THEN _datos := _datos || json_build_object('clave_anterior', _data_old.clave, 'clave_nuevo', _data_new.clave)::jsonb;
			END IF;
			IF _data_old.url_imagen <> _data_new.url_imagen
				THEN _datos := _datos || json_build_object('url_imagen_anterior', _data_old.url_imagen, 'url_imagen_nuevo', _data_new.url_imagen)::jsonb;
			END IF;
			IF _data_old.estado <> _data_new.estado
				THEN _datos := _datos || json_build_object('estado_anterior', _data_old.estado, 'estado_nuevo', _data_new.estado)::jsonb;
			END IF;
			IF _data_old.habilitado <> _data_new.habilitado
				THEN _datos := _datos || json_build_object('habilitado_anterior', _data_old.habilitado, 'habilitado_nuevo', _data_new.habilitado)::jsonb;
			END IF;
			IF _data_old.id_rol <> _data_new.id_rol
				THEN _datos := _datos || json_build_object('id_rol_anterior', _data_old.id_rol, 'id_rol_nuevo', _data_new.id_rol)::jsonb;
			END IF;
			IF _data_old.session <> _data_new.session
				THEN _datos := _datos || json_build_object('session_anterior', _data_old.session, 'session_nuevo', _data_new.session)::jsonb;
			END IF;
			IF _data_old.ultima_modificacion <> _data_new.ultima_modificacion
				THEN _datos := _datos || json_build_object('ultima_modificacion_anterior', _data_old.ultima_modificacion, 'ultima_modificacion_nuevo', _data_new.ultima_modificacion)::jsonb;
			END IF;
			 
		END IF;

		INSERT INTO security.auditoria
		(
			fecha,
			accion,
			schema,
			tabla,
			pk,
			session,
			user_bd,
			data
		)
		VALUES
		(
			CURRENT_TIMESTAMP,
			_accion,
			'usuario',
			'usuario',
			_table_pk,
			_session,
			_user_db,
			_datos::jsonb
			);

		RETURN NULL; 
	END;$$;
 �   DROP FUNCTION security.field_audit(_data_new usuario.usuario, _data_old usuario.usuario, _accion character varying, _session text, _user_db character varying, _table_pk text, _init text);
       security       postgres    false    201    10    201                        1255    96875     f_actualizar_clave(bigint, text)    FUNCTION     F  CREATE FUNCTION usuario.f_actualizar_clave(_user_id bigint, _clave text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
	
	BEGIN
		DELETE FROM
			usuario.token_recuperacion_usuario
		WHERE
			user_id = _user_id;
	
		UPDATE
			usuario.usuario
		SET
			estado = 1,
			clave = _clave
			
		WHERE
			id = _user_id;		
	END

$$;
 H   DROP FUNCTION usuario.f_actualizar_clave(_user_id bigint, _clave text);
       usuario       postgres    false    8                       1255    96198 4   f_almacenar_token_recuperacion_usuario(text, bigint)    FUNCTION     �  CREATE FUNCTION usuario.f_almacenar_token_recuperacion_usuario(_token text, _user_id bigint) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$	
	BEGIN
		INSERT INTO usuario.token_recuperacion_usuario
		(
			user_id,
			token, 
			fecha_creado,
			fecha_vigencia
			
		)
		VALUES 
		(
			_user_id,
			_token,
			current_timestamp,
			current_timestamp + interval '2 hours'
		);

			
	END

$$;
 \   DROP FUNCTION usuario.f_almacenar_token_recuperacion_usuario(_token text, _user_id bigint);
       usuario       postgres    false    8            �            1255    96150     f_buscar_existencia_correo(text)    FUNCTION     �   CREATE FUNCTION usuario.f_buscar_existencia_correo(_correo text) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			usuario.usuario
		WHERE
			correo = _correo;
	end
$$;
 @   DROP FUNCTION usuario.f_buscar_existencia_correo(_correo text);
       usuario       postgres    false    201    8            �            1255    96218     f_buscar_existencia_imagen(text)    FUNCTION     �   CREATE FUNCTION usuario.f_buscar_existencia_imagen(_url_imagen text) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	BEGIN
		return query
		SELECT
			*
		FROM
			usuario.usuario
		WHERE
			url_imagen = _url_imagen;
	end
$$;
 D   DROP FUNCTION usuario.f_buscar_existencia_imagen(_url_imagen text);
       usuario       postgres    false    8    201            %           1255    96918 -   f_deshabilitar_usuario(bigint, boolean, text)    FUNCTION     X  CREATE FUNCTION usuario.f_deshabilitar_usuario(_id_usuario bigint, _habilitado boolean, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
	
	 UPDATE usuario.usuario SET

	    habilitado=_habilitado,
	    session=_session,
	    ultima_modificacion=current_timestamp
       
	where 
	
	  id =_id_usuario;
	 
   
 END
$$;
 f   DROP FUNCTION usuario.f_deshabilitar_usuario(_id_usuario bigint, _habilitado boolean, _session text);
       usuario       postgres    false    8            �            1255    96219 4   f_editar_usuario(text, text, text, text, text, text)    FUNCTION     �  CREATE FUNCTION usuario.f_editar_usuario(_nombre text, _apellido text, _correo text, _clave text, _url_imagen text, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    UPDATE usuario.usuario SET

	 nombre = _nombre ,
	 apellido=_apellido,
	 correo= correo,
	 clave= _clave,
	 url_imagen=_url_imagen ,
	 session=_session,
	 ultima_modificacion=current_timestamp

	where 
	
	  correo =_correo;
 END
$$;
 �   DROP FUNCTION usuario.f_editar_usuario(_nombre text, _apellido text, _correo text, _clave text, _url_imagen text, _session text);
       usuario       postgres    false    8            �            1255    96145 6   f_insertar_usuario(text, text, text, text, text, text)    FUNCTION     �  CREATE FUNCTION usuario.f_insertar_usuario(_nombre text, _apellido text, _correo text, _clave text, _url_imagen text, _session text) RETURNS SETOF void
    LANGUAGE plpgsql
    AS $$
    BEGIN
    INSERT INTO usuario.usuario 

	(
	 nombre ,
	 apellido,
	 correo ,
	 clave ,
	 url_imagen ,
	 estado ,
	 habilitado,
	 id_rol ,
	 session,
	 ultima_modificacion
	) 
	
	VALUES (
    _nombre ,
	_apellido ,
	_correo,
	_clave ,
	_url_imagen ,
	 1,
	 true ,
	 2,
	_session,
	current_timestamp
	);
 END
$$;
 �   DROP FUNCTION usuario.f_insertar_usuario(_nombre text, _apellido text, _correo text, _clave text, _url_imagen text, _session text);
       usuario       postgres    false    8                       1255    96170    f_mostrar_datos_usuario(text)    FUNCTION       CREATE FUNCTION usuario.f_mostrar_datos_usuario(_correo text) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return query
        SELECT
            *
        FROM
            usuario.usuario
        WHERE
            correo = _correo;
    end
$$;
 =   DROP FUNCTION usuario.f_mostrar_datos_usuario(_correo text);
       usuario       postgres    false    8    201            �            1259    96901    mostrar_usuarios_view    VIEW     �   CREATE VIEW usuario.mostrar_usuarios_view AS
 SELECT (0)::bigint AS id_usuario,
    ''::text AS nombre_completo,
    ''::text AS correo,
    true AS habilitado,
    ''::text AS imagen;
 )   DROP VIEW usuario.mostrar_usuarios_view;
       usuario       postgres    false    8            $           1255    96917 1   f_mostrar_datos_usuarios_filtro(text, text, text)    FUNCTION     �  CREATE FUNCTION usuario.f_mostrar_datos_usuarios_filtro(_nombre_filtro text, _apellido_filtro text, _correo_filtro text) RETURNS SETOF usuario.mostrar_usuarios_view
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return query
        SELECT
		
		   usuario.usuario.id,
		   (usuario.usuario.nombre || '  ' || usuario.usuario.apellido),
		   usuario.usuario.correo,
		   usuario.usuario.habilitado,
		   usuario.usuario.url_imagen
		    
   
        FROM
            usuario.usuario
			
        WHERE
		    id_rol!=1
			 AND
			(CASE _nombre_filtro 
				WHEN '' THEN 
					usuario.usuario.nombre ilike '%' || _nombre_filtro || '%'
				ELSE 
					usuario.usuario.nombre ilike '%' || _nombre_filtro || '%'
			    
				END)
				AND
				(CASE _apellido_filtro 
				WHEN '' THEN 
					usuario.usuario.apellido ilike '%' || _apellido_filtro || '%'
				ELSE 
					usuario.usuario.apellido ilike '%' || _apellido_filtro || '%'
			    
				END)
			AND
			(CASE _correo_filtro 
				WHEN '' THEN 
					usuario.usuario.correo ilike '%' || _correo_filtro || '%' 
				ELSE 
					usuario.usuario.correo ilike '%' || _correo_filtro || '%' 
				END);
            
    end
$$;
 x   DROP FUNCTION usuario.f_mostrar_datos_usuarios_filtro(_nombre_filtro text, _apellido_filtro text, _correo_filtro text);
       usuario       postgres    false    229    8                       1255    96201 *   f_obtener_usuario_token_recuperacion(text)    FUNCTION     �  CREATE FUNCTION usuario.f_obtener_usuario_token_recuperacion(_token text) RETURNS SETOF bigint
    LANGUAGE plpgsql
    AS $$
	
	BEGIN
		IF (SELECT COUNT(*) FROM usuario.token_recuperacion_usuario WHERE token = _token) = 0
			THEN RETURN QUERY
				SELECT
					-1::INTEGER;
		elseIF (SELECT COUNT(*)FROM usuario.token_recuperacion_usuario WHERE token = _token AND current_timestamp between fecha_creado AND fecha_vigencia) = 0
			THEN
				DELETE FROM usuario.token_recuperacion_usuario WHERE token = _token;
			
				RETURN QUERY
				SELECT
					-2::INTEGER;
		ELSE	
			RETURN QUERY 
				SELECT
					user_id
				FROM
					usuario.token_recuperacion_usuario
				WHERE
					token = _token;
		
		END IF;

			
	END

$$;
 I   DROP FUNCTION usuario.f_obtener_usuario_token_recuperacion(_token text);
       usuario       postgres    false    8                       1255    96874    f_validar_usuario_usuario(text)    FUNCTION     �  CREATE FUNCTION usuario.f_validar_usuario_usuario(_correo text) RETURNS SETOF usuario.usuario
    LANGUAGE plpgsql
    AS $$
	
	BEGIN
		IF (SELECT COUNT(*) FROM usuario.usuario WHERE correo = _correo) = 0
			then RETURN QUERY
				SELECT
					-1::BIGINT,
					''::CHARACTER VARYING(20),
					''::CHARACTER VARYING(20),
					''::TEXT,
					''::TEXT,
					''::TEXT,
					1::INTEGER,
					true::BOOLEAN,
					1::INTEGER,
					''::TEXT,
					'2018-01-01 00:00'::TIMESTAMP;
					
		elseIF (SELECT COUNT(*) FROM usuario.usuario uu join usuario.token_recuperacion_usuario ut on ut.user_id = uu.id WHERE uu.correo = _correo and current_timestamp between ut.fecha_creado AND ut.fecha_vigencia) > 0

			then RETURN QUERY
				SELECT
					-2::BIGINT,
					''::CHARACTER VARYING(20),
					''::CHARACTER VARYING(20),
					''::TEXT,
					''::TEXT,
					''::TEXT,
					1::INTEGER,
					true::BOOLEAN,
					1::INTEGER,
					''::TEXT,
					'2018-01-01 00:00'::TIMESTAMP;
		ELSEIF (SELECT COUNT(*) FROM usuario.usuario WHERE correo = _correo) > 0
			THEN
				update 
					usuario.usuario
				set
					estado = 2
				where
					correo = _correo;
				
				RETURN QUERY 
				SELECT
					*
				FROM
					usuario.usuario 
				WHERE
					correo = _correo;
		ELSE
			RETURN QUERY
				SELECT
					-1::BIGINT,
					''::CHARACTER VARYING(20),
					''::CHARACTER VARYING(20),
					''::TEXT,
					''::TEXT,
					''::TEXT,
					1::INTEGER,
					true::BOOLEAN,
					1::INTEGER,
					''::TEXT,
					'2018-01-01 00:00'::TIMESTAMP;
		END IF;
			
	END

$$;
 ?   DROP FUNCTION usuario.f_validar_usuario_usuario(_correo text);
       usuario       postgres    false    201    8            �            1259    96535    cancelar_evento_id_seq    SEQUENCE        CREATE SEQUENCE evento.cancelar_evento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE evento.cancelar_evento_id_seq;
       evento       postgres    false    6    223                       0    0    cancelar_evento_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE evento.cancelar_evento_id_seq OWNED BY evento.cancelar_evento.id;
            evento       postgres    false    222            �            1259    96099    categoria_evento_id_seq    SEQUENCE     �   CREATE SEQUENCE evento.categoria_evento_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE evento.categoria_evento_id_seq;
       evento       postgres    false    6    217                       0    0    categoria_evento_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE evento.categoria_evento_id_seq OWNED BY evento.categoria_evento.id;
            evento       postgres    false    216            �            1259    96088    denuncia_evento_id_seq    SEQUENCE     �   CREATE SEQUENCE evento.denuncia_evento_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE evento.denuncia_evento_id_seq;
       evento       postgres    false    6    215                       0    0    denuncia_evento_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE evento.denuncia_evento_id_seq OWNED BY evento.denuncia_evento.id;
            evento       postgres    false    214            �            1259    96077    estado_evento_id_seq    SEQUENCE     �   CREATE SEQUENCE evento.estado_evento_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE evento.estado_evento_id_seq;
       evento       postgres    false    213    6                       0    0    estado_evento_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE evento.estado_evento_id_seq OWNED BY evento.estado_evento.id;
            evento       postgres    false    212            �            1259    96054    interes_id_seq    SEQUENCE     w   CREATE SEQUENCE evento.interes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE evento.interes_id_seq;
       evento       postgres    false    6    211                       0    0    interes_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE evento.interes_id_seq OWNED BY evento.interes.id;
            evento       postgres    false    210            �            1259    96045 	   auditoria    TABLE       CREATE TABLE security.auditoria (
    id bigint NOT NULL,
    fecha timestamp without time zone,
    accion character varying(100),
    schema character varying(200),
    tabla character varying(200),
    session text,
    user_bd character varying(100),
    data jsonb,
    pk text
);
    DROP TABLE security.auditoria;
       security         postgres    false    10            �            1259    96043    auditoria_id_seq    SEQUENCE     {   CREATE SEQUENCE security.auditoria_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE security.auditoria_id_seq;
       security       postgres    false    10    209                       0    0    auditoria_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE security.auditoria_id_seq OWNED BY security.auditoria.id;
            security       postgres    false    208            �            1259    96034    autentication    TABLE       CREATE TABLE security.autentication (
    id bigint NOT NULL,
    codigo_usuario integer,
    ip character varying(100),
    mac character varying(100),
    fecha_inicio timestamp without time zone,
    session text,
    fecha_fin timestamp without time zone
);
 #   DROP TABLE security.autentication;
       security         postgres    false    10            �            1259    96032    autentication_id_seq    SEQUENCE        CREATE SEQUENCE security.autentication_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE security.autentication_id_seq;
       security       postgres    false    10    207                       0    0    autentication_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE security.autentication_id_seq OWNED BY security.autentication.id;
            security       postgres    false    206            �            1259    104793    function_db_view    VIEW     �  CREATE VIEW security.function_db_view AS
 SELECT pp.proname AS b_function,
    oidvectortypes(pp.proargtypes) AS b_type_parameters
   FROM (pg_proc pp
     JOIN pg_namespace pn ON ((pn.oid = pp.pronamespace)))
  WHERE ((pn.nspname)::text <> ALL (ARRAY[('pg_catalog'::character varying)::text, ('information_schema'::character varying)::text, ('admin_control'::character varying)::text, ('vial'::character varying)::text]));
 %   DROP VIEW security.function_db_view;
       security       postgres    false    10            �            1259    96012    rol    TABLE     �   CREATE TABLE usuario.rol (
    id integer NOT NULL,
    descripcion character varying(20),
    session text,
    ultima_modificacion timestamp without time zone
);
    DROP TABLE usuario.rol;
       usuario         postgres    false    8            �            1259    96010 
   rol_id_seq    SEQUENCE     �   CREATE SEQUENCE usuario.rol_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE usuario.rol_id_seq;
       usuario       postgres    false    8    203                       0    0 
   rol_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE usuario.rol_id_seq OWNED BY usuario.rol.id;
            usuario       postgres    false    202            �            1259    96021 !   token_recuperacion_usuario_id_seq    SEQUENCE     �   CREATE SEQUENCE usuario.token_recuperacion_usuario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE usuario.token_recuperacion_usuario_id_seq;
       usuario       postgres    false    205    8                        0    0 !   token_recuperacion_usuario_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE usuario.token_recuperacion_usuario_id_seq OWNED BY usuario.token_recuperacion_usuario.id;
            usuario       postgres    false    204            �            1259    95999    usuario_id_seq    SEQUENCE     x   CREATE SEQUENCE usuario.usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE usuario.usuario_id_seq;
       usuario       postgres    false    201    8            !           0    0    usuario_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE usuario.usuario_id_seq OWNED BY usuario.usuario.id;
            usuario       postgres    false    200            J           2604    96540    cancelar_evento id    DEFAULT     x   ALTER TABLE ONLY evento.cancelar_evento ALTER COLUMN id SET DEFAULT nextval('evento.cancelar_evento_id_seq'::regclass);
 A   ALTER TABLE evento.cancelar_evento ALTER COLUMN id DROP DEFAULT;
       evento       postgres    false    223    222    223            H           2604    96104    categoria_evento id    DEFAULT     z   ALTER TABLE ONLY evento.categoria_evento ALTER COLUMN id SET DEFAULT nextval('evento.categoria_evento_id_seq'::regclass);
 B   ALTER TABLE evento.categoria_evento ALTER COLUMN id DROP DEFAULT;
       evento       postgres    false    217    216    217            G           2604    96093    denuncia_evento id    DEFAULT     x   ALTER TABLE ONLY evento.denuncia_evento ALTER COLUMN id SET DEFAULT nextval('evento.denuncia_evento_id_seq'::regclass);
 A   ALTER TABLE evento.denuncia_evento ALTER COLUMN id DROP DEFAULT;
       evento       postgres    false    215    214    215            F           2604    96082    estado_evento id    DEFAULT     t   ALTER TABLE ONLY evento.estado_evento ALTER COLUMN id SET DEFAULT nextval('evento.estado_evento_id_seq'::regclass);
 ?   ALTER TABLE evento.estado_evento ALTER COLUMN id DROP DEFAULT;
       evento       postgres    false    213    212    213            E           2604    96059 
   interes id    DEFAULT     h   ALTER TABLE ONLY evento.interes ALTER COLUMN id SET DEFAULT nextval('evento.interes_id_seq'::regclass);
 9   ALTER TABLE evento.interes ALTER COLUMN id DROP DEFAULT;
       evento       postgres    false    211    210    211            D           2604    96048    auditoria id    DEFAULT     p   ALTER TABLE ONLY security.auditoria ALTER COLUMN id SET DEFAULT nextval('security.auditoria_id_seq'::regclass);
 =   ALTER TABLE security.auditoria ALTER COLUMN id DROP DEFAULT;
       security       postgres    false    209    208    209            C           2604    96037    autentication id    DEFAULT     x   ALTER TABLE ONLY security.autentication ALTER COLUMN id SET DEFAULT nextval('security.autentication_id_seq'::regclass);
 A   ALTER TABLE security.autentication ALTER COLUMN id DROP DEFAULT;
       security       postgres    false    206    207    207            A           2604    96015    rol id    DEFAULT     b   ALTER TABLE ONLY usuario.rol ALTER COLUMN id SET DEFAULT nextval('usuario.rol_id_seq'::regclass);
 6   ALTER TABLE usuario.rol ALTER COLUMN id DROP DEFAULT;
       usuario       postgres    false    203    202    203            B           2604    96026    token_recuperacion_usuario id    DEFAULT     �   ALTER TABLE ONLY usuario.token_recuperacion_usuario ALTER COLUMN id SET DEFAULT nextval('usuario.token_recuperacion_usuario_id_seq'::regclass);
 M   ALTER TABLE usuario.token_recuperacion_usuario ALTER COLUMN id DROP DEFAULT;
       usuario       postgres    false    205    204    205            @           2604    96004 
   usuario id    DEFAULT     j   ALTER TABLE ONLY usuario.usuario ALTER COLUMN id SET DEFAULT nextval('usuario.usuario_id_seq'::regclass);
 :   ALTER TABLE usuario.usuario ALTER COLUMN id DROP DEFAULT;
       usuario       postgres    false    201    200    201                      0    96537    cancelar_evento 
   TABLE DATA               ]   COPY evento.cancelar_evento (id, razon, id_evento, session, ultima_modificacion) FROM stdin;
    evento       postgres    false    223   �                0    96101    categoria_evento 
   TABLE DATA               Y   COPY evento.categoria_evento (id, descripcion, session, ultima_modificacion) FROM stdin;
    evento       postgres    false    217   =�                0    96090    denuncia_evento 
   TABLE DATA               o   COPY evento.denuncia_evento (id, descripcion, id_evento, session, ultima_modificacion, id_usuario) FROM stdin;
    evento       postgres    false    215   R�      	          0    96079    estado_evento 
   TABLE DATA               V   COPY evento.estado_evento (id, descripcion, session, ultima_modificacion) FROM stdin;
    evento       postgres    false    213   R�                0    96320    evento 
   TABLE DATA               �   COPY evento.evento (id, nombre, descripcion, fecha_inicio, fecha_fin, hora, imagen, id_usuario, id_categoria, id_estado, cantidad_interesados, cantidad_denuncias, session, ultima_modificacion, lugar) FROM stdin;
    evento       postgres    false    220   ��                0    96056    interes 
   TABLE DATA               f   COPY evento.interes (id, id_usuario, id_evento, habilitado, session, ultima_modificacion) FROM stdin;
    evento       postgres    false    211   ��                0    96045 	   auditoria 
   TABLE DATA               c   COPY security.auditoria (id, fecha, accion, schema, tabla, session, user_bd, data, pk) FROM stdin;
    security       postgres    false    209   <�                0    96034    autentication 
   TABLE DATA               h   COPY security.autentication (id, codigo_usuario, ip, mac, fecha_inicio, session, fecha_fin) FROM stdin;
    security       postgres    false    207   W�      �          0    96012    rol 
   TABLE DATA               M   COPY usuario.rol (id, descripcion, session, ultima_modificacion) FROM stdin;
    usuario       postgres    false    203   c�                0    96023    token_recuperacion_usuario 
   TABLE DATA               g   COPY usuario.token_recuperacion_usuario (id, user_id, token, fecha_creado, fecha_vigencia) FROM stdin;
    usuario       postgres    false    205   ��      �          0    96001    usuario 
   TABLE DATA               �   COPY usuario.usuario (id, nombre, apellido, correo, clave, url_imagen, estado, habilitado, id_rol, session, ultima_modificacion) FROM stdin;
    usuario       postgres    false    201   �      "           0    0    cancelar_evento_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('evento.cancelar_evento_id_seq', 6, true);
            evento       postgres    false    222            #           0    0    categoria_evento_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('evento.categoria_evento_id_seq', 11, true);
            evento       postgres    false    216            $           0    0    denuncia_evento_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('evento.denuncia_evento_id_seq', 4, true);
            evento       postgres    false    214            %           0    0    estado_evento_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('evento.estado_evento_id_seq', 1, false);
            evento       postgres    false    212            &           0    0    evento_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('evento.evento_id_seq', 9, true);
            evento       postgres    false    219            '           0    0    interes_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('evento.interes_id_seq', 7, true);
            evento       postgres    false    210            (           0    0    auditoria_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('security.auditoria_id_seq', 30528, true);
            security       postgres    false    208            )           0    0    autentication_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('security.autentication_id_seq', 73, true);
            security       postgres    false    206            *           0    0 
   rol_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('usuario.rol_id_seq', 1, false);
            usuario       postgres    false    202            +           0    0 !   token_recuperacion_usuario_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('usuario.token_recuperacion_usuario_id_seq', 6, true);
            usuario       postgres    false    204            ,           0    0    usuario_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('usuario.usuario_id_seq', 7, true);
            usuario       postgres    false    200            `           2606    96545 $   cancelar_evento cancelar_evento_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY evento.cancelar_evento
    ADD CONSTRAINT cancelar_evento_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY evento.cancelar_evento DROP CONSTRAINT cancelar_evento_pkey;
       evento         postgres    false    223            \           2606    96109 &   categoria_evento categoria_evento_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY evento.categoria_evento
    ADD CONSTRAINT categoria_evento_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY evento.categoria_evento DROP CONSTRAINT categoria_evento_pkey;
       evento         postgres    false    217            Z           2606    96098 $   denuncia_evento denuncia_evento_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY evento.denuncia_evento
    ADD CONSTRAINT denuncia_evento_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY evento.denuncia_evento DROP CONSTRAINT denuncia_evento_pkey;
       evento         postgres    false    215            X           2606    96087     estado_evento estado_evento_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY evento.estado_evento
    ADD CONSTRAINT estado_evento_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY evento.estado_evento DROP CONSTRAINT estado_evento_pkey;
       evento         postgres    false    213            ^           2606    96328    evento evento_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY evento.evento
    ADD CONSTRAINT evento_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY evento.evento DROP CONSTRAINT evento_pkey;
       evento         postgres    false    220            V           2606    96064    interes interes_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY evento.interes
    ADD CONSTRAINT interes_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY evento.interes DROP CONSTRAINT interes_pkey;
       evento         postgres    false    211            T           2606    96053    auditoria auditoria_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY security.auditoria
    ADD CONSTRAINT auditoria_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY security.auditoria DROP CONSTRAINT auditoria_pkey;
       security         postgres    false    209            R           2606    96042     autentication autentication_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY security.autentication
    ADD CONSTRAINT autentication_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY security.autentication DROP CONSTRAINT autentication_pkey;
       security         postgres    false    207            N           2606    96020    rol rol_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY usuario.rol
    ADD CONSTRAINT rol_pkey PRIMARY KEY (id);
 7   ALTER TABLE ONLY usuario.rol DROP CONSTRAINT rol_pkey;
       usuario         postgres    false    203            P           2606    96031 :   token_recuperacion_usuario token_recuperacion_usuario_pkey 
   CONSTRAINT     y   ALTER TABLE ONLY usuario.token_recuperacion_usuario
    ADD CONSTRAINT token_recuperacion_usuario_pkey PRIMARY KEY (id);
 e   ALTER TABLE ONLY usuario.token_recuperacion_usuario DROP CONSTRAINT token_recuperacion_usuario_pkey;
       usuario         postgres    false    205            L           2606    96009    usuario usuario_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY usuario.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);
 ?   ALTER TABLE ONLY usuario.usuario DROP CONSTRAINT usuario_pkey;
       usuario         postgres    false    201            n           2620    104807    estado_evento tg_estado_evento    TRIGGER     �   CREATE TRIGGER tg_estado_evento AFTER INSERT OR DELETE OR UPDATE ON evento.estado_evento FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 7   DROP TRIGGER tg_estado_evento ON evento.estado_evento;
       evento       postgres    false    213    290            r           2620    104810 )   cancelar_evento tg_evento_cancelar_evento    TRIGGER     �   CREATE TRIGGER tg_evento_cancelar_evento AFTER INSERT OR DELETE OR UPDATE ON evento.cancelar_evento FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 B   DROP TRIGGER tg_evento_cancelar_evento ON evento.cancelar_evento;
       evento       postgres    false    290    223            p           2620    104809 ,   categoria_evento tg_evento_categoria_envento    TRIGGER     �   CREATE TRIGGER tg_evento_categoria_envento AFTER INSERT OR DELETE OR UPDATE ON evento.categoria_evento FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 E   DROP TRIGGER tg_evento_categoria_envento ON evento.categoria_evento;
       evento       postgres    false    217    290            o           2620    104808 )   denuncia_evento tg_evento_denundia_evento    TRIGGER     �   CREATE TRIGGER tg_evento_denundia_evento AFTER INSERT OR DELETE OR UPDATE ON evento.denuncia_evento FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 B   DROP TRIGGER tg_evento_denundia_evento ON evento.denuncia_evento;
       evento       postgres    false    215    290            q           2620    104806    evento tg_evento_evento    TRIGGER     �   CREATE TRIGGER tg_evento_evento AFTER INSERT OR DELETE OR UPDATE ON evento.evento FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 0   DROP TRIGGER tg_evento_evento ON evento.evento;
       evento       postgres    false    290    220            m           2620    104805    interes tg_evento_interes    TRIGGER     �   CREATE TRIGGER tg_evento_interes AFTER INSERT OR DELETE OR UPDATE ON evento.interes FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 2   DROP TRIGGER tg_evento_interes ON evento.interes;
       evento       postgres    false    290    211            l           2620    104803 #   token_recuperacion_usuario tg_token    TRIGGER     �   CREATE TRIGGER tg_token AFTER INSERT OR DELETE OR UPDATE ON usuario.token_recuperacion_usuario FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 =   DROP TRIGGER tg_token ON usuario.token_recuperacion_usuario;
       usuario       postgres    false    290    205            k           2620    104804    rol tg_usuario_rol    TRIGGER     �   CREATE TRIGGER tg_usuario_rol AFTER INSERT OR DELETE OR UPDATE ON usuario.rol FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 ,   DROP TRIGGER tg_usuario_rol ON usuario.rol;
       usuario       postgres    false    290    203            j           2620    104801    usuario tg_usuario_usuario    TRIGGER     �   CREATE TRIGGER tg_usuario_usuario AFTER INSERT OR DELETE OR UPDATE ON usuario.usuario FOR EACH ROW EXECUTE PROCEDURE security.f_log_auditoria();
 4   DROP TRIGGER tg_usuario_usuario ON usuario.usuario;
       usuario       postgres    false    201    290            f           2606    96329    evento id_categoria    FK CONSTRAINT     �   ALTER TABLE ONLY evento.evento
    ADD CONSTRAINT id_categoria FOREIGN KEY (id_categoria) REFERENCES evento.categoria_evento(id);
 =   ALTER TABLE ONLY evento.evento DROP CONSTRAINT id_categoria;
       evento       postgres    false    217    220    2908            g           2606    96334    evento id_estado    FK CONSTRAINT     y   ALTER TABLE ONLY evento.evento
    ADD CONSTRAINT id_estado FOREIGN KEY (id_estado) REFERENCES evento.estado_evento(id);
 :   ALTER TABLE ONLY evento.evento DROP CONSTRAINT id_estado;
       evento       postgres    false    2904    220    213            d           2606    96344    denuncia_evento id_evento    FK CONSTRAINT     {   ALTER TABLE ONLY evento.denuncia_evento
    ADD CONSTRAINT id_evento FOREIGN KEY (id_evento) REFERENCES evento.evento(id);
 C   ALTER TABLE ONLY evento.denuncia_evento DROP CONSTRAINT id_evento;
       evento       postgres    false    2910    220    215            c           2606    96349    interes id_evento    FK CONSTRAINT     s   ALTER TABLE ONLY evento.interes
    ADD CONSTRAINT id_evento FOREIGN KEY (id_evento) REFERENCES evento.evento(id);
 ;   ALTER TABLE ONLY evento.interes DROP CONSTRAINT id_evento;
       evento       postgres    false    2910    211    220            i           2606    96546    cancelar_evento id_evento    FK CONSTRAINT     {   ALTER TABLE ONLY evento.cancelar_evento
    ADD CONSTRAINT id_evento FOREIGN KEY (id_evento) REFERENCES evento.evento(id);
 C   ALTER TABLE ONLY evento.cancelar_evento DROP CONSTRAINT id_evento;
       evento       postgres    false    220    223    2910            b           2606    96135    interes id_usuario    FK CONSTRAINT     w   ALTER TABLE ONLY evento.interes
    ADD CONSTRAINT id_usuario FOREIGN KEY (id_usuario) REFERENCES usuario.usuario(id);
 <   ALTER TABLE ONLY evento.interes DROP CONSTRAINT id_usuario;
       evento       postgres    false    211    2892    201            h           2606    96339    evento id_usuario    FK CONSTRAINT     v   ALTER TABLE ONLY evento.evento
    ADD CONSTRAINT id_usuario FOREIGN KEY (id_usuario) REFERENCES usuario.usuario(id);
 ;   ALTER TABLE ONLY evento.evento DROP CONSTRAINT id_usuario;
       evento       postgres    false    220    201    2892            e           2606    96856    denuncia_evento id_usuario    FK CONSTRAINT        ALTER TABLE ONLY evento.denuncia_evento
    ADD CONSTRAINT id_usuario FOREIGN KEY (id_usuario) REFERENCES usuario.usuario(id);
 D   ALTER TABLE ONLY evento.denuncia_evento DROP CONSTRAINT id_usuario;
       evento       postgres    false    2892    215    201            a           2606    96110    usuario rol_id    FK CONSTRAINT     l   ALTER TABLE ONLY usuario.usuario
    ADD CONSTRAINT rol_id FOREIGN KEY (id_rol) REFERENCES usuario.rol(id);
 9   ALTER TABLE ONLY usuario.usuario DROP CONSTRAINT rol_id;
       usuario       postgres    false    2894    201    203               +  x���Kr�0��)�Ph��.��.�d����H`�>8����^w?��a�ݤ�30������@�!p>�3��V��]�����8�A_�~�{Ͱ�"+d�5��Q�(ʱ.���vt�[o�u�"a����=��q`��On&/����LZm��[���>��'�yI�CK���UNޙ*����*UV\2~n�kD��Kl�:��k����ei�8��u�4��D٬�s��D�տ"�)�1��q�Pvi���j��ΤL(��Q�e�AQ���0&���e�ˇ1���z���{�9����N           x�m��r� Dk�
~����<�q����XFD�b��#��$���\u���q����=�ܷn� 6Bm2�XVnj�)��Ͼ��0���F�
�l��5r���g7��֠Õ�� ����jt����!��bjVܡ�N߬��%�\.�*(o����� �Xm��J����Ә|[�˖_�͕��y)�J)�U���ʂ���ւ�%"P)I��E�1�k���,����Y��He,���ig%X��VZ�=sJ��'r@         �   x����m� @�5T���cbSK6�{�6��IW�!��(�HY��{���r�9�4�|���TCz��)~<C��ާ�Xw;��1�1Bhɴ�wý�*�+���o�Ã���>�N����|�M�F�f�KڨV\.��?�~	Bj&4bǱ�"�J�����e+���f��ݔ�*9 �&��@ʡl�R��WRnc�iթpi'[��\������*��C7�0�H$}�(�_�h      	   D   x�3�tL.�,���".#N�<�Ңb��1�[f^bNfUb
LȄӱ 'I���91/95!���� ��         �  x���͎� ���)��.q�]E�,F�Mw�"�q�I���,�����F���W���|�����*��X��G��ȵ���}�)�� �XxZ�� ����S/��(�^�c�'�r0[DF<.�3Ĺ��{ ��#?5���\dy�YN�a�!��+NF����:m��F��m�M��腓"�]1.�{qڻ�玼u��Y� �i	��L�7�9�
��-G&��q���&�m[7��O��:q��x�0��YIf���5�U�B�>�[d�b�Ny�t���q�Ѹ�&E�[%|>'r���o7G`ZB�0������B�2J4C�
����;\c8��ġJ	��U�	��	�G�~��|t�r�ZA$����e�dza�b����m())ZO�L�E2�\��yZd�)�}
c�uvPrL)!�w�xZ�yD@ πb���3Bʷ�:-)��4-�2˲_i.         �   x���K�0 ���\�ff�iK��F!�,Z����0q�^���Ӻ��:�	�m��0�c������D["$%R����)���zﷹ��Nͺ=������)0Dr�XMh��ʁ9��|�u���8�b��������(!����U|Z���"ym�BGꢕR4@>�            x��][o㺹}���<w�_�t6v�@�����V �%�Q����dR���C�B��䘉?:L2��>��ŏeI��HuGA?#�3#L��ϴ֌�����_&_�̋Yl��oUSm�y��nXY-WM�̪����n�l6��i���e;�����󲬫�ȋz��_l����x��8�~�ݨ��|�1�z���FrL��	���򯻟�OԒ����
��cPt�Nt9��0��)�h�؊�� ��9(� E���Af"h�;8Ô0�0^��"#:J��\#Hp	N ���$GH����a��>��(`��":N,�S�7�m�}�F��������s���9��i6y�tL�mn���H<������`1�z�Y�8�K˚C��C
;t	�v�v�v�`�) m'�m�ءXa#�3�Ǣ�Iy�48R,��%���L�C��C�9t	�v�v�v� ێ1h�	h��������=2P��^��[��l�Z4�v����?,�y7jm��b�����r�h����}U��x������S/j;����3 �$b�]���p%;1N+:�����@����K���D�di.�B��:��{�x7$�s����m!���g���N�`���8�ٗ��w���~���n�ls�����lgߋ�������*Z�-m��y�b:&��)VZ��RH�4ϧ�KT�����S�LY�dFV���br�3]N[��r�6�:��|�	���s^ϳb�ܾ�G۲�r�^"����y�����j��EhL�5���������A���i=��&m�[�����-&fƋ�����{�4�Ͽ�;`�s����b6T���0Q�v&��	�K��V�8�b4��Q6�Yռ���u�H�	M�O�񗇮��*��IIb/n�OV��)�+��S>�j���F��Q�TӦ*�A�~Ǖ�TKNX.��DqU�l�����n�R9��jץ�Ƭ�0č�bm|�qOx�!�R��.��(�ӱpmf�8�M�w�����^�].��G�v~>��|5��"8�#A�t��8����H���)�FUZ��M)*�����*���(�_)���� (��Q��>�Ϲ S��2�0����S���z�r�Y�E.��N�\Q�1�\R��xə(��K�^8�K��lF��G�q�o�Xm�6�^��e���Wh�k�^|�NR��A�S��Wd����C�|ޘ�$����3X�#%TgM�#5��/�V���ՕN�3Q�#���nc�e^�lc��	2l�һg���7%z*;��k�Mp�px�i�>�w�JH�Ե4�dEla��UCK-��g8���Lj�]��w>��]�fB+��
�.K��?*�e&<��o����t1/7O�^�eNȂ<?���Г!%"<��:pLY�%�ҙ�dŲ��m����I"Z��&̻�F7̃�
A����&�1����A>DR��}�~]�q(��D������1��L~�z	^��%Y�Ԉ����$8���A
s�ؕ�L
��GQ�t#�.t�'��!�EǠ�$Bo;�dRh��C_�K~�D/�Nt�()��@�9(� E�<TiD#r/�ҭ�QR ���C�����Qŵ��o�#?`��B:N��Cg��]��)�pa!˸`m5�(`��':R
��Cא���cPt��{�H%�.u������f���HQR���sPt��e&�F���c�D�� ���\d&�&Q�t3Vt�)��!�%EǠ�]f�x�lB�e�ۚ�d�xL1S�)
Aʡ3Pt�.@ѥ��-�%�o�(`��N��H)R]C�{�^B�cP� 3��1A�&��iO���f���HQR���sPt�f&y�ML�%�j��C0�L�)AʡkHt�@�1(:��ۊ֢k��j�(`��"+:R��Cg��]����?-Ř�����)
�n������ ��5 :C�����;op�n1�E�[ё��:E����Ld�I�<""��h����.3�D��XёR���D�n��@Ǡ�$@?^�G9�����La�$3�Zё��:E��]F��"�	n�ob&?`��"+:R
��Cא���cP�03�G4�[�rA�t+Ft�( %� �9$�����bE*u[���G~�T#�.t�'��!�)EǠ�AJ:��h&9|��5�L�R�DG�B�r����Pt�����L�x�V���() J\�3	�!���D�7���H����N���F�]�8QN���sPt�.#���!�f���I0�K�Nt�)��!ѽK�!�1(z���k�Im��ptL7S`EG�B�r����Pt���c�:ᄁ���$
�h�؊��� ��5$�@��=�L��;����M�b/�ҭ�Q� �8���\F����퓋oP-ES���qR���D���)ɭ�a�
]:��{)p�#E!H9t��A�(����kl�p{;�-��L4SlEGJA�r�]!Pt��e&�ޙ@q{ɫ�Ha��$
�n����� ��(:E���G?��vg���ݵ^
�%Y�Ԉ�����5 �����1$8��I�@��D��>O��|¨}t\�]n����m�/f��邏G��gڝ{6��i�����-�� f���\gvՀ�����G���N�����=���"9��	�P\�_��&Q�tR~t�8'�.@����qr�.z�ݏ+r$����Ka�t/N<��A�r����SPt��_����g5�$
�j�Ȋ�� ��(�EW��af�c���.�%k?��Ĥ�f�7�2��S�2���|��������;���-GRE�8�=r���->5Y�85�436},��?^0Y5ӳ5��AC���D����(d��dw��[왰�v���*���UL��<�K��%K\��֦ؾn���bF�O��k>��+|��I Ç���֬\r�)M{���Z���a.�|]65u��tj��;�*���&Q���?�)AʡPt	��@�u����L�Ҍ��fG�"_������3� �����e�寯5bߟ��*oG"f����|[}����^��fm��z4�������O��he��/��u�jn�Qi�vO��wO���)�!���)�|KP�~2c��i�ْ�������g�qK��H>11�i�	�O&,�����u������#��iJ��J��қ��n��R�=%EI{��W6���7�S��/.!��|�ؖ����):K9Rq	�ؘ�LK�>��6��/�+�S��|��<��bqY�_��S,.���/�����%.�.\?~|���1��"�i�v����Aqٝ���~c��i��1۠����qN��+>���A���]�k�XRF:| w�=����9�:|��74�<R(ɣ	�����yT�nop��=���ݝܖ�fi�?�mQ��u��5�u�(�E����|77��O��]Kw?�E���=���=�f���nj]-�����/��re��Vy�}n���=���u���l_=9؉�4M��W�'�m�{{4{ъ|cfV�<�v�rʻ�z���D�y˰��ܜ�|G�����vU}��K4 =-��Q�|��ƌ�y3ZY�-Uڹ�>��t�z���>�8�C��q�Lr|"α{/\�]}�9�	�s�^{=yp��Gˑw���S2f�V����ӻ0�Ut�s�#U�(�
aWQ��r�$3BiR�B�"W�ʅ���e*�}3at!��Gva>J��{��Q*<�(��K�	�v�X�������i�<xE��va�Pġ{u��%�h�e�N�q�hg��ч���Y|{³j�4����#��m���o�a���2U��9u�e���̨���I�^g���>��`�3����}�*�v�k�$�SS��y[��3f����<=a�DY��)�N�c������M�AH�	@٩ eI��- FWQ�p�e����b�����T`2V���g�ʹ����0�ݾ��+Zmքn�t(`ܧ�Ƞ
ei�ר�wT��W��N��̝1�{�;Y�B;xCTd�K=d���Pr�T�`x|V��YE�q�짟~    �D4^w         �  x���I��&��z��� i�tg��q��		5�^�[}%\��~/�	�<	$4������RHXFh�����	%��N�� ~���h��O�����w�iے)�����6��?4J� �%a	��7P0uY�m0Ը��P�PF<�ȳ����������۝3㼮�C��@�"��
^$嵝[u�-Tt_މ���.�\CIJ8�~D�f�?kA?���	g�L�~gn�fKv������X(%B>���Q.��ۆrj߹�h_�s�r��U�}M��YZ��?�W��>D���(��W�{��L��s![�M�ۺ���m9{�r��?�P�r�h.�_،s3�SE0��>��ݪ��Ԯ�u��M�^[��,�YN����;& ������^F��5�Cx�s�s��+��A^�(K`�
6~�U���P;M�p�mjJ�4��+�igi.8��n�H�7Xz�̥���n���ݭ@C}g���n�hS�ˁhʋ����K/��o�>�}(K�Br*��ZV�9��dlWm��}�S#���b���

x2+��Z���k�sܮv��@�Tz�
�Έ��2�%���К5텞#ܻeg9j������"-�I�	j_��i��^ݾp5>ƅ��h�����RH"�lGhV:^��XIWoK�j�r���_��2�h_g�Gۅ��ޖ�@9��a5R���g@�YBL��AW��������pu��RD���"x�D�J;���Z����T�os�k��4Rq*�k9k�i����U�i����d�k������R�S�JI�E�@��/�IH5hQ��u<W��6���i��|�D�"^X�{���v�v�n˗���W�� �G�z-��6Ѝ���E��zݔ�|'����ʒ�?����8�ɪ��aۛג�4|>��=P�%�c1l4��L&�3���\o	�='V�+B���R�hIJr<�"��5��i~�~w�HWިk��K�x(X����v�Ǹ�vkmw�IT��h��(���)��G.ba*ݹ��}�g,���S��)���|�#>\��Hޫ&d���^nZ�i�n'�j����R����X�j<��8o��u���j��Y���
�]��;���5�v�"�+�I���Cm��,Ek�|�% ����V�}/�>�S�n��6T+�S֗*�7WL�L@��%Ls3�c�gW�aR[��S���2�4.X���Q���.A{O�`Թ�4@����+R����)�ϣu4�Qw��+_��	�K�܏+E��T����Ҟ4��s���p�<NW*�ԁ�gh-2�]�(_�2�]��G��O��_T�g��-y[������<�Gm�{T��$P����V������ꎌ�=��٧�,�I$���录xo�9���(������t�U��
=����]pZ�Eb�<�Y��Pƍ��Yf{���,�W~�JA�qs�x��a�W�2�1�8D���,�Z�JBr�r�Ռ��N� Q�9��ӹ�j���*0fb��;Z`l�����j���y��v���Ғ>���%�=�iTb엶��ܭ7�naH�K��] A&y2���-_��G�6m[�[���}���/g��c�&�j#Թ����_&U��.ⵕ���_:P[4y߾zx_M�{H;��:��x��hma+���"%î$J���fz�+�~e�CT�Fk�SѶY�)є�ޕy��-o�640ex��ڢ~ B/L	B��0���U�Ua���UɃi�hmy�T*2lۃ���j�WiVw�E.��߄�-��婅�-E�� Kt阺���G�,8�|�n5R[�/���k�@]��WRs+(�5i�R�W����?i�^H� / Y�^6-��:�\ nN��'�"���J�����ᮖa��UY���^T���ΚY��տx�b�Α::���V8SuS�
��ᶲHm*ͼ�
�S4�Q��ЮجǞ���_hBee���\*�a(
��3���b_^N{�o0#@�0����.��JR�R���F��ݘ�{�5Yy�|����R���'c���j���mΙ|9���F����R���K�v���e�=mΚ�ӵ�
��Ej+PEx���R���z���ʑv��\{�C�:�hmy��)0��+a3��1[�s)F\�aO�Y��(����g��[7��y����k����=�--���M)-��'myzA��ξr�Mv�7�=j��<�?
bae6����L���>�ު���r�!KFK�S�oܙ�y;O�t�֛��������J@FK�~֒�!�!��m<4��>�ʹ;�
��S�L~~�	��      �   ;   x�3�-.M,��W��L�M*���".CNǔ�̼�⒢Ĕ�"�����L�d� $�S         _   x�Uɱ�0�Z�"�|�Y��B���u��7��)3`e�sm�H��߾�v��|��]�����JFx:��e|I�ǫ�U�{��ь�      �     x���M��0��ί�h��{Z��bW�J��#Y�I\�d���o'��(����X�d�3��0�nNeu��9v�.��Ų"ݒ�/9�v7y�1u��=Q��X�B4�U���rN�c2z���Y��N6<��)�)�|�n����!���S�qLy�`�9-��}���.�<��O9��I%���CK�L�����P�S۔���ǧ#ea�7�QF�T�)_��p���|),*M��c$��9u8���0~���MH��X+�1T#Gt�W��Fy tZ�Р	"�&F41��
�������K]
s��㾟-��]�A��o��H[mAي��\"��}��q��㆚�|�F{n���:�D ��:�<.-0!p�8�� �
�����MW�'��p��mejN~�q�?�5:����2R[Y	r��X_� �x[��ǟ�f���Ѕ���26���{a`bٖ������J}R�Fy����#�z��_��l�+I^/S�2y����:����|�]�1\�o,%��Ccl#D�j˫m]U�w��Nn     