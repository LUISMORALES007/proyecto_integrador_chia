﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Master/MasterRegistrados.master" AutoEventWireup="true" CodeFile="~/Controller/Administrador/Reportes.aspx.cs" Inherits="Views_Administrador_Reportes" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="container">
          <div class="row">
        <div class="col-3">

        </div>
          <div class="col-8">
                <h1> REPORTES ADMINISTRADOR </h1>
        </div>
          <div class="col-1">
             
        </div>

    </div>

        <br />
     
        <div class="row">
             
            <div class="col-5">

                    

            </div>
              <div class="col-4">

                     <asp:DropDownList ID="DDL_Reporte" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DDL_Reporte_SelectedIndexChanged"   >
         <asp:ListItem Value=”0″>--Seleccionar Reporte--</asp:ListItem>
           <asp:ListItem Value=”1″>Eventos Cancelados</asp:ListItem>
          <asp:ListItem Value=”2″>Eventos Denunciados</asp:ListItem>
         <asp:ListItem Value=”3″>Eventos Activos por Fecha</asp:ListItem>
         
        </asp:DropDownList>
                  <br />
                  
                 
            </div>
              <div class="col-3">

              
            

            </div>
 
        

            </div>
        <br />

        <div class="row">
            <div class="col-3">

            </div>
              <div class="col-6">
                    <div class="alert alert-success" role="alert">
                        <h6>Podra generar el informe que desea tan solo seleccionandolo de la lista</h6>
            </div>
            </div>
              <div class="col-3">

            </div>

        </div>

      
        <br />
 
          
          <div class="row">
              <div class="col-2">
                  <asp:TextBox ID="TX_Fecha_Inicio" runat="server" TextMode="Date" Visible="false"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RFV_Fecha_Inicio" runat="server" ControlToValidate="TX_Fecha_Inicio" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Evento"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RAV_Fecha_Inicio" runat="server"  Type="Date"  ErrorMessage="* Fecha no valida" ForeColor="Red" ValidationGroup="VG_Evento" ControlToValidate="TX_Fecha_Inicio" Display="Dynamic"></asp:RangeValidator>

                
              </div>

               <div class="col-2">

                   <asp:Button ID="B_Fecha" runat="server" Text="Buscar" ValidationGroup="VG_Evento"  CssClass="btn btn-primary" OnClick="B_Fecha_Click" Visible="false" />

              </div>

                <asp:Label ID="LA_Mensaje" runat="server" Text=" " Visible="false" ForeColor="Red"></asp:Label>
   
                   <br />
                   <br />
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
              <CR:CrystalReportViewer ID="CRV_Reporte1" runat="server" AutoDataBind="true" Visible="false"  />
              <CR:CrystalReportSource ID="CRS_Reporte1" runat="server">
                  <Report FileName="~\Reportes\Reporte1.rpt">
                  </Report>
              </CR:CrystalReportSource>


            <CR:CrystalReportViewer ID="CRV_Reporte2" runat="server" AutoDataBind="true" Visible="false"/>
            <CR:CrystalReportSource ID="CRS_Reporte2" runat="server">
                <Report FileName="~\Reportes\Reporte2.rpt">
                </Report>
              </CR:CrystalReportSource>

               <CR:CrystalReportViewer ID="CRV_Reporte3" runat="server" AutoDataBind="true" Visible="false"/>
            <CR:CrystalReportSource ID="CRS_Reporte3" runat="server">
                <Report FileName="~\Reportes\Reporte3.rpt">
                </Report>
              </CR:CrystalReportSource>

               </ContentTemplate>   
         </asp:UpdatePanel>

              </div>
              
           
             

    
    </div>

</asp:Content>

