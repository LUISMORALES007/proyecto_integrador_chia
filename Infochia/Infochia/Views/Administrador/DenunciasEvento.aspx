﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Master/MasterRegistrados.master" AutoEventWireup="true" CodeFile="~/Controller/Administrador/DenunciasEvento.aspx.cs" Inherits="Views_Administrador_DenunciasEvento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      <style>

        .grid_eventos {

            margin-left:-10%;
        }
           .modalBackground{
            background-color: black;
            filter:alpha(opacity=90) !important;
            opacity:0.6 !important;
            z-index:20;
           }


         .modalpopup2 {
            padding-left:5%;
            padding-top:2%;
            position:relative;
            width:500px;
            height:200px;
            background-color:whitesmoke;
            border-radius: 22px 22px 22px 22px;
            -moz-border-radius: 22px 22px 22px 22px;
           -webkit-border-radius: 22px 22px 22px 22px;
            border: 2px solid #000000;
            -webkit-box-shadow: -2px 3px 16px 2px rgba(0,0,0,0.75);
           -moz-box-shadow: -2px 3px 16px 2px rgba(0,0,0,0.75);
          box-shadow: -2px 3px 16px 2px rgba(0,0,0,0.75);
        }

           .boton {

               margin-right:5%;
           }
     

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

          

  <div class="container">
 


    <div class="row">
        <div class="col-3">

        </div>
          <div class="col-6">
               <h1> DENUNCIA EVENTO</h1>
        </div>
          <div class="col-3">

        </div>

    </div>
        </div>
    <br />
 
    <div class="container">
        
        <div class="row">
            <div class="col-10">
        <div>



            <table class="w-100">
                <tr>
                    <td>
                        Nombre Evento:
                        <asp:TextBox ID="TX_Buscar_Nombre" runat="server" CssClass="form-control"></asp:TextBox>
                    </td>
                    <td>
                        Categoria:
                        <asp:DropDownList ID="DDL_Buscar_Categoria" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <br />
                        <asp:Button ID="B_Buscar_Denuncia" runat="server" Text="Buscar Denuncias" CssClass="btn btn-info" OnClick="B_Buscar_Denuncia_Click" />
                    </td>
                </tr>
            </table>


        </div>
      
                </div>

            <div class="col-2">

                  <asp:ImageButton ID="IB_Regresar" runat="server" Height="48px" Width="50px" ImageUrl="~/assets/img/Botones/back-icon.png"  />

            </div>
    </div>
       




        </div>
       <div class="grid_eventos">
            <br />
            <br />
          

          <asp:GridView ID="GV_Denuncias" runat="server" AutoGenerateColumns="False" CellPadding="5" ForeColor="#333333" GridLines="None" DataKeyNames="id"  Width="1342px"  AllowPaging="True" CellSpacing="2" PageSize="20" DataSourceID="ODS_Denuncias" OnRowCommand="GV_Denuncias_RowCommand" OnRowDataBound="GV_Denuncias_RowDataBound" CssClass="table-bordered">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:TemplateField HeaderText="Nombre">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("nombre") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
              <asp:TemplateField HeaderText="Categoria Evento">
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("descripcion_categoria") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Estado Evento">
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%# Bind("descripcion_estado") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fecha Inicio">
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("fecha_inicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
                 <asp:TemplateField HeaderText="Fecha Fin">
                <ItemTemplate>
                    <asp:Label ID="Label8" runat="server" Text='<%# Bind("fecha_fin", "{0:dd/MM/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Hora Inicio">
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("hora") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Lugar">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("lugar") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Cantidad Denuncias">
                <ItemTemplate>
                    <asp:Label ID="Label9" runat="server" Text='<%# Bind("cantidad_denuncias") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        
            <asp:TemplateField HeaderText="Imagen Evento">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" Height="64px" Width="64px"  ImageUrl='<%# Eval("imagen") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Mas Informacion">
                <ItemTemplate>
                    <asp:ImageButton ID="IB_Ver_Evento" runat="server" CommandName="info" Height="24px" ImageUrl="~/assets/img/Botones/visibilidad.png" Width="24px" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'  />
                    <br />
                    <asp:Button ID="B_Cancelar" CssClass="btn btn-danger" runat="server" Text="Cancelar Evento"  />
                    <asp:Label ID="LA_Cancelado" runat="server" Text="" ForeColor="Red"></asp:Label>
                      <asp:Panel ID="Panel2" runat="server" CssClass="modalpopup2">
                     
                          <strong><h4> ¿Desea CANCELAR el evento?</h4>  </strong>    
                     &nbsp;&nbsp;&nbsp;&nbsp;
                      <br />
                    <br />
                  <asp:Button ID="B_CancelarEvento" runat="server" Text="Cancelar Evento" CommandName="cancelar"  CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' CssClass="boton btn btn-info"   />
               
                    &nbsp;&nbsp;&nbsp;&nbsp;
                     <asp:Button ID="B_No" runat="server" Height="34px" Text="Salir" Width="101px" CssClass="boton btn btn-danger" />
                   
                </asp:Panel>
                   <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" runat="server"  CancelControlID="B_No" PopupControlID="Panel2" BackgroundCssClass="modalBackground" TargetControlID="B_Cancelar" ></ajaxToolkit:ModalPopupExtender>


                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <EmptyDataTemplate>
            NO HAY DENUNCIAS
        </EmptyDataTemplate>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>


            <asp:ObjectDataSource ID="ODS_Denuncias" runat="server" SelectMethod="mostrarDenuncias" TypeName="DAOAdministrador">
                <SelectParameters>
                    <asp:SessionParameter Name="_denuncias" SessionField="datos_denuncia_filtrar" Type="Object" />
                </SelectParameters>
            </asp:ObjectDataSource>

        





        </div>
      </ContentTemplate>

    </asp:UpdatePanel>




</asp:Content>

