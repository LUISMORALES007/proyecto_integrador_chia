﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Master/MasterRegistrados.master" AutoEventWireup="true" CodeFile="~/Controller/Administrador/DeshabilitarUsuario.aspx.cs" Inherits="Views_Administrador_DeshabilitarUsuario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
    <div class="row">
        <div class="col-3">

        </div>
          <div class="col-6">
               <h1>DESHABILITAR USUARIOS</h1>
        </div>
          <div class="col-3">

        </div>

    </div>
    <br />
 
    <div class="container">

        <div class="row">

            <div class="col-12">


                <table class="w-100">
                    <tr>
                        <td>
                            Nombre:
                            <asp:TextBox ID="TX_Nombre" runat="server" CssClass="form-control"></asp:TextBox>
                        </td>
                          <td>
                              Apellido:
                            <asp:TextBox ID="TX_Apellido" runat="server" CssClass="form-control"></asp:TextBox>
                        </td>
                        <td>
                            Correo:
                            <asp:TextBox ID="TX_Correo" runat="server" CssClass="form-control"></asp:TextBox>
                          
                        </td>
                        <td>  
                            <br />
                            <asp:Button ID="B_Buscar" runat="server" Text="Buscar" CssClass="btn btn-info" OnClick="B_Buscar_Click" />
                        </td>
                    </tr>
                </table>
                       </div>

            </div>
        <br />
        <br />
   

            <div class="row">

                 <asp:GridView ID="GV_Listar_Usuarios" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" DataKeyNames="id_usuario"  AllowPaging="True" PageSize="25" DataSourceID="ODS_Usuarios" Width="1097px" OnRowCommand="GV_Listar_Usuarios_RowCommand" OnRowDataBound="GV_Listar_Usuarios_RowDataBound" CssClass="table-bordered">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:TemplateField HeaderText="Nombres">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("nombre_completo") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Correo Electronico">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("correo") %>' Width="90%"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="¿Habilitado?">
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("habilitado") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Imagen Perfil">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" Height="64px" Width="64px"  ImageUrl='<%# Eval("imagen") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Habilitar/ Deshabilitar">
                <ItemTemplate>

                    <asp:Button ID="B_Habilitar" runat="server" Text="Habilitar" CssClass="btn btn-success"  Visible="false" CommandName="habilitar" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' />
                      <asp:Button ID="B_Deshabilitar" runat="server" Text="Deshabilitar" CssClass="btn btn-danger" Visible="false"  CommandName="deshabilitar" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' />

                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <EmptyDataTemplate>
            NO HAY USUARIOS REGISTRADOS
        </EmptyDataTemplate>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>

                 <asp:ObjectDataSource ID="ODS_Usuarios" runat="server" SelectMethod="mostrarUsuarios" TypeName="DAOAdministrador">
                     <SelectParameters>
                         <asp:SessionParameter Name="_usuarios" SessionField="datos_buscar_usuario" Type="Object" />
                     </SelectParameters>
                 </asp:ObjectDataSource>

            </div>

 
 
    </div>


   

                  </ContentTemplate>

    </asp:UpdatePanel>

</asp:Content>

