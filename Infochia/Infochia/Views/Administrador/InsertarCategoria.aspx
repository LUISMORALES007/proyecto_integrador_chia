﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Master/MasterRegistrados.master" AutoEventWireup="true" CodeFile="~/Controller/Administrador/InsertarCategoria.aspx.cs" Inherits="Views_Administrador_InsertarCategoria" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>

        .grid {

            margin-left:20%;
        }

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  

     <div class="container">
    <div class="row">
        <div class="col-3">

        </div>
          <div class="col-6">
               <h1>Insertar / Editar Categorias</h1>
        </div>
          <div class="col-3">

        </div>

      </div>
    </div>
    <br />
    <br />
    <br />

    
               <asp:GridView runat="server" ID="GV_Categorias_Administrador" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="id,session" DataSourceID="ODS_Categorias_Administrador" ForeColor="#333333" GridLines="None" Width="600px" AllowPaging="True" OnRowCommand="GV_Categorias_Administrador_RowCommand" ShowFooter="True" CssClass="table-bordered grid">
                   <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                   <Columns>
                       <asp:TemplateField HeaderText="Descripcion Categoria Evento">
                           <EditItemTemplate>
                               <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("descripcion") %>' Height="21px" Width="199px"></asp:TextBox>
                           </EditItemTemplate>
                           <FooterTemplate>
                               <br />
                               <asp:TextBox ID="TX_Descripcion_Categoria" runat="server" Height="26px" Width="204px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RFV_Descripcion" runat="server" ControlToValidate="TX_Descripcion_Categoria" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Categoria"></asp:RequiredFieldValidator>
                   <asp:RegularExpressionValidator ID="REV_Descripcion" runat="server"  ControlToValidate="TX_Descripcion_Categoria"  ErrorMessage="*Minimo 4 caracteres - Maximo 20" ForeColor="Red" SetFocusOnError="True" ValidationExpression="^[a-zA-Z0-9 ]{4,20}$"  ValidationGroup="VG_Categoria" ></asp:RegularExpressionValidator>
                           </FooterTemplate>
                           <ItemTemplate>
                               <asp:Label ID="Label1" runat="server" Text='<%# Bind("descripcion") %>'></asp:Label>
                           </ItemTemplate>
                       </asp:TemplateField>
                       <asp:TemplateField HeaderText="Ultima Modificacion Categoria">
                           <EditItemTemplate>
                                 <asp:Label ID="Label3" runat="server" Text='<%# Bind("ultima_modificacion") %>'></asp:Label>
                           </EditItemTemplate>
                                    <ItemTemplate>
                               <asp:Label ID="Label2" runat="server" Text='<%# Bind("ultima_modificacion") %>'></asp:Label>
                           </ItemTemplate>
                       </asp:TemplateField>
                       <asp:TemplateField ShowHeader=" False">
                           <EditItemTemplate>
                               <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Actualizar"></asp:LinkButton>
                               &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                           </EditItemTemplate>
                            <FooterTemplate>
                                
                                <asp:Button ID="Button1" runat="server" Text="Insertar" CommandArgument='<%# Bind("id") %>' CommandName="insertar" ValidationGroup="VG_Categoria"   />
                           </FooterTemplate>
                           <ItemTemplate>
                               <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" Text="Editar"></asp:LinkButton>
                           </ItemTemplate>
                       </asp:TemplateField>
                   </Columns>
                   <EditRowStyle BackColor="#999999" />
                   <EmptyDataTemplate>
                       NO HAY CATEGORIAS
                   </EmptyDataTemplate>
                   <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                   <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                   <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                   <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                   <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                   <SortedAscendingCellStyle BackColor="#E9E7E2" />
                   <SortedAscendingHeaderStyle BackColor="#506C8C" />
                   <SortedDescendingCellStyle BackColor="#FFFDF8" />
                   <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
               </asp:GridView>


               <asp:ObjectDataSource ID="ODS_Categorias_Administrador" runat="server" SelectMethod="mostrarCategoriasAdministrador" TypeName="DAOAdministrador" UpdateMethod="modificarCategoria">
                   <UpdateParameters>
                       <asp:Parameter Name="descripcion" Type="String" />
                       <asp:Parameter Name="ultima_modificacion" Type="String" />
                       <asp:Parameter Name="session" Type="String" />
                       <asp:Parameter Name="id" Type="Int32" />
                   </UpdateParameters>
               </asp:ObjectDataSource>


        
   
</asp:Content>

