﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Master/MasterRegistrados.master" AutoEventWireup="true" CodeFile="~/Controller/Administrador/VerDetalleDenuncia.aspx.cs" Inherits="Views_Administrador_VerDetalleDenuncia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    
  <div class="container">
 


    <div class="row">
        <div class="col-3">

        </div>
          <div class="col-6">
               <h1>DETALLE DENUNCIA</h1>
        </div>
          <div class="col-3">

        </div>

    </div>
        </div>
    <br />
    <br />
    <br />

    <div class="container">

        <div class="row">
              <div class="col-1">
                <asp:ImageButton ID="IB_Regresar" runat="server" Height="48px" Width="50px" ImageUrl="~/assets/img/Botones/back-icon.png" OnClick="IB_Regresar_Click"  />

            </div>
            <div class="col-9">

                  <div class="grid_eventos">

                      <table class="table">
                                      
                                        <thead class="table-info">
                                            <tr>
                                                 <td class="text-center">  
                                            
                                                    </td>  
                                                <td class="text-center">  
                                                    <strong><asp:Label ID="LA_Titulo" runat="server" Text="DATOS CREADOR DEL EVENTO:"></asp:Label></strong>    
                                                    </td>  
                                                 <td class="text-center">  
                                               
                                                    </td>  
                                            </tr>

                                        <tr class="table-bordered">
                                          
                                            <th scope="col">Nombre Creador</th>
                                            <th scope="col">Correo Electronico</th>
                                            <th scope="col">Imagen de Perfil</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="table-bordered">
                                      
                                            <td>    <asp:Label ID="LA_Nombre_Creador" runat="server" Text=""></asp:Label>   </td>
                                            <td> <asp:Label ID="LA_Correo" runat="server" Text=""></asp:Label></td>
                                            <td> <asp:Image ID="IM_Creador" runat="server"  Width="29px" Height="29px"/></td>
                                        </tr>
                                        
                                        </tbody>
                                    </table>
                      
            <br />
            <br />
           

          <asp:GridView ID="GV_Detalle_Denuncia" runat="server" AutoGenerateColumns="False" CellPadding="5" ForeColor="#333333" GridLines="None"   Width="840px"  AllowPaging="True" CellSpacing="2" PageSize="20" DataSourceID="ODS_Detalle_Denuncia">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:TemplateField HeaderText="Descripcion">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("descripcion") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Nombre Denunciante">
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("nombre_completo") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Correo">
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("correo") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>       
            <asp:TemplateField HeaderText="Imagen Usuario">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" Height="64px" Width="64px"  ImageUrl='<%# Eval("imagen") %>' />
                </ItemTemplate>
            </asp:TemplateField>
     
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <EmptyDataTemplate>
            NO HAY EVENTOS DENUNCIADOS
        </EmptyDataTemplate>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>


                      <asp:ObjectDataSource ID="ODS_Detalle_Denuncia" runat="server" SelectMethod="mostrarDetalleDenuncias" TypeName="DAOAdministrador">
                          <SelectParameters>
                              <asp:Parameter Name="id_evento" Type="Int64" />
                          </SelectParameters>
                      </asp:ObjectDataSource>

            </div>


            

        </div>
         
    </div>
 </div>

</asp:Content>

