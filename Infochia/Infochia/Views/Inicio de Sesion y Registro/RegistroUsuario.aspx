﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeFile="~/Controller/Inicio de Sesion y Registro/RegistroUsuario.aspx.cs" Inherits="Views_Inicio_de_Sesion_y_Registro_RegistroUsuario" %>

<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <style>

        .fondo {

            background:#f6fafe;
                        
        }
        
    </style>

  <title>Registro Usuario</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="../../assets/img/favicon.png" rel="icon">
  <link href="../../assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="../../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../../assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="../../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="../../assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="../../assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="../../assets/css/style.css" rel="stylesheet">


</head>

<form  runat="server">
      <header id="header">
                    <div class="container d-flex">

                      <div class="logo mr-auto">
                        <h1 class="text-light"><a href="index.html">Bocor<span>.</span></a></h1>
                        <!-- Uncomment below if you prefer to use an image logo -->
                         <a><img src="assets/img/logo.png" alt="" class="img-fluid"></a>
                      </div>

                      <nav class="nav-menu d-none d-lg-block">
                        <ul>
                           
                         <li class="get-started"><asp:LinkButton ID="LB_Regresar" runat="server" OnClick="LB_Regresar_Click">Regresar</asp:LinkButton> </li>
                        </ul>
                      </nav><!-- .nav-menu -->

                    </div>
                  </header><!-- End Header -->

    <div class="container ">
        <div class="row">
             <br />
    
    <br /><br />
    <br /><br />
           
        </div>

    </div>



       <div class="row justify-content-center" >
                          <div class="col-md-5" >
                                     <div class="card" >
                                                     <div class="card-header text-center">
                                                        <h5>Registro Usuario</h5>
                                                       </div>
                                                          <div class="card-body">
                                                                 
                                                             
                                                                   <div class="form-group row">
                                                                        <label for="exampleInputEmail1" class="col-md-4 col-form-label text-md-right">
                                                                        <h6>Nombre</h6>
                                                                    </label>
                                                                       <div  class="col-md-6">
                                                                              <asp:TextBox ID="TX_Nombre" runat="server" class="form-control"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RFV_Nombre" runat="server" ControlToValidate="TX_Nombre" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Registro"></asp:RequiredFieldValidator>
                                                                              <asp:RegularExpressionValidator ID="RV_Nombre" runat="server" ControlToValidate="TX_Nombre" ErrorMessage="*Digite solo letras por favor" ForeColor="Red" ValidationExpression="^[a-zA-Z ñÑ]*$" ValidationGroup="VG_Registro"></asp:RegularExpressionValidator>
                                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TX_Nombre" Display="Dynamic" ErrorMessage="*Minimo 6 caracteres - Maximo 20" ForeColor="Red" SetFocusOnError="True" ValidationExpression="^[\s\S]{6,20}$" ValidationGroup="VG_Registro"></asp:RegularExpressionValidator>
                                                                              
                                                                       </div>

                                                                    </div>

                                                                  <div class="form-group row">
                                                                        <label for="exampleInputEmail1" class="col-md-4 col-form-label text-md-right">
                                                                        <h6>Apellido</h6>
                                                                    </label>
                                                                       <div  class="col-md-6">
                                                                            <asp:TextBox ID="TX_Apellido" runat="server" class="form-control"></asp:TextBox>
                                                                             <asp:RequiredFieldValidator ID="RFV_Apellido" runat="server" ControlToValidate="TX_Apellido" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Registro"></asp:RequiredFieldValidator>
                                                                              <asp:RegularExpressionValidator ID="RV_Apellido" runat="server" ControlToValidate="TX_Apellido" ErrorMessage="*Digite solo letras por favor" ForeColor="Red" ValidationExpression="^[a-zA-Z ñÑ]*$" ValidationGroup="VG_Registro"></asp:RegularExpressionValidator>
                                                                         <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TX_Apellido" Display="Dynamic" ErrorMessage="*Minimo 6 caracteres - Maximo 20" ForeColor="Red" SetFocusOnError="True" ValidationExpression="^[\s\S]{6,20}$" ValidationGroup="VG_Registro"></asp:RegularExpressionValidator>

                                                                       </div>

                                                                    </div>

                                                               <div class="form-group row">
                                                                        <label for="exampleInputEmail1" class="col-md-4 col-form-label text-md-right">
                                                                        <h6>Correo Electronico</h6>
                                                                    </label>
                                                                       <div  class="col-md-6">
                                                                            <asp:TextBox ID="TX_Correo" TextMode="Email"  runat="server" class="form-control"></asp:TextBox>
                                                                           <asp:RegularExpressionValidator ID="REV_Correo_Electronico_Registro" runat="server" ControlToValidate="TX_Correo" Display="Dynamic" ErrorMessage="*Debe introducir un correo." ForeColor="Red" SetFocusOnError="True" ValidationExpression="^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{2,3})(\]?)$" ValidationGroup="VG_Registro"></asp:RegularExpressionValidator>
                                                                              <asp:RequiredFieldValidator ID="RFV_Correo" runat="server" ControlToValidate="TX_Correo" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Registro"></asp:RequiredFieldValidator>
                                                                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="TX_Correo" ErrorMessage="* Caracter no permitido" ValidationExpression="(''|[^'])*" ValidationGroup="VG_Registro"  ForeColor="Red" ></asp:RegularExpressionValidator>
                                                                       </div>

                                                                    </div>
                                                                      <div class="form-group row">
                                                                        <label for="inputPassword" class="col-md-4 col-form-label text-md-right">
                                                                        <h6>Contraseña</h6>
                                                                    </label>
                                                                       <div  class="col-md-6">
                                                                               <asp:TextBox ID="TX_Clave" TextMode="Password"  runat="server" class="form-control"></asp:TextBox>
                                                                                 <asp:RegularExpressionValidator ID="REV_Clave_Registro" runat="server" ControlToValidate="TX_Clave" Display="Dynamic" ErrorMessage="*La contraseña no cumple con los requisitos minimos de seguridad, Minimo 1 mayuscula 1 minuscula un caracter especial y minimo 6 caracteres de longitud " ForeColor="Red" SetFocusOnError="True" ValidationExpression="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@|#|.|,|_|-|\|!| |#|$|%|&|/|(|)|?|Ñ|ñ]).{6,20}$" ValidationGroup="VG_Registro"></asp:RegularExpressionValidator>
                                                                                  <asp:RequiredFieldValidator ID="RFV_Clave" runat="server" ControlToValidate="TX_Clave" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Registro"></asp:RequiredFieldValidator>
                                                                                      <asp:RegularExpressionValidator ID="REV_SQL" runat="server" ControlToValidate="TX_Clave" ErrorMessage="* Caracter no permitido" ValidationExpression="(''|[^'])*" ValidationGroup="VG_Registro"  ForeColor="Red" ></asp:RegularExpressionValidator>
                                                                       </div>

                                                                    </div>

                                                                     <div class="form-group row">
                                                                        <label for="inputPassword" class="col-md-4 col-form-label text-md-right">
                                                                        <h6>Repetir Contraseña</h6>
                                                                    </label>
                                                                       <div  class="col-md-6">
                                                                            <asp:TextBox ID="TX_Repetir_Clave" TextMode="Password"  runat="server" class="form-control"></asp:TextBox>
                                                                              <asp:CompareValidator ID="CV_Repetir_Clave_Registro" runat="server" ControlToCompare="TX_Clave" ControlToValidate="TX_Repetir_Clave" Display="Dynamic" ErrorMessage="*La contraseña no coincide." ForeColor="Red" ValidationGroup="VG_Registro"></asp:CompareValidator>
                                                                            <asp:RequiredFieldValidator ID="RFV_Repetir_Clave" runat="server" ControlToValidate="TX_Repetir_Clave" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Registro"></asp:RequiredFieldValidator>
                                                                              <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TX_Repetir_Clave" ErrorMessage="* Caracter no permitido" ValidationExpression="(''|[^'])*" ValidationGroup="VG_Registro"  ForeColor="Red" ></asp:RegularExpressionValidator>
                                                                       </div>

                                                                    </div>

                                                                   <div class="form-group row">
                                                                        <label for="exampleInputEmail1" class="col-md-4 col-form-label text-md-right">
                                                                        <h6>Imagen Perfil</h6>
                                                                    </label>
                                                                       <div  class="col-md-6">

                                                                                 <asp:FileUpload ID="FU_Imagen_Usuario" runat="server" class="form-control-file" />
                                                                                  <asp:RequiredFieldValidator ID="RFV_Imagen_Registro" runat="server" ControlToValidate="FU_Imagen_Usuario" Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Registro"></asp:RequiredFieldValidator>

                                                                       </div>

                                                                    </div>
                                                                     
                                                              
                                                                   <div class="form-group row">
                                                                        <label for="exampleInputEmail1" class="col-md-4 col-form-label text-md-right">
                                                                        <h6>Términos y condiciones de privacidad</h6>
                                                                    </label>
                                                                       <div  class="col-md-6">
                                                                           <asp:textbox ID="TX_Politica" runat="server" class="form-control" Text="Conforme a las disposiciones contenidas en la Ley Estatutaria 1581 de 2012 y al Decreto 1377 de 2013 que desarrolla parcialmente la Ley de Habeas Data, QUE HAY PA HACER S.A., considerada como responsable del tratamiento de datos personales, desarrolla la presente política de privacidad y de protección de datos personales, en cuanto a su recolección, almacenamiento y administración.

Lo anterior, en armonía con el cumplimiento de las disposiciones especiales que hacen referencia al tratamiento de datos personales de naturaleza financiera, crediticia, comercial y de servicios previstas en la Ley 1266 de 2008, así como en las normas que la reglamenten y modifiquen, que cobijan el origen, mantenimiento, administración y extinción de obligaciones derivadas de relaciones de tal naturaleza.

QUE HAY PA HACER S.A., sin perjuicio de lo establecido en los formatos que pueden hallarse en su página, respeta la privacidad de sus usuarios, toda vez que la información que se solicita y el visitante suministra voluntariamente, autoriza expresamente a QUE HAY PA HACER S.A., al tratamiento de sus datos personales, en el entendido que tal información hará parte de un archivo y/o base de datos, cumpliendo con los principios de finalidad y libertad, puesto que aquellos datos son pertinentes y adecuados para la finalidad para la cual son recolectados o requeridos conforme a la normatividad vigente.

QUE HAY PA HACER S.A., conservará los archivos o bases de datos que contengan datos personales por el período que la normatividad vigente así se lo exija o lo permita, la vigencia de las bases de datos estará atada al ejercicio del objeto social de la Entidad, adicionalmente no transmitirá ni utilizará la información suministrada excediendo los límites establecidos por la ley, exceptuando casos en los que el titular autorice aun tercero o cuando dicha información sea requerida por una autoridad competente.

Si dentro de los 30 días siguientes, a la notificación de esta Política De Privacidad Y Protección De Datos Personales, el cliente no manifiesta a QUE HAY PA HACER S.A., información en contrario, se entenderá la autorización tacita para el manejo de la información del usuario, de conformidad al numeral 4 del Artículo 10 del decreto 1377 de 2013.

El usuario podrá solicitar la modificación, actualización o bloqueo de sus datos personales en cualquier momento, mediante el envío de una carta autenticada, al domicilio legal de la entidad o al correo electrónico quehaypahacer@gmail.com" Height="90px" ReadOnly="True" TextMode="MultiLine" Width="356px"></asp:textbox>

                                                                            
                                                                                      <br />
                                                                           <asp:checkbox ID="CH_Politica" runat="server" ></asp:checkbox>
                                                                        
                                                                           <span>Acepto los términos y condiciones</span>
                                                                           <asp:label ID="LA_Politica" runat="server" text=""></asp:label>
                                                                          
                                                                       </div>

                                                                    </div>

                                                              <br />
                                                                      <div class="form-group row mb-0">
                                                                                  <div class="col-md-8 offset-md-4">
                                                                                      <asp:Button ID="BTN_Registro" runat="server" class="btn btn-primary" Text="Registrarse" OnClick="BTN_Registro_Click" ValidationGroup="VG_Registro"  />

                                                                                  </div>
                                                                          </div>


                                                              </div>

                                    </div>

                              <br />
                              <br />
                                           <asp:updatepanel runat="server" ID="timer">  
                                                       <ContentTemplate>
                                         
                                                   <asp:Label ID="LA_Mensaje" runat="server" Text="" CssClass="alert alert-danger" Visible="False"></asp:Label>
                                                         <asp:Label ID="LA_Mensaje_Info" runat="server" Text="" CssClass="alert alert-info" Visible="False"></asp:Label>
                                                 <asp:ScriptManager runat="server" ID="SManager"></asp:ScriptManager>
                                              <asp:Timer runat="server" ID="Timer1" OnTick="Timer1_Tick" Enabled="False" Interval="1000"></asp:Timer>

                        
                                                  </ContentTemplate>
                                              </asp:updatepanel>


                           </div>
                 </div>


     
    
</form>
  <script src="../../assets/vendor/jquery/jquery.min.js"></script>
                  <script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
                  <script src="../../assets/vendor/jquery.easing/jquery.easing.min.js"></script>
                  <script src="../../assets/vendor/php-email-form/validate.js"></script>
                  <script src="../../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
                  <script src="../../assets/vendor/venobox/venobox.min.js"></script>
                  <script src="../../assets/vendor/aos/aos.js"></script>
                    <script src="../../assets/js/particles.js"></script>
                    <script src="../../assets/js/app.js"></script>
                  
                  <!-- Template Main JS File -->
                  <script src="../../assets/js/main.js"></script>

