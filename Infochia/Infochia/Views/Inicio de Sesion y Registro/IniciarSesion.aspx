﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeFile="~/Controller/Inicio de Sesion y Registro/IniciarSesion.aspx.cs" Inherits="Views_Inicio_de_Sesion_y_Registro_IniciarSesion" %>

<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <style>

        .fondo {

            background:#f6fafe;
                        
        }
        
    </style>

  <title>Iniciar Sesion</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="../../assets/img/favicon.png" rel="icon">
  <link href="../../assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="../../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../../assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="../../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="../../assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="../../assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="../../assets/css/style.css" rel="stylesheet">


</head>

<body class="fondo" >
<form id="form1" runat="server" >

                          
       <header id="header">
                    <div class="container d-flex">

                      <div class="logo mr-auto">
                        <h1 class="text-light"><a href="index.html">Bocor<span>.</span></a></h1>
                        <!-- Uncomment below if you prefer to use an image logo -->
                         <a><img src="assets/img/logo.png" alt="" class="img-fluid"></a>
                      </div>

                      <nav class="nav-menu d-none d-lg-block">
                        <ul>
                           
                         <li class="get-started"><asp:LinkButton ID="LB_Regresar" runat="server" OnClick="LB_Regresar_Click">Regresar</asp:LinkButton> </li>
                        </ul>
                      </nav><!-- .nav-menu -->

                    </div>
                  </header><!-- End Header -->

    <div class="container ">
        <div class="row">
             <br />
    <br />
    <br /><br />
    <br /><br />
            <br />
            <br />
        </div>

    </div>

   

    <div class="container  ">
           <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                           <Triggers>
                            <asp:AsyncPostBackTrigger  ControlID="Timer1" EventName="Tick" />
                        </Triggers>
                      <ContentTemplate>

                                                 <div class="row justify-content-center" >
                                                  <div class="col-md-8" >
                                                         <div class="card" >
                                                         <div class="card-header text-center">
                                                            <h5>Iniciar Sesion</h5>
                                                        </div>
                                                              <div class="card-body">
                                                                   <div class="form-group row">
                                                                        <label for="inputEmail" class="col-md-4 col-form-label text-md-right">
                                                                        <h6>Correo Electronico</h6>
                                                                    </label>
                                                                       <div class="col-md-6" >
                                                                              <asp:TextBox ID="TX_Correo" TextMode="Email"  runat="server" CssClass="form-control" placeholder="Correo Electronico" ></asp:TextBox>
                                                                  
                                                                             <asp:RegularExpressionValidator ID="REV_Correo_Electronico_Registro" runat="server" ControlToValidate="TX_Correo" Display="Dynamic" ErrorMessage="*Debe introducir un correo." ForeColor="Red" SetFocusOnError="True" ValidationExpression="^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{2,3})(\]?)$" ValidationGroup="VG_Inicio_Sesion"></asp:RegularExpressionValidator>
                                                                      <asp:RequiredFieldValidator ID="RFV_Correo" runat="server" ControlToValidate="TX_Correo" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Inicio_Sesion"></asp:RequiredFieldValidator>
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TX_Correo" ErrorMessage="* Caracter no permitido" ValidationExpression="(''|[^'])*" ValidationGroup="VG_Inicio_Sesion"  ForeColor="Red" ></asp:RegularExpressionValidator>
                                                                       </div>

                                                                    </div>
                                                                  <div class="form-group row">
                                                                        <label for="inputPassword" class="col-md-4 col-form-label text-md-right">
                                                                        <h6>Contraseña</h6>
                                                                    </label>
                                                                      <div class="col-md-6">
                                                                        <asp:TextBox ID="TX_Clave" TextMode="Password"  runat="server" CssClass="form-control" placeholder="Contraseña"></asp:TextBox>
                                                                       
                                                                             <asp:RegularExpressionValidator ID="REV_Clave_Registro" runat="server" ControlToValidate="TX_Clave" Display="Dynamic" ErrorMessage="*Minimo 6 caracteres - Maximo 20" ForeColor="Red" SetFocusOnError="True" ValidationExpression="^[\s\S]{6,20}$" ValidationGroup="VG_Inicio_Sesion"></asp:RegularExpressionValidator>
                                                                             <asp:RequiredFieldValidator ID="RFV_Clave" runat="server" ControlToValidate="TX_Clave" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Inicio_Sesion"></asp:RequiredFieldValidator>
                                                                          <asp:RegularExpressionValidator ID="REV_SQL" runat="server" ControlToValidate="TX_Clave" ErrorMessage="* Caracter no permitido" ValidationExpression="(''|[^'])*" ValidationGroup="VG_Inicio_Sesion"  ForeColor="Red" ></asp:RegularExpressionValidator>
                                                                      </div>

                                                                    </div>

                                                                      <div class="form-group row mb-0">
                                                                                  <div class="col-md-8 offset-md-4">
                                                                                      <asp:ScriptManager ID="ScriptManager1" runat="server">
                                                                                      </asp:ScriptManager>
               
                                                                                      <div class="row">
                                                                                          <div class="col-4">
                                                                                                <asp:Button ID="BTN_Inicio_Sesion" runat="server" Text="Iniciar Sesion"  ValidationGroup="VG_Inicio_Sesion" OnClick="BTN_Inicio_Sesion_Click" CssClass="btn btn-primary" />

                                                                                          </div>
                                                                                          <div class="col-5">
                                                                                                 <asp:linkbutton runat="server" ID="LB_Recuperar"  OnClick="LB_Recuperar_Click">Recuperar Contraseña</asp:linkbutton>

                                                                                          </div>
                                                                                          <div class="col-3">


                                                                                          </div>

                                                                                      </div>
                                                                                           
                                                                                
                                                                                        
                                                                                      <br />
                                                                                      <br />
                                                                                            

                                                                                  </div>
                                                                          </div>
                                                               </div>

                                                          </div>
                                                      <br />
                                                      <br />

                                                        <asp:Label ID="LA_Mensaje" runat="server" ForeColor="Red"  CssClass="alert alert-danger" Visible="False"></asp:Label>
                                                  </div>
                                                </div>

                           </ContentTemplate>
                  
                  </asp:UpdatePanel>
                    <asp:Timer ID="Timer1" runat="server" Enabled="False" Interval="1000" OnTick="Timer1_Tick">
                        </asp:Timer>

        </div>
 
    <div class="container">

        <div class="row">
            <br />
            <br />
            <br />  <br />
            <br />
        </div>

    </div>
 

       
          


</form>
</body>
 <!-- Vendor JS Files -->
      
                  <script src="../../assets/vendor/jquery/jquery.min.js"></script>
                  <script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
                  <script src="../../assets/vendor/jquery.easing/jquery.easing.min.js"></script>
                  <script src="../../assets/vendor/php-email-form/validate.js"></script>
                  <script src="../../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
                  <script src="../../assets/vendor/venobox/venobox.min.js"></script>
                  <script src="../../assets/vendor/aos/aos.js"></script>
                    <script src="../../assets/js/particles.js"></script>
                    <script src="../../assets/js/app.js"></script>
                  
                  <!-- Template Main JS File -->
                  <script src="../../assets/js/main.js"></script>