﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="~/Controller/Inicio de Sesion y Registro/GenerarToken.aspx.cs" Inherits="Views_Inicio_de_Sesion_y_Registro_GenerarToken" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Generar Token</title>
    <link href="../../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="../../assets/img/favicon.png" rel="icon">
  <link href="../../assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="../../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../../assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="../../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="../../assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="../../assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="../../assets/css/style.css" rel="stylesheet">
</head>
<body>
       <form id="form1" runat="server">
    <header id="header">
                    <div class="container d-flex">

                      <div class="logo mr-auto">
                        <h1 class="text-light"><a href="index.html">Bocor<span>.</span></a></h1>
                        <!-- Uncomment below if you prefer to use an image logo -->
                         <a><img src="assets/img/logo.png" alt="" class="img-fluid"></a>
                      </div>

                      <nav class="nav-menu d-none d-lg-block">
                        <ul>
                           
                         <li class="get-started"><asp:LinkButton ID="LB_Regresar" runat="server" OnClick="LB_Regresar_Click">Regresar</asp:LinkButton> </li>
                        </ul>
                      </nav><!-- .nav-menu -->

                    </div>
                  </header><!-- End Header -->
     <div class="container ">
        <div class="row">
             <br />
    <br />
    <br /><br />
    <br /><br />
            <br />
            <br />
        </div>

    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-header text-center">
                        <h5>Generar Token</h5>
                    </div>

                    <div class="card-body">

                     

                            <div class="form-group row">

                                <label for="inputEmail" class="col-md-4 col-form-label text-md-right">Correo Electronico</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="TX_Correo" TextMode="Email" runat="server" Width="232px"></asp:TextBox>

                                    <asp:RegularExpressionValidator ID="REV_Correo_Electronico_Registro" runat="server" ControlToValidate="TX_Correo" Display="Dynamic" ErrorMessage="*Debe introducir un correo." ForeColor="Red" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="VG_Token"></asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="RFV_Correo" runat="server" ControlToValidate="TX_Correo" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Token"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">

                                    <asp:Button ID="BTN_Enviar_Token" runat="server" class="btn btn-primary" Text="Recuperar" ValidationGroup="VG_Token" OnClick="BTN_Enviar_Token_Click" />
                                    <br />
                                    
                                </div>
                            </div>
                       
                       
                    </div>
                     
                </div>

                 
            </div>
           
          
        </div>
        
    </div>
           <br />
           <br />
           <div class="row">
               <div class="col-4"></div>
                 <div class="col-6">
                      <asp:Label ID="L_Mensaje" runat="server" CssClass="alert alert-danger" Visible="False"></asp:Label>
             <asp:Label ID="L_Mensaje_Info" runat="server" CssClass="alert alert-info" Visible="False"></asp:Label>

                 </div>
                   <div class="col-2"></div>
           </div>

           
     </form>



    <script src="../../assets/vendor/jquery/jquery.min.js"></script>
                  <script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
                  <script src="../../assets/vendor/jquery.easing/jquery.easing.min.js"></script>
                  <script src="../../assets/vendor/php-email-form/validate.js"></script>
                  <script src="../../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
                  <script src="../../assets/vendor/venobox/venobox.min.js"></script>
                  <script src="../../assets/vendor/aos/aos.js"></script>
                    <script src="../../assets/js/particles.js"></script>
                    <script src="../../assets/js/app.js"></script>
                  
                  <!-- Template Main JS File -->
                  <script src="../../assets/js/main.js"></script>
</body>
</html>

