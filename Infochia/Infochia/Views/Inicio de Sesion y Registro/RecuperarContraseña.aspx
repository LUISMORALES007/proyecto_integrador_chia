﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="~/Controller/Inicio de Sesion y Registro/RecuperarContraseña.aspx.cs" Inherits="Views_Inicio_de_Sesion_y_Registro_RecuperarContraseña" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Recuperar Contraseña</title>
    <link href="../../assets/img/favicon.png" rel="icon">
  <link href="../../assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="../../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../../assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="../../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="../../assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="../../assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="../../assets/css/style.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">

         <header id="header">
                    <div class="container d-flex">

                      <div class="logo mr-auto">
                        <h1 class="text-light"><a href="index.html">Bocor<span>.</span></a></h1>
                        <!-- Uncomment below if you prefer to use an image logo -->
                         <a><img src="assets/img/logo.png" alt="" class="img-fluid"></a>
                      </div>

                      <nav class="nav-menu d-none d-lg-block">
                        <ul>
                           
                       
                        </ul>
                      </nav><!-- .nav-menu -->

                    </div>
                  </header><!-- End Header -->

    <div class="container ">
        <div class="row">
             <br />
    <br />
    <br /><br />
    <br /><br />
            <br />
            <br />
        </div>

    </div>


        <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-header text-center">
                        <h5>Recuperar Contraseña</h5>
                    </div>

                    <div class="card-body">

                            <div class="form-group row">
                                <label for="inputPassword" class="col-md-4 col-form-label text-md-right">Digite la Nueva Contraseña</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="TX_Clave" TextMode="Password" runat="server"></asp:TextBox>

                                 <asp:RegularExpressionValidator ID="REV_Clave_Registro" runat="server" ControlToValidate="TX_Clave" Display="Dynamic" ErrorMessage="*La contraseña no cumple con los requisitos minimos de seguridad, Minimo 1 mayuscula 1 minuscula un caracter especial y minimo 6 caracteres de longitud " ForeColor="Red" SetFocusOnError="True" ValidationExpression="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@|#|.|,|_|-|\|!| |#|$|%|&|/|(|)|?|Ñ|ñ]).{6,20}$" ValidationGroup="VG_Registro"></asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TX_Clave" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Recuperar"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-md-4 col-form-label text-md-right">RepetirContraseña</label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="TX_Repetir_Clave" TextMode="Password" runat="server"></asp:TextBox>

                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TX_Clave" ControlToValidate="TX_Repetir_Clave" Display="Dynamic" ErrorMessage="*La contraseña no coincide." ForeColor="Red" ValidationGroup="VG_Recuperar"></asp:CompareValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TX_Repetir_Clave" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Recuperar"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <asp:Button ID="Button1" runat="server" class="btn btn-primary" Text="Recuperar Contraseña" ValidationGroup="VG_Recuperar" OnClick="BTN_Recuperar_Click" />
                                </div>
                            </div>

                       
                    </div>
                </div>
            </div>
        </div>
    </div>


    </form>
</body>

       <script src="../../assets/vendor/jquery/jquery.min.js"></script>
                  <script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
                  <script src="../../assets/vendor/jquery.easing/jquery.easing.min.js"></script>
                  <script src="../../assets/vendor/php-email-form/validate.js"></script>
                  <script src="../../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
                  <script src="../../assets/vendor/venobox/venobox.min.js"></script>
                  <script src="../../assets/vendor/aos/aos.js"></script>
                    <script src="../../assets/js/particles.js"></script>
                    <script src="../../assets/js/app.js"></script>
                  
                  <!-- Template Main JS File -->
                  <script src="../../assets/js/main.js"></script>
</html>
