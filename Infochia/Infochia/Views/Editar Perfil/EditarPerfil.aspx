﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Master/MasterRegistrados.master" AutoEventWireup="true" CodeFile="~/Controller/Editar Perfil/EditarPerfil.aspx.cs" Inherits="Views_Editar_Perfil_EditarPerfil" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>

        .imagen {

            width:55%;
            height:35%;
            margin-left:20%;

        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
    <br />

      <div class="container">
    <div class="row">
        <div class="col-3">

        </div>
          <div class="col-6">
               <h1> Editar Perfil</h1>
        </div>
          <div class="col-3">

        </div>

      </div>
    </div>
    <br />
    <div class="container">
        <div class="row justify-content-center" >
            <div class="col-md-4">
                <br />
                                     <br />
                                     
                                    
                                      <asp:Image ID="IM_Perfil_Actual" runat="server" class="card-img-top"  CssClass="imagen"/>
                                     <br />
                                     <br />
                                     <br />
                                     <br />
                                     <div class="card" >
                                      
                                         <div class="card-body">
                                    <h5 class="card-title">Imagen Actual Perfil</h5>
                                    <p class="card-text">Para cambiarla adjunte una nueva y oprima Editar Perfil.</p>
                                    <asp:FileUpload ID="FU_Imagen_Usuario" runat="server" CssClass="form-control-file" class="form-control-file"/>
                                  </div>
                              </div>
                                      


            </div>

                                                  <div class="col-md-8" >
                                                         <div class="card" >
                                                         <div class="card-header text-center">
                                                            <h5>Perfil</h5>
                                                        </div>
                                                              <div class="card-body">
                                                                      <div class="form-group row">
                                                                        <label for="inputEmail" class="col-md-4 col-form-label text-md-right">
                                                                        <h6>Nombre</h6>
                                                                    </label>
                                                                       <div class="col-md-6" >
                                                                            <asp:TextBox ID="TX_Nombre" runat="server" CssClass="form-control"></asp:TextBox>
                              
                                                                           <asp:RequiredFieldValidator ID="RFV_Nombre" runat="server" ControlToValidate="TX_Nombre" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Editar"></asp:RequiredFieldValidator>
                                                                          <asp:RegularExpressionValidator ID="RV_Nombre" runat="server" ControlToValidate="TX_Nombre" ErrorMessage="*Digite solo letras por favor" ForeColor="Red" ValidationExpression="^[a-zA-Z ñÑ]*$" ValidationGroup="VG_Editar"></asp:RegularExpressionValidator>
                             
                           
                                                                            </div>

                                                                      </div>

                                              
                                                      
                                                                      <div class="form-group row">
                                                                        <label for="inputEmail" class="col-md-4 col-form-label text-md-right">
                                                                        <h6>Apellido</h6>
                                                                    </label>
                                                                       <div class="col-md-6" >
                                                                      <asp:TextBox ID="TX_Apellido" runat="server" CssClass="form-control"></asp:TextBox>
                                                                  <asp:RequiredFieldValidator ID="RFV_Apellido" runat="server" ControlToValidate="TX_Apellido" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Editar"></asp:RequiredFieldValidator>
                                                                  <asp:RegularExpressionValidator ID="RV_Apellido" runat="server" ControlToValidate="TX_Apellido" ErrorMessage="*Digite solo letras por favor" ForeColor="Red" ValidationExpression="^[a-zA-Z ñÑ]*$" ValidationGroup="VG_Editar"></asp:RegularExpressionValidator>
                            
                                                                            </div>

                                                                      </div>


                                                                      <div class="form-group row">
                                                                        <label for="inputEmail" class="col-md-4 col-form-label text-md-right">
                                                                        <h6>Correo</h6>
                                                                    </label>
                                                                       <div class="col-md-6" >
                                                                                <asp:TextBox ID="TX_Correo" runat="server" CssClass="form-control"></asp:TextBox>
                                                                            <asp:RegularExpressionValidator ID="REV_Correo_Electronico_Registro" runat="server" ControlToValidate="TX_Correo" Display="Dynamic" ErrorMessage="*Debe introducir un correo." ForeColor="Red" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="VG_Inicio_Sesion"></asp:RegularExpressionValidator>
                                                                      <asp:RequiredFieldValidator ID="RFV_Correo" runat="server" ControlToValidate="TX_Correo" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Inicio_Sesion"></asp:RequiredFieldValidator>


                                                                            </div>

                                                                      </div>

                                                                     
                                                            
                                                            
                                                                      <div class="form-group row">
                                                                        <label for="inputEmail" class="col-md-4 col-form-label text-md-right">
                                                                        <h6>Contraseña</h6>
                                                                    </label>
                                                                       <div class="col-md-6" >
                                                                                <asp:TextBox ID="TX_Clave" TextMode="Password"  runat="server" CssClass="form-control"></asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="RFV_Clave" runat="server" ControlToValidate="TX_Clave" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Editar"></asp:RequiredFieldValidator>
                                                                              <asp:RegularExpressionValidator ID="REV_Clave_Registro" runat="server" ControlToValidate="TX_Clave" Display="Dynamic" ErrorMessage="*Minimo 6 caracteres - Maximo 20" ForeColor="Red" SetFocusOnError="True" ValidationExpression="^[\s\S]{6,20}$" ValidationGroup="VG_Editar"></asp:RegularExpressionValidator>
                            
                                                                            </div>

                                                                      </div>

                                                         
                                                          
                                                                      <div class="form-group row">
                                                                        <label for="inputEmail" class="col-md-4 col-form-label text-md-right">
                                                                        <h6>Repetir Contraseña</h6>
                                                                    </label>
                                                                       <div class="col-md-6" >
                                                                           <asp:TextBox ID="TX_Repetir_Clave" TextMode="Password"  runat="server" CssClass="form-control"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RFV_Repetir_Clave" runat="server" ControlToValidate="TX_Repetir_Clave" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Editar"></asp:RequiredFieldValidator>
                                                                            <asp:CompareValidator ID="CV_Repetir_Clave_Registro" runat="server" ControlToCompare="TX_Clave" ControlToValidate="TX_Repetir_Clave" Display="Dynamic" ErrorMessage="*La contraseña no coincide." ForeColor="Red" ValidationGroup="VG_Editar"></asp:CompareValidator>
                           

                                                                            </div>

                                                                      </div>

                                                              
                                                       
                                                                     <div class="form-group row mb-0">
                                                                                  <div class="col-md-8 offset-md-4">
                                                                                              <asp:Button ID="BTN_Editar_Perfil" runat="server" class="btn btn-primary" Text="Editar Perfil"  ValidationGroup="VG_Editar" OnClick="BTN_Editar_Perfil_Click" />

                                                                     </div>

                                                              </div>
                                                        

                                          </div>
            </div>
   <br />
    <asp:Label ID="LA_Mensaje" runat="server" Text="" CssClass="alert alert-danger" Visible="False"></asp:Label>
        </div>
    </div>
  </div>
                               
                                 
</asp:Content>

