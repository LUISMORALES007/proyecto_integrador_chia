﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Master/MasterRegistrados.master" AutoEventWireup="true" CodeFile="~/Controller/Evento/VerEditarEvento.aspx.cs" Inherits="Views_Evento_VerEditarEvento" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
       <style type="text/css">

           .modalBackground{
            background-color: black;
            filter:alpha(opacity=90) !important;
            opacity:0.6 !important;
            z-index:20;
           }


         .modalpopup2 {
            padding-left:5%;
            padding-top:2%;
            position:relative;
            width:500px;
            height:320px;
            background-color:whitesmoke;
            border-radius: 22px 22px 22px 22px;
            -moz-border-radius: 22px 22px 22px 22px;
           -webkit-border-radius: 22px 22px 22px 22px;
            border: 2px solid #000000;
            -webkit-box-shadow: -2px 3px 16px 2px rgba(0,0,0,0.75);
           -moz-box-shadow: -2px 3px 16px 2px rgba(0,0,0,0.75);
          box-shadow: -2px 3px 16px 2px rgba(0,0,0,0.75);
        }

           .boton {

               margin-right:5%;
           }
           .auto-style1 {
               width: 240px;
           }
           .auto-style2 {
               width: 240px;
               height: 77px;
           }
           .auto-style3 {
               width: 305px;
               height: 77px;
           }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
         




    <div class="row">
        <div class="col-4">


             <asp:ImageButton ID="IB_Regresar" runat="server" Height="48px" Width="50px" ImageUrl="~/assets/img/Botones/back-icon.png" OnClick="IB_Regresar_Click" />
        </div>
          <div class="col-4">
               <h1>EDITAR EVENTOS</h1>
        </div>
          <div class="col-4">

        </div>
    </div>

     <div class="container">
     
               
        
          <div class="row justify-content-center" >

                                 <div class="col-md-4">
                                     <br />
                                     <br />
                                     
                                      <asp:Image ID="IM_Imagen" runat="server" class="card-img-top"/>
                                     <br />
                                     <br />
                                     <br />
                                     <br />
                                     <div class="card" >
                                      
                                         <div class="card-body">
                                    <h5 class="card-title">Imagen Actua Evento</h5>
                                    <p class="card-text">Para cambiarla adjunte una nueva y oprima modificar evento.</p>
                                     <asp:FileUpload ID="FU_Url_Evento" runat="server" class="form-control-file"/>
                                  </div>
                              </div>
                                      

                                   </div>
                               <div class="col-md-8" >                              
          
                                    <br />
                                                         <div class="card" >
                                                         <div class="card-header text-center">
                                                            <h5>Formulario</h5>
                                                        </div>
                                                              <div class="card-body">

                                                                  <div class="form-row">
                                                                <div class="col-md-6 mb-3">
                                                                    <label for="validationDefault01">Estado Actual del evento</label>

                                                                         <asp:DropDownList ID="DDL_Estado" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="DDL_Estado_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                          <asp:RequiredFieldValidator ID="RFV_Estado" runat="server" ControlToValidate="DDL_Estado" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Evento"></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div class="col-md-6 mb-3">
                                                                    <label for="validationDefault02">Nombre Evento</label>
                                                                           <asp:TextBox ID="TX_Nombre" runat="server" CssClass="form-control"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RFV_Nombre" runat="server" ControlToValidate="TX_Nombre" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Evento"></asp:RequiredFieldValidator>
                                                                        <asp:RegularExpressionValidator ID="REV_Nombre" runat="server" ControlToValidate="TX_Nombre"  ErrorMessage="*Minimo 4 caracteres - Maximo 25" ForeColor="Red" SetFocusOnError="True" ValidationExpression="^[\s\S]{4,25}$" ValidationGroup="VG_Evento" ></asp:RegularExpressionValidator>
          
                                                                </div>
                                                                </div>

                                                                  <div class="form-row">
                                                                <div class="col-md-6 mb-3">
                                                                  <label for="validationDefault01">Descripcion</label>
                                                                  <asp:TextBox ID="TX_Descripcion" runat="server" Height="83px" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                                  <asp:RequiredFieldValidator ID="RFV_Descripcion" runat="server" ControlToValidate="TX_Descripcion" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Evento"></asp:RequiredFieldValidator>
                                                                   <asp:RegularExpressionValidator ID="REV_Descripcion" runat="server"  ControlToValidate="TX_Descripcion"  ErrorMessage="*Minimo 4 caracteres - Maximo 45" ForeColor="Red" SetFocusOnError="True" ValidationExpression="^[\s\S]{4,45}$"  ValidationGroup="VG_Evento" ></asp:RegularExpressionValidator>
           
                                                                </div>
                                                                <div class="col-md-6 mb-3">
                                                                  <label for="validationDefault02">Categoria</label>
                                                                     <asp:DropDownList ID="DDL_Categoria" runat="server" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                      <asp:RequiredFieldValidator ID="RFV_Categoria" runat="server" ControlToValidate="DDL_Categoria" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Evento"></asp:RequiredFieldValidator>
                                                                </div>
                                                              </div>

                                                                        
                                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                <ContentTemplate>

                                                                  <div class="form-row">
                                                                    <div class="col-md-6 mb-3">
                                                                      <label for="validationDefault01">Fecha Inicio</label>
                                                                               <asp:TextBox ID="TX_Fecha_Inicio" runat="server" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                                                 <asp:RequiredFieldValidator ID="RFV_Fecha_Inicio" runat="server" ControlToValidate="TX_Fecha_Inicio" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Evento"></asp:RequiredFieldValidator>
                                                                                <asp:RangeValidator ID="RAV_Fecha_Inicio" runat="server"  Type="Date"  ErrorMessage="* Fecha no valida" ForeColor="Red" ValidationGroup="VG_Evento" ControlToValidate="TX_Fecha_Inicio" Display="Dynamic"></asp:RangeValidator>
              
           
                                                                    </div>
                                                                    <div class="col-md-6 mb-3">
                                                                      <label for="validationDefault02">Fecha Fin</label>
                                                                        <asp:TextBox ID="TX_Fecha_Fin" runat="server" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                                 <asp:RequiredFieldValidator ID="RFV_Fecha_Fin" runat="server" ControlToValidate="TX_Fecha_Fin" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Evento"></asp:RequiredFieldValidator>
                                                                  <asp:CompareValidator ID="CV_Fecha_fin" ControlToCompare="TX_Fecha_Inicio"  ControlToValidate="TX_Fecha_Fin" Type="Date" Operator="GreaterThanEqual"   ErrorMessage="*La fecha de fin no puede ser menor a la fecha de inicio" runat="server" ForeColor="Red"></asp:CompareValidator>
           
                                                                    </div>
                                                                  </div>

                                                                  <div class="form-row">
                                                                        <div class="col-md-6 mb-3">
                                                                          <label for="validationDefault01">Horal Inicio</label>
                                                                             <asp:TextBox ID="TX_Hora" runat="server" TextMode="Time" CssClass="form-control"></asp:TextBox>
                                                                           <asp:RequiredFieldValidator ID="RFV_Hora" runat="server" ControlToValidate="TX_Hora" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Evento"></asp:RequiredFieldValidator>
                                                                           <asp:Label ID="LA_Hora" runat="server" ForeColor="Red" ></asp:Label>
                                                                        </div>

                                                                          

                                                                        <div class="col-md-6 mb-3">
                                                                          <label for="validationDefault02">Lugar</label>
                                                                         <asp:TextBox ID="TX_Lugar" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RFV_Lugar" runat="server" ControlToValidate="TX_Lugar" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Evento"></asp:RequiredFieldValidator>
                                                                   <asp:RegularExpressionValidator ID="REV_Lugar" runat="server"  ControlToValidate="TX_Lugar"  ErrorMessage="*Minimo 4 caracteres - Maximo 45" ForeColor="Red" SetFocusOnError="True" ValidationExpression="^[\s\S]{4,45}$"  ValidationGroup="VG_Evento" ></asp:RegularExpressionValidator>
         
                                                                        </div>
                                                                      </div>
                                                                          </ContentTemplate>   
                                                                         </asp:UpdatePanel>

                                                              
                                                                  <br />
                                                        

                                                                 <div class="form-row">
                                                                                <div class="form-group col-md-6">
                                                                                   <asp:Button ID="B_Modificar_Evento" runat="server" Text="Modificar Evento" class="btn btn-primary"  ValidationGroup="VG_Evento" OnClick="B_Modificar_Evento_Click"  />

                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                  <asp:Button ID="B_Eliminar_Evento" runat="server" Text="Cancelar Evento" class="btn btn-danger"  />

                                                                                </div>
                                                                              </div>

                                                             </div>

                                                             
                                                         </div>



                                    </div>
               </div>

       </div>

          







                <asp:Panel ID="Panel2" runat="server" CssClass="modalpopup2">
                      Ingrese su contraseña<br />
                   <asp:TextBox ID="TX_Validar_Clave" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RFV_Clave" runat="server" ControlToValidate="TX_Validar_Clave" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Cancelar"></asp:RequiredFieldValidator>
                       <asp:RegularExpressionValidator ID="REV_Clave" runat="server" ControlToValidate="TX_Validar_Clave" Display="Dynamic" ErrorMessage="*Minimo 4 Caracteres- Maximo 20 " ForeColor="Red" SetFocusOnError="True" ValidationExpression="^[\s\S]{4,20}$" ValidationGroup="VG_Cancelar"></asp:RegularExpressionValidator>
                      <br />
                      <br />
                    ¿Porque desea cancelar el evento?
                     &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="TX_Cancelar" runat="server" Height="75px" TextMode="MultiLine" Width="232px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TX_Cancelar" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Cancelar"></asp:RequiredFieldValidator>
                      <asp:RegularExpressionValidator ID="REV_Cancelar" runat="server" ControlToValidate="TX_Cancelar" Display="Dynamic" ErrorMessage="*Minimo 6 caracteres - Maximo 90" ForeColor="Red" SetFocusOnError="True" ValidationExpression="^[\s\S]{6,90}$" ValidationGroup="VG_Cancelar"></asp:RegularExpressionValidator>
                      <br />
                    <br />
                    <asp:Button ID="B_Validar_Clave" runat="server" Text="Cancelar Evento" Width="159px"  CssClass="boton btn btn-info" OnClick="B_Validar_Clave_Click" ValidationGroup="VG_Cancelar"  />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                     <asp:Button ID="B_No" runat="server" Height="34px" Text="Salir" Width="101px" CssClass="boton btn btn-danger" OnClick="B_No_Click"/>
                   
                </asp:Panel>
                   <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" runat="server"  CancelControlID="B_No" PopupControlID="Panel2" BackgroundCssClass="modalBackground" TargetControlID="B_Eliminar_Evento" ></ajaxToolkit:ModalPopupExtender>


                           <asp:Label ID="LA_Mensaje_Validar" runat="server" ForeColor="Red"></asp:Label>
                            <asp:Label ID="LA_Mensaje" runat="server" ForeColor="#33CC33"></asp:Label>

          

            

    
        
</asp:Content>

