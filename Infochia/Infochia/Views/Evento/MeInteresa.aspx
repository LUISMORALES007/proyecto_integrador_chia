﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Master/MasterRegistrados.master" AutoEventWireup="true" CodeFile="~/Controller/Evento/MeInteresa.aspx.cs" Inherits="Views_Evento_MeInteresa" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>

        .gridestilo {
            margin-left:-12%;
        }


    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="container">
 


    <div class="row">
        <div class="col-4">

        </div>
          <div class="col-4">
               <h1>ME INTERESA</h1>
        </div>
          <div class="col-4">

        </div>

    </div>
        </div>
    <br />
    <br />
    
    <div class="gridestilo">
      <asp:GridView ID="GV_Me_Interesa" runat="server" AutoGenerateColumns="False" CellPadding="5" ForeColor="#333333" GridLines="None" DataKeyNames="id"  Width="1342px" AllowPaging="True" CellSpacing="2" PageSize="20" DataSourceID="ODS_Me_Interesa" OnRowCommand="GV_Me_Interesa_RowCommand" CssClass="table-bordered">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:TemplateField HeaderText="Nombre">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("nombre") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fecha Inicio">
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("fecha_inicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
                <asp:TemplateField HeaderText="Fecha Fin">
                <ItemTemplate>
                    <asp:Label ID="Label8" runat="server" Text='<%# Bind("fecha_fin", "{0:dd/MM/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Hora Inicio">
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("hora") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Lugar">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("lugar") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Categoria Evento">
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("descripcion_categoria") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Estado Evento">
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%# Bind("descripcion_estado") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
                <asp:TemplateField HeaderText="Interesados">
                <ItemTemplate>
                    <asp:Label ID="Label9" runat="server" Text='<%# Bind("cantidad_interesados") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Imagen Evento">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" Height="70px" Width="70px"  ImageUrl='<%# Eval("imagen") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="No Me Interesa">
                <ItemTemplate>
                    <asp:ImageButton ID="IB_Info_Evento" runat="server" CommandName="eliminar" Height="56px" ImageUrl="~/assets/img/Botones/basura.png" Width="61px" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'  />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <EmptyDataTemplate>
            NO LE INTERESA NINGUN EVENTO
        </EmptyDataTemplate>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>



      <asp:ObjectDataSource ID="ODS_Me_Interesa" runat="server" SelectMethod="mostrarIntereses" TypeName="DAOEvento">
          <SelectParameters>
              <asp:Parameter Name="usuario" Type="Int64" />
          </SelectParameters>
      </asp:ObjectDataSource>
    </div>


</asp:Content>

