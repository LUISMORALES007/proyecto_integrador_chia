﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Master/MasterRegistrados.master" AutoEventWireup="true" CodeFile="~/Controller/Evento/BuscarEvento.aspx.cs" Inherits="Views_Evento_BuscarEvento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>

        .grid_eventos {

            margin-left:-10%;
        }

     

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

          

  <div class="container">
 


    <div class="row">
        <div class="col-3">

        </div>
          <div class="col-6">
               <h1>BUSCAR EVENTOS</h1>
        </div>
          <div class="col-3">

        </div>

    </div>
        </div>
    <br />
  
    <div class="container">
        
        <div class="row">
            <div class="col-10">
        <div>



            <table class="w-100">
                <tr>
                    <td>
                         &nbsp &nbsp &nbsp
                        ¿Que Buscas?
                        <asp:TextBox ID="TX_Buscar_Nombre" runat="server" CssClass="form-control"></asp:TextBox>
                    </td>
                    <td>
                         &nbsp &nbsp &nbsp
           
                        Categoria:
                        <asp:DropDownList ID="DDL_Buscar_Categoria" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </td>
                    <td>
                         &nbsp &nbsp &nbsp
                        Fecha del Evento:
                        <asp:TextBox ID="TX_Buscar_Fecha_Inicio" runat="server" TextMode="Date" CssClass="form-control"></asp:TextBox>
                        <asp:RangeValidator ID="RangeValidator1" runat="server"  Type="Date"  ErrorMessage="* Fecha no valida" ForeColor="Red" ValidationGroup="VG_Evento" ControlToValidate="TX_Buscar_Fecha_Inicio" Display="Dynamic"></asp:RangeValidator>
                    </td>
                    <td>
                        <br />
                        <asp:Button ID="B_Buscar_Evento" runat="server" Text="Buscar Evento" CssClass="btn btn-info" OnClick="B_Buscar_Evento_Click" ValidationGroup="VG_Evento" />
                    </td>
                </tr>
            </table>


        </div>
      
                </div>

            <div class="col-2">

                  <asp:ImageButton ID="IB_Regresar" runat="server" Height="48px" Width="50px" ImageUrl="~/assets/img/Botones/back-icon.png" OnClick="IB_Regresar_Click"  />

            </div>
    </div>
       




        </div>
       <div class="grid_eventos">
            <br />
            <br />

  
           <div class="table-responsive-xl">


      
         

          <asp:GridView ID="GV_Eventos" runat="server" AutoGenerateColumns="False" CellPadding="5" ForeColor="#333333" GridLines="None" DataKeyNames="id,id_usuario,id_categoria,id_estado,descripcion,cantidad_interesados,fecha_fin"  Width="1342px" DataSourceID="ODS_Mostrar_Eventos" OnRowCommand="GV_Eventos_RowCommand" OnRowDataBound="GV_Eventos_RowDataBound" AllowPaging="True" CellSpacing="2" PageSize="20" CssClass="table-bordered" >
        <AlternatingRowStyle BackColor="White" ForeColor="#284775"  />
        <Columns  >
            <asp:TemplateField HeaderText="Nombre" >
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("nombre") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fecha Inicio">
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("fecha_inicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Hora Inicio">
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("hora") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Lugar">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("lugar") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Categoria Evento">
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("descripcion_categoria") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Estado Evento">
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%# Bind("descripcion_estado") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Imagen Evento">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" Height="64px" Width="64px"  ImageUrl='<%# Eval("imagen") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Mas Informacion">
                <ItemTemplate>
                    <asp:Button ID="BTN_Info_Evento" runat="server" CommandName="info"  CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'  CssClass="btn btn-info"  Text="Mas Info"/>
                  
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <EmptyDataTemplate>
            NO HAY EVENTOS PARA ASISTIR EN ESTE MOMENTO
        </EmptyDataTemplate>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
                </div>
            <asp:ObjectDataSource ID="ODS_Mostrar_Eventos" runat="server" SelectMethod="mostrarEventos" TypeName="DAOEvento">
                <SelectParameters>
                    <asp:SessionParameter Name="_eventos" SessionField="datos_evento_filtrar" Type="Object" />
                </SelectParameters>
            </asp:ObjectDataSource>





        </div>
      </ContentTemplate>

    </asp:UpdatePanel>
    
</asp:Content>

