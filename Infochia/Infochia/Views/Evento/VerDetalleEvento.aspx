﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Master/MasterRegistrados.master" AutoEventWireup="true" CodeFile="~/Controller/Evento/VerDetalleEvento.aspx.cs" Inherits="Views_Evento_VerDetalleEvento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style>

        .tarjeta {

            border-radius: 10px 10px 10px 10px;
           -moz-border-radius: 10px 10px 10px 10px;
          -webkit-border-radius: 10px 10px 10px 10px;
           border: 0px solid #000000;
           -webkit-box-shadow: -7px 2px 14px -9px rgba(0,0,0,0.75);
          -moz-box-shadow: -7px 2px 14px -9px rgba(0,0,0,0.75);
         box-shadow: -7px 2px 14px -9px rgba(0,0,0,0.75);
             height:68%;
             width:105%;
             padding:5px;
             background-color:white;
             margin-top:23%;
        }
        .imagen {
            margin-top:-2%;
        }

        .imagen_ {
            padding:5%;
        }

           .modalBackground{
            background-color: black;
            filter:alpha(opacity=90) !important;
            opacity:0.6 !important;
            z-index:20;
           }


         .modalpopup2 {
            padding-left:5%;
            padding-top:2%;
            position:relative;
            width:500px;
            height:290px;
            background-color:whitesmoke;
            border-radius: 22px 22px 22px 22px;
            -moz-border-radius: 22px 22px 22px 22px;
           -webkit-border-radius: 22px 22px 22px 22px;
            border: 2px solid #000000;
            -webkit-box-shadow: -2px 3px 16px 2px rgba(0,0,0,0.75);
           -moz-box-shadow: -2px 3px 16px 2px rgba(0,0,0,0.75);
          box-shadow: -2px 3px 16px 2px rgba(0,0,0,0.75);
        }

           .boton {

               margin-right:5%;
           }
              

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
          <ContentTemplate>
        
    <div class="container-xl">
      

        <div class="row">

            <div class="col-sm-4">
             
                <div class="tarjeta">
                    <asp:Image ID="IM_Imagen_Evento"  Width="100%" Height="100%"  runat ="server"  ImageUrl="# " CssClass="imagen_"  />
                </div>
       
                        
            </div>

            <div class="col-sm-7">
                <br />
                <br />  <br />
                <br />
            
            <div class="card" style="width: 45rem;">
            
        
                  <div class="card-body">
                    <h4 class="card-title"><strong><asp:Label ID="LA_Nombre" runat="server" Text=" "></asp:Label></strong> &nbsp;&nbsp;<asp:Image ID="IM_Me_Gusta" runat="server" ImageUrl="~/assets/img/Iconos/thumb-up.png" Visible="False" /><asp:Image ID="IM_Cancelado" runat="server"  ImageUrl="~/assets/img/Iconos/cruzar.png" Visible="False"/> </h4>
                    <div class="alert alert-success" role="alert"><p class="card-text"><strong> Descripcion: &nbsp;&nbsp;</strong><asp:Label ID="LA_Descripcion" runat="server" Text=" "></asp:Label></p> </div>
                      <div class="alert alert-info" role="alert"><strong>Cantidad de Interesados:&nbsp;&nbsp; </strong> <asp:Label ID="LA_Interesados" runat="server" Text="Label"></asp:Label></div>
                      <div class="alert alert-secondary" role="alert"><strong>Fecha Inicio: &nbsp;&nbsp;</strong> <asp:Label ID="LA_Fecha_inicio" runat="server" Text=""></asp:Label></div>
                        <div class="alert alert-secondary" role="alert"><strong>Fecha Fin:&nbsp;&nbsp;</strong>  <asp:Label ID="LA_Fecha_Fin" runat="server" Text=""></asp:Label></div>
                      <div class="alert alert-primary" role="alert">  <strong>Hora:&nbsp;&nbsp; </strong><asp:Label ID="LA_Hora" runat="server" Text=""></asp:Label> </div>
                      <div class="alert alert-warning" role="alert"> <strong>Lugar de Realizacion&nbsp;&nbsp;</strong> <asp:Label ID="LA_Lugar" runat="server" Text=""></asp:Label> </div>
                      <div class="alert alert-dark" role="alert"> <strong>Organizador del Evento:&nbsp;&nbsp;</strong>  <asp:Label ID="LA_Nombre_Organizador" runat="server" Text="Informacion Disponible Solo para Usuarios Registrados"></asp:Label></div>
                      <div class="alert alert-dark" role="alert"> <strong>Correo Contacto:&nbsp;&nbsp;</strong>  <asp:Label ID="LA_Correo_Organizador" runat="server" Text="Informacion Disponible Solo para Usuarios Registrados"></asp:Label> </div>
                      <br />
                      <div class="row">
                          <div class="col-6">

                                <asp:Button ID="B_Me_Interesa" runat="server" CssClass="btn btn-info" Text="Me interesa" Enabled="False" Visible="False" OnClick="B_Me_Interesa_Click" />
               
                                  <asp:Button ID="B_No_Me_Interesa" runat="server" Text="Ya no me interesa" CssClass="btn btn-warning"  Enabled="False" Visible="False" Height="37px" OnClick="B_No_Me_Interesa_Click"  />
                          </div>
                     
                           

                             <div class="col-6">
                                    <asp:Button ID="B_Denunciar" runat="server"  CssClass="btn btn-danger" Text="Denunciar Evento" Enabled="False" Visible="False" />

                          </div>

                      </div>
                 
   
                
              </div>
                    <asp:Label ID="LA_Mensaje" runat="server" Text="" ForeColor="Red" CssClass="alert alert-danger" Visible="False"></asp:Label>
                      <asp:Label ID="LA_Mensaje_Info" runat="server" Text="" ForeColor="Green" CssClass="alert alert-danger" Visible="False"></asp:Label>
                
           </div>
                 
            </div>

                <asp:Panel ID="Panel2" runat="server" CssClass="modalpopup2">
                    
                    ¿Porque desea Denunciar el evento?
                    <br />
                     &nbsp;&nbsp;&nbsp;&nbsp;
                      <br />
                    <asp:TextBox ID="TX_Denuncia" runat="server" Height="83px" TextMode="MultiLine" Width="281px" CssClass="offset-sm-0"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RFV_Denuncia" runat="server" ControlToValidate="TX_Denuncia" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Denuncia"></asp:RequiredFieldValidator>
                      <asp:RegularExpressionValidator ID="REV_Denuncia" runat="server" ControlToValidate="TX_Denuncia" Display="Dynamic" ErrorMessage="*Minimo 6 caracteres - Maximo 90" ForeColor="Red" SetFocusOnError="True" ValidationExpression="^[\s\S]{6,90}$" ValidationGroup="VG_Denuncia"></asp:RegularExpressionValidator>
                      <br />
                    <br />
                    <asp:Button ID="B_Enviar_Denuncia" runat="server" Text="Denunciar Evento" Width="159px"  CssClass="boton btn btn-info"  ValidationGroup="VG_Denuncia" OnClick="B_Enviar_Denuncia_Click"  />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                     <asp:Button ID="B_No" runat="server" Height="34px" Text="Salir" Width="101px" CssClass="boton btn btn-danger" OnClick="B_No_Click" />
                   
                </asp:Panel>
                   <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" runat="server"  CancelControlID="B_No" PopupControlID="Panel2" BackgroundCssClass="modalBackground" TargetControlID="B_Denunciar" ></ajaxToolkit:ModalPopupExtender>
      
         

                <div class="imagen col-sm-1">
                    
                  <asp:ImageButton ID="IB_Regresar" runat="server" Height="48px" Width="50px" ImageUrl="~/assets/img/Botones/back-icon.png" OnClick="IB_Regresar_Click"   />

            
            </div>

        </div>

    </div>
          </ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>

