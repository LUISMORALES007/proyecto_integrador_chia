﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Master/MasterRegistrados.master" AutoEventWireup="true" CodeFile="~/Controller/Evento/InsertarEvento.aspx.cs" Inherits="Views_Evento_InsertarEvento" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
    <style type="text/css">
        .auto-style1 {
            display: block;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #495057;
            background-clip: padding-box;
            border-radius: .25rem;
            transition: none;
            border: 1px solid #ced4da;
            background-color: #fff;
        }
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    

   


    <div class="row">
        <div class="col-4">

        </div>
          <div class="col-4">
               <h1>CREAR EVENTOS</h1>
        </div>
          <div class="col-4">

        </div>

    </div>
    <div class="container">

          <div class="row justify-content-center" >
                                <div class="col-md-10" >                              
                                                 
                                    <br />
                                      
                                                         <div class="card" >
                                                         <div class="card-header text-center">
                                                            <h5>Formulario</h5>
                                                        </div>
                                                              <div class="card-body">
                                                     
                                                                  <div class="form-row">
                                                                   <div class="form-group col-md-6">
                                                                  <label for="inputEmail4">Nombre</label>
                                                                    <asp:TextBox ID="TX_Nombre" runat="server" CssClass="form-control "></asp:TextBox>
 
                                                                            <asp:RequiredFieldValidator ID="RFV_Nombre" runat="server" ControlToValidate="TX_Nombre" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Evento" ></asp:RequiredFieldValidator>
                                                                        <asp:RegularExpressionValidator ID="REV_Nombre" runat="server" ControlToValidate="TX_Nombre"  ErrorMessage="*Minimo 4 caracteres - Maximo 25" ForeColor="Red" SetFocusOnError="True" ValidationExpression="^[\s\S]{4,25}$" ValidationGroup="VG_Evento"  ></asp:RegularExpressionValidator>
                                                    
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                  <label for="inputPassword4">Descripcion</label>
                                                                   <asp:TextBox ID="TX_Descripcion" runat="server" Height="83px" TextMode="MultiLine"  CssClass="form-control"></asp:TextBox>
                                                                              <asp:RequiredFieldValidator ID="RFV_Descripcion" runat="server" ControlToValidate="TX_Descripcion" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Evento"></asp:RequiredFieldValidator>
                                                                               <asp:RegularExpressionValidator ID="REV_Descripcion" runat="server"  ControlToValidate="TX_Descripcion"  ErrorMessage="*Minimo 4 caracteres - Maximo 45" ForeColor="Red" SetFocusOnError="True" ValidationExpression="^[\s\S]{4,45}$"  ValidationGroup="VG_Evento" ></asp:RegularExpressionValidator>
                                                                </div>

                                                                  </div>

                                                                  <div class="form-row">
                                                                    <div class="form-group col-md-6">
                                                                      <label for="inputEmail4">Categoria</label>
                                                                        <asp:DropDownList ID="DDL_Categoria" runat="server"  CssClass="form-control">
                                                                                </asp:DropDownList>
                                                                                  <asp:RequiredFieldValidator ID="RFV_Categoria" runat="server" ControlToValidate="DDL_Categoria" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Evento"></asp:RequiredFieldValidator>
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                      <label for="inputPassword4">Lugar</label>
                                                                        <asp:TextBox ID="TX_Lugar" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RFV_Lugar" runat="server" ControlToValidate="TX_Lugar" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Evento"></asp:RequiredFieldValidator>
                                                                    <asp:RegularExpressionValidator ID="REV_Lugar" runat="server"  ControlToValidate="TX_Lugar"  ErrorMessage="*Minimo 4 caracteres - Maximo 45" ForeColor="Red" SetFocusOnError="True" ValidationExpression="^[\s\S]{4,45}$"  ValidationGroup="VG_Evento" ></asp:RegularExpressionValidator>
       
                                                                    </div>
                                                                  </div>

                                                                  <div class="form-row">
                                                                        <div class="form-group col-md-6">
                                                                          <label for="inputEmail4">Fecha Inicio</label>
                                                                          <asp:TextBox ID="TX_Fecha_Inicio" runat="server" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="RFV_Fecha_Inicio" runat="server" ControlToValidate="TX_Fecha_Inicio" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Evento"></asp:RequiredFieldValidator>
                                                                            <asp:RangeValidator ID="RangeValidator1" runat="server"  Type="Date"  ErrorMessage="* Fecha no valida" ForeColor="Red" ValidationGroup="VG_Evento" ControlToValidate="TX_Fecha_Inicio" Display="Dynamic"></asp:RangeValidator>

                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                          <label for="inputPassword4">Fecha Fin</label>
                                                                            <asp:TextBox ID="TX_Fecha_Fin" runat="server" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                                                                 <asp:RequiredFieldValidator ID="RFV_Fecha_Fin" runat="server" ControlToValidate="TX_Fecha_Fin" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Evento"></asp:RequiredFieldValidator>
                                                                                 <asp:CompareValidator ID="cmpVal1" ControlToCompare="TX_Fecha_Inicio"  ControlToValidate="TX_Fecha_Fin" Type="Date" Operator="GreaterThanEqual"   ErrorMessage="*La fecha de fin no puede ser menor a la fecha de inicio" runat="server" ForeColor="Red"></asp:CompareValidator>
           
                                                                        </div>
                                                                      </div>

                                                                           <div class="form-row">
                                                                                    <div class="form-group col-md-6">
                                                                                      <label for="inputEmail4">Hora Inicio </label>
                                                                                       <asp:TextBox ID="TX_Hora" runat="server" TextMode="Time" CssClass="form-control"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RFV_Hora" runat="server" ControlToValidate="TX_Hora" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Evento"></asp:RequiredFieldValidator>
                                                                                    <asp:Label ID="LA_Hora" runat="server" ForeColor="Red" ></asp:Label>
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                      <label for="inputPassword4">Imagen evento</label>
                                                                                         <asp:FileUpload ID="FU_Url_Evento" runat="server"  class="form-control-file" />
                                                                                <asp:RequiredFieldValidator ID="RFV_Imagen" runat="server" ControlToValidate="FU_Url_Evento" ErrorMessage="*" ForeColor="Red" ValidationGroup="VG_Evento"></asp:RequiredFieldValidator>
                                                                                    </div>
                                                                                  </div>

                   
                                                                       <asp:Button ID="B_Crear_Evento" runat="server" Text="Crear Evento" class="btn btn-primary" OnClick="B_Crear_Evento_Click" ValidationGroup="VG_Evento"  />
                                                                                    
    
                                                                </div>

                                                   </div>
                      
                                          <asp:Label ID="LA_Mensaje" runat="server"  CssClass="alert alert-danger" Visible="False"></asp:Label>
                                                                                          <asp:Label ID="LA_Mensaje_Info" runat="server"  CssClass="alert alert-info" Visible="False"></asp:Label>
                            </div>   

          </div>   

    </div>

   
</asp:Content>

