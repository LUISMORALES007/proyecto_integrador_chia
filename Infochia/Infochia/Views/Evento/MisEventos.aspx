﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Master/MasterRegistrados.master" AutoEventWireup="true" CodeFile="~/Controller/Evento/MisEventos.aspx.cs" Inherits="Views_Evento_MisEventos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>

        .gridestilo {
            margin-left:-15%;
        }


        .auto-style1 {
            margin-right: 0px;
        }


    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="container">


    <div class="row">
        <div class="col-4">

        </div>
          <div class="col-4">
               <h1>MIS EVENTOS</h1>
        </div>
          <div class="col-4">

        </div>

    </div>
        </div>
    <br />
    <br />
    <br />

    <div class="gridestilo">

        <div class="row">

            <div class="col-12">

                <div class="table-responsive-xl">

    <asp:GridView ID="GV_Mis_Eventos" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="ODS_Mis_Eventos" ForeColor="#333333" GridLines="None" DataKeyNames="id,id_usuario,id_categoria,id_estado" OnRowCommand="GV_Mis_Eventos_RowCommand" Width="1335px" AllowPaging="True" PageSize="25" CssClass="table-bordered" >
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:TemplateField HeaderText="Nombre">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("nombre") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Descripcion">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("descripcion") %>' Width="90%"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fecha Inicio">
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("fecha_inicio", "{0:dd/MM/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fecha Fin">
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("fecha_fin", "{0:dd/MM/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Hora Inicio">
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("hora") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Lugar">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("lugar") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label9" runat="server" Text='<%# Bind("lugar") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Categoria Evento">
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("descripcion_categoria") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Estado Evento">
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%# Bind("descripcion_estado") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Interesados">
                <ItemTemplate>
                    <asp:Label ID="Label8" runat="server" Text='<%# Bind("cantidad_interesados") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Imagen Evento">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" Height="64px" Width="64px"  ImageUrl='<%# Eval("imagen") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Ver detalle / Editar">
                <ItemTemplate>
                    
                    <asp:Button ID="BTN_Ver_Evento" runat="server" Text="Ver Evento" CommandName="ver"  CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'  CssClass="btn btn-warning"/>

                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <EmptyDataTemplate>
            NO HAY EVENTOS REGISTRADOS
        </EmptyDataTemplate>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
                </div>
    <asp:ObjectDataSource ID="ODS_Mis_Eventos" runat="server" SelectMethod="mostrarMisEventos" TypeName="DAOEvento">
        <SelectParameters>
            <asp:Parameter Name="usuario" Type="Int64" />
        </SelectParameters>
    </asp:ObjectDataSource>
                   </div>


        </div>
     </div>
</asp:Content>

