﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de ECancelarEvento
/// </summary>
public class ECancelarEvento
{
    private long idEvento;
    private String razon;
    private String session;

    public long IdEvento { get => idEvento; set => idEvento = value; }
    public string Razon { get => razon; set => razon = value; }
    public string Session { get => session; set => session = value; }
}