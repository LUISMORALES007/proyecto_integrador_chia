﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de EEvento
/// </summary>
public class EEvento
{
    public EEvento()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }
           
            private long idEvento;
            private String nombre;
            private String descripcion;
            private String fechaInicio;
            private String fechaFin;
           private String hora;
            private String imagen;
            private long idUsuario;
            private int idCategoria;
            private int idEstado;
            private int cantidadInteresados;
            private int cantidadDenuncias;
            private String session;
           private String lugar;

    public long IdEvento { get => idEvento; set => idEvento = value; }
    public string Nombre { get => nombre; set => nombre = value; }
    public string Descripcion { get => descripcion; set => descripcion = value; }
    public string FechaInicio { get => fechaInicio; set => fechaInicio = value; }
    public string FechaFin { get => fechaFin; set => fechaFin = value; }
    public string Hora { get => hora; set => hora = value; }
    public string Imagen { get => imagen; set => imagen = value; }
    public long IdUsuario { get => idUsuario; set => idUsuario = value; }
    public int IdCategoria { get => idCategoria; set => idCategoria = value; }
    public int IdEstado { get => idEstado; set => idEstado = value; }
    public int CantidadInteresados { get => cantidadInteresados; set => cantidadInteresados = value; }
    public int CantidadDenuncias { get => cantidadDenuncias; set => cantidadDenuncias = value; }
    public string Session { get => session; set => session = value; }
    public string Lugar { get => lugar; set => lugar = value; }
}