﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de EUsuario
/// </summary>
public class EUsuario
{
    private long codigo_usuario;
    private String nombre;
    private String apellido;
    private String correo;
    private String clave;
    private String url_imagen;
    private String session;
    private String ip;
    private String mac;
    private int rol_usuario;
    private Boolean habilitado;

    public string Nombre { get => nombre; set => nombre = value; }
    public string Apellido { get => apellido; set => apellido = value; }
    public string Correo { get => correo; set => correo = value; }
    public string Clave { get => clave; set => clave = value; }
    public string Url_imagen { get => url_imagen; set => url_imagen = value; }
    public string Session { get => session; set => session = value; }
    public string Ip { get => ip; set => ip = value; }
    public string Mac { get => mac; set => mac = value; }
    public long Codigo_usuario { get => codigo_usuario; set => codigo_usuario = value; }
    public int Rol_usuario { get => rol_usuario; set => rol_usuario = value; }
    public bool Habilitado { get => habilitado; set => habilitado = value; }
}