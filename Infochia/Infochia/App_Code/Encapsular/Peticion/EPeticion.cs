﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de EPeticion
/// </summary>
public class EPeticion
{
    public EPeticion()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    private string nombre;
    private string correo;
    private string asunto;
    private string mensaje;
    private string session;

    public string Nombre { get => nombre; set => nombre = value; }
    public string Correo { get => correo; set => correo = value; }
    public string Asunto { get => asunto; set => asunto = value; }
    public string Mensaje { get => mensaje; set => mensaje = value; }
    public string Session { get => session; set => session = value; }
}