﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;


/// <summary>
/// Descripción breve de DAOAdministrador
/// </summary>
public class DAOAdministrador
{
    public DAOAdministrador()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public DataTable mostrarUsuariosFiltro(String nombreFiltro, String apellidoFiltro, String correoFiltro)
    {
        DataTable datosUsuarios = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter(" usuario.f_mostrar_datos_usuarios_filtro", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            dataAdapter.SelectCommand.Parameters.Add("_nombre_filtro", NpgsqlDbType.Text).Value = nombreFiltro;
            dataAdapter.SelectCommand.Parameters.Add("_apellido_filtro", NpgsqlDbType.Text).Value = apellidoFiltro;
            dataAdapter.SelectCommand.Parameters.Add("_correo_filtro", NpgsqlDbType.Text).Value = correoFiltro;

            conection.Open();
            dataAdapter.Fill(datosUsuarios);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datosUsuarios;
    }

    public DataTable mostrarUsuarios(DataTable _usuarios)
    {

        return _usuarios;
    }

    public DataTable habilitarDeshabilitarUsuario(long id_usuario, Boolean habilitado, String session)
    {
        DataTable datosUsuarios = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter(" usuario.f_deshabilitar_usuario", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            dataAdapter.SelectCommand.Parameters.Add("_id_usuario", NpgsqlDbType.Bigint).Value = id_usuario;
            dataAdapter.SelectCommand.Parameters.Add("_habilitado", NpgsqlDbType.Boolean).Value = habilitado;
            dataAdapter.SelectCommand.Parameters.Add("_session", NpgsqlDbType.Text).Value = session;

            conection.Open();
            dataAdapter.Fill(datosUsuarios);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datosUsuarios;
    }

    public DataTable mostrarCategoriasAdministrador()
    {
        DataTable datosCategoria = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_mostrar_categorias_administrador", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            conection.Open();
            dataAdapter.Fill(datosCategoria);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datosCategoria; 
    }



    public DataTable modificarCategoria( String descripcion,String ultima_modificacion,String session, int id)
    {
        
        DataTable datosCategoria = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter(" evento.f_modificar_categoria", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            dataAdapter.SelectCommand.Parameters.Add("_id", NpgsqlDbType.Integer).Value = id;
            dataAdapter.SelectCommand.Parameters.Add("_descripcion", NpgsqlDbType.Text).Value = descripcion;
            dataAdapter.SelectCommand.Parameters.Add("_session", NpgsqlDbType.Text).Value = session;

            conection.Open();
            dataAdapter.Fill(datosCategoria);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datosCategoria;
    }

    public DataTable insertarCategoria(String descripcion,  String session)
    {

        DataTable datosCategoria = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter(" evento.f_insertar_categoria", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

       
            dataAdapter.SelectCommand.Parameters.Add("_descripcion", NpgsqlDbType.Text).Value = descripcion;
            dataAdapter.SelectCommand.Parameters.Add("_session", NpgsqlDbType.Text).Value = session;

            conection.Open();
            dataAdapter.Fill(datosCategoria);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datosCategoria;
    }

    public DataTable mostrarDenunciasFiltro(String nombreFiltro, int categoriaFiltro)
    {
        DataTable datosFiltro = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter(" evento.f_mostrar_denuncias_filtro", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;


            dataAdapter.SelectCommand.Parameters.Add("_nombre_filtro", NpgsqlDbType.Text).Value = nombreFiltro;
            dataAdapter.SelectCommand.Parameters.Add("_categoria_filtro", NpgsqlDbType.Integer).Value = categoriaFiltro;
         


            conection.Open();
            dataAdapter.Fill(datosFiltro);

            if (conection != null)
            {
                conection.Close();
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }

        return datosFiltro;
    }


    public DataTable mostrarDenuncias(DataTable _denuncias)
    {

        return _denuncias;
    }

    public DataTable mostrarDetalleDenuncias(long id_evento)
    {
        id_evento = ((long)HttpContext.Current.Session["detalle_denuncia"]);
        DataTable datos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter(" evento.f_mostrar_detalle_denuncia_evento", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;


            dataAdapter.SelectCommand.Parameters.Add("_id_evento", NpgsqlDbType.Bigint).Value = id_evento;
       



            conection.Open();
            dataAdapter.Fill(datos);

            if (conection != null)
            {
                conection.Close();
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }

        return datos;
    }

    public DataTable mostrarUsuarioDenuncia(long id_evento)
    {
        DataTable datos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter(" evento.f_mostrar_usuario_denuncia_evento", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;


            dataAdapter.SelectCommand.Parameters.Add("_id_evento", NpgsqlDbType.Bigint).Value = id_evento;




            conection.Open();
            dataAdapter.Fill(datos);

            if (conection != null)
            {
                conection.Close();
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }

        return datos;
    }


    public DataTable cancelarEventosUsuario(long id_usuario, String session)
    {

        DataTable datosEventos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter(" evento.f_cancelar_eventos_usuario", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;


            dataAdapter.SelectCommand.Parameters.Add("_id_usuario", NpgsqlDbType.Bigint).Value =id_usuario;
            dataAdapter.SelectCommand.Parameters.Add("_session", NpgsqlDbType.Text).Value = session;

            conection.Open();
            dataAdapter.Fill(datosEventos);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datosEventos;
    }


    public DataTable validarExistenciaRazon(long id_evento)
    {

        DataTable datos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_validar_existencia_razon", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;


            dataAdapter.SelectCommand.Parameters.Add("_id_evento", NpgsqlDbType.Bigint).Value = id_evento;
           

            conection.Open();
            dataAdapter.Fill(datos);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datos;
    }

    public void enviarPeticion(EPeticion datosPeticiones)
    {
        DataTable datosPeticion = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter(" usuario.f_insertar_peticion", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;


            dataAdapter.SelectCommand.Parameters.Add("_nombre_usuario", NpgsqlDbType.Text).Value = datosPeticiones.Nombre;
            dataAdapter.SelectCommand.Parameters.Add("_correo", NpgsqlDbType.Text).Value = datosPeticiones.Correo;
            dataAdapter.SelectCommand.Parameters.Add("_asunto", NpgsqlDbType.Text).Value = datosPeticiones.Asunto;
            dataAdapter.SelectCommand.Parameters.Add("_mensaje", NpgsqlDbType.Text).Value = datosPeticiones.Mensaje;
            dataAdapter.SelectCommand.Parameters.Add("_session", NpgsqlDbType.Text).Value = datosPeticiones.Session;

            conection.Open();
            dataAdapter.Fill(datosPeticion);

            if (conection != null)
            {
                conection.Close();
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
    }



    public DataTable mostrarPeticion()
    {
        DataTable datosPeticion = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("usuario.f_listar_peticion", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;



            conection.Open();
            dataAdapter.Fill(datosPeticion);

            if (conection != null)
            {
                conection.Close();
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datosPeticion;

    }

    public void eliminarPeticion(long id)
    {
        DataTable datosPeticion = new DataTable();

        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("usuario.f_eliminar_peticion", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dataAdapter.SelectCommand.Parameters.Add("_id", NpgsqlDbType.Bigint).Value = id;

            conection.Open();
            dataAdapter.Fill(datosPeticion);

            if (conection != null)
            {
                conection.Close();
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
    }

}