﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using NpgsqlTypes;
/// <summary>
/// Descripción breve de DAOReportes
/// </summary>
public class DAOReportes
{
    public DAOReportes()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public DataTable reporteEventosCancelados()
    {
        DataTable datos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("reporte.f_mostrar_eventos_cancelados", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            conection.Open();
            dataAdapter.Fill(datos);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datos;
    }

    public DataTable reporteEventosDenunciados()
    {
        DataTable datos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("reporte.f_mostrar_eventos_denunciados", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            conection.Open();
            dataAdapter.Fill(datos);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datos;
    }

    public DataTable mostrarEventosActivos( String fechaFiltro)
    {
        DataTable datosFiltro = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter(" reporte.f_mostrar_eventos_activos", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dataAdapter.SelectCommand.Parameters.Add("_fecha_inicio_filtro", NpgsqlDbType.Date).Value = DateTime.Parse(fechaFiltro).Date;


            conection.Open();
            dataAdapter.Fill(datosFiltro);

            if (conection != null)
            {
                conection.Close();
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }

        return datosFiltro;
    }
}