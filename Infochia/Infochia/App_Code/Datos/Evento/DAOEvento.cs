﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de DAOEvento
/// </summary>
public class DAOEvento
{
    public DAOEvento()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }
    public DataTable mostrarCategoria()
    {
        DataTable datosCategoria = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_mostrar_categorias", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            conection.Open();
            dataAdapter.Fill(datosCategoria);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datosCategoria;
    }



    
    public void insertarEvento(EEvento evento)
    {
        DataTable datosRegistroEvento= new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_insertar_evento", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            dataAdapter.SelectCommand.Parameters.Add("_nombre", NpgsqlDbType.Text).Value = evento.Nombre;
            dataAdapter.SelectCommand.Parameters.Add("_descripcion", NpgsqlDbType.Text).Value = evento.Descripcion;
            dataAdapter.SelectCommand.Parameters.Add("_fecha_inicio", NpgsqlDbType.Date).Value =DateTime.Parse(evento.FechaInicio).Date;
            dataAdapter.SelectCommand.Parameters.Add("_fecha_fin", NpgsqlDbType.Date).Value = DateTime.Parse(evento.FechaFin).Date;
             dataAdapter.SelectCommand.Parameters.Add("_hora", NpgsqlDbType.Time).Value = TimeSpan.Parse(evento.Hora);
            dataAdapter.SelectCommand.Parameters.Add("_imagen", NpgsqlDbType.Text).Value = evento.Imagen;
            dataAdapter.SelectCommand.Parameters.Add("_id_usuario", NpgsqlDbType.Bigint).Value = evento.IdUsuario;
            dataAdapter.SelectCommand.Parameters.Add("_id_categoria", NpgsqlDbType.Integer).Value = evento.IdCategoria;
            dataAdapter.SelectCommand.Parameters.Add("_session", NpgsqlDbType.Text).Value = evento.Session;
            dataAdapter.SelectCommand.Parameters.Add("_lugar", NpgsqlDbType.Text).Value = evento.Lugar;
            conection.Open();
            dataAdapter.Fill(datosRegistroEvento);

            if (conection != null)
            {
                conection.Close();
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
    }

    public DataTable mostrarMisEventos(long usuario)
    {

            usuario = (long)HttpContext.Current.Session["codigo_usuario"];


        DataTable datos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_mostrar_mis_eventos", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dataAdapter.SelectCommand.Parameters.Add("_id_usuario", NpgsqlDbType.Bigint).Value = usuario;

            conection.Open();
            dataAdapter.Fill(datos);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datos;
    }

    public DataTable mostrarEstado()
    {
        DataTable datosEstados = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_mostrar_estados", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            conection.Open();
            dataAdapter.Fill(datosEstados);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datosEstados;
    }


    public DataTable mostrarEventoId(long id)
    {
       
        DataTable datos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_mostrar_evento_id", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dataAdapter.SelectCommand.Parameters.Add("_id_evento", NpgsqlDbType.Bigint).Value =id;

            conection.Open();
            dataAdapter.Fill(datos);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datos;
    }


    public Boolean validarExistenciaImagen(String urlImagen)
    {
        DataTable urlIDatos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_buscar_existencia_imagen_evento", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dataAdapter.SelectCommand.Parameters.Add("_url_imagen", NpgsqlDbType.Text).Value = urlImagen;

            conection.Open();
            dataAdapter.Fill(urlIDatos);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }

        if (urlIDatos.Rows.Count == 0)
        {

            return false;
        }
        else
        {
            if (urlImagen.Equals(urlIDatos.Rows[0]["imagen"]))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }
    public void EditarEvento(EEvento evento)
    {
        DataTable datosModificarEvento = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_editar_evento", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            dataAdapter.SelectCommand.Parameters.Add("_id", NpgsqlDbType.Bigint).Value = evento.IdEvento;
            dataAdapter.SelectCommand.Parameters.Add("_nombre", NpgsqlDbType.Text).Value = evento.Nombre;
            dataAdapter.SelectCommand.Parameters.Add("_descripcion", NpgsqlDbType.Text).Value = evento.Descripcion;
            dataAdapter.SelectCommand.Parameters.Add("_fecha_inicio", NpgsqlDbType.Date).Value = DateTime.Parse(evento.FechaInicio).Date;
            dataAdapter.SelectCommand.Parameters.Add("_fecha_fin", NpgsqlDbType.Date).Value = DateTime.Parse(evento.FechaFin).Date;
            dataAdapter.SelectCommand.Parameters.Add("_hora", NpgsqlDbType.Time).Value = TimeSpan.Parse(evento.Hora);
            dataAdapter.SelectCommand.Parameters.Add("_imagen", NpgsqlDbType.Text).Value = evento.Imagen;
            dataAdapter.SelectCommand.Parameters.Add("_id_categoria", NpgsqlDbType.Integer).Value = evento.IdCategoria;
            dataAdapter.SelectCommand.Parameters.Add("_id_estado", NpgsqlDbType.Integer).Value = evento.IdEstado;
            dataAdapter.SelectCommand.Parameters.Add("_session", NpgsqlDbType.Text).Value = evento.Session;
            dataAdapter.SelectCommand.Parameters.Add("_lugar", NpgsqlDbType.Text).Value = evento.Lugar;

            conection.Open();
            dataAdapter.Fill(datosModificarEvento);

            if (conection != null)
            {
                conection.Close();
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
    }

    public void EliminarEvento(EEvento evento)
    {
        DataTable datosModificarEvento = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_editar_estado_evento", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            dataAdapter.SelectCommand.Parameters.Add("_id_evento", NpgsqlDbType.Bigint).Value = evento.IdEvento;
            dataAdapter.SelectCommand.Parameters.Add("_session", NpgsqlDbType.Text).Value = evento.Session;

            conection.Open();
            dataAdapter.Fill(datosModificarEvento);

            if (conection != null)
            {
                conection.Close();
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
    }

    public DataTable mostrarEventosFiltro(String nombreFiltro,int categoriaFiltro,String fechaFiltro)
    {
        DataTable datosFiltro = new DataTable( );
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter(" evento.f_mostrar_eventos_filtro", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

         
            dataAdapter.SelectCommand.Parameters.Add("_nombre_filtro", NpgsqlDbType.Text).Value = nombreFiltro;
            dataAdapter.SelectCommand.Parameters.Add("_categoria_filtro", NpgsqlDbType.Integer).Value = categoriaFiltro;
            dataAdapter.SelectCommand.Parameters.Add("_fecha_inicio_filtro", NpgsqlDbType.Date).Value = DateTime.Parse(fechaFiltro).Date;
     

            conection.Open();
            dataAdapter.Fill(datosFiltro);

            if (conection != null)
            {
                conection.Close();
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }

        return datosFiltro;
    }


    public DataTable mostrarEventos(DataTable _eventos)
    {

        return _eventos;
    }

    public void insertarRazonEvento(ECancelarEvento evento)
    {
        DataTable datosRegistroEvento = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_insertar_cancelacion_evento", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            dataAdapter.SelectCommand.Parameters.Add("_razon", NpgsqlDbType.Text).Value = evento.Razon;
            dataAdapter.SelectCommand.Parameters.Add("_id_evento", NpgsqlDbType.Bigint).Value = evento.IdEvento;
            dataAdapter.SelectCommand.Parameters.Add("_session", NpgsqlDbType.Text).Value = evento.Session;

            conection.Open();
            dataAdapter.Fill(datosRegistroEvento);

            if (conection != null)
            {
                conection.Close();
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
    }

    public DataTable mostrarEventosFinalizados()
    {

        DataTable datos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_mostrar_eventos_finalizados", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
      

            conection.Open();
            dataAdapter.Fill(datos);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datos;
    }

    public DataTable mostrarContacto(long id)
    {

        DataTable datos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_mostrar_contacto_evento", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dataAdapter.SelectCommand.Parameters.Add("_id_usuario", NpgsqlDbType.Bigint).Value = id;

            conection.Open();
            dataAdapter.Fill(datos);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datos;
    }

    public DataTable mostrarEventoCancelado(long id_evento)
    {

        DataTable datos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_mostrar_evento_cancelado", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dataAdapter.SelectCommand.Parameters.Add("_id_evento", NpgsqlDbType.Bigint).Value = id_evento;

            conection.Open();
            dataAdapter.Fill(datos);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datos;
    }


    public DataTable validarInteresUsuario(long id_usuario,long id_evento)
    {

        DataTable datos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_validar_interes_usuario", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dataAdapter.SelectCommand.Parameters.Add("_id_usuario", NpgsqlDbType.Bigint).Value = id_usuario;
            dataAdapter.SelectCommand.Parameters.Add("_id_evento", NpgsqlDbType.Bigint).Value = id_evento;

            conection.Open();
            dataAdapter.Fill(datos);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datos;
    }

    public DataTable insertarInteresUsuario(long id_usuario,long id_evento,string session)
    {

        DataTable datos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_insertar_interes_evento", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dataAdapter.SelectCommand.Parameters.Add("_id_usuario", NpgsqlDbType.Bigint).Value = id_usuario;
            dataAdapter.SelectCommand.Parameters.Add("_id_evento", NpgsqlDbType.Bigint).Value = id_evento;
            dataAdapter.SelectCommand.Parameters.Add("_session", NpgsqlDbType.Text).Value =session;
            conection.Open();
            dataAdapter.Fill(datos);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datos;
    }

    public int cantidadInteresadosEvento(long id_evento)
    {
        DataTable datosEvento = new DataTable();
        int conteo;
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_contar_interesa", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dataAdapter.SelectCommand.Parameters.Add("_id_evento", NpgsqlDbType.Bigint).Value = id_evento;


            conection.Open();
            dataAdapter.Fill(datosEvento);

            if (conection != null)
            {
                conection.Close();
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }

        conteo = int.Parse(datosEvento.Rows[0]["cantidad_interesados_evento"].ToString()); ;


        return conteo;
    }


    public DataTable actualizarCantidadInteresados(long id_evento,int cantidad_interesados,string session)
    {

        DataTable datos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_editar_interesados_evento", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dataAdapter.SelectCommand.Parameters.Add("_id_evento", NpgsqlDbType.Bigint).Value = id_evento;
            dataAdapter.SelectCommand.Parameters.Add("_cantidad_interesados", NpgsqlDbType.Integer).Value =cantidad_interesados;
            dataAdapter.SelectCommand.Parameters.Add("_session", NpgsqlDbType.Text).Value = session;
            conection.Open();
            dataAdapter.Fill(datos);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datos;
    }

    public DataTable actualizarEstadoInteresados(long id_usuario, long id_evento,Boolean habilitado, string session)
    {

        DataTable datos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_editar_estado_interesados_evento", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dataAdapter.SelectCommand.Parameters.Add("_id_usuario", NpgsqlDbType.Bigint).Value = id_usuario;
            dataAdapter.SelectCommand.Parameters.Add("_id_evento", NpgsqlDbType.Bigint).Value = id_evento;
            dataAdapter.SelectCommand.Parameters.Add("_habilitado", NpgsqlDbType.Boolean).Value = habilitado;
            dataAdapter.SelectCommand.Parameters.Add("_session", NpgsqlDbType.Text).Value = session;
            conection.Open();
            dataAdapter.Fill(datos);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datos;
    }
    ///denuncias

    public DataTable validarDenunciaUsuario(long id_usuario, long id_evento)
    {

        DataTable datos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_validar_denuncia_usuario", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dataAdapter.SelectCommand.Parameters.Add("_id_usuario", NpgsqlDbType.Bigint).Value = id_usuario;
            dataAdapter.SelectCommand.Parameters.Add("_id_evento", NpgsqlDbType.Bigint).Value = id_evento;

            conection.Open();
            dataAdapter.Fill(datos);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datos;
    }

    public DataTable insertarDenunciaUsuario(string descripcion, long id_evento, long id_usuario,  string session)
    {

        DataTable datos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_insertar_denuncia_evento", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dataAdapter.SelectCommand.Parameters.Add("_descripcion", NpgsqlDbType.Text).Value = descripcion;
            dataAdapter.SelectCommand.Parameters.Add("_id_evento", NpgsqlDbType.Bigint).Value = id_evento;
            dataAdapter.SelectCommand.Parameters.Add("_id_usuario", NpgsqlDbType.Bigint).Value = id_usuario;
            dataAdapter.SelectCommand.Parameters.Add("_session", NpgsqlDbType.Text).Value = session;
            conection.Open();
            dataAdapter.Fill(datos);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datos;
    }

    public int cantidadDenunciasEvento(long id_evento)
    {
        DataTable datosEvento = new DataTable();
        int conteo;
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter(" evento.f_contar_denuncias", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dataAdapter.SelectCommand.Parameters.Add("_id_evento", NpgsqlDbType.Bigint).Value = id_evento;


            conection.Open();
            dataAdapter.Fill(datosEvento);

            if (conection != null)
            {
                conection.Close();
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }

        conteo = int.Parse(datosEvento.Rows[0]["cantidad_denuncias_evento"].ToString()); ;


        return conteo;
    }

    public DataTable actualizarCantidadDenuncias(long id_evento, int cantidad_denuncias, string session)
    {

        DataTable datos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_editar_denuncias_evento", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dataAdapter.SelectCommand.Parameters.Add("_id_evento", NpgsqlDbType.Bigint).Value = id_evento;
            dataAdapter.SelectCommand.Parameters.Add("_cantidad_denuncias", NpgsqlDbType.Integer).Value = cantidad_denuncias;
            dataAdapter.SelectCommand.Parameters.Add("_session", NpgsqlDbType.Text).Value = session;
            conection.Open();
            dataAdapter.Fill(datos);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datos;
    }


    public DataTable mostrarIntereses(long usuario)
    {
        usuario = (long)HttpContext.Current.Session["codigo_usuario"];
        DataTable datos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("evento.f_mostrar_me_interesa_eventos", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dataAdapter.SelectCommand.Parameters.Add("_id_usuario", NpgsqlDbType.Bigint).Value = usuario;

            conection.Open();
            dataAdapter.Fill(datos);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
        return datos;
    }
}