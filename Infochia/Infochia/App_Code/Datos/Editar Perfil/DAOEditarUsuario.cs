﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using Npgsql;
using NpgsqlTypes;

/// <summary>
/// Descripción breve de DAOEditarUsuario
/// </summary>
public class DAOEditarUsuario
{
    public DAOEditarUsuario()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public void editarUsuario(EUsuario datosActualizar)
    {
        DataTable datosRegistroCompleto = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("usuario.f_editar_usuario", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;


            dataAdapter.SelectCommand.Parameters.Add("_nombre", NpgsqlDbType.Text).Value = datosActualizar.Nombre;
            dataAdapter.SelectCommand.Parameters.Add("_apellido", NpgsqlDbType.Text).Value = datosActualizar.Apellido;
            dataAdapter.SelectCommand.Parameters.Add("_correo", NpgsqlDbType.Text).Value = datosActualizar.Correo;
            dataAdapter.SelectCommand.Parameters.Add("_clave", NpgsqlDbType.Text).Value = datosActualizar.Clave;
            dataAdapter.SelectCommand.Parameters.Add("_url_imagen", NpgsqlDbType.Text).Value = datosActualizar.Url_imagen;
            dataAdapter.SelectCommand.Parameters.Add("_session", NpgsqlDbType.Text).Value = datosActualizar.Session;

            conection.Open();
            dataAdapter.Fill(datosRegistroCompleto);

            if (conection != null)
            {
                conection.Close();
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }
    }

    public Boolean validarExistenciaImagen(String urlImagen)
    {
        DataTable urlIDatos = new DataTable();
        NpgsqlConnection conection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["Conexion_BD"].ConnectionString);

        try
        {
            NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter("usuario.f_buscar_existencia_imagen", conection);
            dataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dataAdapter.SelectCommand.Parameters.Add("_url_imagen", NpgsqlDbType.Text).Value = urlImagen;

            conection.Open();
            dataAdapter.Fill(urlIDatos);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
        finally
        {
            if (conection != null)
            {
                conection.Close();
            }
        }

        if (urlIDatos.Rows.Count == 0)
        {

            return false;
        }
        else
        {
            if (urlImagen.Equals(urlIDatos.Rows[0]["url_imagen"]))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }
}