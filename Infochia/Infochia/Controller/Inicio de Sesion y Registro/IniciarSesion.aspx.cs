﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class Views_Inicio_de_Sesion_y_Registro_IniciarSesion : System.Web.UI.Page
{

    static int conteo = 25;
    static int intentos=0;
   

    protected void Page_Load(object sender, EventArgs e)
    {
        LA_Mensaje.Text = " ";
        
    

            if (HttpContext.Current.Session["conteo_recarga"] == null)
            {
                  conteo = 25;
             }
            else
            {  
                if (Timer1.Enabled == false)
                {
                

                conteo = (int)(HttpContext.Current.Session["conteo_recarga"]);
                LA_Mensaje.Text = " ";
                Timer1.Enabled = true;
          
          
                }

            }

    }

    protected void BTN_Inicio_Sesion_Click(object sender, EventArgs e)
    {
        

         Session["usuario_session"] = Session.SessionID;

                EUsuario iniciarsesion = new EUsuario();
               String claveEncriptada = new EncriptarDatos().encriptar(JsonConvert.SerializeObject(TX_Clave.Text));

                iniciarsesion.Correo = TX_Correo.Text;
                iniciarsesion.Clave = claveEncriptada;

                DataTable datosTabla = new DAOUsuario().validacionInicioSesion( iniciarsesion);

        if (datosTabla.Rows.Count != 0)
        {
            EUsuario datosUsuario = new EUsuario();
            MAC datosConexion = new MAC();

            DataTable datosUsuarioTabla = new DAOUsuario().datosUsuario(datosTabla.Rows[0]["correo_usuario"].ToString());

            datosUsuario.Codigo_usuario = long.Parse(datosUsuarioTabla.Rows[0]["id"].ToString());
            datosUsuario.Nombre= datosUsuarioTabla.Rows[0]["nombre"].ToString();
            datosUsuario.Apellido= datosUsuarioTabla.Rows[0]["apellido"].ToString();
            datosUsuario.Correo = datosUsuarioTabla.Rows[0]["correo"].ToString();
            datosUsuario.Url_imagen= datosUsuarioTabla.Rows[0]["url_imagen"].ToString();
            datosUsuario.Rol_usuario = int.Parse(datosUsuarioTabla.Rows[0]["id_rol"].ToString());
            datosUsuario.Habilitado = Boolean.Parse(datosUsuarioTabla.Rows[0]["habilitado"].ToString());
            datosUsuario.Session = Session.SessionID;
            datosUsuario.Ip = datosConexion.ip();
            datosUsuario.Mac = datosConexion.mac();
        

            if (datosUsuario.Habilitado.Equals(true)) {

                Session["usuario_datos"] = datosUsuario;
                Session["correo_usuario"] = datosUsuario.Correo;
                Session["codigo_usuario"] = datosUsuario.Codigo_usuario;
            

                  new DAOUsuario().guardarSesion(datosUsuario);


                Response.Redirect("~\\Views\\Principal\\Default.aspx");

                    
             }else{

                LA_Mensaje.Visible = true;
                LA_Mensaje.Text = "El Usuario Esta Deshabilitado";
               
            }
        
        }
        else
        {

            LA_Mensaje.Visible = true;
            LA_Mensaje.Text = "* El Correo o la Contraseña ingresados son incorrectos, intente nuevamente";
            intentos++;

            if (intentos >= 3) {

                Timer1.Enabled = true;
                intentos = 0;
            }

        }


        TX_Correo.Text = " ";
    }

    protected void LB_Recuperar_Click(object sender, EventArgs e)
    {
        Response.Redirect("~\\Views\\Inicio de Sesion y Registro\\GenerarToken.aspx");
    }

   

    protected void Timer1_Tick(object sender, EventArgs e)
    {

        contar();


    }

    protected void contar() {

        conteo--;
        Session["conteo_recarga"] = conteo;
        LA_Mensaje.Text = "* Ha superado el numero de intentos permitido para iniciar sesion, intente en " + conteo.ToString() + "  segundos.";
        BTN_Inicio_Sesion.Enabled = false;



        if (conteo == 0)
        {
            LA_Mensaje.Visible = false;
            LA_Mensaje.Text = " ";
            BTN_Inicio_Sesion.Enabled = true;
            Timer1.Enabled = false;
            Session["conteo_recarga"] = null;
          
        }
    }


    protected void LB_Regresar_Click(object sender, EventArgs e)
    {
        Response.Redirect("~\\Views\\Principal\\Default.aspx");
    }
}