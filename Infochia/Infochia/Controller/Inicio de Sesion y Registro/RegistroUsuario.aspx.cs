﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Drawing;


public partial class Views_Inicio_de_Sesion_y_Registro_RegistroUsuario : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LA_Mensaje.Text = " ";
      
    }

    static int conteo = 5;

    protected void BTN_Registro_Click(object sender, EventArgs e)
    {

        if (CH_Politica.Checked == true)
        {


            Session["usuario_session"] = Session.SessionID;

            EUsuario registrarusuario = new EUsuario();
            FileUpload FU_IDocumento = FU_Imagen_Usuario;



            if (FU_IDocumento.PostedFile.FileName != "" && !(System.IO.Path.GetExtension(FU_IDocumento.PostedFile.FileName).Equals(".jpg") || System.IO.Path.GetExtension(FU_IDocumento.PostedFile.FileName).Equals(".jpng") || System.IO.Path.GetExtension(FU_IDocumento.PostedFile.FileName).Equals(".png") || System.IO.Path.GetExtension(FU_IDocumento.PostedFile.FileName).Equals(".bmp")))
            {
                LA_Mensaje.Visible = true;
                LA_Mensaje_Info.Visible = false;
                LA_Mensaje.ForeColor = Color.Red;
                LA_Mensaje.Text = "*Formato no valido, solo adjunte imagenes por favor";

                return;
            }
            else
            {
                if (FU_IDocumento.PostedFile.ContentLength > 5000000)
                {
                    LA_Mensaje.Visible = true;
                    LA_Mensaje_Info.Visible = false;
                    LA_Mensaje.ForeColor = Color.Red;
                    LA_Mensaje.Text = "* La imagen adjuntada supera el limite permitido de tamaño que son 5 MB";
                    return;
                }
                else
                {
                    registrarusuario.Url_imagen = "~\\Imagenes\\Usuarios\\" + System.IO.Path.GetFileName(FU_IDocumento.PostedFile.FileName);
                    FU_IDocumento.PostedFile.SaveAs(Server.MapPath(registrarusuario.Url_imagen));

                }


            }

            String claveEncriptada = new EncriptarDatos().encriptar(JsonConvert.SerializeObject(TX_Clave.Text));

            registrarusuario.Nombre = CultureInfo.CurrentCulture.TextInfo.ToTitleCase((TX_Nombre.Text).ToLower());
            registrarusuario.Apellido = CultureInfo.CurrentCulture.TextInfo.ToTitleCase((TX_Apellido.Text).ToLower());
            registrarusuario.Correo = (TX_Correo.Text).ToLower();
            registrarusuario.Clave = claveEncriptada;
            registrarusuario.Session = (String)Session["usuario_session"];



            if (!new DAOUsuario().validarExistenciaCorreo(registrarusuario.Correo))
            {

                 new DAOUsuario().registro(registrarusuario);
                Timer1.Enabled = true;


            }
            else
            {
                LA_Mensaje.Visible = true;
                LA_Mensaje_Info.Visible = false;
                LA_Mensaje.ForeColor = Color.Red;
                LA_Mensaje.Text = "*El correo que desea registrar ya se encuentra en el sistema, por favor intente con otro ";

            }

            TX_Nombre.Text = " ";
            TX_Apellido.Text = " ";
            TX_Correo.Text = " ";



        }
        else
        {
            LA_Politica.ForeColor = Color.Red;
            LA_Politica.Text = "* Debe aceptar las politicas de privacidad";
         
        }

        }


    protected void Timer1_Tick(object sender, EventArgs e)
    {
        conteo--;
        LA_Mensaje.Visible = false;
        LA_Mensaje_Info.Visible = true;
        LA_Mensaje_Info.ForeColor = Color.Green;
        LA_Mensaje_Info.Text = "Se ha Registrado correctamente, la pagina se redireccionara al inicio de sesion en " + conteo.ToString() + "  segundos.";

        if (conteo==0) {

            Response.Redirect("~\\Views\\Inicio de Sesion y Registro\\IniciarSesion.aspx");

        }

    }

    protected void LB_Regresar_Click(object sender, EventArgs e)
    {
        Response.Redirect("~\\Views\\Principal\\Default.aspx");
    }

  
}