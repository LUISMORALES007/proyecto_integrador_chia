﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Views_Evento_BuscarEvento : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable datosCategoria = null;
        datosCategoria = new DAOEvento().mostrarCategoria();
        DataTable datosEventosFiltrados = new DAOEvento().mostrarEventosFiltro("", -1, "2019 - 03 - 31");
       

        if (!IsPostBack)
        {
            DataTable fff = new DAOEvento().mostrarEventos(datosEventosFiltrados);
            Session["datos_evento_filtrar"] = fff;
            GV_Eventos.DataBind();

            RangeValidator1.MinimumValue = DateTime.Now.Date.ToString("dd-MM-yyyy");
            RangeValidator1.MaximumValue = DateTime.Now.Date.AddYears(1).ToString("dd-MM-yyyy");

            DDL_Buscar_Categoria.Items.Clear();
            DDL_Buscar_Categoria.Items.Add("--Seleccionar--");
            DDL_Buscar_Categoria.Items.FindByText("--Seleccionar--").Value = "none";
            for (int i = 0; i < datosCategoria.Rows.Count; i++)
            {
                DDL_Buscar_Categoria.Items.Add(datosCategoria.Rows[i]["descripcion"].ToString());
            }
            DDL_Buscar_Categoria.DataBind();

        }
    }

    protected void IB_Regresar_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~\\Views\\Principal\\Default.aspx");
    }

    protected void B_Buscar_Evento_Click(object sender, EventArgs e)
    {
        String nombreEvento = TX_Buscar_Nombre.Text;
        int categoriaEvento = -1;
       String fechaEvento = "2019-03-31";

        DataTable datosEventosFiltrados = new DataTable();
        DataTable datosGridView = new DataTable();

        if (String.IsNullOrWhiteSpace(TX_Buscar_Nombre.Text))
        {
            nombreEvento = "";
        }

        if (DDL_Buscar_Categoria.SelectedIndex!=0) {

            categoriaEvento = DDL_Buscar_Categoria.SelectedIndex;

        }

        if (!String.IsNullOrWhiteSpace(TX_Buscar_Fecha_Inicio.Text))
        {
            fechaEvento = TX_Buscar_Fecha_Inicio.Text;
        }



        datosEventosFiltrados = new DAOEvento().mostrarEventosFiltro(nombreEvento, categoriaEvento, fechaEvento);
         datosGridView = new DAOEvento().mostrarEventos(datosEventosFiltrados);
        Session["datos_evento_filtrar"] = datosGridView;
        GV_Eventos.DataBind();


    }

    protected void GV_Eventos_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType != DataControlRowType.DataRow)
        {
            return;
        }
    

        DataTable datosAsignados = new DataTable();
        Button button = (Button)e.Row.FindControl("BTN_Info_Evento");
        Label labelEstado = (Label)e.Row.FindControl("label7");


        datosAsignados = (new DAOEvento().mostrarEventosFinalizados());

        button.Enabled = true;
        button.Visible = true;

        for (int contador1 = 0; contador1 < datosAsignados.Rows.Count; contador1++)
        {
            if ((datosAsignados.Rows[contador1]["descripcion_estado"].ToString()).Equals(labelEstado.Text))
            {
                button.Enabled = false;
                button.Visible = false;
            }
        }
        

    }

    protected void GV_Eventos_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        int rowIndex = int.Parse(e.CommandArgument.ToString());

        if (e.CommandName.Equals("info"))
        {
            EEvento detalleEventos = new EEvento();

           detalleEventos.IdUsuario = (long)this.GV_Eventos.DataKeys[rowIndex]["id_usuario"];
            detalleEventos.Nombre = Convert.ToString(((Label)GV_Eventos.Controls[0].Controls[rowIndex + 1].FindControl("label1")).Text);
            detalleEventos.Descripcion = Convert.ToString(this.GV_Eventos.DataKeys[rowIndex]["descripcion"]);
            detalleEventos.CantidadInteresados= (int)this.GV_Eventos.DataKeys[rowIndex]["cantidad_interesados"];
            detalleEventos.IdEstado = (int)this.GV_Eventos.DataKeys[rowIndex]["id_estado"];
            detalleEventos.IdEvento = (long)this.GV_Eventos.DataKeys[rowIndex]["id"];
            detalleEventos.FechaInicio = Convert.ToString(((Label)GV_Eventos.Controls[0].Controls[rowIndex + 1].FindControl("label3")).Text);
            detalleEventos.FechaFin= Convert.ToString(this.GV_Eventos.DataKeys[rowIndex]["fecha_fin"]);
            detalleEventos.Hora= Convert.ToString(((Label)GV_Eventos.Controls[0].Controls[rowIndex + 1].FindControl("label5")).Text);
            detalleEventos.Imagen = Convert.ToString(((Image)GV_Eventos.Controls[0].Controls[rowIndex + 1].FindControl("Image1")).ImageUrl);
            detalleEventos.Lugar= Convert.ToString(((Label)GV_Eventos.Controls[0].Controls[rowIndex + 1].FindControl("label2")).Text);

            Session["detalle_eventos"] = detalleEventos;

            Response.Redirect("~\\Views\\Evento\\VerDetalleEvento.aspx");
        }



    }
}