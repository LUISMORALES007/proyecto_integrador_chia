﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Views_Evento_MisEventos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty((string)HttpContext.Current.Session["correo_usuario"] as string))
        {     
            Response.Redirect("~\\Views\\Principal\\Default.aspx");
        }

        if (((EUsuario)Session["usuario_datos"]).Rol_usuario != 2)
        {

            Response.Redirect("~\\Views\\Principal\\Default.aspx");
        }

    }



    protected void GV_Mis_Eventos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int rowIndex = int.Parse(e.CommandArgument.ToString());

        if (e.CommandName.Equals("ver"))
        {
            EEvento misEventos = new EEvento();
          
           misEventos.IdEvento = (long)this.GV_Mis_Eventos.DataKeys[rowIndex]["id"];
          misEventos.Imagen= Convert.ToString(((Image)GV_Mis_Eventos.Controls[0].Controls[rowIndex + 1].FindControl("Image1")).ImageUrl);
     

            Session["mis_eventos"] = misEventos;

            Response.Redirect("~\\Views\\Evento\\VerEditarEvento.aspx");
        }
     }



  
}