﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Newtonsoft.Json;
using System.Drawing;

public partial class Views_Evento_VerEditarEvento : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (string.IsNullOrEmpty((string)HttpContext.Current.Session["correo_usuario"] as string))
        {
            Response.Redirect("~\\Views\\Principal\\Default.aspx");
        }

        if (((EUsuario)Session["usuario_datos"]).Rol_usuario != 2)
        {

            Response.Redirect("~\\Views\\Principal\\Default.aspx");
        }





        DataTable datosCategoria = null;
        DataTable datosEstado= null;
        DataTable datosEvento = new DataTable();
        datosCategoria = new DAOEvento().mostrarCategoria();
        datosEstado = new DAOEvento().mostrarEstado();

        if (!IsPostBack)
        {
            datosEvento = new DAOEvento().mostrarEventoId(((EEvento)HttpContext.Current.Session["mis_eventos"]).IdEvento);
            RAV_Fecha_Inicio.MinimumValue = DateTime.Now.Date.ToString("dd-MM-yyyy");
            RAV_Fecha_Inicio.MaximumValue = DateTime.Now.Date.AddYears(1).ToString("dd-MM-yyyy");


            DDL_Estado.Items.Clear();
            DDL_Estado.Items.Add("--Seleccionar--");
            DDL_Estado.Items.FindByText("--Seleccionar--").Value = "none";
            for (int i = 0; i < datosEstado.Rows.Count; i++)
            {
                DDL_Estado.Items.Add(datosEstado.Rows[i]["descripcion"].ToString());
            }
            DDL_Estado.DataBind();


            DDL_Categoria.Items.Clear();
            DDL_Categoria.Items.Add("--Seleccionar--");
            DDL_Categoria.Items.FindByText("--Seleccionar--").Value = "none";
            for (int i = 0; i < datosCategoria.Rows.Count; i++)
            {
                DDL_Categoria.Items.Add(datosCategoria.Rows[i]["descripcion"].ToString());
            }
            DDL_Categoria.DataBind();


            DateTime fi = DateTime.Parse(datosEvento.Rows[0]["fecha_inicio"].ToString()).Date;
           string fecha_inicio = Convert.ToString(fi);
           string fecha_inicio_dia = fecha_inicio.Substring(0, 2);
            string fecha_inicio_mes = fecha_inicio.Substring(3, 2);
            string fecha_inicio_anio = fecha_inicio.Substring(6, 4);
            fecha_inicio = fecha_inicio_anio +"-"+ fecha_inicio_mes+"-"+ fecha_inicio_dia;

            DateTime ff = DateTime.Parse(datosEvento.Rows[0]["fecha_fin"].ToString()).Date;
            string fecha_fin = Convert.ToString(ff);
            string fecha_fin_dia =fecha_fin.Substring(0, 2);
            string fecha_fin_mes = fecha_fin.Substring(3, 2);
            string fecha_fin_anio = fecha_fin.Substring(6, 4);
            fecha_fin = fecha_fin_anio + "-" + fecha_fin_mes + "-" + fecha_fin_dia;

            DDL_Estado.SelectedIndex = int.Parse(datosEvento.Rows[0]["id_estado"].ToString());
            TX_Nombre.Text = datosEvento.Rows[0]["nombre"].ToString();
            TX_Descripcion.Text = datosEvento.Rows[0]["descripcion"].ToString();
            DDL_Categoria.SelectedIndex = int.Parse(datosEvento.Rows[0]["id_categoria"].ToString());
            TX_Fecha_Inicio.Text = fecha_inicio;
            TX_Fecha_Fin.Text= fecha_fin;
            TX_Hora.Text= datosEvento.Rows[0]["hora"].ToString();
            IM_Imagen.ImageUrl= datosEvento.Rows[0]["imagen"].ToString();
            TX_Lugar.Text= datosEvento.Rows[0]["lugar"].ToString();

            Session["DDL_Estado"]= int.Parse(datosEvento.Rows[0]["id_estado"].ToString());
            Session["DDL_Categoria"] = int.Parse(datosEvento.Rows[0]["id_categoria"].ToString());

            if (DDL_Estado.SelectedIndex==3)
            {
                DDL_Estado.Enabled = false;
                TX_Nombre.Enabled = false;
                TX_Descripcion.Enabled = false;
                DDL_Categoria.Enabled = false;
                TX_Fecha_Inicio.Enabled = false;
                TX_Fecha_Fin.Enabled = false;
                TX_Hora.Enabled = false;
                FU_Url_Evento.Enabled = false;
                TX_Lugar.Enabled = false;
                B_Eliminar_Evento.Visible = false;
                B_Modificar_Evento.Visible = false;
                Panel2.Visible = false;
            }
        }

        validarComponentesFecha();
        LA_Mensaje.Text = " ";
        LA_Mensaje_Validar.Text = " ";
    }


    protected void B_Modificar_Evento_Click(object sender, EventArgs e)
    {


        DateTime fechaInicial = Convert.ToDateTime(TX_Fecha_Inicio.Text).Date;
        DateTime FechAc = DateTime.Now.Date.ToLocalTime();
        DateTime horaText = Convert.ToDateTime(TX_Hora.Text);
        DateTime horAct = DateTime.Now;
        int ht = horaText.Hour;
        int ha = horAct.Hour;
        int mt = horaText.Minute;
        int ma = horAct.Minute;

        if (fechaInicial > FechAc)
        {

            funcionEditar();

        }
        else if (ht < ha)
        {

            if (DDL_Estado.SelectedIndex != 1)
            {

                funcionEditar();

            }
            else
            {

                LA_Hora.Text = "*Debe ingresar una hora mayor a la hora actual";
            }

        }
        else if ((ht == ha) && (mt < ma))
        {
            if (DDL_Estado.SelectedIndex != 1) {

                funcionEditar();

            }
            else { 

            LA_Hora.Text = "*Debe ingresar una hora mayor a la hora actual";
            }
        }
        else
        {

            funcionEditar();

        }

    }

    protected void funcionEditar()
    {

        DAOEvento ModificarEvento = new DAOEvento();
        EEvento datosEvento = new EEvento();

        FileUpload FU_IDocumento = FU_Url_Evento;
        ClientScriptManager cm = this.ClientScript;

        long numeroImagen = 0;

        if (FU_IDocumento.PostedFile.FileName != "")
        {
            String extensionImagen = System.IO.Path.GetExtension(FU_IDocumento.PostedFile.FileName).ToLower();
            if (!(extensionImagen.Equals(".jpg") || extensionImagen.Equals(".jpng") || extensionImagen.Equals(".png") || extensionImagen.Equals(".bmp") || extensionImagen.Equals(".jpeg")))
            {
                LA_Mensaje.ForeColor = Color.Red;
                LA_Mensaje.Text = "*Formato no valido, solo adjunte imagenes por favor";
                return;
            }
            else
            {
                if (FU_IDocumento.PostedFile.ContentLength > 5000000)
                {
                    LA_Mensaje.ForeColor = Color.Red;
                    LA_Mensaje.Text = "* La imagen adjuntada supera el limite permitido de tamaño que son 5 MB";
                    return;
                }
                else {
                    datosEvento.Imagen = "~\\Imagenes\\Eventos\\" + numeroImagen.ToString() + "_" + System.IO.Path.GetFileName(FU_IDocumento.PostedFile.FileName);

                    do
                    {
                        numeroImagen++;
                        datosEvento.Imagen = "~\\Imagenes\\Eventos\\" + numeroImagen.ToString() + "_" + System.IO.Path.GetFileName(FU_IDocumento.PostedFile.FileName);

                    } while (new DAOEvento().validarExistenciaImagen(datosEvento.Imagen));

                    FU_IDocumento.PostedFile.SaveAs(Server.MapPath(datosEvento.Imagen));
                  

                }

            }
  

        }
        else
        {
            datosEvento.Imagen = ((EEvento)HttpContext.Current.Session["mis_eventos"]).Imagen;
        }

        if (DDL_Categoria.SelectedValue == "none")
        {

            DDL_Categoria.SelectedIndex = (int)HttpContext.Current.Session["DDL_Categoria"];

        }


        if (DDL_Estado.SelectedValue=="none") {

            DDL_Estado.SelectedIndex = (int)HttpContext.Current.Session["DDL_Estado"];

        }


        datosEvento.IdEvento = ((EEvento)HttpContext.Current.Session["mis_eventos"]).IdEvento;
        datosEvento.IdEstado = DDL_Estado.SelectedIndex;
        datosEvento.Nombre = TX_Nombre.Text;
        datosEvento.Descripcion = TX_Descripcion.Text;
        datosEvento.IdCategoria = DDL_Categoria.SelectedIndex;
        datosEvento.FechaInicio = TX_Fecha_Inicio.Text;
        datosEvento.FechaFin = TX_Fecha_Fin.Text;
        datosEvento.Hora = TX_Hora.Text;
        datosEvento.Session = Session.SessionID;
        datosEvento.Lugar = TX_Lugar.Text;

       ModificarEvento.EditarEvento(datosEvento);

        LA_Mensaje.Text = "El evento se ha editado correctamente";
        Session["DDL_Estado"] =null;
        Session["DDL_Categoria"] =null;

        Response.Redirect("~\\Views\\Evento\\MisEventos.aspx");
    }


    protected void IB_Regresar_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~\\Views\\Evento\\MisEventos.aspx");
    }

    protected void B_Validar_Clave_Click(object sender, EventArgs e)
    {

        EUsuario validarUsuario = new EUsuario();
        String claveEncriptada = new EncriptarDatos().encriptar(JsonConvert.SerializeObject(TX_Validar_Clave.Text));

        validarUsuario.Correo = ((EUsuario)HttpContext.Current.Session["usuario_datos"]).Correo;
        validarUsuario.Clave = claveEncriptada;

        DataTable datosTabla = new DAOUsuario().validacionInicioSesion(validarUsuario);

        if (datosTabla.Rows.Count != 0)
        {
            DAOEvento evento = new DAOEvento();
            EEvento datosEliminar = new EEvento();
            ECancelarEvento razonEvento = new ECancelarEvento();      
            
            datosEliminar.IdEvento = ((EEvento)HttpContext.Current.Session["mis_eventos"]).IdEvento;
            datosEliminar.Session = Session.SessionID;
            razonEvento.Razon = TX_Cancelar.Text;
            razonEvento.IdEvento = ((EEvento)HttpContext.Current.Session["mis_eventos"]).IdEvento;
            razonEvento.Session = Session.SessionID;

            evento.insertarRazonEvento(razonEvento);
            evento.EliminarEvento(datosEliminar);
            
            Response.Redirect("~\\Views\\Evento\\MisEventos.aspx");


            LA_Mensaje_Validar.Text = " ";
        }
        else {
         
            B_No.Click += new EventHandler(B_No_Click);
            LA_Mensaje_Validar.Text = "La contraseña es incorrecta";
        }
          
        }

    protected void B_No_Click(object sender, EventArgs e)
    {

    }

    protected void DDL_Estado_SelectedIndexChanged(object sender, EventArgs e)
    {

        validarComponentesFecha();

    }

    protected void validarComponentesFecha() {
        if (DDL_Estado.SelectedIndex != 1)
        {

            TX_Fecha_Inicio.Enabled = false;
            TX_Fecha_Fin.Enabled = false;
            TX_Hora.Enabled = false;
            RFV_Fecha_Inicio.Enabled = false;
            RAV_Fecha_Inicio.Enabled = false;
            RFV_Fecha_Fin.Enabled = false;
            CV_Fecha_fin.Enabled = false;
            RFV_Hora.Enabled = false;


        }
        else
        {

            TX_Fecha_Inicio.Enabled = true;
            TX_Fecha_Fin.Enabled = true;
            TX_Hora.Enabled = true;
            RFV_Fecha_Inicio.Enabled = true;
            RAV_Fecha_Inicio.Enabled = true;
            RFV_Fecha_Fin.Enabled = true;
            CV_Fecha_fin.Enabled = true;
            RFV_Hora.Enabled = true;

        }




    }
}