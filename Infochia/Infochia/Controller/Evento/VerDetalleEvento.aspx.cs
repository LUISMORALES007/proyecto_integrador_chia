﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

public partial class Views_Evento_VerDetalleEvento : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {


        if (Session["usuario_datos"] != null)
        {
            long validaUsuarioSession = ((long)HttpContext.Current.Session["codigo_usuario"]);
            long validaIdEvento = ((EEvento)HttpContext.Current.Session["detalle_eventos"]).IdEvento;
            DataTable validarUsuarioInteresado = new DAOEvento().validarInteresUsuario(validaUsuarioSession, validaIdEvento);

            DataTable contacto = new DAOEvento().mostrarContacto(((EEvento)HttpContext.Current.Session["detalle_eventos"]).IdUsuario);

            LA_Nombre_Organizador.Text = contacto.Rows[0]["nombre"].ToString() + " " + contacto.Rows[0]["apellido"].ToString();
            LA_Correo_Organizador.Text = contacto.Rows[0]["correo"].ToString();

            B_Denunciar.Visible = true;
            B_Me_Interesa.Visible = true;
            B_Denunciar.Enabled = true;
            B_Me_Interesa.Enabled = true;
            Panel2.Visible = true;

            if (validarUsuarioInteresado.Rows.Count != 0 && Boolean.Parse(validarUsuarioInteresado.Rows[0]["habilitado"].ToString()) == true)
            {

                B_Me_Interesa.Enabled = false;
                B_Me_Interesa.Visible = false;
                B_No_Me_Interesa.Enabled = true;
                B_No_Me_Interesa.Visible = true;
                IM_Me_Gusta.Visible = true;
            }

            if (((EUsuario)Session["usuario_datos"]).Rol_usuario == 1)
            {

                B_Denunciar.Visible = false;
                B_Me_Interesa.Visible = false;
                Panel2.Visible = false;

            }
        }
        else {
            Panel2.Visible = false;
        }

      

            try
            {
               


                EEvento datosEventoSeleccionado = ((EEvento)HttpContext.Current.Session["detalle_eventos"]);

                DateTime ff = DateTime.Parse(datosEventoSeleccionado.FechaFin).Date;
                string fecha_fin = Convert.ToString(ff);
                string fecha_fin_dia = fecha_fin.Substring(0, 2);
                string fecha_fin_mes = fecha_fin.Substring(3, 2);
                string fecha_fin_anio = fecha_fin.Substring(6, 4);

                fecha_fin = fecha_fin_dia + "/" + fecha_fin_mes + "/" + fecha_fin_anio;
                      if (((EEvento)HttpContext.Current.Session["detalle_eventos"]).IdEstado!=5) {

                                        IM_Imagen_Evento.ImageUrl = datosEventoSeleccionado.Imagen;
                                        LA_Nombre.Text = datosEventoSeleccionado.Nombre;
                                        LA_Descripcion.Text = datosEventoSeleccionado.Descripcion;
                                        LA_Interesados.Text = Convert.ToString(datosEventoSeleccionado.CantidadInteresados);
                                        LA_Fecha_inicio.Text = datosEventoSeleccionado.FechaInicio;
                                        LA_Fecha_Fin.Text = fecha_fin;
                                        LA_Hora.Text = datosEventoSeleccionado.Hora;
                                        LA_Lugar.Text = datosEventoSeleccionado.Lugar;
                      }else {

                             DataTable razonCancelacion = new DAOEvento().mostrarEventoCancelado(((EEvento)HttpContext.Current.Session["detalle_eventos"]).IdEvento);
                           DataTable validarRazon = new DAOAdministrador().validarExistenciaRazon(((EEvento)HttpContext.Current.Session["detalle_eventos"]).IdEvento);

                if (validarRazon.Rows.Count != 0)
                {
                    LA_Descripcion.Text = "Cancelado por :  " + razonCancelacion.Rows[0]["razon"].ToString();

                }
                else {

                    LA_Descripcion.Text = "Cancelado por el administrador ";

                }
                
                    //  datosEvento.Rows[0]["nombre"].ToString();
                    IM_Cancelado.Visible = true;
                                    IM_Imagen_Evento.ImageUrl = datosEventoSeleccionado.Imagen;
                                    LA_Nombre.Text = datosEventoSeleccionado.Nombre;
                                    LA_Nombre.ForeColor = Color.Red;
                                    LA_Descripcion.ForeColor = Color.Red;
                                    LA_Interesados.Text = "Cancelado";
                                   LA_Interesados.ForeColor = Color.Red;
                                    LA_Fecha_inicio.Text = datosEventoSeleccionado.FechaInicio;
                                    LA_Fecha_inicio.ForeColor = Color.Red;
                                     LA_Fecha_Fin.Text = "Cancelado";
                                     LA_Fecha_Fin.ForeColor = Color.Red;
                                    LA_Hora.Text = "Cancelado";
                                   LA_Hora.ForeColor = Color.Red;
                                    LA_Lugar.Text = datosEventoSeleccionado.Lugar + "  Cancelado";
                                   LA_Lugar.ForeColor = Color.Red;
                                   LA_Nombre_Organizador.ForeColor = Color.Red;
                                    LA_Correo_Organizador.ForeColor = Color.Red;
                                    B_Denunciar.Visible = false;
                                    B_Me_Interesa.Visible = false;
                                    Panel2.Visible = false;
            }
            }
            catch
            {
                Response.Redirect("~\\Views\\Evento\\VerDetalleEvento.aspx");

            }

    

    }

    protected void IB_Regresar_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~\\Views\\Evento\\BuscarEvento.aspx");
    
        LA_Nombre_Organizador.Text = " Informacion Disponible Solo para Usuarios Registrados";
        LA_Correo_Organizador.Text = " Informacion Disponible Solo para Usuarios Registrados";
        B_Denunciar.Visible = false;
        B_Me_Interesa.Visible = false;
        B_Denunciar.Enabled = false;
        B_Me_Interesa.Enabled = false;

    }

    protected void B_Me_Interesa_Click(object sender, EventArgs e)
    {
        LA_Mensaje.Text = " ";
        int cantidad_interesados;
        DAOEvento datosInteres = new DAOEvento();
        long validaUsuarioSession = ((long)HttpContext.Current.Session["codigo_usuario"]);
        long validaUsuarioEvento = ((EEvento)HttpContext.Current.Session["detalle_eventos"]).IdUsuario;
        long validaIdEvento = ((EEvento)HttpContext.Current.Session["detalle_eventos"]).IdEvento;
        DataTable validarUsuarioInteresado = datosInteres.validarInteresUsuario(validaUsuarioSession, validaIdEvento);
        // datosTabla.Rows.Count != 0
        if (validaUsuarioSession == validaUsuarioEvento) {

            LA_Mensaje.Visible = true;
            LA_Mensaje_Info.Visible = false;
            LA_Mensaje.Text = "* No puede clickear me interesa al evento que USTED creo";

        } else if (validarUsuarioInteresado.Rows.Count != 0) {

            datosInteres.actualizarEstadoInteresados(validaUsuarioSession, validaIdEvento, true, Session.SessionID);
            cantidad_interesados = datosInteres.cantidadInteresadosEvento(validaIdEvento);
            datosInteres.actualizarCantidadInteresados(validaIdEvento, cantidad_interesados, Session.SessionID);
            LA_Interesados.Text = Convert.ToString(cantidad_interesados);
            B_Me_Interesa.Enabled = false;
            B_Me_Interesa.Visible = false;
            B_No_Me_Interesa.Enabled = true;
            B_No_Me_Interesa.Visible = true;
            IM_Me_Gusta.Visible = true;


        }
        else { 

            datosInteres.insertarInteresUsuario(validaUsuarioSession, validaIdEvento, Session.SessionID);
            cantidad_interesados = datosInteres.cantidadInteresadosEvento(validaIdEvento);
            datosInteres.actualizarCantidadInteresados(validaIdEvento, cantidad_interesados, Session.SessionID);
            LA_Interesados.Text = Convert.ToString(cantidad_interesados);
            B_Me_Interesa.Enabled = false;
            B_Me_Interesa.Visible = false;
            B_No_Me_Interesa.Enabled = true;
            B_No_Me_Interesa.Visible = true;
            IM_Me_Gusta.Visible =true;

        } 
        
    }

    protected void B_No_Me_Interesa_Click(object sender, EventArgs e)
    {

        int cantidad_interesados;
        DAOEvento datosInteres = new DAOEvento();
        long validaUsuarioSession = ((long)HttpContext.Current.Session["codigo_usuario"]);
        long validaIdEvento = ((EEvento)HttpContext.Current.Session["detalle_eventos"]).IdEvento;

        datosInteres.actualizarEstadoInteresados(validaUsuarioSession, validaIdEvento, false, Session.SessionID);
        cantidad_interesados = datosInteres.cantidadInteresadosEvento(validaIdEvento);
        datosInteres.actualizarCantidadInteresados(validaIdEvento, cantidad_interesados, Session.SessionID);
        LA_Interesados.Text = Convert.ToString(cantidad_interesados);
        B_Me_Interesa.Enabled = true;
        B_Me_Interesa.Visible = true;
        B_No_Me_Interesa.Enabled =false;
        B_No_Me_Interesa.Visible = false;
        IM_Me_Gusta.Visible = false;

    }

    protected void B_No_Click(object sender, EventArgs e)
    {

    }

    protected void B_Enviar_Denuncia_Click(object sender, EventArgs e)
    {
        LA_Mensaje.Text = " ";
        LA_Mensaje.ForeColor = Color.Red;
        int cantidad_denuncias;
        string descripcion = TX_Denuncia.Text;
        DAOEvento datosDenuncia = new DAOEvento();
        long validaUsuarioSession = ((long)HttpContext.Current.Session["codigo_usuario"]);
        long validaUsuarioEvento = ((EEvento)HttpContext.Current.Session["detalle_eventos"]).IdUsuario;
        long validaIdEvento = ((EEvento)HttpContext.Current.Session["detalle_eventos"]).IdEvento;
        DataTable validarUsuarioDenuncia = datosDenuncia.validarDenunciaUsuario(validaUsuarioSession, validaIdEvento);
        // datosTabla.Rows.Count != 0
        if (validaUsuarioSession == validaUsuarioEvento)
        {
            LA_Mensaje_Info.Visible = false;
            LA_Mensaje.Visible = true;
            LA_Mensaje.Text = "* No puede DENUNCIAR al evento que USTED creo";

        }
        else if (validarUsuarioDenuncia.Rows.Count != 0)
        {

            LA_Mensaje_Info.Visible = false;
            LA_Mensaje.Visible = true;
            LA_Mensaje.Text = "* Usted ya realizo una DENUNCIA a este evento";

        }
        else
        {
            
            datosDenuncia.insertarDenunciaUsuario(descripcion, validaIdEvento, validaUsuarioSession, Session.SessionID);
            cantidad_denuncias = datosDenuncia.cantidadDenunciasEvento(validaIdEvento);
            datosDenuncia.actualizarCantidadDenuncias(validaIdEvento, cantidad_denuncias, Session.SessionID);
            LA_Mensaje_Info.Visible = true;
            LA_Mensaje.Visible = false;
            LA_Mensaje_Info.Text = "Se ha enviado la denuncia con exito,el administrador del sistema la revisara... ";
            
        }


    }
}