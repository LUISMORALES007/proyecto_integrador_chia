﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Drawing;

public partial class Views_Evento_InsertarEvento : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (string.IsNullOrEmpty((string)HttpContext.Current.Session["correo_usuario"] as string))
        {
            Response.Redirect("~\\Views\\Principal\\Default.aspx");
        }

        if (((EUsuario)Session["usuario_datos"]).Rol_usuario != 2)
        {

            Response.Redirect("~\\Views\\Principal\\Default.aspx");
        }

        DataTable datosCategoria = null;
        datosCategoria = new DAOEvento().mostrarCategoria();

        if (!IsPostBack)
        {
            RangeValidator1.MinimumValue = DateTime.Now.Date.ToString("dd-MM-yyyy");
            RangeValidator1.MaximumValue = DateTime.Now.Date.AddYears(1).ToString("dd-MM-yyyy");



            DDL_Categoria.Items.Clear();
            DDL_Categoria.Items.Add("--Seleccionar--");
            DDL_Categoria.Items.FindByText("--Seleccionar--").Value = "none";
            for (int i = 0; i < datosCategoria.Rows.Count; i++)
            {
                DDL_Categoria.Items.Add(datosCategoria.Rows[i]["descripcion"].ToString());
            }
            DDL_Categoria.DataBind();


        }
        LA_Mensaje.Text = " ";
    }

    protected void B_Crear_Evento_Click(object sender, EventArgs e)
    {
        LA_Hora.Text = " ";
        DateTime fechaInicial = Convert.ToDateTime(TX_Fecha_Inicio.Text).Date;
        DateTime FechAc = DateTime.Now.Date.ToLocalTime();
        DateTime horaText = Convert.ToDateTime(TX_Hora.Text);
        DateTime horAct = DateTime.Now;
        int ht = horaText.Hour;
         int ha = horAct.Hour ;
        int mt = horaText.Minute;
        int ma = horAct.Minute;

        if (fechaInicial > FechAc)
        {

            funcionGuardar();

        }
        else if (ht < ha)
        {
           
   
            LA_Hora.Text = "*Debe ingresar una hora mayor a la hora actual";

        }
        else if ((ht == ha)&&(mt < ma))
        {
        
            LA_Hora.Text = "*Debe ingresar una hora mayor a la hora actual";

        }
        else {

            funcionGuardar();

        }
      



    }

    protected void funcionGuardar() {

        DAOEvento insertarEvento = new DAOEvento();
        EEvento datosEvento = new EEvento();

        FileUpload FU_IDocumento = FU_Url_Evento;
        ClientScriptManager cm = this.ClientScript;
        
        
        if (FU_IDocumento.PostedFile.FileName != "" && !(System.IO.Path.GetExtension(FU_IDocumento.PostedFile.FileName).Equals(".jpg") || System.IO.Path.GetExtension(FU_IDocumento.PostedFile.FileName).Equals(".jpng") || System.IO.Path.GetExtension(FU_IDocumento.PostedFile.FileName).Equals(".png") || System.IO.Path.GetExtension(FU_IDocumento.PostedFile.FileName).Equals(".bmp")))
        {
            LA_Mensaje_Info.Visible = false;
            LA_Mensaje.Visible = true;
            LA_Mensaje.ForeColor = Color.Red;
            LA_Mensaje.Text = "*Formato no valido, solo adjunte imagenes por favor";
            return;
        }
        else
        {
            if (FU_IDocumento.PostedFile.ContentLength > 5000000)
            {
                LA_Mensaje_Info.Visible = false;
                LA_Mensaje.Visible = true;
                LA_Mensaje.ForeColor = Color.Red;
                LA_Mensaje.Text = "* La imagen adjuntada supera el limite permitido de tamaño que son 5 MB";
                return;
            }
            else {

                datosEvento.Imagen = "~\\Imagenes\\Eventos\\" + System.IO.Path.GetFileName(FU_IDocumento.PostedFile.FileName);
                FU_IDocumento.PostedFile.SaveAs(Server.MapPath(datosEvento.Imagen));

            }

           
        }
        if (DDL_Categoria.SelectedValue == "none")
        {

            DDL_Categoria.SelectedIndex =1;

        }

        datosEvento.Nombre = TX_Nombre.Text;
        datosEvento.Descripcion = TX_Descripcion.Text;
        datosEvento.FechaInicio = TX_Fecha_Inicio.Text;
        datosEvento.FechaFin = TX_Fecha_Fin.Text;
        datosEvento.Hora = TX_Hora.Text;
        datosEvento.IdUsuario = ((EUsuario)Session["usuario_datos"]).Codigo_usuario;
        datosEvento.IdCategoria = DDL_Categoria.SelectedIndex;
        datosEvento.Lugar = TX_Lugar.Text;
        datosEvento.Session = Session.SessionID;

       insertarEvento.insertarEvento(datosEvento);
        LA_Mensaje.Visible = false;
        LA_Mensaje_Info.Visible = true;
        LA_Mensaje_Info.ForeColor = Color.Green;
        LA_Mensaje_Info.Text = "El evento se ha registrado correctamente";

        TX_Nombre.Text = " ";
        TX_Descripcion.Text = " ";
        TX_Fecha_Inicio.Text = " ";
        TX_Fecha_Fin.Text = " ";
        TX_Hora.Text = " ";
        LA_Hora.Text = " ";
        TX_Lugar.Text = " ";
        DDL_Categoria.SelectedIndex = 0;
      
    }
  
}