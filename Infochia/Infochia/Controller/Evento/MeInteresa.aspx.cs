﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Views_Evento_MeInteresa : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty((string)HttpContext.Current.Session["correo_usuario"] as string))
        {
            Response.Redirect("~\\Views\\Principal\\Default.aspx");
        }

        if (((EUsuario)Session["usuario_datos"]).Rol_usuario != 2)
        {

            Response.Redirect("~\\Views\\Principal\\Default.aspx");
        }
    }


    protected void GV_Me_Interesa_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int rowIndex = int.Parse(e.CommandArgument.ToString());

        if (e.CommandName.Equals("eliminar"))
        {
            int cantidad_interesados;
            DAOEvento datosInteres = new DAOEvento();
            long UsuarioSession = ((long)HttpContext.Current.Session["codigo_usuario"]);
            long idEvento = (long)this.GV_Me_Interesa.DataKeys[rowIndex]["id"]; ;

            datosInteres.actualizarEstadoInteresados(UsuarioSession, idEvento, false, Session.SessionID);
            cantidad_interesados = datosInteres.cantidadInteresadosEvento(idEvento);
            datosInteres.actualizarCantidadInteresados(idEvento, cantidad_interesados, Session.SessionID);

            GV_Me_Interesa.DataBind();


        }

     }
}