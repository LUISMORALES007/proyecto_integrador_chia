﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Views_Administrador_DeshabilitarUsuario : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty((string)HttpContext.Current.Session["correo_usuario"] as string))
        {
            Response.Redirect("~\\Views\\Principal\\Default.aspx");
        }

        if (((EUsuario)Session["usuario_datos"]).Rol_usuario != 1)
        {

            Response.Redirect("~\\Views\\Principal\\Default.aspx");
        }

        DataTable datosUsuariosFiltrados = new DAOAdministrador().mostrarUsuariosFiltro( "", "", "");


        if (!IsPostBack)
        {
            DataTable grid = new DAOAdministrador().mostrarUsuarios(datosUsuariosFiltrados);
            Session["datos_buscar_usuario"] = grid;
            GV_Listar_Usuarios.DataBind();
        }

        }

    protected void B_Buscar_Click(object sender, EventArgs e)
    {

        String nombreUsuario = "";
        String apellidoUsuario = "";
        String correoUsuario = "";

        DataTable datosUsuariosFiltrados = new DataTable();
        DataTable datosGridView = new DataTable();

        nombreUsuario = TX_Nombre.Text;
        apellidoUsuario = TX_Apellido.Text;
        correoUsuario = TX_Correo.Text;

        datosUsuariosFiltrados = new DAOAdministrador().mostrarUsuariosFiltro(nombreUsuario, apellidoUsuario, correoUsuario);
        datosGridView = new DAOAdministrador().mostrarUsuarios(datosUsuariosFiltrados);
        Session["datos_buscar_usuario"] = datosGridView;
        GV_Listar_Usuarios.DataBind();


    }

    protected void GV_Listar_Usuarios_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int rowIndex = int.Parse(e.CommandArgument.ToString());
        long idUsuario = (long)this.GV_Listar_Usuarios.DataKeys[rowIndex]["id_usuario"];
        DAOAdministrador ActualizarEstadoUsuario = new DAOAdministrador();

        if (e.CommandName.Equals("habilitar"))
        {

            ActualizarEstadoUsuario.habilitarDeshabilitarUsuario(idUsuario,true,Session.SessionID);
            Response.Redirect("~\\Views\\Administrador\\DeshabilitarUsuario.aspx");
        }
        else if (e.CommandName.Equals("deshabilitar"))
        {


            ActualizarEstadoUsuario.cancelarEventosUsuario(idUsuario, Session.SessionID);
            ActualizarEstadoUsuario.habilitarDeshabilitarUsuario(idUsuario, false, Session.SessionID);
            Response.Redirect("~\\Views\\Administrador\\DeshabilitarUsuario.aspx");

        }

        GV_Listar_Usuarios.DataBind();

        }

    protected void GV_Listar_Usuarios_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        Label labelEstado = (Label)e.Row.FindControl("label3");
        Button botonh  = (Button)e.Row.FindControl("B_Habilitar");
        Button botond = (Button)e.Row.FindControl("B_Deshabilitar");

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            if (labelEstado.Text.Equals("True"))
            {

                labelEstado.Text = "Habilitado";
                botond.Visible = true;
                botonh.Visible =false;
            }
            else {

                labelEstado.Text = "Deshabilitado";
                botonh.Visible = true;
                botond.Visible = false;
            }
        }

    }
}