﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Views_Administrador_Peticiones : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (string.IsNullOrEmpty((string)HttpContext.Current.Session["correo_usuario"] as string))
        {
            Response.Redirect("~\\Views\\Principal\\Default.aspx");
        }

        if (((EUsuario)Session["usuario_datos"]).Rol_usuario != 1)
        {

            Response.Redirect("~\\Views\\Principal\\Default.aspx");
        }
    }
}