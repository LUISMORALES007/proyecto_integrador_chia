﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Views_Administrador_VerDetalleDenuncia : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (string.IsNullOrEmpty((string)HttpContext.Current.Session["correo_usuario"] as string))
        {
            Response.Redirect("~\\Views\\Principal\\Default.aspx");
        }

        if (((EUsuario)Session["usuario_datos"]).Rol_usuario != 1)
        {

            Response.Redirect("~\\Views\\Principal\\Default.aspx");
        }

        long idEventoDenuncia = ((long)HttpContext.Current.Session["detalle_denuncia"]);
        DataTable usuarioEventoDenunciado = new DAOAdministrador().mostrarUsuarioDenuncia(idEventoDenuncia);
        //usuarioEventoDenunciado.Rows[0]["imagen"].ToString();
        LA_Nombre_Creador.Text = usuarioEventoDenunciado.Rows[0]["nombre_completo"].ToString();
        LA_Correo.Text= usuarioEventoDenunciado.Rows[0]["correo"].ToString();
        IM_Creador.ImageUrl= usuarioEventoDenunciado.Rows[0]["imagen"].ToString();


    }




    protected void IB_Regresar_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~\\Views\\Administrador\\DenunciasEvento.aspx");
    }
}