﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

public partial class Views_Administrador_Reportes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty((string)HttpContext.Current.Session["correo_usuario"] as string))
        {
            Response.Redirect("~\\Views\\Principal\\Default.aspx");
        }

        if (((EUsuario)Session["usuario_datos"]).Rol_usuario != 1) {

            Response.Redirect("~\\Views\\Principal\\Default.aspx");
        }


        RAV_Fecha_Inicio.MinimumValue = DateTime.Now.Date.ToString("dd-MM-yyyy");
        RAV_Fecha_Inicio.MaximumValue = DateTime.Now.Date.AddYears(1).ToString("dd-MM-yyyy");

    }

    protected InfoReportes ObtenerInforme1()
    {
       
        DataRow fila;  //dr
        DataTable informacion = new DataTable(); //dt
      InfoReportes datos = new InfoReportes();
        informacion = datos.Tables["EventosCancelados"];
       DAOReportes reportes= new DAOReportes();

        DataTable intermedio = reportes.reporteEventosCancelados();

        for (int i = 0; i < intermedio.Rows.Count; i++)
        {
            fila = informacion.NewRow();


            fila["Nombre"] = intermedio.Rows[i]["nombre"].ToString();
            fila["Descripcion"] = intermedio.Rows[i]["descripcion"].ToString();
            fila["Fecha Inicio"] = DateTime.Parse(intermedio.Rows[i]["fecha_inicio"].ToString()).Date;
            fila["Fecha Fin"] = DateTime.Parse(intermedio.Rows[i]["fecha_fin"].ToString()).Date;
            fila["Hora"] = TimeSpan.Parse(intermedio.Rows[i]["hora"].ToString());
            fila["Lugar"] = intermedio.Rows[i]["lugar"].ToString();
            fila["Imagen"] = streamFile(intermedio.Rows[i]["imagen"].ToString()); //calcular ruta completa steamfile
            fila["Id Uduario"] = long.Parse(intermedio.Rows[i]["id_usuario"].ToString());
            fila["Creador"] = intermedio.Rows[i]["nombre_completo"].ToString();

            informacion.Rows.Add(fila);
        }

        if (intermedio.Rows.Count == 0)
        {
            LA_Mensaje.Visible = true;
            LA_Mensaje.Text = "---- NO HAY EVENTOS CANCELADOS ---";
            CRV_Reporte1.Visible = false;
        }

        return datos;
    }


    protected InfoReportes ObtenerInforme2()
    {
        DataRow fila;  //dr
        DataTable informacion = new DataTable(); //dt
        InfoReportes datos = new InfoReportes();
        informacion = datos.Tables["Denunciados"];
        DAOReportes reportes = new DAOReportes();
     

        DataTable intermedio = reportes.reporteEventosDenunciados();

        for (int i = 0; i < intermedio.Rows.Count; i++)
        {
            fila = informacion.NewRow();

            
            fila["Nombre"] = intermedio.Rows[i]["nombre"].ToString();
            fila["Categoria"] = intermedio.Rows[i]["categoria"].ToString();
            fila["Estado"] = intermedio.Rows[i]["estado"].ToString();
            fila["Fecha Inicio"] = DateTime.Parse(intermedio.Rows[i]["fecha_inicio"].ToString()).Date;
            fila["Fecha Fin"] = DateTime.Parse(intermedio.Rows[i]["fecha_fin"].ToString()).Date;
            fila["Hora"] = TimeSpan.Parse(intermedio.Rows[i]["hora"].ToString());
            fila["Lugar"] = intermedio.Rows[i]["lugar"].ToString();
            fila["Denuncias"] = int.Parse(intermedio.Rows[i]["cantidad_denuncias"].ToString());
            fila["Imagen"] = streamFile(intermedio.Rows[i]["imagen"].ToString()); //calcular ruta completa steamfile
           
           
            informacion.Rows.Add(fila);

        }

        if (intermedio.Rows.Count == 0)
        {
            LA_Mensaje.Visible = true;
            LA_Mensaje.Text = "---- NO HAY EVENTOS DENUNCIADOS ---";
            CRV_Reporte2.Visible = false;
        }

        return datos;
    }


    protected InfoReportes ObtenerInforme3()
    {
        DataRow fila;  //dr
        DataTable informacion = new DataTable(); //dt
        InfoReportes datos = new InfoReportes();
        informacion = datos.Tables["EventosActivos"];
        DAOReportes reportes = new DAOReportes();


        DateTime fi = DateTime.Parse(TX_Fecha_Inicio.Text).Date;
        string fecha_inicio = Convert.ToString(fi);
        string fecha_inicio_dia = fecha_inicio.Substring(0, 2);
        string fecha_inicio_mes = fecha_inicio.Substring(3, 2);
        string fecha_inicio_anio = fecha_inicio.Substring(6, 4);
        fecha_inicio = fecha_inicio_anio + "-" + fecha_inicio_mes + "-" + fecha_inicio_dia;

        DataTable intermedio = reportes.mostrarEventosActivos(fecha_inicio);

        for (int i = 0; i < intermedio.Rows.Count; i++)
        {
            fila = informacion.NewRow();


            fila["Nombre"] = intermedio.Rows[i]["nombre"].ToString();
            fila["Fecha Inicio"] = DateTime.Parse(intermedio.Rows[i]["fecha_inicio"].ToString()).Date;
            fila["Hora"] = TimeSpan.Parse(intermedio.Rows[i]["hora"].ToString());
            fila["Lugar"] = intermedio.Rows[i]["lugar"].ToString();
            fila["Imagen"] = streamFile(intermedio.Rows[i]["imagen"].ToString()); //calcular ruta completa steamfile
            fila["Id Categoria"] = int.Parse(intermedio.Rows[i]["id_categoria"].ToString());
            fila["Categoria"] = intermedio.Rows[i]["descripcion_categoria"].ToString();
            fila["Interesados"] = int.Parse(intermedio.Rows[i]["cantidad_interesados"].ToString());
            informacion.Rows.Add(fila);
        }

        if (intermedio.Rows.Count == 0)
        {
            LA_Mensaje.Visible = true;
            LA_Mensaje.Text = "---- NO HAY EVENTOS ACTIVOS EN LA FECHA QUE SELECCIONO ---";
            CRV_Reporte3.Visible = false;
        }

        return datos;
    }

    private byte[] streamFile(string filename)
    {
        FileStream fs;
        if (!filename.Equals(""))
        {
            fs = new FileStream(Server.MapPath(filename), FileMode.Open, FileAccess.Read);
        }
        else
        {
            fs = new FileStream(Server.MapPath("~/assets/img/Iconos/thumb-up.png"), FileMode.Open, FileAccess.Read);
        }

        // Create a byte array of file stream length
        byte[] ImageData = new byte[fs.Length];

        //Read block of bytes from stream into the byte array
        fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

        //Close the File Stream
        fs.Close();
        return ImageData; //return the byte data
    }


   

    protected void DDL_Reporte_SelectedIndexChanged(object sender, EventArgs e)
    {
        switch (DDL_Reporte.SelectedIndex)
        {
            case 1:
                TX_Fecha_Inicio.Visible = false;
                B_Fecha.Visible = false;
                LA_Mensaje.Visible = false;
                CRV_Reporte1.Visible = true;
                CRV_Reporte2.Visible = false;
                CRV_Reporte3.Visible = false;

                try
                {
                    InfoReportes reporte = ObtenerInforme1();
                    CRS_Reporte1.ReportDocument.SetDataSource(reporte);
                    CRV_Reporte1.ReportSource = CRS_Reporte1;
                }
                catch (Exception)
                {

                    throw;
                }


                break;
            case 2:
                TX_Fecha_Inicio.Visible = false;
                B_Fecha.Visible = false;
                LA_Mensaje.Visible = false;
                CRV_Reporte1.Visible = false;
                CRV_Reporte2.Visible = true;
                CRV_Reporte3.Visible = false;

                try
                {
                    InfoReportes reporte2 = ObtenerInforme2();
                    CRS_Reporte2.ReportDocument.SetDataSource(reporte2);
                    CRV_Reporte2.ReportSource = CRS_Reporte2;
                }
                catch (Exception)
                {

                    throw;
                }

                break;

            case 3:
                LA_Mensaje.Visible = false;
                CRV_Reporte1.Visible = false;
                CRV_Reporte2.Visible = false;
                CRV_Reporte3.Visible = false;

                TX_Fecha_Inicio.Visible = true;
                B_Fecha.Visible = true;




                break;
            default:

                TX_Fecha_Inicio.Visible = false;
                B_Fecha.Visible = false;
                CRV_Reporte1.Visible = false;
                CRV_Reporte2.Visible = false;
                CRV_Reporte3.Visible = false;
                LA_Mensaje.Visible = false;
                break;
        }
    }

    protected void B_Fecha_Click(object sender, EventArgs e)
    {
   
        CRV_Reporte1.Visible = false;
        CRV_Reporte2.Visible = false;
        CRV_Reporte3.Visible = true;

        try
        {
            LA_Mensaje.Visible = false;
            InfoReportes reporte3 = ObtenerInforme3();
            CRS_Reporte3.ReportDocument.SetDataSource(reporte3);
            CRV_Reporte3.ReportSource = CRS_Reporte3;
        }
        catch (Exception)
        {

            throw;
        }


    }
}