﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Views_Administrador_DenunciasEvento : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty((string)HttpContext.Current.Session["correo_usuario"] as string))
        {
            Response.Redirect("~\\Views\\Principal\\Default.aspx");
        }

        if (((EUsuario)Session["usuario_datos"]).Rol_usuario != 1)
        {

            Response.Redirect("~\\Views\\Principal\\Default.aspx");
        }

        DataTable datosCategoria = null;
        datosCategoria = new DAOEvento().mostrarCategoria();
        DataTable datosDenunciaFiltrados = new DAOAdministrador().mostrarDenunciasFiltro("", -1);


        if (!IsPostBack)
        {
            DataTable fff = new DAOAdministrador().mostrarDenuncias(datosDenunciaFiltrados);
            Session["datos_denuncia_filtrar"] = fff;
            GV_Denuncias.DataBind();


            DDL_Buscar_Categoria.Items.Clear();
            DDL_Buscar_Categoria.Items.Add("--Seleccionar--");
            DDL_Buscar_Categoria.Items.FindByText("--Seleccionar--").Value = "none";
            for (int i = 0; i < datosCategoria.Rows.Count; i++)
            {
                DDL_Buscar_Categoria.Items.Add(datosCategoria.Rows[i]["descripcion"].ToString());
            }
            DDL_Buscar_Categoria.DataBind();

        }



    }

    protected void B_Buscar_Denuncia_Click(object sender, EventArgs e)
    {
        String nombreEvento = TX_Buscar_Nombre.Text;
        int categoriaEvento = -1;
       

        DataTable datosEventosFiltrados = new DataTable();
        DataTable datosGridView = new DataTable();

        if (String.IsNullOrWhiteSpace(TX_Buscar_Nombre.Text))
        {
            nombreEvento = "";
        }

        if (DDL_Buscar_Categoria.SelectedIndex != 0)
        {

            categoriaEvento = DDL_Buscar_Categoria.SelectedIndex;

        }




        datosEventosFiltrados = new DAOAdministrador().mostrarDenunciasFiltro(nombreEvento, categoriaEvento);
        datosGridView = new DAOAdministrador().mostrarDenuncias(datosEventosFiltrados);
        Session["datos_denuncia_filtrar"] = datosGridView;
        GV_Denuncias.DataBind();
    }

    protected void GV_Denuncias_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int rowIndex = int.Parse(e.CommandArgument.ToString());

        if (e.CommandName.Equals("info"))
        {

            Session["detalle_denuncia"] = (long)this.GV_Denuncias.DataKeys[rowIndex]["id"];

            Response.Redirect("~\\Views\\Administrador\\VerDetalleDenuncia.aspx");



        }
        else if (e.CommandName.Equals("cancelar")) {

            DAOEvento evento = new DAOEvento();
            EEvento datosEliminar = new EEvento();
            ECancelarEvento razonEvento = new ECancelarEvento();

            datosEliminar.IdEvento = (long)this.GV_Denuncias.DataKeys[rowIndex]["id"]; ;
            datosEliminar.Session = Session.SessionID;
            razonEvento.Razon = "El administrador  CANCELO el evento por Multiples denuncias";
            razonEvento.IdEvento = (long)this.GV_Denuncias.DataKeys[rowIndex]["id"]; ;
            razonEvento.Session = Session.SessionID;

            evento.insertarRazonEvento(razonEvento);
            evento.EliminarEvento(datosEliminar);
            GV_Denuncias.DataBind();
            Response.Redirect("~\\Views\\Administrador\\DenunciasEvento.aspx");
        }

    }

    protected void GV_Denuncias_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Label labelEstadoEvento = (Label)e.Row.FindControl("Label7");
        Button botonCancelar = (Button)e.Row.FindControl("B_Cancelar");
        Label labelCancelar = (Label)e.Row.FindControl("LA_Cancelado");
        Panel panel = (Panel)e.Row.FindControl("Panel2");

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            if (labelEstadoEvento.Text.Equals("Cancelado"))
            {
                botonCancelar.Visible = false;
                labelCancelar.Text = "Evento Cancelado";
                panel.Visible = false;
            }
            else
            {
                botonCancelar.Visible = true;
                labelCancelar.Text = " ";
                panel.Visible = true;
            }
        }
    }
}