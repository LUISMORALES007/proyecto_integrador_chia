﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Views_Master_MasterUsuarios : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["usuario_datos"] == null)
        {
            B_Cerrar_Session.Visible = false;
            LB_Menu_Eventos.Visible = false;
            LB_Menu_Administrador.Visible = false;
          
        }
        else if (((EUsuario)Session["usuario_datos"]).Session == null)
        {
            B_Cerrar_Session.Visible = false;
            LB_Menu_Eventos.Visible = false;
            LB_Menu_Administrador.Visible = false;
    
        }
        else {

            switch (((EUsuario)Session["usuario_datos"]).Rol_usuario)
            {
                case 1:

                    //modulo administrador del sistema
                    LB_Menu_Administrador.Visible = true;
                    B_Cerrar_Session.Visible = true;
                    B_Inicio_Sesion.Visible = false;
                    B_Registro.Visible = false;

                    break;

                case 2:

                    //usuario Miembro
                    LB_Menu_Eventos.Visible = true;
                    B_Cerrar_Session.Visible = true;
                    B_Inicio_Sesion.Visible = false;
                    B_Registro.Visible = false;
                    break;

                default:

                    //default

                    break;
            }
        }
    }

    protected void B_Registro_Click(object sender, EventArgs e)
    {
        Response.Redirect("~\\Views\\Inicio de Sesion y Registro\\RegistroUsuario.aspx");
    }

    protected void B_Inicio_Sesion_Click(object sender, EventArgs e)
    {
        Response.Redirect("~\\Views\\Inicio de Sesion y Registro\\IniciarSesion.aspx");
    }

    protected void B_Cerrar_Session_Click(object sender, EventArgs e)
    {

        Session["usuario_datos"] = null;
        Session["usuario_session"] = null;
        Session["codigo_usuario"] = null;
        Session.Clear();
        Session.Abandon();
        Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

        DAOUsuario user = new DAOUsuario();
        EUsuario datos = new EUsuario();
        datos.Session = Session.SessionID;
        user.cerrarSesion(datos);

        LB_Menu_Eventos.Visible = false;
        LB_Menu_Administrador.Visible = false;
        B_Cerrar_Session.Visible = false;
        B_Inicio_Sesion.Visible = true;
        B_Registro.Visible = true;
    }

    protected void LB_Menu_Eventos_Click(object sender, EventArgs e)
    {
        Response.Redirect("~\\Views\\Principal\\DefaultRegistrados.aspx");
    }

    protected void LB_Menu_Administrador_Click(object sender, EventArgs e)
    {
        Response.Redirect("~\\Views\\Principal\\DefaultRegistrados.aspx");
    }

    protected void LB_Buscar_Click(object sender, EventArgs e)
    {
        Response.Redirect("~\\Views\\Evento\\BuscarEvento.aspx");
    }

    protected void LB_Buscar_B_Click(object sender, EventArgs e)
    {
        Response.Redirect("~\\Views\\Evento\\BuscarEvento.aspx");
    }

    protected void BTN_Enviar_Solicitud_Click(object sender, EventArgs e)
    {
        EPeticion peticion = new EPeticion();
        DAOAdministrador insertarPeticion = new DAOAdministrador();

       peticion.Nombre = TX_Nombre.Text;
        peticion.Correo = (TX_Correo.Text).ToLower();
        peticion.Asunto= TX_Asunto.Text;
        peticion.Mensaje = TX_Mensaje.Text;
        peticion.Session = Session.SessionID;

        insertarPeticion.enviarPeticion(peticion);
        LA_Mensaje_Confirmacion.ForeColor =Color.Green;
        LA_Mensaje_Confirmacion.Text = "Su mensaje se ha enviado con exito, el administrador lo revisara en pocos minutos";

        TX_Nombre.Text = "";
        TX_Correo.Text = "";
        TX_Asunto.Text = "";
        TX_Mensaje.Text = " ";

    }
}
