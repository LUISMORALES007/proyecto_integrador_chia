﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Views_Master_MasterRegistrados : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

       


        if (Session["usuario_datos"] == null)
        {

            MenuUsuarioMiembro.Visible = false;
            MenuAdministrador.Visible = false;
            Panel_Opciones.Visible = false;
            LA_Nombre_Usuario.Text = " ";
            IM_Perfil.Visible = false;

        }
        else if (((EUsuario)Session["usuario_datos"]).Session == null)
        {
            MenuUsuarioMiembro.Visible = false;
            MenuAdministrador.Visible = false;
            Panel_Opciones.Visible = false;
            LA_Nombre_Usuario.Text = " ";
            IM_Perfil.Visible = false;
        }
        else
        {
            MenuUsuarioMiembro.Visible = false;
            MenuAdministrador.Visible = false;
            LA_Nombre_Usuario.Text = ((EUsuario)Session["usuario_datos"]).Nombre;
            IM_Perfil.ImageUrl = ((EUsuario)Session["usuario_datos"]).Url_imagen;

            switch (((EUsuario)Session["usuario_datos"]).Rol_usuario)
            {
                case 1:

                    //modulo administrador del sistema

                    MenuAdministrador.Visible = true;
                    IM_Perfil.Visible = false;
                    LA_Nombre_Usuario.Text="Administrador ";


                    ;
                    break;

                case 2:
                
                    //usuario Miembro
             
                    MenuUsuarioMiembro.Visible = true;
                    
                    break;

                default:

                    //default

                    break;
            }
        }

        
    }

    protected void B_Cerrar_Session_Click(object sender, EventArgs e)
    {
        Session["usuario_datos"] = null;
        Session["usuario_session"] = null;
        Session["codigo_usuario"] = null;
        Session.Clear();
        Session.Abandon();
        Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

        DAOUsuario user = new DAOUsuario();
        EUsuario datos = new EUsuario();
        datos.Session = Session.SessionID;
        user.cerrarSesion(datos);

        MenuUsuarioMiembro.Visible = false;
        MenuAdministrador.Visible = false;
       
        Response.Redirect("~\\Views\\Principal\\Default.aspx");
    }

    protected void B_Editar_Perfil_Click(object sender, EventArgs e)
    {

        Response.Redirect("~\\Views\\Editar Perfil\\EditarPerfil.aspx");
    }

    protected void MenuUsuarioMiembro_MenuItemClick(object sender, MenuEventArgs e)
    {

    }



    protected void MenuAdministrador_MenuItemClick(object sender, MenuEventArgs e)
    {
    
    }

    protected void LB_Buscar_Click(object sender, EventArgs e)
    {
        Response.Redirect("~\\Views\\Evento\\BuscarEvento.aspx");
    }

    protected void LB_Crear_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Views/Evento/InsertarEvento.aspx");
    }

    protected void LB_Mis_Eventos_Click(object sender, EventArgs e)
    {
         Response.Redirect("~/Views/Evento/MisEventos.aspx");
    }

    protected void LB_Me_Interesa_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Views/Evento/MeInteresa.aspx");
    }



    protected void LB_Reportes_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Views/Administrador/Reportes.aspx");
    }

    protected void LB_Denuncias_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Views/Administrador/DenunciasEvento.aspx");
    }

    protected void LB_Crear_Categoria_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Views/Administrador/InsertarCategoria.aspx");
    }

    protected void LB_Deshabilitar_usuario_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Views/Administrador/DeshabilitarUsuario.aspx");
    }

    protected void LB_Peticiones_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Views/Administrador/Peticiones.aspx");
    }
}
