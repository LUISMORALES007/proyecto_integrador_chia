﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Newtonsoft.Json;
using System.Drawing;

public partial class Views_Editar_Perfil_EditarPerfil : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            if (!IsPostBack)
            {
                TX_Nombre.Text = ((EUsuario)(Session["usuario_datos"])).Nombre;
                TX_Apellido.Text = ((EUsuario)(Session["usuario_datos"])).Apellido;
                TX_Correo.Text = ((EUsuario)(Session["usuario_datos"])).Correo;
                IM_Perfil_Actual.ImageUrl = ((EUsuario)(Session["usuario_datos"])).Url_imagen;
                if (((EUsuario)(Session["usuario_datos"])).Rol_usuario==1) {

                    TX_Correo.Enabled = true;

                } else {
                    TX_Correo.Enabled = true;
                }
            }
        }
        catch {
            Response.Redirect("~\\Views\\Principal\\Default.aspx");
        }
    }

    protected void BTN_Editar_Perfil_Click(object sender, EventArgs e)
    {
        if (TX_Clave.Text.Equals(TX_Repetir_Clave.Text)) {


            DAOEditarUsuario editar = new DAOEditarUsuario();
            EUsuario datosIntroducidos = new EUsuario();

            FileUpload FU_IDocumento = FU_Imagen_Usuario;
            ClientScriptManager cm = this.ClientScript;
            long numeroImagen = 0;

            if (FU_IDocumento.PostedFile.FileName != "")
            {
                String extensionImagen = System.IO.Path.GetExtension(FU_IDocumento.PostedFile.FileName).ToLower();
                if (!(extensionImagen.Equals(".jpg") || extensionImagen.Equals(".jpng") || extensionImagen.Equals(".png") || extensionImagen.Equals(".bmp") || extensionImagen.Equals(".jpeg")))
                {
                    LA_Mensaje.Visible = true;
                    LA_Mensaje.ForeColor = Color.Red;
                    LA_Mensaje.Text = "*Formato no valido, solo adjunte imagenes por favor";
                    return;
                }
                else
                {
                    if (FU_IDocumento.PostedFile.ContentLength > 5000000)
                    {
                        LA_Mensaje.Visible = true;
                        LA_Mensaje.ForeColor = Color.Red;
                        LA_Mensaje.Text = "* La imagen adjuntada supera el limite permitido de tamaño que son 5 MB";
                        return;
                    }
                    else
                    {

                        datosIntroducidos.Url_imagen = "~\\Imagenes\\Usuarios\\" + numeroImagen.ToString() + "_" + System.IO.Path.GetFileName(FU_IDocumento.PostedFile.FileName);

                        do
                        {
                            numeroImagen++;
                            datosIntroducidos.Url_imagen = "~\\Imagenes\\Usuarios\\" + numeroImagen.ToString() + "_" + System.IO.Path.GetFileName(FU_IDocumento.PostedFile.FileName);

                        } while (new DAOEditarUsuario().validarExistenciaImagen(datosIntroducidos.Url_imagen));

                        FU_IDocumento.PostedFile.SaveAs(Server.MapPath(datosIntroducidos.Url_imagen));
                        ((EUsuario)(Session["usuario_datos"])).Url_imagen = datosIntroducidos.Url_imagen;

                    }

                 }
            }
            else
            {
                datosIntroducidos.Url_imagen= ((EUsuario)(Session["usuario_datos"])).Url_imagen;
            }

            String claveEncriptada = new EncriptarDatos().encriptar(JsonConvert.SerializeObject(TX_Clave.Text));

            datosIntroducidos.Nombre = CultureInfo.CurrentCulture.TextInfo.ToTitleCase((TX_Nombre.Text).ToLower());
            datosIntroducidos.Apellido = CultureInfo.CurrentCulture.TextInfo.ToTitleCase((TX_Apellido.Text).ToLower());
            datosIntroducidos.Correo = TX_Correo.Text;
            datosIntroducidos.Clave = claveEncriptada;
            datosIntroducidos.Session = Session.SessionID;

          

            ((EUsuario)Session["usuario_datos"]).Nombre = datosIntroducidos.Nombre;
            ((EUsuario)Session["usuario_datos"]).Apellido = datosIntroducidos.Apellido;
            ((EUsuario)Session["usuario_datos"]).Correo = datosIntroducidos.Correo;
            ((EUsuario)Session["usuario_datos"]).Clave = datosIntroducidos.Clave;
            ((EUsuario)Session["usuario_datos"]).Session = datosIntroducidos.Session;

            editar.editarUsuario(datosIntroducidos);
            Response.Redirect("~\\Views\\Editar Perfil\\EditarPerfil.aspx");
         
       

        }

    }
}